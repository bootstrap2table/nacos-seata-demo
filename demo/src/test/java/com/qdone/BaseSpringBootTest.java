package com.qdone;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.SneakyThrows;
import org.apache.commons.collections4.MapUtils;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;

import java.util.*;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

/**
 * 单元测试基类
 * 单元测试JVM排序时,是按照方法名称的hashcode从小到大的顺序执行
 * 单元测试NAME_ASCENDING排序时,是按照方法名称英文顺序排序(推荐)
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("dev")
public class BaseSpringBootTest {

    @Autowired
    protected MockMvc mockMvc;

    protected String contentType = "application/json;charset=UTF-8";

    protected String tokenKey = "Authorization";
    /**
     * 单元测试用户令牌
     */
    protected String tokenValue = "Bearer huiyongxiang|eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOm51bGwsInVJZCI6IkNOMDIxMjA1IiwiY3JlYXRlZCI6MTYzMzY2MzYzNjAyMiwibW9iaWxlIjoiMTM0NzI4OTU4NjkiLCJuaWtlTmFtZSI6bnVsbCwiZXhwIjoxNjMzNzUwMDM2LCJpYXQiOjE2MzM2NjM2MzZ9.zgHcWod8NTilvb5NKDt19bWp1k-C6hGLQlcHjGV2Z0V_EjCH15PwofL8_vLDZ_-sNna2Y9TnK1HMGdSUeAzcrg";

    @BeforeEach
    public void build() {
        JSONObject data = new JSONObject();
        data.put("userMobile", "mall_admin");
        data.put("userPassword", "25d55ad283aa400af464c76d713c07ad");
        final String result = HttpUtil.post("http://27.115.49.42:15109/user/api/mall/login", data.toJSONString());
        JSONObject object = JSONObject.parseObject(result);
        tokenValue = org.apache.commons.lang3.StringUtils.defaultIfBlank(object.getString("data"), tokenValue);
    }

    @SneakyThrows
    protected ResultActions mockGet(String url, java.util.Map<String, String> queryParams) {
        org.springframework.util.MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        if (MapUtils.isNotEmpty(queryParams)) {
            for (java.util.Map.Entry<String, String> entry : queryParams.entrySet()) {
                params.add(entry.getKey(), entry.getValue());
            }
        }
        return mockMvc.perform(mockBuilder(HttpMethod.GET, url).queryParams(params)).andDo(print());
    }

    @SneakyThrows
    protected ResultActions mockDelete(String url) {
        return mockDelete(url, null);
    }

    @SneakyThrows
    protected ResultActions mockDelete(String url, Map<String, String> queryParams) {
        org.springframework.util.MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        if (MapUtils.isNotEmpty(queryParams)) {
            for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                params.add(entry.getKey(), entry.getValue());
            }
        }
        return mockMvc.perform(mockBuilder(HttpMethod.DELETE, url).queryParams(params)).andDo(print());
    }

    @SneakyThrows
    protected ResultActions mockPost(String url, List<?> obj) {
        return mockMvc.perform(mockBuilder(HttpMethod.POST, url).content(JSON.toJSONString(obj))).andDo(print());
    }

    @SneakyThrows
    protected ResultActions mockPost(String url, Object obj) {
        return mockMvc.perform(mockBuilder(HttpMethod.POST, url).content(JSON.toJSONString(obj))).andDo(print());
    }

    protected MockHttpServletRequestBuilder mockBuilder(HttpMethod method, String url) {
        return MockMvcRequestBuilders.request(method, url).header(tokenKey, tokenValue).contentType(contentType).accept(contentType);
    }
}
