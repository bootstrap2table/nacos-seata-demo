package com.qdone;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.qdone.common.util.DateUtil;
import com.qdone.module.model.Solr;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang.*;
import org.apache.commons.lang.math.*;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.lang.time.StopWatch;
import org.apache.commons.validator.routines.DateValidator;
import org.apache.commons.validator.routines.RegexValidator;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.Collator;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author 付为地
 *
 * 简单测试commons-lang的数据
 *    ArrayUtils – 用于对数组的操作，如添加、查找、删除、子数组、倒序、元素类型转换等；
 *    BitField – 用于操作位元，提供了一些方便而安全的方法；
 *    BooleanUtils – 用于操作和转换boolean或者Boolean及相应的数组；
 *    CharEncoding – 包含了Java环境支持的字符编码，提供是否支持某种编码的判断；
 *    CharRange – 用于设定字符范围并做相应检查；
 *    CharSet – 用于设定一组字符作为范围并做相应检查；
 *    CharSetUtils – 用于操作CharSet；
 *    CharUtils –用于操作char值和Character对象；
 *    ClassUtils – 用于对Java类的操作，不使用反射；
 *    ObjectUtils –用于操作Java对象，提供null安全的访问和其他一些功能；
 *    RandomStringUtils – 用于生成随机的字符串；
 *    SerializationUtils – 用于处理对象序列化，提供比一般Java序列化更高级的处理能力；
 *    StringEscapeUtils –用于正确处理转义字符，产生正确的Java、JavaScript、HTML、XML和SQL代码；
 *    StringUtils –处理String的核心类，提供了相当多的功能；
 *    SystemUtils –在java.lang.System基础上提供更方便的访问，如用户路径、Java版本、时区、操作系统等判断；
 * 针对动态jexl表达式，可以参考com.qdone.common.util.JexlUtil
 *    利用org.apache.commons.jexl2.JexlEngine，实现动态jexl表达式,简单实例
 */
@FixMethodOrder(MethodSorters.JVM)
public class DemoApacheCommonsTest {
	/**
	 * ArrayUtils用于对数组的操作，如添加、查找、删除、子数组、倒序、元素类型转换等
	 */
	@Test
	public void testArrayUtils() {
		// data setup
		int[] intArray1 = { 2, 4, 8, 16 };
		int[][] intArray2 = { { 1, 2 }, { 2, 4 }, { 3, 8 }, { 4, 16 } };
		Object[][] notAMap = { { "A", new Double(100) }, { "B", new Double(80) }, { "C", new Double(60) },
				{ "D", new Double(40) }, { "E", new Double(20) } };

		// printing arrays
		System.out.println("intArray1: " + ArrayUtils.toString(intArray1));
		System.out.println("intArray2: " + ArrayUtils.toString(intArray2));
		System.out.println("notAMap: " + ArrayUtils.toString(notAMap));

		// finding items
		// 判断某个数组数组是不是包含某个元素
		System.out.println("intArray1 contains '8'? " + ArrayUtils.contains(intArray1, 8));
		System.out.println("intArray1 index of '8'? " + ArrayUtils.indexOf(intArray1, 8));
		System.out.println("intArray1 last index of '8'? " + ArrayUtils.lastIndexOf(intArray1, 8));

		// cloning and resversing
		// 数组的克隆和反转
		int[] intArray3 = ArrayUtils.clone(intArray1);
		System.out.println("intArray3: " + ArrayUtils.toString(intArray3));
		ArrayUtils.reverse(intArray3);
		System.out.println("intArray3 reversed: " + ArrayUtils.toString(intArray3));

		// primitive to Object array
		// 转换成包装类操作
		Integer[] integerArray1 = ArrayUtils.toObject(intArray1);
		System.out.println("integerArray1: " + ArrayUtils.toString(integerArray1));

		// build Map from two dimensional array
		// 将Objec[][]二维数组转换成map
		@SuppressWarnings("rawtypes")
		Map map = ArrayUtils.toMap(notAMap);
		System.out.println("object[][] to map:" + map);
		Double res = (Double) map.get("C");
		System.out.println("get 'C' from map: " + res);
	}

	/**
	 * StringUtils 处理String的核心类，提供了相当多的功能；
	 *
	 *    public static boolean isAlpha(String str); 只由字母组成
	 *    public static boolean isAlphaSpace(String str); 只有字母和空格组成
	 *    public static boolean isAlphanumeric(String str);只由字母和数字组成
	 *    public static boolean isAlphanumericSpace(String str);只由字母数字和空格组成
	 *    public static boolean isNumeric(String str);只由数字组成
	 *    public static boolean isNumericSpace(String str);只由数字和空格组成
	 */
	@Test
	public void testStringUtils() {
		// data setup
		String str1 = "";
		String str2 = "";
		String str3 = "\t";
		String str4 = null;
		String str5 = "123";
		String str6 = "ABCDEFG";
		String str7 = "Itfeels good to use JakartaCommons.\r\n";

		// check for empty strings
		System.out.println("==============================");
		System.out.println("Is str1 blank? " + StringUtils.isBlank(str1));
		System.out.println("Is str2 blank? " + StringUtils.isBlank(str2));
		System.out.println("Is str3 blank? " + StringUtils.isBlank(str3));
		System.out.println("Is str4 blank? " + StringUtils.isBlank(str4));

		// check for numerics
		System.out.println("==============================");
		System.err.println(StringUtils.isNumeric("1"));
		System.err.println(StringUtils.isNumeric("-1"));
		System.err.println(StringUtils.isNumeric("+1"));
		System.err.println(StringUtils.isNumeric(",1"));
		System.out.println("Is str5 numeric? " + StringUtils.isNumeric(str5));
		System.out.println("Is str6 numeric? " + StringUtils.isNumeric(str6));

		// reverse strings / whole words 倒序生成字符串
		System.out.println("==============================");
		System.out.println("str6: " + str6);
		System.out.println("str6reversed: " + StringUtils.reverse(str6));
		System.out.println("str7: " + str7);
		String str8 = StringUtils.chomp(str7);
		str8 = StringUtils.reverseDelimited(str8, ' ');
		System.out.println("str7 reversed whole words : \r\n" + str8);

		// build header (useful to print logmessages that are easy to locate)
		System.out.println("==============================");
		System.out.println("print header:");
		String padding = StringUtils.repeat("=", 2);
		String msg = StringUtils.center(" Customised Header ", 50, "%");
		Object[] raw = new Object[] { padding, msg, padding };
		String header = StringUtils.join(raw, "\r\n");
		System.out.println(header);
		System.out.println("==========================");
		System.out.println("是否为空isEmpty?" + StringUtils.isEmpty(""));
		System.out.println("是否为空isEmpty?" + StringUtils.isEmpty(" "));
		System.out.println("是否为空isEmpty?" + StringUtils.isEmpty(null));
		System.out.println("==========================");
		// static boolean isBlank(CharSequence str) 判断字符串是否为空或空字符串或null;
		// static boolean isNotBlank(CharSequence str) 判断字符串是否非空或非null;
		System.out.println("是空为空或空字符串isBlank?" + StringUtils.isBlank(""));
		System.out.println("是空为空或空字符串isBlank?" + StringUtils.isBlank(null));
		System.out.println("是空为空或空字符串isBlank?" + StringUtils.isBlank(" "));
		System.out.println("====================");
		System.out.println("不是空为空或空字符串isNotBlank?" + StringUtils.isNotBlank(""));
		System.out.println("不是空为空或空字符串isNotBlank?" + StringUtils.isNotBlank(null));
		System.out.println("不是空为空或空字符串isNotBlank?" + StringUtils.isNotBlank(" "));
		System.out.println("缩进显示abbreviate:" + StringUtils.abbreviate("abcdefg", 20));
		System.out.println("缩进显示abbreviate:" + StringUtils.abbreviate("abcdefg", 4));
		System.out.println("====================");
		System.out.println("首字母大写capitalize:" + StringUtils.capitalize("abcdefg"));
		System.out.println("首字母小写uncapitalize:" + StringUtils.uncapitalize("Abcdefg"));
		System.out.println("====================");
		System.out.println("字符串center显示在一个大字符串的位置:" + StringUtils.center("abcdefg", 20));
		// 总长度20,abcdefg放在中间,其他部分采用*_填充,不够或者多的就自动截取*_
		System.out.println("字符串center显示在一个大字符串的位置:" + StringUtils.center("abcdefg", 20, "*_"));
		System.out.println("字符串leftPad显示在一个大字符串的位置:" + StringUtils.leftPad("abc", 10, "*"));
		System.out.println("字符串rightPad显示在一个大字符串的位置:" + StringUtils.rightPad("abc", 10, "*"));
		System.out.println("====================");
		System.out.println("字符串repeat重复次数:" + StringUtils.repeat("abc", 5));
		System.out.println("====================");
		System.out.println("字符串全小写?" + StringUtils.isAllLowerCase("abC"));
		System.out.println("字符串全小写?" + StringUtils.isAllLowerCase("abc"));
		System.out.println("字符串全大写?" + StringUtils.isAllUpperCase("abC"));
		System.out.println("字符串全大写?" + StringUtils.isAllUpperCase("ABC"));
		System.out.println("====================");
		System.out.println("只有字母组成isAlpha?" + StringUtils.isAlpha("abdefg"));
		System.out.println("只有字母和空格组成isAlphaSpace?" + StringUtils.isAlphaSpace("abdefg "));
		System.out.println("只有字母和数字组成isAlphanumeric?" + StringUtils.isAlphanumeric("a2bdefg"));
		System.out.println("只有数字组成isNumeric?" + StringUtils.isNumeric("a2bdefg"));
		System.out.println("只有数字和空格组成isNumericSpace?" + StringUtils.isNumericSpace("abdefg "));
		System.out.println("====================");
		System.out.println("小字符串[ab]在大字符串[ababsssababa]中出现次数:" + StringUtils.countMatches("ababsssababa", "ab"));
		System.out.println("====================");
		System.out.println("字符串[abcdef]倒序reverse输出" + StringUtils.reverse("abcdef"));
		System.out.println("====================");
		System.out.println("字符串大小写转换,空格不动:" + StringUtils.swapCase("I am a-A*a"));

	}

	/**
	 * 针对ObjectUtils本处放弃apache-commons-lang包
	 * 采用spring-util包里面的ObjectUtils
	 * org.springframework.util.ObjectUtils
	 * 常用方法:ObjectUtils.isEmpty(Object obj)
	 *        ObjectUtils.isEmpty(Object[] array)
	 *        ObjectUtils.isArray(Object obj)
	 *        //Check whether the given array contains the given element.
	 *        ObjectUtils.containsElement(Object[] array, Object element)
	 *        //Check whether the given array of enum constants contains a constant with the given name, ignoring case when determining a match.
	 *        ObjectUtils.containsConstant(Enum<?>[] enumValues, String constant)
	 *        //Check whether the given array of enum constants contains a constant with the given name, ignoring case when determining a match.
	 *        ObjectUtils.containsConstant(Enum<?>[] enumValues, String constant,boolean caseSensitive) //Return whether the given throwable is a checked
	 *        exception: //that is, neither a RuntimeException nor an Error
	 *        ObjectUtils.isCheckedException(Throwable ex)
	 */
	@Test
	public void testObjectUtils() {
		System.err.println(ObjectUtils.isEmpty(null));
		System.err.println(ObjectUtils.isArray(null));
	}

	/*
	 * org.apache.commons.lang.math
	 * 在Jakarta Commons中，
	 * 专门处理数学计算的类分别可以在两个地方找到：
	 * 一是Commons Lang的org.apache.commons.lang.math包中，
	 * 二是在Commons Math这个单独的子项目中。
	 *  由于后者主要是处理复数、矩阵等，相对使用比较少，在我的笔记中就只简单讲讲Commons Lang中的math包。
	 *  对后者感兴趣的可以看看 http://jakarta.apache.org/commons/math/
	 *
	 * org.apache.commons.lang.math包中共有10个类，这些类可以归纳成四组：
	 * 1- 处理分数的Fraction类；
	 * 2-处理数值的NumberUtils类；
	 * 3-处理数值范围的Range、NumberRange、IntRange、LongRange、FloatRange、DoubleRange类；
	 * 4-处理随机数的JVMRandom和RandomUtils类。
	 */
	@Test
	public void testMathUtils() {
		demoFraction();
		demoNumberUtils();
		demoNumberRange();
		demoRandomUtils();
	}
	/**
	 * 测试apache.commons.lang.math
	 * 处理数值的Fraction处理分数类
	 */
	private static void demoFraction() {
		System.out.println(StringUtils.center(" demoFraction ", 30, "="));
		Fraction myFraction = Fraction.getFraction(144, 90);
		// FractionmyFraction = Fraction.getFraction("1 54/90");
		System.out.println("144/90 as fraction: " + myFraction);
		System.out.println("144/90 to proper: " + myFraction.toProperString());
		System.out.println("144/90 as double: " + myFraction.doubleValue());
		System.out.println("144/90 reduced: " + myFraction.reduce());
		System.out.println("144/90 reduced proper: " + myFraction.reduce().toProperString());
		System.out.println();
	}
	/**
	 * 测试apache.commons.lang.math
	 * 处理数值的NumberUtils类；
	 */
	private static void demoNumberUtils() {
		System.out.println(StringUtils.center(" demoNumberUtils ", 30, "="));
		System.out.println("Is 0x3Fa number? "
				+StringUtils.capitalize(BooleanUtils.toStringYesNo(NumberUtils
				.isNumber("0x3F")))+ ".");
		double[] array = { 1.0, 3.4, 0.8, 7.1, 4.6 };
		double max = NumberUtils.max(array);
		double min = NumberUtils.min(array);
		String arrayStr =ArrayUtils.toString(array);
		System.out.println("Max of " + arrayStr + " is: " + max);
		System.out.println("Min of " + arrayStr + " is: " + min);
		System.out.println();
	}
	/**
	 * 测试apache.commons.lang.math
	 * 处理数值范围的Range、NumberRange、IntRange、LongRange、FloatRange、DoubleRange类
	 */
	private static void demoNumberRange() {
		System.out.println(StringUtils.center(" demoNumberRange ", 30, "="));
		Range normalScoreRange = new DoubleRange(90, 120);
		double score1 = 102.5;
		double score2 = 79.9;
		System.out.println("Normal score rangeis: " + normalScoreRange);
		System.out.println("Is "
				+ score1
				+ "a normal score? "
				+ StringUtils
				.capitalize(BooleanUtils.toStringYesNo(normalScoreRange
						.containsDouble(score1)))+ ".");
		System.out.println("Is "
				+ score2
				+ "a normal score? "
				+ StringUtils
				.capitalize(BooleanUtils.toStringYesNo(normalScoreRange
						.containsDouble(score2)))+ ".");
		System.out.println();
	}
	/**
	 * 测试apache.commons.lang.math.RandomUtils
	 */
	private static void demoRandomUtils() {
		System.out.println(StringUtils.center(" demoRandomUtils ", 30, "="));
		for (int i = 0; i < 5; i++) {
			System.out.println(RandomUtils.nextInt(100));
		}
		System.out.println();
	}
	/**
	 * 测试org.apache.commons.lang.time
	 * 日期工具类
	 * 来看我在Common Lang中最后要讲的一个包：org.apache.commons.lang.time。这个包里面包含了如下5个类：
	 DateFormatUtils – 提供格式化日期和时间的功能及相关常量；
	 DateUtils – 在Calendar和Date的基础上提供更方便的访问；
	 DurationFormatUtils – 提供格式化时间跨度的功能及相关常量；
	 FastDateFormat – 为java.text.SimpleDateFormat提供一个的线程安全的替代类；
	 StopWatch – 是一个方便的计时器。
	 */
	@Test
	public void testTimeUtils() {
		demoDateUtils();
		demoStopWatch();
	}
	/**
	 * 测试org.apache.commons.validator
	 * 数据验证
	 */
	@Test
	public void testValidatorUtils() {
		/* 验证日期 */
		// 获取日期验证
		DateValidator dvalidator = DateValidator.getInstance();
		// 验证/转换日期
		Date fooDate = dvalidator.validate("20141141111", "dd/MM/yyyy");
		if (fooDate == null) {
			// 错误 不是日期
			System.err.println("不是日期");
		}
		// 设置参数
		boolean caseSensitive = false;
		String regex1 = "^([A-Z]*)(?:\\-)([A-Z]*)*$";
		String regex2 = "^([A-Z]*)$";
		String[] regexs = new String[] { regex1, regex2 };
		// 创建验证
		RegexValidator validator = new RegexValidator(regexs, caseSensitive);
		// 验证返回boolean
		boolean valid = validator.isValid("abc-def");
		System.err.println(valid);
		// 验证返回字符串
		String result = validator.validate("abc-def");
		System.err.println(result);
		// 验证返回数组
		String[] groups = validator.match("abc-def");
		System.err.println(groups);
		//4.取得类名
		System.out.println(ClassUtils.getShortClassName(DemoApacheCommonsTest.class));
		//取得其包名
		System.out.println(ClassUtils.getPackageName(DemoApacheCommonsTest.class));
	}

	/**
	 * 集合操作
	 * 1、并集:set1跟set2的并集
	 * CollectionUtils.union(set1,set2);
	 * 2、交集主要使用intersection
	 * CollectionUtils.intersection(set1,set2);
	 * CollectionUtils.retainAll(set1,set2);
	 * 3、差集:set1跟set2的差集
	 *  CollectionUtils.subtract(set1,set2);
	 * 4.交集的补集:set1跟set2的交集的补集
	 *  CollectionUtils.disjunction(set1,set2)
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testCollectionsUtils() {
		Set<Integer> set1 =new HashSet<Integer>();
		set1.add(1);
		set1.add(2);
		set1.add(3);

		Set<Integer> set2 =new HashSet<Integer>();
		set2.add(2);
		set2.add(3);
		set2.add(4);
		//判断集合是否为空
		System.out.println("=========判断集合是否为空============");
		System.out.println(CollectionUtils.isEmpty(set1));   //true
		System.out.println(CollectionUtils.isEmpty(set2));   //true
		//判断集合是否不为空
		System.out.println("=========判断集合是否不为空============");
		System.out.println(CollectionUtils.isNotEmpty(set1));   //true
		System.out.println(CollectionUtils.isNotEmpty(set2));   //true
		//比较两集合值
		System.out.println("=========比较集合内容============");
		System.out.println(CollectionUtils.isEqualCollection(set1,set2));   //false
		System.out.println(CollectionUtils.isEqualCollection(set1,set2));   //false
		//并集
		System.out.println("=========并集============");
		Collection<Integer> col =CollectionUtils.union(set1,set2);
		for(Integer temp:col){
			System.out.println(temp);
		}
		//交集
		System.out.println("=========交集============");
		col =CollectionUtils.intersection(set1, set2);
		/*col =CollectionUtils.retainAll(set1, set2);*/
		for(Integer temp:col){
			System.out.println(temp);
		}
		//差集
		System.out.println("====set1和set2的=====差集============");
		col =CollectionUtils.subtract(set1, set1);
		for(Integer temp:col){
			System.out.println(temp);
		}
		System.out.println("=========交集的补集============");
		//交集的补集
		System.out.println(CollectionUtils.disjunction(set1,set2));
		System.out.println("=========集合包含所有============");
		System.out.println(CollectionUtils.containsAny(set1,set2));
		//祛除重复
		List<String> source=new ArrayList<String>();
		source.add("123456");
		source.add("123456");
		source.add("apple");
		source.add("apple");
		source.add("zhangsan");
		System.out.println("源集合:"+JSON.toJSONString(source));
		List<String> result=new ArrayList<String>(new HashSet<String>(source));
		System.out.println("结果集合:"+JSON.toJSONString(result));
        /* List<String> result1=new ArrayList<String>(new HashSet<String>(null));
         System.out.println("结果集合:"+JSON.toJSONString(result1));*/
		List<Integer> integerList = new ArrayList<Integer>();
		integerList.addAll(Arrays.asList(1,2,3,4,5,6,7,8));
		System.out.println("原始数组集合: " + ArrayUtils.toString(integerList));
		//过滤获取集合中的偶数
//		CollectionUtils.filter(integerList, new Predicate(){
//			@Override
//			public boolean evaluate(Object input) {
//				if(Integer.valueOf(input.toString()) % 2 == 0) {
//					return true;
//				}
//				return false;
//			}
//		});
		//过滤获取集合中的奇数
		org.apache.commons.collections4.CollectionUtils.filterInverse(integerList, new org.apache.commons.collections4.Predicate<Integer>() {
			@Override
			public boolean evaluate(Integer input) {
				if(input.intValue() % 2 == 0) {
					return true;
				}
				return false;
			}
		});
		System.out.println("过滤之后的数组集合: " + ArrayUtils.toString(integerList));

	}

	/**
	 * IoUtils流的操作
	 * IOUtils:读取UTF-8数据时,需要注意UTF-8文件中的BOM字节,请采用如下方式来读取
	 */
	@Test
	public void testIoUtils() throws IOException{
		LineIterator line= IOUtils.lineIterator(new BOMInputStream(FileUtils.openInputStream(new File("D://data.txt"))), "UTF-8");
		System.out.println(line);
	}
	/**
	 * FileUtils:针对文件的一些操作
	 * @throws Exception
	 */
	@Test
	public void testFileUtils() throws Exception{
		//拷贝文件
		//FileUtils.copyFile(new File("D://data.txt"), new File("E:data.txt"));
		// 拷贝文件夹
		//FileUtils.copyDirectory(new File("D://antetype"), new File("E://data"), true);
		// 删除某个文件夹
		//FileUtils.cleanDirectory(new File("E://data"));
		// 拷贝文件到文件夹
		//FileUtils.copyFileToDirectory(new File("D://data.txt"), new File("D://antetype//file"));
		// 输入流拷贝文件
		//FileUtils.copyInputStreamToFile(FileUtils.openInputStream(new File("D://data.txt")),new File("D://antetype//file.txt"));
		// 输入流拷贝文件
		//FileUtils.copyToFile(FileUtils.openInputStream(new File("D://data.txt")), new File("D://antetype//file.txt"));
		// 拷贝网络文件到文件
		//FileUtils.copyURLToFile(new URL("http://localhost/qdone/html/index.html"), new File("D://antetype//test.html"));
		//列出目录下的所有文件
		String[] arr = org.aspectj.util.FileUtil.listFiles(new File("D://qdone"));
		for (int i = 0; i < arr.length; i++) {
			System.err.println(arr[i]);
		}
		System.err.println("=================");
		//获取路径最里层的文件夹名称
		System.err.println(FilenameUtils.getBaseName("D:/360Downloads/Apk"));
		//获取文件类型
		System.err.println(FilenameUtils.getExtension("D:/360Downloads/Apk/data.txt"));
		//获取文件路径的根目录
		System.err.println(FilenameUtils.getPrefix("D:/360Downloads/Apk/data.txt"));
		//获得两个文件的相对路径
		System.err.println(org.apache.tools.ant.util.FileUtils.getRelativePath(new File("D:/360Downloads"), new File("D:/antetype/data.txt")) );
	}

	private static void demoDateUtils() {
		System.out.println(StringUtils.center(" demoDateUtils ", 30, "="));
		Date date = new Date();
		String isoDateTime =DateFormatUtils.ISO_DATETIME_FORMAT.format(date);
		String isoTime =DateFormatUtils.ISO_TIME_NO_T_FORMAT.format(date);
		FastDateFormat fdf = FastDateFormat.getInstance("yyyy-MM-dd hh:mm:ss S E");
		String customDateTime =fdf.format(date);
		System.out.println("ISO_DATETIME_FORMAT: " + isoDateTime);
		System.out.println("ISO_TIME_NO_T_FORMAT: " + isoTime);
		System.out.println("Custom FastDateFormat: " +customDateTime);
		System.out.println("Default format: " + date);
		System.out.println("Round HOUR: " + DateUtils.round(date,Calendar.HOUR));
		System.out.println("Truncate HOUR: " +DateUtils.truncate(date, Calendar.HOUR));
		System.out.println();
	}

	private static void demoStopWatch() {
		System.out.println(StringUtils.center(" demoStopWatch ", 30, "="));
		StopWatch sw = new StopWatch();
		sw.start();
		operationA();
		sw.stop();
		System.out.println("operationA used " + sw.getTime() + " milliseconds.");
		System.out.println();
	}

	private static void operationA() {
		try {
			Thread.sleep(999);
		}
		catch (InterruptedException e) {
			// do nothing
		}
	}

	/**
	 * 针对List排序
	 */
	@Test
	public void testListSort() {
		List<Map<String,String>> arr= Lists.newArrayList();
		Map<String,String> mp= Maps.newHashMap();
		mp.put("name","sex");
		mp.put("memo","测试");
		mp.put("price","12.24");
		arr.add(mp);
		Map<String,String> mp1= Maps.newHashMap();
		mp1.put("name","age");
		mp1.put("memo","年龄");
		mp1.put("price","24.15");
		arr.add(mp1);
		Map<String,String> mp2= Maps.newHashMap();
		mp2.put("name","bank");
		mp2.put("memo","银行");
		mp2.put("price","15.35");
		arr.add(mp2);
		Map<String,String> mp3= Maps.newHashMap();
		mp3.put("name","car");
		mp3.put("memo","汽车");
		mp3.put("price","8.38");
		arr.add(mp3);
		Map<String,String> mp4= Maps.newHashMap();
		mp4.put("name",null);
		mp4.put("memo","哈撒尅");
		mp4.put("price","6.37");
		arr.add(mp4);
		Map<String,String> mp5= Maps.newHashMap();
		mp5.put("name","");
		mp5.put("memo","空值");
		mp5.put("price","");
		arr.add(mp5);
		System.err.println(JSON.toJSONString(arr, SerializerFeature.WriteMapNullValue));
		//针对price排序
		Collections.sort(arr, new Comparator<Map<String, String>>() {
			@Override
			public int compare(Map<String, String> o1, Map<String, String> o2) {
				if(StringUtils.isEmpty(o1.get("price"))||StringUtils.isEmpty(o2.get("price"))){//空值前置
					return -1;
				}else{
					Double a=Double.parseDouble(StringUtils.isEmpty(o1.get("price"))?"0":o1.get("price"));
					Double b=Double.parseDouble(StringUtils.isEmpty(o2.get("price"))?"0":o2.get("price"));
					return a.compareTo(b);
				}
			}
		});


		System.err.println(JSON.toJSONString(arr, SerializerFeature.WriteMapNullValue));
		//针对map排序,此处针对null放在前面，也可以放在后面
		Comparator<Map> comparator = Comparator.comparing(o -> (Comparable) o.get("name"), Comparator.nullsFirst(Comparable::compareTo));
		arr.sort(comparator);
		System.err.println(JSON.toJSONString(arr, SerializerFeature.WriteMapNullValue));
		//针对map排序,针对memo字段的汉语顺序排序
		Collections.sort(arr, new Comparator<Map>() {
			@Override
			public int compare(Map o1, Map o2) {
				//获取英文环境
				Comparator<Object> com = Collator.getInstance(Locale.CHINA);
				return com.compare(o1.get("memo"),o2.get("memo"));
			}
		});
		System.err.println(JSON.toJSONString(arr, SerializerFeature.WriteMapNullValue));
		//针对map排序,针对name字段的英语顺序排序,空值需要判断
		Collections.sort(arr, new Comparator<Map>() {
			@Override
			public int compare(Map o1, Map o2) {
				//获取英文环境
				Comparator<Object> com = Collator.getInstance(Locale.ENGLISH);
				if(ObjectUtils.isEmpty(o1.get("name"))||ObjectUtils.isEmpty(o2.get("name"))){
					return -1;
				}
				return com.compare(o1.get("name"),o2.get("name"));
			}
		});
	/*	Collections.sort(arr, Comparator.nullsFirst(
				new Comparator<Map>() {
					@Override
					public int compare(Map o1, Map o2) {
						//获取英文环境
						Comparator<Object> com = Collator.getInstance(Locale.ENGLISH);
						return com.compare(o1.get("name"),o2.get("name"));
					}
				}
		));*/

		System.err.println(JSON.toJSONString(arr, SerializerFeature.WriteMapNullValue));

		//数组转换成LIST
		String[] array=new String[]{"a","b","c","d"};
		List<String> arrList=Arrays.asList(array);
		//List转换成数组
		String[] stringArr = new String[arrList.size()];
		arrList.toArray(stringArr);
		System.err.println("数组是:"+stringArr);
	}

	/**
	 * double跟百分数互转
	 */
	@Test
	public void testDouble2Percent() throws ParseException {
		Double num=NumberFormat.getPercentInstance().parse("25.5%").doubleValue();
		System.err.println("百分数转换浮点:"+num);
		java.text.NumberFormat percentFormat = java.text.NumberFormat.getPercentInstance();
		percentFormat.setMaximumFractionDigits(3); //最大小数位数
		System.err.println("浮点转换百分数:"+percentFormat.format(num));
	}


	/**
	 * map排序，按照key和value排序
	 */
	@Test
	public void testMapKeyOrValueSort() {
		Map<Integer, Integer> arg=new HashMap<>();
		arg.put(5,3);
		arg.put(4,5);
		arg.put(6,2);
		arg.put(2,6);
		System.err.println(arg);
        //按照key升序
		Map<Integer, Integer> keySortMap=new TreeMap<Integer, Integer>(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		});
		keySortMap.putAll(arg);
		System.err.println(keySortMap);
        //按照value排序
		Map<Integer, Integer> sortedMap = new LinkedHashMap<Integer, Integer>();
		List<Map.Entry<Integer, Integer>> entryList = new ArrayList<Map.Entry<Integer, Integer>>(arg.entrySet());
		Collections.sort(entryList, new Comparator<Map.Entry<Integer, Integer>>() {
			@Override
			public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});
		Iterator<Map.Entry<Integer, Integer>> iter = entryList.iterator();
		Map.Entry<Integer, Integer> tmpEntry = null;
		while (iter.hasNext()) {
			tmpEntry = iter.next();
			sortedMap.put(tmpEntry.getKey(), tmpEntry.getValue());
		}
		System.err.println(sortedMap);

	}

	/**
	 * BigInteger数据处理
	 */
	@Test
	public void testBigInteger() {
		BigInteger bi1 = new BigInteger("123456789") ;	// 声明BigInteger对象
		BigInteger bi2 = new BigInteger("987654321") ;	// 声明BigInteger对象
		System.out.println("加法操作：" + bi2.add(bi1)) ;	// 加法操作
		System.out.println("减法操作：" + bi2.subtract(bi1)) ;	// 减法操作
		System.out.println("乘法操作：" + bi2.multiply(bi1)) ;	// 乘法操作
		System.out.println("除法操作：" + bi2.divide(bi1)) ;	// 除法操作
		System.out.println("最大数：" + bi2.max(bi1)) ;	 // 求出最大数
		System.out.println("最小数：" + bi2.min(bi1)) ;	 // 求出最小数
		BigInteger result[] = bi2.divideAndRemainder(bi1) ;	// 求出余数的除法操作
		System.out.println("商是：" + result[0] +"；余数是：" + result[1]) ;
	}
	/**
	 * 测试JDK1.8新特性
	 * stream使用
	 * 参考资料:https://blog.csdn.net/qq_33142257/article/details/62891612
	 */
	@Test
	public void testJdk8Stream() {
		List<Solr> list = Lists.newArrayList();
		Date dt=new Date();
		list.add(new Solr("1",123,"Solr全文检索","solr",dt));
		list.add(new Solr("2",215,"ES全文检索","ES",dt));
		list.add(new Solr("3",152,"Redis缓存工具","redis",dt));
		list.add(new Solr("3",152,"Redis缓存工具","redis",dt));
		System.err.println("执行前:"+JSON.toJSONString(list));
		List<Solr> unList=list.stream().distinct().collect(Collectors.toList());
		System.err.println("祛除重复:"+JSON.toJSONString(unList));
		/*排序，按price降序排列，如果要降续则改成：(a, b) -> b.getId() - a.getId(); a和b都是变量名（可以按自己意愿取名字），都是list中的对象的实例*/
		List<Solr> descList=list.stream().sorted((a,b)->b.getPrice()-a.getPrice()).collect(Collectors.toList());
		System.err.println("按照price降序:"+JSON.toJSONString(descList));
		/*map, 提取对象中的某一元素，例子中我取的name，注意list中类型对应，如果取的是id或者班级，就应该是integer类型*/
		List<String> filterList = list.stream().filter(t->t.getName().equals("redis")).map(t -> t.getName()).collect(Collectors.toList());
		System.err.println("按照name提取出来:"+JSON.toJSONString(filterList));
		/*统计，统计所有人价格的和, 主要我设置的分数属性是double类型的，所以用mapToDouble，如果是int类型的，则需要用mapToInt*/
		double sum = list.stream().mapToDouble(t -> t.getPrice()).sum();
		int count = list.stream().mapToInt(t -> t.getPrice()).sum();
		System.err.println("价格总和:"+sum+"\t 总记录数:"+count);
		/*分组， 按照字段中某个属性将list分组*/
		Map<String, List<Solr>> map = list.stream().collect(Collectors.groupingBy(t -> t.getName()));
		System.err.println("按名称分组："+map);
		System.err.println("====================================");
		/*然后再对map处理，这样就方便取出自己要的数据*/
		for(Map.Entry<String, List<Solr>> entry : map.entrySet()){
			System.err.println("按名称分组-- key:"+entry.getKey());
			System.err.println("按名称分组-- value:"+entry.getValue());
			System.err.println("----------------------------------");
		}
		/*过滤集合*/
		List<Solr> filterDataList = list.stream().filter(t->t.getPrice()<150).collect(Collectors.toList());
		System.err.println("过滤集合:"+JSON.toJSONString(filterDataList));
		/*过滤单项*/
		Optional<Solr> result = list.stream().filter(t->t.getPrice()<150).findFirst();
		System.err.println("过滤单项:"+result.get());
		/*集合包含*/
		System.err.println("集合包含:"+list.stream().filter(t->t.getId().equals("4")).findAny().isPresent());
	}
	/**
	 * date转换
	 */
	@Test
	public void testDate() {
		//字符串转换date
        Date curr= DateUtil.parseDate("2012-12-25 20:35:26");
		System.err.println(curr);
		//date转换instant
		Instant instant=Instant.ofEpochMilli(curr.getTime());
		System.err.println(instant);
		//instant转换date
		Date chang=Date.from(instant);
		System.err.println(chang);
		//instant转换localDateTime
		LocalDateTime localDateTime=instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
		System.err.println(localDateTime);
        //localDateTime转换instant
		Instant instant2=localDateTime.atZone(ZoneId.systemDefault()).toInstant();
		System.err.println(instant2);


		String a="1631757958.401";
		String b="1631757958.401123";
		String c="1631757958";
		Instant instantC=Instant.ofEpochMilli(Long.valueOf(c));
		System.err.println(instantC);
		Date changC=Date.from(instant);
		System.err.println(changC);
		System.err.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss S").format(changC));
		LocalDateTime localDateTimeC=instantC.atZone(ZoneId.systemDefault()).toLocalDateTime();
		System.err.println(localDateTimeC);

	}

	/**
	 * Optional获取对象的属性时,代码更加优雅
	 * https://zhuanlan.zhihu.com/p/148271600
	 * list转换成map写法,value为空时,会出现NPE
	 */
	@Test
	public void testOptional() {
		List<Solr> arr=new ArrayList<>();
		Solr solr=Solr.builder().name("张三").price(250).build();
		Solr solr1=Solr.builder().name("李四").price(320).build();
		String id=Optional.ofNullable(solr).map(Solr::getId).orElse(null);
		System.err.println(id);
		arr.add(solr);
		arr.add(solr1);
//		arr.stream().filter(t->Objects.nonNull(t)&&Objects.nonNull(t.getName())&&Objects.nonNull(t.getPrice())).collect(Collectors.toMap(Solr::getName, Function.identity()));
		Map<String, Integer> mp=arr.stream().filter(t->Objects.nonNull(t)&&Objects.nonNull(t.getName())&&Objects.nonNull(t.getPrice())).collect(Collectors.toMap(Solr::getName, Solr::getPrice));
		System.out.println(mp);
	    List<String> arrs=new ArrayList<>();
	    arrs.add("a_b");
		arrs.add("b_b");
		arrs.add("c_b");
		arrs.forEach(StringUtils::uncapitalize);
		System.err.println(JSON.toJSONString(arrs));
	}

	/**
	 * 代码简化写法
	 */
	@Test
	public void testSmart() {
		//mybatis-plus查询结果转换成不重复id集合
		/*List<String> ids=boxMixedAlarmMapper.selectList(new QueryWrapper<BoxMixedAlarmEntity>().lambda()
				.eq(BoxMixedAlarmEntity::getIsDeleted,false)
				.and(warp->warp.eq(BoxMixedAlarmEntity::getAssetsCode,entity.getKeyword())
						.or()
						.eq(BoxMixedAlarmEntity::getSourceServiceNo,entity.getKeyword())
				)).stream().map(t->t.getSourceRefEventId()).distinct().collect(Collectors.toList());

        //mybatis-plus的更新处理
		boxMixedAlarmMapper.update(null, new LambdaUpdateWrapper<BoxMixedAlarmEntity>()
                .set(BoxMixedAlarmEntity::getUpdater, entity.getUpdater())
                .set(BoxMixedAlarmEntity::getUpdateTime, entity.getUpdateTime())
                .set(BoxMixedAlarmEntity::getOperationStatus, entity.getOperationStatus())
                .set(BoxMixedAlarmEntity::getHandleNo, entity.getHandleNo())
                .eq(BoxMixedAlarmEntity::getId, entity.getId())
        );
		*/
       	//初始化list,可以采用构造器模式创建solr对象
		List<Solr> arr=new ArrayList<>();
		Solr solr=Solr.builder().id("123456").name("张三").price(250).build();
		Solr solr1=Solr.builder().id("222222").name("李四").price(320).build();
		//通过jdk8的optional.ofNullable().orElse();优雅的给参数赋值
		String id=Optional.ofNullable(solr).map(Solr::getId).orElse(null);
		System.err.println(id);
		arr.add(solr);
		arr.add(solr1);
		//快速将arr数组中的id属性，拼接成逗号分割字符串
		final String solrIds = arr.stream().map(Solr::getId).collect(Collectors.joining(","));
		System.err.println(solrIds);
        //通过collect.toMap()将arr转换成指定的map,需要过滤掉value空值情况
		Map<String, Integer> mp=arr.stream().filter(t->Objects.nonNull(t)&&Objects.nonNull(t.getName())&&Objects.nonNull(t.getPrice())).collect(Collectors.toMap(Solr::getName, Solr::getPrice));
		System.out.println(mp);
		Map<String, Solr> solrMap=arr.stream().filter(t->Objects.nonNull(t)&&Objects.nonNull(t.getName())&&Objects.nonNull(t.getPrice())).collect(Collectors.toMap(Solr::getId, Function.identity()));
		System.out.println(solrMap);
		//通过arrrs.forEach方式代码更简洁
		List<String> arrs=new ArrayList<>();
		arrs.add("a_b");
		arrs.add("b_b");
		arrs.add("c_b");
		arrs.forEach(StringUtils::uncapitalize);
		System.err.println(JSON.toJSONString(arrs));
		//更多的jdk8的stream写法，请参考上面testJdk8Stream
		//针对list和map的初始化,不建议使用List.of(),Map.of(),以及Arrays.asList(),以上方式初始化的数据,调用方法时会提示UnsupportedOperationException异常
		/**
		 * List<String> strArr=List.of("a","b","c");
		 * System.err.println(strArr);
		 * strArr.add("d");
		 * System.err.println(strArr);
		 * Exception in thread "main" java.lang.UnsupportedOperationException
		 *
		 * List<String> stringArr=Arrays.asList(new String[]{"a","b","c"});
		 * Map<String, String> mp=Map.of("a","b","c","a","b","c");
		 * mp.put("e","f");
		 * System.err.println(mp);
		 * Exception in thread "main" java.lang.UnsupportedOperationException
		 */
		//set的初始化
		Set<String> gst= Sets.newHashSet("a","b","c");
		gst.add("d");
		System.err.println(gst);
		//list的初始化
		List<String> strList=  Lists.newArrayList("a","b","c");
		strList.add("d");
		System.err.println(strList);
		System.err.println(SystemUtils.USER_TIMEZONE);
	}
}
