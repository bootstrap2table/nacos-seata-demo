package com.qdone;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.mcy.common.entity.Solr;
import com.qdone.common.mq.JMSProducer;
import com.qdone.framework.core.page.CoreUtil;
import io.github.swagger2markup.GroupBy;
import io.github.swagger2markup.Language;
import io.github.swagger2markup.Swagger2MarkupConfig;
import io.github.swagger2markup.Swagger2MarkupConverter;
import io.github.swagger2markup.builder.Swagger2MarkupConfigBuilder;
import io.github.swagger2markup.markup.builder.MarkupLanguage;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.jms.Destination;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 简单MockMVC测试
 */
/*@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("uat")
@WebAppConfiguration*/
@FixMethodOrder(MethodSorters.JVM)
public class DemoApplicationTests extends BaseWebTest{

	@Autowired
	private JMSProducer jmsProducer;

	@Autowired
	RedissonClient redisson;

	/**
	 * 携带token
	 * 传递header的post方式请求
	 */
	@Test
	public void testPostRestTemplate(){
		String url="http://localhost:8084/user/testRestTemplate";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		headers.add("token","123456");
		Solr req=new Solr();
		req.setId("1111");
		req.setCreatetime(new Date());
		req.setName("张三丰");
		/*MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<String, String>();
		* HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(paramMap, headers);
		* */
		HttpEntity<com.mcy.common.entity.Solr> request = new HttpEntity<com.mcy.common.entity.Solr>(req, headers);
		ResponseEntity<Map> response = testRestTemplate.postForEntity( url, request , Map.class );
		if(response.getStatusCode().value()==200){
			System.err.println(response.getBody());
		}
	}

	/**
	 * 携带token
	 * 传递header的get方式请求
	 */
	@Test
	public void testGetRestTemplate(){
		String url="http://localhost:9090/testGetTemplate";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		headers.add("token","123456");
		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<String, String>();
		paramMap.set("name","apple");
		paramMap.set("sex","147");
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(paramMap, headers);
		ResponseEntity<Map> response =testRestTemplate.exchange(url, HttpMethod.GET,request,Map.class);
		if(response.getStatusCode().value()==200){
			System.err.println(response.getBody());
		}
	}

	@Test
	public void testMokcController() throws Exception {
		com.mcy.common.entity.Solr req=new com.mcy.common.entity.Solr();
		req.setId("1111");
		req.setCreatetime(new Date());
		req.setName("张三丰");
		String str=mockMvc.perform(MockMvcRequestBuilders.post("/testRestTemplate")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.header("token",123456789)
				.content(JSON.toJSONString(req, SerializerFeature.WriteMapNullValue))
				.accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		System.err.println("testRestTemplate测试结果:"+str);
	}


	@Test
	public void testMokcGetController() throws Exception {
		com.mcy.common.entity.Solr req=new com.mcy.common.entity.Solr();
		req.setId("1111");
		req.setCreatetime(new Date());
		req.setName("张三丰");
		String str=mockMvc.perform(MockMvcRequestBuilders.get("/testGetTemplate")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.header("token",123456789)
				.param("name","张三丰").param("age","20")
				.accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		System.err.println("testRestTemplate测试结果:"+str);
	}


	@Test
	public void testHello() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/testJson").accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print());
	}


	@Test
	public void testJmsQueue() {
		Destination destination = new ActiveMQQueue("springboot.queue.test");
		for (int i = 0; i < 5; i++) {
			jmsProducer.sendMessage(destination, "生成队列消息"+i);
			jmsProducer.sendMessage(new ActiveMQQueue("qghappy"), "qghappy通用生成队列消息"+i);
			jmsProducer.sendMessage(new ActiveMQQueue("edgm"), "edgm通用生成队列消息"+i);
		}
	}

	@Test
	public void testJmsTopic() {
		Destination topic = new ActiveMQTopic("springboot.topic.test");
		for (int i = 0; i < 10; i++) {
			jmsProducer.publish(topic, "发布主题消息" + i);
		}
		jmsProducer.publish(new ActiveMQTopic("springboot.topic"), "发布主题消息");
	}

	@Test
	public void testJms() {
		Destination destination = new ActiveMQQueue("springboot.queue.test");
		Destination topic = new ActiveMQTopic("springboot.topic.test");
		for (int i = 0; i < 10; i++) {
			jmsProducer.sendMessage(destination, "生成队列消息"+i);
			jmsProducer.sendMessage("springboot.queue.test", "队列消息"+i);
			jmsProducer.publish(topic, "发布主题消息" + i);
			jmsProducer.publish("springboot.topic.test", "主题消息"+i);
		}
	}


	/**
	 * 输出Ascii格式多文件
	 */
	@Test
	public void generateAsciiDocs() throws Exception {
		//	输出Ascii格式
		Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
				.withMarkupLanguage(MarkupLanguage.ASCIIDOC)
				.build();

		Swagger2MarkupConverter.from(new URL("http://localhost:8080/v2/api-docs"))
				.withConfig(config)
				.build()
				.toFolder(Paths.get("src/docs/asciidoc/generated"));
	}
	/**
	 * 输出Markdown格式单文件
	 */
	@Test
	public void generateMarkdownDocs() throws Exception {
		//	输出Markdown格式
		Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
				.withMarkupLanguage(MarkupLanguage.MARKDOWN)
				.build();

		Swagger2MarkupConverter.from(new URL("http://localhost:8080/v2/api-docs"))
				.withConfig(config)
				.build()
				.toFolder(Paths.get("src/docs/markdown/generated"));
	}
	/**
	 * 输出Confluence格式单文件
	 */
	@Test
	public void generateConfluenceDocs() throws Exception {
		//	输出Confluence使用的格式
		Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
				.withMarkupLanguage(MarkupLanguage.CONFLUENCE_MARKUP)
				.build();

		Swagger2MarkupConverter.from(new URL("http://localhost:8080/v2/api-docs"))
				.withConfig(config)
				.build()
				.toFolder(Paths.get("src/docs/confluence/generated"));
	}
	/**
	 * 输出Ascii格式单文件
	 */
	@Test
	public void generateAsciiDocsToFile() throws Exception {
		//	输出Ascii到单文件
		Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
				.withMarkupLanguage(MarkupLanguage.ASCIIDOC)
				.withOutputLanguage(Language.ZH)
				.withPathsGroupedBy(GroupBy.TAGS)
				.withGeneratedExamples()
				.withoutInlineSchema()
				.build();
		//url不要使用https方式，否则读取url地址会报错
		Swagger2MarkupConverter.from(new URL("http://127.0.0.1:8089/v2/api-docs"))
//		Swagger2MarkupConverter.from(new URL("http://localhost:9999/v2/api-docs?group=OpenApi"))
				.withConfig(config)
				.build()
				.toFile(Paths.get("target/docs/asciidoc/generated/index"));
	}
	/**
	 * 输出Markdown格式单文件
	 */
	@Test
	public void generateMarkdownDocsToFile() throws Exception {
		//	输出Markdown到单文件
		Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
				.withMarkupLanguage(MarkupLanguage.MARKDOWN)
				.withOutputLanguage(Language.ZH)
				.withPathsGroupedBy(GroupBy.TAGS)
				.withGeneratedExamples()
				.withoutInlineSchema()
				.build();

		Swagger2MarkupConverter.from(new URL("http://localhost:8080/c4i-bi/v2/api-docs"))
				.withConfig(config)
				.build()
				.toFile(Paths.get("target/docs/markdown/generated/index"));
	}


	@Test
	public void testRedisson() throws Exception {
		RList<String> list = redisson.getList("anyList");
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.get(0);
		List<String> arr= list.readAll();
		for (String str:arr) {
			System.err.println(str);
		}
		list.remove("a");
		List<String> arr1= list.readAll();
		for (String str:arr1) {
			System.err.println(str);
		}
	}

	@Test
	public void testGetString() {
		String str=CoreUtil.getStringByUrl("http://localhost:88/template/controller.ftl");
		System.err.println(str);
	}
}
