package com.qdone.framework.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * 数据源配置
 * 多数据源时，可以配置多个本类,可以参考
 * https://gitee.com/bootstrap2table/boot_master/blob/feature/jta/druid/src/main/java/com/qdone/framework/config/MyBatisConfig.java
 * https://github.com/ityouknow/spring-boot-examples/blob/master/spring-boot-mybatis/spring-boot-mybatis-xml-mulidatasource/src/main/java/com/neo/datasource/DataSource1Config.java
 */
@Configuration
@MapperScan(basePackages = {"com.qdone.module.dao"}, sqlSessionTemplateRef  = "sqlSessionTemplate")
public class DataSourceConfig {
    @Autowired
    Environment env;

    /**
     * seata需要重新配置数据源，
     * 这里配置下druid的相关参数
     * @return
     */
    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        DruidDataSource dataSource =new DruidDataSource();
        dataSource.setInitialSize(env.getProperty("spring.datasource.druid.initial-size",Integer.class));
        dataSource.setMinIdle(env.getProperty("spring.datasource.druid.min-idle",Integer.class));
        dataSource.setMaxActive(env.getProperty("spring.datasource.druid.max-active",Integer.class));
        dataSource.setMaxWait(env.getProperty("spring.datasource.druid.max-wait",Long.class));
        dataSource.setTestOnBorrow(env.getProperty("spring.datasource.druid.test-on-borrow",Boolean.class));
        dataSource.setTestOnReturn(env.getProperty("spring.datasource.druid.test-on-return",Boolean.class));
        dataSource.setTestWhileIdle(env.getProperty("spring.datasource.druid.test-while-idle",Boolean.class));
        dataSource.setValidationQuery(env.getProperty("spring.datasource.druid.validation-query"));
        return dataSource;
    }
    //1.3seata用法
    /*
    @Bean
    public DataSourceProxy dataSourceProxy(DataSource dataSource) {
        return new DataSourceProxy(dataSource);
    }

    @Bean(name = "sqlSessionFactory")
    @ConditionalOnMissingBean // 当容器里没有指定的Bean的情况下创建该对象
    @Primary
    public SqlSessionFactory sqlSessionFactory(DataSourceProxy dataSourceProxy,@Value("${mybatis.config-location}")  String mapperConfigLocation,@Value("${mybatis.mapper-locations}")  String mapperLocations) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSourceProxy);
        sqlSessionFactoryBean.setConfigLocation(new PathMatchingResourcePatternResolver().getResource(mapperConfigLocation));
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(mapperLocations));
        return sqlSessionFactoryBean.getObject();
    }*/

    //1.4seata配置用法
    @Bean(name = "sqlSessionFactory")
    @ConditionalOnMissingBean // 当容器里没有指定的Bean的情况下创建该对象
    @Primary
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource,@Value("${mybatis.config-location}")  String mapperConfigLocation,@Value("${mybatis.mapper-locations}")  String mapperLocations) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setConfigLocation(new PathMatchingResourcePatternResolver().getResource(mapperConfigLocation));
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(mapperLocations));
        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "sqlSessionTemplate")
    @Primary
    public SqlSessionTemplate sqlSessionTemplate(@Qualifier("sqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

}