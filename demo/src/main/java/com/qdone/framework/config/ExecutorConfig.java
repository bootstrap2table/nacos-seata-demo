/*
package com.qdone.framework.config;

*/
/*import java.util.concurrent.Executor;*//*

import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

*/
/**
 * 线程池配置
 *//*

@Configuration
@EnableAsync
public class ExecutorConfig {
	
	private int corePoolSize = 5;// 线程池维护线程的最少数量

	private int maxPoolSize = 50;// 线程池维护线程的最大数量

	private int queueCapacity = 20; // 缓存队列
	
	private int keepAlive = 60;// 允许的空闲时间

	@Bean(name = "taskExecutor")
	@ConditionalOnMissingBean(TaskExecutor.class)
	public TaskExecutor taskExecutor() {
		MdcThreadPoolTaskExecutor executor = new MdcThreadPoolTaskExecutor();
		executor.setCorePoolSize(corePoolSize);
		executor.setMaxPoolSize(maxPoolSize);
		executor.setQueueCapacity(queueCapacity);
		executor.setThreadNamePrefix("taskExecutor-");
		// rejection-policy：当pool已经达到max size的时候，如何处理新任务
		// CALLER_RUNS：不在新线程中执行任务，而是由调用者所在的线程来执行
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy()); // 对拒绝task的处理策略
		executor.setKeepAliveSeconds(keepAlive);
		executor.initialize();
		return executor;
	}

	*/
/**
	 * 配置MDC,记录线程上下文信息，方便全局链路追踪
	 *//*

	class MdcThreadPoolTaskExecutor extends ThreadPoolTaskExecutor {

		*/
/**
		 * 所有线程都会委托给这个execute方法，在这个方法中我们把父线程的MDC内容赋值给子线程
		 * https://logback.qos.ch/manual/mdc.html#managedThreads
		 *
		 * @param runnable
		 *//*

		@Override
		public void execute(Runnable runnable) {
			// 获取父线程MDC中的内容，必须在run方法之前，否则等异步线程执行的时候有可能MDC里面的值已经被清空了，这个时候就会返回null
			Map<String, String> context = MDC.getCopyOfContextMap();
			super.execute(() -> run(runnable, context));
		}
		*/
/**
		 * 子线程委托的执行方法
		 * 父线程MDC内容
		 *//*

		private void run(Runnable runnable, Map<String, String> context) {
			// 将父线程的MDC内容传给子线程
			MDC.setContextMap(context);
			try {
				// 执行异步操作
				runnable.run();
			} finally {
				// 清空MDC内容
				MDC.clear();
			}
		}

	}
}
*/
