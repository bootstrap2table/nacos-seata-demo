package com.qdone.framework.util.cache;


import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.ibatis.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;

import com.qdone.framework.util.SpringContextUtil;
 
/**
 * 使用Redis来做Mybatis的二级缓存
 * 实现Mybatis的Cache接口
 */
public class MybatisRedisCache implements Cache {

    Logger logger = LoggerFactory.getLogger(MybatisRedisCache.class);
 
    // 读写锁
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);

    private static final long EXPIRE_TIME_IN_MINUTES = 5; // redis过期时间

	private RedisTemplate<String, Object> redisTemplate; 
    
 
    private String id;
 
    public MybatisRedisCache(final String id) {
        if (id == null) {
            throw new IllegalArgumentException("Mybatis Cache instances require an ID");
        }
        logger.debug("mybatis cache id: " + id);
        this.id = id;
    }
 
    @Override
    public String getId() {
        return this.id;
    }
 
    @Override
    public void putObject(Object key, Object value) {
        try {
			if (value != null) {
			    // 向Redis中添加数据，有效时间是5分钟
				getRedisTemplate().opsForValue().set(key.toString(), value, EXPIRE_TIME_IN_MINUTES, TimeUnit.MINUTES);
			    logger.debug("mybatis cache put query result to redis");
			}
		} catch (Exception e) {
			logger.error("mybatis cache put query result to redis failed:"+e);
		}
    }
 
    @Override
    public Object getObject(Object key) {
        try {
            if (key != null) {
                Object obj = getRedisTemplate().opsForValue().get(key.toString());
                logger.debug("mybatis cache get query result from redis");
                return obj;
            }
        } catch (Exception e) {
            logger.error("mybatis cache get query result from redis failed:"+e);
        }
        return null;
    }
 
    @Override
    public Object removeObject(Object key) {
        try {
            if (key != null) {
            	getRedisTemplate().delete(key.toString());
                logger.debug("mybatis  cache remove query result from redis");
            }
        } catch (Exception e) {
        	logger.error("mybatis cache remove query result from redis failed:"+e);
        }
        return null;
    }
 
    @Override
    public void clear() {
        try {
            Set<String> keys = getRedisTemplate().keys("*:" + this.id + "*");
            if (!CollectionUtils.isEmpty(keys)) {
            	getRedisTemplate().delete(keys);
                logger.debug("mybatis cache clear query result from redis");
            }
        } catch (Exception e) {
        	logger.debug("mybatis cache clear query result from redis failed:"+e);
        }
    }
 
    @Override
    public int getSize() {
        Long size = (Long) getRedisTemplate().execute(new RedisCallback<Long>() {
            @Override
            public Long doInRedis(RedisConnection connection) throws DataAccessException {
                return connection.dbSize();
            }
        });
        return size.intValue();
    }
 
    @Override
    public ReadWriteLock getReadWriteLock() {
        return this.readWriteLock;
    }
    
    
	private RedisTemplate<String, Object> getRedisTemplate(){
    	if(redisTemplate==null){
    		this.redisTemplate= (RedisTemplate<String, Object>) SpringContextUtil.getBean("cacheClient");
    	}
    	return this.redisTemplate;
    }
}