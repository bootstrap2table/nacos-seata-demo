package com.qdone.framework.util.task;


import com.alibaba.ttl.threadpool.TtlExecutors;
import com.linkedin.parseq.EngineBuilder;
import com.linkedin.parseq.batching.BatchingSupport;

import java.util.concurrent.*;


/**
 * Task的执行者工厂类
 */
public class EngineFactory {

    // 单例模式
    private static EngineFactory INSTANCE = new EngineFactory();
    private final EngineAgent defaultEngine;

    // 初始化任务执行者
    private EngineFactory() {
        // Java虚拟机的可用的处理器数量
        int numCores = Runtime.getRuntime().availableProcessors();
        // 默认 任务执行线程池大小 与 任务调度池程池大小
        defaultEngine = getEngine(numCores + 1,2*numCores+1, 60,"async-parseq-task-",1, numCores + 1);
    }

    public static EngineAgent defaultEngine() {
        return INSTANCE.defaultEngine;
    }


    /**
     * 初始化异步处理框架
     *
     * @param coreSize     任务执行线程池核心数
     * @param maxSize     任务执行线程池最大数
     * @param keepAlive    线程池最大空闲时间(秒)
     * @param namePrefix   线程池名称的前缀
     * @param scheduleSize 任务调度池程池大小
     * @param queueNum     任务对列数量
     * @return
     */
    public static EngineAgent getEngine(int coreSize,int maxSize, int keepAlive,String namePrefix,int scheduleSize, int queueNum) {
        ScheduledExecutorService scheduler = new ScheduledThreadPoolExecutor(scheduleSize);
        //任务队列
        BlockingQueue<Runnable> queue=(BlockingQueue)(queueNum > 0 ? new LinkedBlockingQueue(queueNum) : new SynchronousQueue());
        //拒绝策略
        RejectedExecutionHandler handler=new ThreadPoolExecutor.AbortPolicy();
        ExecutorService executorService =new MdcExecutor(coreSize,
                maxSize,
                keepAlive,
                TimeUnit.SECONDS,queue, MdcExecutor.mdcThreadFactory(namePrefix),handler
        );
        final EngineBuilder builder = new EngineBuilder().setTaskExecutor(TtlExecutors.getTtlExecutorService(executorService)).setTimerScheduler(TtlExecutors.getTtlScheduledExecutorService(scheduler));
        final BatchingSupport batchingSupport = new BatchingSupport();
        // 批处理
        builder.setPlanDeactivationListener(batchingSupport);
        return new EngineAgent(builder.build(), TtlExecutors.getTtlExecutorService(executorService), TtlExecutors.getTtlScheduledExecutorService(scheduler));
    }



}
