package com.qdone.framework.util.task;


import com.linkedin.parseq.Engine;
import com.linkedin.parseq.Task;
import com.linkedin.parseq.promise.Promises;
import com.linkedin.parseq.promise.SettablePromise;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.apm.toolkit.trace.SupplierWrapper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Supplier;


/**
 * 异步处理框架
 * Task的执行者代理类
 * @author 付为地
 */
@Slf4j
public class EngineAgent {

    /**
     *Task的执行者
     */
    private Engine engine;
    /**
     *线程池
     */
    private ExecutorService executors;
    /**
     *任务调度池程
     */
    private ScheduledExecutorService scheduler;

    public EngineAgent(Engine engine, ExecutorService executors, ScheduledExecutorService scheduler) {
        this.engine = engine;
        this.executors = executors;
        this.scheduler = scheduler;
    }

    /**
     * 异步执行任务
     * 采用skywalking加入traceId
     * @param supplier
     * @param <T>
     * @return
     */
    public <T> SettablePromise<T> async(Supplier<T> supplier) {
        final SettablePromise<T> promise = Promises.settable();
        getExecutors().execute(() -> {
            try {
                promise.done(SupplierWrapper.of(supplier).get());
            } catch (Exception e) {
                log.error("ParSeq EngineAgent execute async task error",e);
                promise.fail(e);
            }
        });
        return promise;
    }

    /**
     * 执行任务，返回回调结果
     * @param supplier
     * @param <T> 传入参数
     * @return 异步回调结果
     */
    public <T> Task<T> task(Supplier<T> supplier) {
        return Task.async(() -> async(supplier));
    }

    /**
     * 执行任务
     * @param task
     */
    public void run(final Task<?> task) {
        engine.run(task);
    }

    /**
     * 关闭引擎
     */
    public void shutdown() {
        engine.shutdown();
        executors.shutdown();
    }

    public ExecutorService getExecutors() {
        return executors;
    }

    public ScheduledExecutorService getScheduler() {
        return scheduler;
    }

    public Engine getEngine() {
        return engine;
    }
}
