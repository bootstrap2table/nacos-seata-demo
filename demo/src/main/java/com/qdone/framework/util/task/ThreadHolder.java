package com.qdone.framework.util.task;

import com.alibaba.ttl.TransmittableThreadLocal;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * threadLocal工具类
 * @author 付为地
 */
public class ThreadHolder {

    private static final ThreadHolder INSTANCE = new ThreadHolder();

    private ThreadHolder() {
    }

    public static ThreadHolder getInstance() {
        return INSTANCE;
    }

    /**
     * TransmittableThreadLocal使用
     */
    private TransmittableThreadLocal<Map<String, String>> fastThreadLocal =  new TransmittableThreadLocal<Map<String, String>>() {
        @Override
        protected Map<String, String> initialValue() {
            return new ConcurrentHashMap<>();
        }
    };

    /**
     * 存入数据
     */
    public String put(String key, String value) {
        return fastThreadLocal.get().put(key, value);
    }

    /**
     * 获取数据
     */
    public String get(String key) {
        return fastThreadLocal.get().get(key);
    }

    /**
     * 移除数据
     */
    public String remove(String key) {
        return fastThreadLocal.get().remove(key);
    }

    /**
     *返回初始MAP
     */
    public Map<String, String> entries() {
        return fastThreadLocal.get();
    }

}
