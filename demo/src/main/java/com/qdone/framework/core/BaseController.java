package com.qdone.framework.core;

import com.google.common.collect.Maps;
import com.qdone.framework.core.constant.Constants;
import com.qdone.framework.core.page.CoreUtil;
import com.qdone.framework.core.page.PageList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.text.Collator;
import java.util.*;

public class BaseController {


	protected static final List<Object> noRrcords = new ArrayList<Object>(0);
	
	/**
	 * 表单验证结果
	 * @param userResult:表单验证参数
	 * @return 表单验证结果
	 */
	protected  static List<Map<String, String>> validErrorResult(BindingResult userResult) {
	    	List<Map<String, String>> errorList=new ArrayList<Map<String, String>>();
	        if (userResult.hasErrors()) {
	            List<FieldError> errors = userResult.getFieldErrors();
				for (FieldError error : errors) {
						//响应验证结果
						if(StringUtils.isNotEmpty(error.getField())){
							Map<String, String> map  = Maps.newHashMap();
							map.put("field", error.getField());
							map.put("message", error.getDefaultMessage());
							errorList.add(map);
						}
				}
				/*Comparator<Map> comparator = Comparator.comparing(o -> (Comparable) o.get("field"), Comparator.nullsFirst(Comparable::compareTo));
				errorList.sort(comparator);*/
				//针对field字段不为空，才做排序，空值需要单独处理
				if(CollectionUtils.isNotEmpty(errorList)){
					Collections.sort(errorList, new Comparator<Map>() {
						@Override
						public int compare(Map o1, Map o2) {
							//获取英文环境
							Comparator<Object> com = Collator.getInstance(java.util.Locale.ENGLISH);
							return com.compare(o1.get("field"),o2.get("field"));
						}
					});
				}
	        }
	        return errorList;
	}


	/**
	 * 所有ActionMap 统一从这里获取
	 * 
	 * @return
	 */
	public Map<String, Object> getRootMap() {
		Map<String, Object> rootMap = new LinkedHashMap<String, Object>();
		return rootMap;
	}

	/**
	 * 所有ActionMap 统一从这里获取
	 * 
	 * @return
	 */
	public Map<String, Object> getRootMap(int length) {
		Map<String, Object> rootMap = new HashMap<String, Object>(length);
		return rootMap;
	}

	public Map<String, Object> getRootMap(Map<String, Object> map) {
		if (map == null) {
			map = new HashMap<String, Object>();
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	public ModelAndView forword(String viewName, @SuppressWarnings("rawtypes") Map context) {
		viewName = Constants.TEMPLATE + viewName;
		return new ModelAndView(viewName, context);
	}

	public static String forword(String page) {
		return Constants.TEMPLATE + page;
	}

	@SuppressWarnings("unchecked")
	public ModelAndView redirect(String viewName, @SuppressWarnings("rawtypes") Map context) {
		return new ModelAndView(new RedirectView(viewName), context);
	}

	public ModelAndView redirect(String viewName) {
		return new ModelAndView(new RedirectView(viewName));
	}

	public ModelAndView error(String errMsg) {
		return new ModelAndView("error");
	}

	/**
	 * 过滤查询参数的字符串""
	 * 
	 * @param mp
	 * @return
	 */
	public Map<String, Object> filterEmpty(Map<String, Object> mp) {
		Iterator<Map.Entry<String, Object>> entryKeyIterator = mp.entrySet().iterator();
		while (entryKeyIterator.hasNext()) {
			Map.Entry<String, Object> e = entryKeyIterator.next();
			mp.put(e.getKey(), CoreUtil.conventEmpty2Null(e.getValue()));
		}
		return mp;
	}

	/**
	 * 响应bootstrap-table分页
	 */
	public final Map<String, Object> responseSelectPage(PageList<?> pageNoList) {
		Map<String, Object> result = Maps.newHashMap();
		if (!ObjectUtils.isEmpty(pageNoList) &&CollectionUtils.isNotEmpty(pageNoList.getList())) {
			result.put("rows", pageNoList.getList());
		} else {
			result.put("rows", noRrcords);
		}
		// 总记录条数
		if (!ObjectUtils.isEmpty(pageNoList) && !ObjectUtils.isEmpty(pageNoList.getPageInfo().getRecordCount())) {
			result.put("total", pageNoList.getPageInfo().getRecordCount());
		} else {
			result.put("total", 0);
		}
		return result;
	}

	/**
	 * 不用分页使用此方法封装返回值
	 * 
	 * @param list
	 * @return
	 */
	public final Map<String, Object> responseSelectNoPage(List<?> list) {
		Map<String, Object> result = Maps.newHashMap();
		if (CollectionUtils.isEmpty(list)) {
			result.put("rows", noRrcords);
			result.put("total", 0);
		} else {
			result.put("rows", list);
			result.put("total", list.size());
		}
		return result;
	}
}
