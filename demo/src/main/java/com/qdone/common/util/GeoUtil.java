package com.qdone.common.util;

import com.alibaba.fastjson.JSON;
import com.mcy.common.entity.NetworkEntity;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 网点经纬度计算
 * author 付为地
 */
@Component
@Slf4j
public class GeoUtil {

    /**
     * Redis Geo Key
     */
    public static final String GEO_KEY = "redis:geo:caculate:util:key";

    /**
     * 30公里内
     */
    private static final double range = 30;

    /**
     * scan每次查询数量
     */
    private static final int limit = 10000;

    @Autowired
    private RedisTemplate redisTemplate;



    /**
     * 添加网点经纬度数据
     * @param lon 经度
     * @param lat 维度
     * @param dto 数据
     * @return
     */
    public Long add(double lon, double lat, NetworkEntity dto) {
        return add(GEO_KEY, lon, lat, JSON.toJSONString(dto));
    }

    /**
     * 添加网点经纬度数据
     * @param key 唯一key
     * @param lon 经度
     * @param lat 维度
     * @param dto 数据
     * @return
     */
    public Long add(String key, double lon, double lat, NetworkEntity dto) {
        return add(key, lon, lat, JSON.toJSONString(dto));
    }

    /**
     * 添加网点经纬度数据
     * 添加保证唯一键
     * @param key 唯一key
     * @param lon 经度
     * @param lat 维度
     * @param json NetworkEntity必须是网点转换成的JSON数据
     * @return
     */
    @SneakyThrows
    public Long add(String key, double lon, double lat, String json) {
        if(StringUtils.isNotEmpty(json)){
            NetworkEntity param=JSON.parseObject(json,NetworkEntity.class);
            if(!ObjectUtils.isEmpty(param)&&StringUtils.isNotEmpty(param.getCode())){
                Cursor<ZSetOperations.TypedTuple<Object>> cursor = redisTemplate.opsForZSet().scan(key, ScanOptions.scanOptions().match("*" +param.getCode()+"*").count(limit).build());
                while (cursor.hasNext()) {
                    ZSetOperations.TypedTuple<Object> typedTuple = cursor.next();
                    NetworkEntity network=JSON.parseObject(Objects.toString(typedTuple.getValue()),NetworkEntity.class);
                    if(!ObjectUtils.isEmpty(network)&&network.getCode().equals(param.getCode())){
                        redisTemplate.opsForZSet().remove(GeoUtil.GEO_KEY,typedTuple.getValue());
                    }
                }
                cursor.close();
                return redisTemplate.opsForGeo().add(key, new Point(lon, lat), json);
            }else{
                return 0L;
            }
        }else{
            return 0L;
        }
    }

    /**
     * 过滤指定经纬度最近网点
     * @param key 唯一key
     * @param lon 经度
     * @param lat 维度
     * @return
     */
    public List<NetworkEntity> filter(String key, double lon, double lat) {
        List<NetworkEntity> data=new ArrayList<>();
        Circle circle = new Circle(new Point(lon, lat), new Distance(range, RedisGeoCommands.DistanceUnit.KILOMETERS));
        RedisGeoCommands.GeoRadiusCommandArgs param = RedisGeoCommands
                .GeoRadiusCommandArgs
                .newGeoRadiusArgs()
                .includeDistance()
                .includeCoordinates()
                .sortAscending();
        GeoResults<RedisGeoCommands.GeoLocation<String>> radius = redisTemplate.boundGeoOps(key).radius(circle, param);
        for (GeoResult<RedisGeoCommands.GeoLocation<String>> result : radius) {
            RedisGeoCommands.GeoLocation<String> content = result.getContent();
            log.info("检索的结果，唯一标识：{}，位置：{}，距离：{}.",content.getName(), content.getPoint(), result.getDistance());
            NetworkEntity pojo=JSON.parseObject(content.getName(),NetworkEntity.class);
            pojo.setDistance(result.getDistance().getValue());
            data.add(pojo);
        }
        //创造时间排序
        if (CollectionUtils.isNotEmpty(data)) {
            List<NetworkEntity> dataList=data.stream().sorted((a, b)->b.getCreateTime().compareTo(a.getCreateTime())).collect(Collectors.toList());
            return dataList;
        }
        return data;
    }

    /**
     * 过滤指定经纬度最近网点
     * @param lon 经度
     * @param lat 维度
     * @return
     */
    public List<NetworkEntity> filter(double lon, double lat) {
        return filter(GEO_KEY,lon,lat);
    }


}
