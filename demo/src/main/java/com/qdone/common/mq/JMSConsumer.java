package com.qdone.common.mq;

import com.alibaba.fastjson.JSON;
import com.qdone.common.util.JwtUtils;
import com.qdone.common.util.MathUtil;
import com.qdone.framework.core.constant.Constants;
import com.qdone.module.model.Student;
import com.qdone.module.service.StudentService;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 消费者 
 */
@Component
public class JMSConsumer {
	
	private Logger logger = LoggerFactory.getLogger(JMSConsumer.class);
	
    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private RedissonClient redisson;

    @Autowired
    StudentService studentService;
    
    /**
     * 接收队列消息
     * @param msg
     *        接收的队列消息
     */
    @JmsListener(destination = "springboot.queue.test",containerFactory="queueListenerContainerFactory")
    @SendTo("springboot.queue.reverse") //双向队列
    public String receiveQueue(String msg) {
        logger.debug("接收到队列{springboot.queue.test}的消息：{}",msg);
        if(!StringUtils.isEmpty(msg)){//存储redis中，便于查询
            RList<String> list = redisson.getList("redissonQueueList");
            list.add(msg);
            if(list.remainTimeToLive()==-1){
                list.expire(30, TimeUnit.DAYS);//默认30天
            }
        	jwtUtils.set((Constants.ActiveMqQueueData.SPRINGBOOT_QUEUE_TEST+msg).getBytes(), 30*60*24*7, msg);
        }
        return "------------------>我已成功接收到队列[springboot.queue.test]信息,确认完毕！";
    }
    
    /**
     * 接收队列消息
     * @param msg
     *        接收的队列消息
     */
    @JmsListener(destination = "springboot.queue.reverse",containerFactory="queueListenerContainerFactory")
    public void receive(String msg) {
        logger.debug("接收到队列{springboot.queue.reverse}的消息：{}",msg);
    }
   
    
    /*
     * 接收主题消息
     * 主题消息，只需要一台服务器接收存储就可以了
     * 数据多了，会出现重复
     * @param msg
     *        接收的订阅消息
     */
    @JmsListener(destination = "springboot.topic.test",containerFactory="topicListenerContainerFactory")
    public void receiveTopic(String text) {
    	/* if(!StringUtils.isEmpty(text)){//存储redis中，便于查询
             RList<String> list = redisson.getList("redissonTopicList");
             list.add(text);
         	 jwtUtils.set((Constants.ActiveMqQueueData.SPRINGBOOT_TOPIC_TEST+text).getBytes(), 30*60*24*7, text);
         }*/
        logger.debug("接收到主题{springboot.topic.test}消息：{}",text);
    }


    /**
     * 接收到日志队列消息
     * @param msg
     *        接收的队列消息
     */
    @JmsListener(destination = "JMSLogQueue",containerFactory="queueListenerContainerFactory")
    public void receiveJmsLog(String msg) {
        logger.debug("接收到队列{JMSLogQueue}的消息：{}",msg);
    }



    /**
     * 接收积分微服务消息
     * 积分绑定唯一订单号
     * 业务的积分降级数据
     * 拿到之后需要判断表中积分状态
     * 送过就不处理,没有送就送积分
     */
    @JmsListener(destination = "mall.business.score",containerFactory="queueListenerContainerFactory")
    public void mallScoreFallBack(String msg) {
        logger.info("积分服务接收到业务系统积分降级的消息：{}",msg);
        HashMap param= JSON.parseObject(msg, HashMap.class);
        if(ObjectUtils.isNotEmpty(param)){
            Student one=studentService.query(Student.builder().sex("1").id(123456).build());
            if(ObjectUtils.isNotEmpty(one)){
                Student stu=new Student();
                stu.setId(123456);
                stu.setAge(MathUtil.bigDecimal(param.get("money")).intValue()/100);
                stu.setBirthday(new Date());
                stu.setSex("1");
                stu.setSname("积分降级服务");
                studentService.update(stu);
            }
        }
    }

}