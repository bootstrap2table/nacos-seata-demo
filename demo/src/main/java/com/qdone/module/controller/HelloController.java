package com.qdone.module.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.qdone.common.util.Result;
import com.qdone.common.util.ZxingQRCode;
import com.qdone.module.model.Solr;
import com.qdone.module.model.Student;
import com.qdone.module.model.User;
import com.qdone.module.service.SolrService;
import com.qdone.module.service.StudentService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Gensis on 2016/9/9.
 */
@ApiIgnore
@Controller
public class HelloController {

	@Autowired
	StudentService studentService;

    // 从 application.properties 中读取配置，如取不到默认值为Hello
    @Value("${application.hello:Hello}")
    private String hello;

    @Value("${qrcode.logo}")
    private String logo;
    
   /* @RequestMapping("/")
    public String welcome() {
    	System.err.println("你好啊");
        return "redirect:/swagger-ui.html";
    }*/
    @RequestMapping("/helloJsp")
    public String helloJsp(Map<String, Object> map) {
        System.out.println("HelloController.helloJsp().hello=" + hello);
        map.put("hello", hello);
        return "helloJsp";

    }
    
    @RequestMapping("/page1")
    public ModelAndView page1(){  
        // 页面位置 /WEB-INF/jsp/page/page.jsp  
        ModelAndView mav = new ModelAndView("page");  
        mav.addObject("content", "哈哈哈第一种");  
        return mav;  
    }  
    
    @RequestMapping("/pageTest")
    @ResponseBody
    /*@Cacheable(value="defaultCache",key="#root.targetClass.toString().substring(#root.targetClass.toString().lastIndexOf('.')+1).concat('.'+#root.method.name).concat('.'+#user.getName()+'.'+#user.getAge())")*/
//    @Cacheable(value="defaultCache",key="#user.getName()")
    @Cacheable(value="defaultCache",keyGenerator="keyGenerator")
    public String pageTest(User user){
    	System.err.println("进入pageTest方法，本次传入参数:name="+user.getName()+",age="+user.getAge());
    	return user.getName();
    }  
    
    
    @RequestMapping("/pageTest1")
    @ResponseBody
    /*@Cacheable(value="defaultCache",key="#root.targetClass.toString().substring(#root.targetClass.toString().lastIndexOf('.')+1).concat('.'+#root.method.name).concat('.'+#user.getName()+'.'+#user.getAge())")*/
    @Cacheable(value="view",key="#user.getName()")
    public String pageTest1(User user){
    	System.err.println("进入pageTest1方法，本次传入参数:name="+user.getName()+",age="+user.getAge());
    	return user.getName();
    }  
	

    @RequestMapping("/toQrcode")
	public String toQrcode(HttpServletRequest request,
			HttpServletResponse response) {
		String content="apple95272591234";
		request.getSession().setAttribute("qrdata", content);
		return "qr_code";
	}
	
	
	
	@RequestMapping("/toZxingQrcode")
	public String toZxingQrcode(HttpServletRequest request,
			HttpServletResponse response) {
		String content="apple95272591234";
		request.getSession().setAttribute("qrdata", content);
		return "zxing_qr_code";
	}
	
	@RequestMapping("/getZxingQrcode")
	public void getZxingQrcode(HttpServletRequest request,
			HttpServletResponse response) {
		System.err.println("执行生成二维码: StaffController.getZxingQrcode()");
		try {
			String content=(String) request.getSession().getAttribute("qrdata");
			//生成带图标的二维码
			BufferedImage img= ZxingQRCode.genBarcode(content, 38,logo);
			//生成二维码QRCode图片  
            ImageIO.write(img, "jpg", response.getOutputStream());
		} catch (Exception e) {
			System.err.println("执行生成二维码: StaffController.getZxingQrcode() 异常:"+e.getMessage());
		}
		System.err.println("执行生成二维码: StaffController.getZxingQrcode() 完毕");
	}
	
	 @RequestMapping("/testJson")
	 @ResponseBody
	 public Map<String,Object> testJson(){  
	    	System.err.println("进入testJson方法");
	    	/*Map<String,Object> result=new ConcurrentHashMap<String,Object>();*/
	    	Map<String,Object> result=new HashMap<String,Object>();
	    	result.put("a", null);
	    	result.put("b", "");
	    	return result;
	 }



	@RequestMapping("/httpLog")
	@ResponseBody
	public Result<Map<String,String>> httpLog(@RequestBody Map<String,String> param){
		System.err.println("进入httpLog方法");
		System.err.println(param);
		return Result.success(param);
	}
	/**
	 * 模拟全局事务提交
	 * 两个库同时添加数据，
	 * 有一个抛出异常是否会回滚
	 * solr账户price添加
	 * student账户age增加
	 */
	@RequestMapping("/seata")
	@ResponseBody
	public Result<Object> seata(@ApiParam(required = true, value = "操作编号", name = "pkid") @RequestParam(value = "pkid") Integer pkid){
		studentService.seata(pkid);
		return Result.success(null);
	}


	@PostMapping("/testRestTemplate")
	@ResponseBody
	public Map<String,Object> testRestTemplate(@RequestBody Solr entity,@RequestHeader String token){
		System.err.println("进入testRestTemplate方法");
		System.err.println("token参数"+token);
		System.err.println("body参数:"+ JSON.toJSONString(entity));
		Map<String,Object> result= Maps.newLinkedHashMap();
		result.put("a", null);
		result.put("b", "");
		return result;
	}

	@GetMapping("/testGetTemplate")
	@ResponseBody
	public Map<String,Object> testGetTemplate(@RequestParam String name,@RequestParam String age,@RequestHeader String token){
		System.err.println("进入testGetTemplate方法");
		System.err.println("token参数"+token);
		System.err.println("name参数:"+name+" age:"+age);
		Map<String,Object> result= Maps.newLinkedHashMap();
		result.put("a", null);
		result.put("b", "");
		return result;
	}

}