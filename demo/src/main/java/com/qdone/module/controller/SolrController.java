package com.qdone.module.controller;

import com.google.common.collect.Maps;
import com.qdone.common.util.Result;
import com.qdone.common.util.SerialNo;
import com.qdone.framework.annotation.Function;
import com.qdone.framework.core.BaseController;
import com.qdone.module.model.Solr;
import com.qdone.module.model.User;
import com.qdone.module.service.SolrService;
import com.qdone.support.async.log.db.annotation.ActionLog;
import io.seata.core.context.RootContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *solr管理
 * @付为地
 * @date 2018-05-15 03:16:12
 */
@Controller
@RequestMapping("/solr")
@Api(tags = "管理",description = "信息管理")
public class SolrController extends BaseController{
  

    @Autowired
	private SolrService solrService;
    
    
    /**
	 * 页面初始化
	 */
	@ApiOperation(value = "列表",notes = "进入列表页", httpMethod = "GET")
	@RequestMapping(value = "init",method = RequestMethod.GET)
	public String init(){
		return "solr/selectSolr";
	}
	
    /**
	 * 分页查询数据
	 */
	@RequestMapping(value="/selectPage",method=RequestMethod.POST)
	@ResponseBody
    //@Function(value = "分页",saveDb = false)
	@ApiOperation(value = "分页列表", notes = "分页列表", httpMethod = "POST",response = Map.class)
	public Map<String, Object> selectPage(@RequestBody Solr entity){
		return responseSelectPage(solrService.selectPage(entity));
	}
	
	/**
	 * 跳转添加
	*/
    @RequestMapping(value="/preInsert",method=RequestMethod.GET)
    @ApiOperation(value = "跳转添加", notes = "进入添加页面", httpMethod = "GET")
	public String preInsert(HttpServletRequest req){
		return "solr/insertSolr";
	} 
	
    /**
     * 添加数据
     */
	@RequestMapping(value="/insert",method=RequestMethod.PUT)
	@ResponseBody
	//@Function("添加")
	@ActionLog(moudle="solr管理",actionType = "添加solr数据")
	@ApiOperation(value = "添加", notes = "创建", httpMethod = "PUT",response = Boolean.class)
	public Boolean insert(@ApiParam(name = "对象", value = "传入json格式", required = true)  @RequestBody Solr entity) {
		entity.setId(SerialNo.getUNID());
		return solrService.insert(entity).getOperateResult()>0?Boolean.TRUE:Boolean.FALSE;
	}
	
	/**
	 * 跳转更新
	*/
	@RequestMapping(value="/preUpdate",method=RequestMethod.GET)
	@ApiOperation(value = "跳转更新", notes = "进入更新页面", httpMethod = "GET")
	public ModelAndView preUpdate(String id){
		Map<String,Object> param= Maps.newHashMap();
		param.put("solr",solrService.view(id));
		return new ModelAndView("solr/updateSolr",param);
	}

    /**
     * 更新数据
     */
	@RequestMapping(value="/update",method=RequestMethod.POST)
	@ResponseBody
	/*//@Function("更新")*/
	@ActionLog(moudle="solr管理",actionType = "更新solr数据")
	@ApiOperation(value = "更新", notes = "更新信息", httpMethod = "POST",response = Boolean.class)
	public Boolean update(@ApiParam(name = "对象", value = "传入json格式", required = true) Solr entity) {
		return solrService.update(entity).getOperateResult()>0?Boolean.TRUE:Boolean.FALSE;
	}
	
    /**
     * 删除数据
     */
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	@ResponseBody
	//@Function("删除")
	@ApiOperation(value = "删除",notes = "删除", httpMethod = "POST",response = Boolean.class)
	public Boolean delete(@RequestBody List<Solr> ids) {
		return solrService.batchDelete(ids)>0?Boolean.TRUE:Boolean.FALSE;
	}

	/**
	 * 测试swagger返回类型
	 */
	@ApiOperation(value = "测试swagger返回类型", notes = "测试swagger返回类型", httpMethod = "POST",response = User.class)
	@RequestMapping(value = "/getUser", method = RequestMethod.POST)
	@ResponseBody
	public Result<User> getUser(@RequestBody User user) {
		return Result.success(new User("张三","123456",1,"111111111111"));
	}



	/**
	 * seata模拟分布式事务
	 */
	@RequestMapping(value="/seata",method=RequestMethod.GET)
	@ResponseBody
	//@Function("seata分布式事务")
	@ApiOperation(value = "seata", notes = "seata", httpMethod = "POST",response = Boolean.class)
	public Boolean seata(@ApiParam(name = "对象", value = "传入json格式", required = true) String pkid) {
		System.err.println("solrService传入参数：pkid={"+pkid+"},xid={"+RootContext.getXID()+"}");
		Solr solr=new Solr();
		solr.setId(pkid);
		solr.setTitle("seata模拟分布式事务");
		solr.setPrice(99);
		solr.setName("seata分布式事务");
		solr.setCreatetime(new Date());
		return solrService.insert(solr).getOperateResult()>0?Boolean.TRUE:Boolean.FALSE;
	}
    
}
