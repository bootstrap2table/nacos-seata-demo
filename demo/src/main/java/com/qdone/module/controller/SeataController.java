package com.qdone.module.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.mcy.common.constant.Constant;
import com.qdone.module.model.Student;
import com.qdone.module.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.apm.toolkit.trace.Tag;
import org.apache.skywalking.apm.toolkit.trace.Tags;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;

/**
 * seata分布式事务
 */
@Controller
@RequestMapping("/boot")
@RefreshScope
@Slf4j
public class SeataController {


	@Autowired
	StudentService studentService;

	/*@NacosValue(value = "${service.name:1}", autoRefreshed = true)*/
	@Value("${service.name:123}")
	private String serverName;

	@Value("${useLocalCache:false}")
	private boolean useLocalCache;


	/**
	 * 获取nacos配置中心的配置
	 */
	@RequestMapping(value = "/nacosValue", method =RequestMethod.GET)
	@ResponseBody
	@SentinelResource(value = "nacosValue")
	public String nacosValue() {
		return serverName+"\t"+useLocalCache;
	}

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	@ResponseBody
	public String hello(HttpServletRequest request) throws InterruptedException {
		System.err.println("demo获取的token:"+request.getHeader(Constant.X_AMZ_SECURITY_TOKEN));
		return "hello";
	}

	/**
	 * 测试seata分布式事务
	 * @param userId
	 * @param money
	 * @return
	 */
	@GetMapping("/merge")
	@ResponseBody
	@Trace(operationName = "boot微服务")
	@Tags({@Tag(key="用户名",value ="arg[0]" ),
			@Tag(key="交易金额",value ="arg[1]" ),
			@Tag(key="全局链路",value ="arg[2]" ),
			@Tag(key="执行结果",value ="returnedObj" )
	})
	/*@Function("SEATA事务")
	@ActionLog(moudle="积分系统",actionType = "赠送积分")
	*/
	public String merge(@RequestParam("userId") String userId, @RequestParam("money") BigDecimal money) {
		/*public String merge(@RequestParam("userId") String userId, @RequestParam("money") BigDecimal money,@RequestHeader String GLOBAL_LOG_PRIFIX) {*/
		/*public String merge(@RequestParam("userId") String userId, @RequestParam("money") BigDecimal money,HttpServletRequest request) {*/
		//休眠35秒，超过ribbon配置30秒，服务会自动降级
//		String GLOBAL_LOG_PRIFIX=StringUtils.isNotEmpty(request.getHeader(Constant.GLOBAL_LOG_PRIFIX))?request.getHeader(Constant.GLOBAL_LOG_PRIFIX):request.getParameter(Constant.GLOBAL_LOG_PRIFIX);
/*		List arr= Collections.singletonList(request.getHeader("sw6"));
		for (int i = 0; i <arr.size() ; i++) {
			System.err.println(arr.get(i));
		}
		log.info("boot获取Trace:"+ JSON.toJSONString(request.getHeader("sw6")));*/
		/*log.info("boot获取id:"+request.getHeader("GLOBAL_LOG_PRIFIX"));*/
		/*Thread.sleep(35000);*/
		Student stu=new Student();
		stu.setId(123456);
		stu.setAge(money.intValue()/100);
		stu.setBirthday(new Date());
		stu.setSex("1");
		stu.setSname("seata");
		studentService.insert(stu);
		return "success";
	}
}