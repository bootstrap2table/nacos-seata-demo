package com.qdone.module.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.qdone.framework.core.page.MutiSort;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import lombok.*;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import java.math.BigDecimal;

/**
* 地域宏观信息
*
* @author fwd
* @email 1335157415@qq.com
* @date 2020-11-20 19:35:14
*/
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "地域宏观信息", description = "GdtForcastList实体类")
public class GdtForcastList extends MutiSort {

	private static final long serialVersionUID = 1L;

	/**
	* 自增ID
	*/
	@ApiModelProperty(value = "自增ID",required = true,position=0)
	private Integer id;
	/**
	* 地域层级
	*/
	@ApiModelProperty(value = "地域层级",required = false,position=1)
	private String regionalLevel;
	/**
	* 省编码
	*/
	@ApiModelProperty(value = "省编码",required = false,position=2)
	private String provCd;
	/**
	* 省名称
	*/
	@ApiModelProperty(value = "省名称",required = false,position=3)
	private String provName;
	/**
	* 市编码
	*/
	@ApiModelProperty(value = "市编码",required = false,position=4)
	private String cityCd;
	/**
	* 市名称
	*/
	@ApiModelProperty(value = "市名称",required = false,position=5)
	private String cityName;
	/**
	* 区编码
	*/
	@ApiModelProperty(value = "区编码",required = false,position=6)
	private String countyCd;
	/**
	* 区名称
	*/
	@ApiModelProperty(value = "区名称",required = false,position=7)
	private String countyName;
	/**
	* 总销售瓶数
	*/
	@ApiModelProperty(value = "总销售瓶数",required = false,position=8)
	private BigDecimal saleTotal;
	/**
	* 门店数
	*/
	@ApiModelProperty(value = "门店数",required = false,position=9)
	private BigDecimal marketTotal;
	/**
	* 单日单店产出
	*/
	@ApiModelProperty(value = "单日单店产出",required = false,position=10)
	private BigDecimal dailyStoreTotal =new BigDecimal(0.0000);
	/**
	* y1 true:达标,false:未达标
	*/
	@ApiModelProperty(value = "y1 true:达标,false:未达标",required = false,position=11)
	private Boolean y1 ;
	/**
	* y2 true:达标,false:未达标
	*/
	@ApiModelProperty(value = "y2 true:达标,false:未达标",required = false,position=12)
	private Boolean y2;
	/**
	* y3 true:达标,false:未达标
	*/
	@ApiModelProperty(value = "y3 true:达标,false:未达标",required = false,position=13)
	private Boolean y3;
	/**
	* 状态:1未填写 2:进行中 3:已完成
	*/
	@ApiModelProperty(value = "状态:1未填写 2:进行中 3:已完成",required = false,position=14)
	private String status ="0";
	/**
	* 操作人
	*/
	@ApiModelProperty(value = "操作人",required = false,position=15)
	private String operator;
	/**
	* 操作时间
	*/
	@ApiModelProperty(value = "操作时间",required = false,position=16)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy/MM/dd HH:mm:ss")
	private Date operatorTime;
	/**
	* 创建者
	*/
	@ApiModelProperty(value = "创建者",required = false,position=17)
	private String creator;
	/**
	* 创建时间
	*/
	@ApiModelProperty(value = "创建时间",required = false,position=18)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy/MM/dd HH:mm:ss")
	private Date createTime;
	/**
	* 最后修改者
	*/
	@ApiModelProperty(value = "最后修改者",required = false,position=19)
	private String lastModified;
	/**
	* 最后修改时间
	*/
	@ApiModelProperty(value = "最后修改时间",required = false,position=20)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy/MM/dd HH:mm:ss")
	private Date lastModifiedTime;
}
