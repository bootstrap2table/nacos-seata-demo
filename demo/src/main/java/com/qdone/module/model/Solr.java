package com.qdone.module.model;

import java.util.Date;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.ObjectUtils;

import com.qdone.framework.core.page.MutiSort;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 该代码由付为地的编码机器人自动生成 时间：2017-09-21 13:44:39
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Solr对象", description = "数据库实体")
public class Solr extends MutiSort  {

	private static final long serialVersionUID = 1L;

	// Fields
	@ApiModelProperty(value = "主键信息", required = true)
	private String id;
	@ApiModelProperty(value = "价格")
	private Integer price;// 价格
	@ApiModelProperty(value = "标题")
	private String title;// 标题
	@ApiModelProperty(value = "称呼")
	private String name;// 称呼
	@ApiModelProperty(value = "创建时间")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date createtime;// 创建时间


}