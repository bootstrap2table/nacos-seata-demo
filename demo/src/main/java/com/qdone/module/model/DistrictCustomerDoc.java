package com.qdone.module.model;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * 区人客
 * 活跃门店数响应参数
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "活跃门店数", description = "活跃门店数")
public class DistrictCustomerDoc implements Serializable {
    private static final long serialVersionUID = 3997124446365032382L;

    @ApiModelProperty(value = "活跃门店数[ASN1]", required = true)
    @JSONField(name="ASN1")
    private String asn1;
    @ApiModelProperty(value = "铺货率[ASN8]",name = "ASN8",notes = "ASN8")
    @JSONField(name="ASN8")
    private String asn8 ;
    @ApiModelProperty(value = "铺货率 vs 上月")
    private String ASN12;
    @ApiModelProperty(value = "market门店数量")
    private String ASN13;
    @ApiModelProperty(value = "活跃门店目标数")
    private String ASN21;
    @ApiModelProperty(value = "活跃门店达成率")
    private String ASN22;
    @ApiModelProperty(value = "月新增门店数")
    private String ASN23;
    @ApiModelProperty(value = "月新增门店数目标")
    private String ASN24;
    @ApiModelProperty(value = "月新增门店数达成率")
    private String ASN25;
    @ApiModelProperty(value = "渠道编码")
    private String CHNL_CD;
    @ApiModelProperty(value = "渠道")
    private String PRA_CHANNEL;
    @ApiModelProperty(value = "统计时间")
    private String CAL_YEAR_MONTH;

}
