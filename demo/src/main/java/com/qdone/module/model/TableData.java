package com.qdone.module.model;

import com.qdone.framework.core.page.MutiSort;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "代码生成表对象", description = "代码生成表实体")
public class TableData extends MutiSort {

    @ApiModelProperty(value = "表名")
    String tableName;
    @ApiModelProperty(value = "数据库引擎")
    String engine;
    @ApiModelProperty(value = "表备注")
    String tableComment;
    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    Date createTime;
}
