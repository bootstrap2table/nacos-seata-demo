package com.qdone.module.model;

import lombok.*;
import java.util.Date;
import com.qdone.framework.core.page.MutiSort;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
  *该代码由付为地的编码机器人自动生成
  *时间：2018-07-12 17:14:32
*/
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "", description = "Student实体类") 
public class Student  extends MutiSort {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id", required = true) 
    private Integer id;
    @ApiModelProperty(value = "姓名", required = true) 
    private String sname;//姓名
    @ApiModelProperty(value = "性别", required = false) 
    private String sex;//性别
    @ApiModelProperty(value = "年龄", required = false) 
    private Integer age;//年龄
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "生日", required = false) 
    private Date birthday;//生日

}