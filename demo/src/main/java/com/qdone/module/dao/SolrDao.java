package com.qdone.module.dao;

import com.qdone.module.model.Solr;

import java.util.List;

public interface SolrDao {

	/**
	 * 分页查询
	 * @param entity
	 * @return
	 */
	public List<Solr> select(Solr entity);
	
	/**
	 * 查看
	 * @param pk
	 * @return
	 */
	public Solr view(String pk) ;
	
	/**
	 * 查询单个
	 * @param object
	 * @return
	 */
	public Solr query(Solr object) ;
	
	/**
	 * 新增
	 * @param object
	 * @return
	 */
	public int insert(Solr object) ;
	
	/**
	 * 修改
	 * @param object
	 * @return
	 */
	public int update(Solr object)  ;
	
	
	/**
	 * 删除
	 * @param pk
	 * @return
	 */
	public int delete(String pk);
	
	/**
	 * 批量插入
	 * @param arr
	 */
	public int batchInsert(List<Solr> arr);
	
	/**
	 * 批量修改
	 * @param arr
	 */
	public int batchUpdate(List<Solr> arr);
	
	/**
	 * 批量删除
	 */
	public int batchDelete(List<Solr> pkList);
}
