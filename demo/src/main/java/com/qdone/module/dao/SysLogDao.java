package com.qdone.module.dao;

import com.qdone.module.model.SysLog;

import java.util.List;
public interface SysLogDao {
	
	/**
	 * 查询集合
	 * @param entity
	 * @return
	 */
	public List<SysLog> select(SysLog entity);
	
	/**
	 * 新增
	 * @param object
	 * @return
	 */
	public int insert(SysLog object) ;
	
	/**
	 * 查看
	 * @param pk
	 * @return
	 */
	public SysLog view(Integer pk) ;
	
	/**
	 * 查询单个
	 * @param object
	 * @return
	 */
	public SysLog query(SysLog object) ;
	
	/**
	 * 删除
	 * @param pk
	 * @return
	 */
	public int delete(Integer pk);

}
