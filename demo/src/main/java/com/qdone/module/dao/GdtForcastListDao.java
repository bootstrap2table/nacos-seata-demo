package com.qdone.module.dao;
import com.qdone.module.model.GdtForcastList;

import java.util.List;

/**
* gdtForcastList服务接口
* 地域宏观信息
* @author fwd
* @email 1335157415@qq.com
* @date 2020-11-20 19:35:14
*/
public interface GdtForcastListDao {

    /**
    * 分页查询
    * @param entity 查询参数
    * @return 分页查询结果
    */
    public List<GdtForcastList> select(GdtForcastList entity);

    /**
    * 查看
    * @param pk 主键
    * @return 根据主键查询结果
    */
    public GdtForcastList view(Integer pk);

    /**
    * 新增(主键使用mybatis生成,获取使用数据库自增)
    * @param entity 新增参数
    * @return 新增结果,结果中含有主键，操作结果
    */
    public int insert(GdtForcastList entity);


    /**
    * 更新
    * @param entity 更新参数
    * @return 更新结果,结果中含有操作结果
    */
    public int update(GdtForcastList entity);


    /**
    * 批量添加或修改
    * @param arr 数据
    * @return 批量添加或修改
    */
    public int batchMerge(List<GdtForcastList> arr);


    /**
    * 删除
    * @param pkList 主键集合
    * @return 根据主键集合删除数据
    */
    public int batchDelete(List<GdtForcastList> pkList);

}
