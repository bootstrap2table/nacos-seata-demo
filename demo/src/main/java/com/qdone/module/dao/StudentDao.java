package com.qdone.module.dao;

import com.qdone.module.model.Student;

import java.util.List;

/**
 * TODO 本代码由代码生成工具生成
 *
 * @author 付为地
 * @date 2018-07-12 05:14:32
 */
public interface StudentDao{

	/**
	 * 分页查询
	 * @return
	 */
	public List<Student> select(Student object);
	
	/**
	 * 查看
	 * @param pk
	 * @return
	 */
	public Student view(Integer pk) ;
	
	
	/**
	 * 查询方法
	 * @param object
	 * @return
	 */
	public Student query(Student object) ;
	
	/**
	 * 新增
	 * @param object
	 * @return
	 */
	public int insert(Student object) ;
	
	/**
	 * 修改
	 * @param object
	 * @return
	 */
	public int update(Student object) ;
	
	
	/**
	 * 删除
	 * @param pk
	 * @return
	 */
	public int delete(Integer pk);
	
	
	/**
	 * 批量插入
	 * @param object
	 */
	public int batchInsert(List<Student> object);

	/**
	 * 批量修改
	 * @param object
	 */
	public int batchUpdate(List<Student> object);
	
	/**
	 * 批量删除
	 */
	public int batchDelete(List<Student> pkList);
}
