package com.qdone.module.service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qdone.framework.core.page.CoreUtil;
import com.qdone.framework.core.page.PageInfo;
import com.qdone.framework.core.page.PageList;
import com.qdone.module.dao.StudentDao;
import com.qdone.module.model.Student;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;

/**
 * TODO 本代码由代码生成工具生成
 *
 * @author 付为地
 * @date 2018-07-12 05:14:32
 */

@Service("studentService")
@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT)
public class StudentService{

    Logger log=LoggerFactory.getLogger(StudentService.class);
    
	
	@Autowired
    private StudentDao studentDao;

	@Autowired
	SolrService  solrService;

	@Autowired
	RestTemplate restTemplate;
	
	
    /**
	 * 分页查询
	 * @param object 查询参数
	 * @return 分页查询结果
	 */
	
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public PageList<Student> selectPage(Student object) {
	    PageList<Student> pageList = new PageList<Student>();
		PageInfo pageInfo = CoreUtil.createBootStrapPage(object, true);
		String orders = CoreUtil.createSort(pageInfo, true);
		/* PageHelper.offsetPage(pageInfo.getCurrentNumber(), pageInfo.getPageSize());
        PageHelper.orderBy(orders);// 插件排序*/
		PageHelper.startPage(pageInfo.getCurrentNumber(), pageInfo.getPageSize(),orders);
		List<Student> list =  studentDao.select(object);
		// 设置总记录数
		pageList.setList(list);
		Page<Student> pg = (Page<Student>) list;
		pageInfo.setRecordCount(pg.getTotal());
		pageList.setPageInfo(pageInfo);
	    return pageList;
	}
	

	/**
	 * 新增
	 * @param object 新增参数
	 * @return 新增结果
	 */
	
	@Transactional(readOnly = false,rollbackFor = Exception.class)
	public Student insert(Student object) {
		object.setOperateResult(studentDao.insert(object));
		return object;
	}

    /**
	 * 更新
	 * @param object 更新参数
	 * @return 更新结果
	 */
	
	@Transactional(readOnly = false,rollbackFor = Exception.class)
	public Student update(Student object){
		object.setOperateResult(studentDao.update(object));
		return object;
	}

	/**
	 * 查看
	 * @param pk 主键
	 * @return 根据主键查询结果
	 */
	
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public Student view(Integer pk) {
		log.info("执行StudentServiceImpl.view参数： pk=>" + pk);
        return studentDao.view(pk);   
	} 
	
    /**
	 * 查询单个
	 * @param object 对象参数
	 * @return  根据传入参数查询单个对象
	 */
	 @Transactional(propagation = Propagation.NOT_SUPPORTED)
	public Student query(Student object){
        return studentDao.query(object);
	}
	 
    /**
	 * 查询集合
	 * @param object 对象参数
	 * @return 根据传入参数查询对象集合
	 */
	
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public List<Student> selectList(Student object) {
        return studentDao.select(object);
	}
	
   /**
	 * 删除
	 * @param pk 主键
	 * @return 根据主键删除数据
	 */
	
	@Transactional(readOnly = false,rollbackFor = Exception.class)
	public int delete(Integer pk) {
		log.info("执行StudentServiceImpl.delete参数： pk=>" + pk);
		return studentDao.delete(pk); 
	}
	
	/**
	 * 批量新增
	 * @param arr 对象集合 
	 * @return 批量新增集合参数
	 */
	
	@Transactional(readOnly = false,rollbackFor = Exception.class)
	public int batchInsert(List<Student> object) {
         return studentDao.batchInsert(object);
	}
	
	/**
	 * 批量修改
	 * @param object 对象集合
	 * @return 批量更新集合参数
	 */
	
	@Transactional(readOnly = false,rollbackFor = Exception.class)
	public int batchUpdate(List<Student> object) {
		/*log.info("执行StudentServiceImpl.batchUpdate参数： List=>" + JSON.toJSONStringWithDateFormat(object, "yyyy-MM-dd HH:mm:ss"));*/
        return studentDao.batchUpdate(object); 
	}

	/**
	 * 模拟分布式事务，两个表同时提交数据
	 * @param pkid
	 * @return
	 */
	@Transactional(readOnly = false,rollbackFor = Exception.class)
	public int seata(int pkid) {
		System.err.println("studentService传入参数：pkid={"+pkid+"},xid={"+ RootContext.getXID()+"}");
		Student stu=new Student();
		stu.setId(pkid);
		stu.setBirthday(new Date());
		stu.setAge(99);
		stu.setSname("seata操作student");
		insert(stu);
		/*String url = "http://127.0.0.1:9999/solr/seata?pkid=" + pkid;
		try {
			restTemplate.getForEntity(url, Void.class);
		} catch (Exception e) {
			log.error("seata url {} ,error:", url);
			throw new RuntimeException();
		}*/
		return 0;
	}

    /**
	 * 批量删除
	 * @param pkList 对象集合
	 * @return 批量删除集合参数
	 */

	@Transactional(readOnly = false,rollbackFor = Exception.class)
	public int batchDelete(List<Student> pkList) {
        return studentDao.batchDelete(pkList);
	}

	
}
