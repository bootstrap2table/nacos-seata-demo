package com.qdone.module.service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qdone.framework.core.page.CoreUtil;
import com.qdone.framework.core.page.PageInfo;
import com.qdone.framework.core.page.PageList;
import com.qdone.module.dao.SysLogDao;
import com.qdone.module.model.SysLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * @author 付为地
 * @date 2018-05-15 03:16:12
 */

@Service("SysLogService")
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
public class SysLogService {

    @Autowired
    private SysLogDao sysLogDao;

    /**
     * 分页查询
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public PageList<SysLog> selectPage(SysLog object) {
        PageList<SysLog> pageList = new PageList<SysLog>();
        PageInfo pageInfo = CoreUtil.createBootStrapPage(object, true);
        String orders = CoreUtil.createSort(pageInfo, true);
        /* PageHelper.offsetPage(pageInfo.getCurrentNumber(), pageInfo.getPageSize());
        PageHelper.orderBy(orders);// 插件排序*/
        PageHelper.startPage(pageInfo.getCurrentNumber(), pageInfo.getPageSize(),orders);
        List<SysLog> list = sysLogDao.select(object);
        // 设置总记录数
        pageList.setList(list);
        Page<SysLog> pg = (Page<SysLog>) list;
        pageInfo.setRecordCount(pg.getTotal());
        pageList.setPageInfo(pageInfo);
        return pageList;
    }

    /**
     * 新增
     */
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public int insert(SysLog object) {
        return sysLogDao.insert(object);
    }


    /**
     * 浏览单个
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public SysLog view(Integer pk) {
        return sysLogDao.view(pk);
    }

    /**
     * 查询单个
     *
     * @param object
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public SysLog query(SysLog object) {
        return sysLogDao.query(object);
    }

    /**
     * 查询集合
     *
     * @param object
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public List<SysLog> selectList(SysLog object) {
        return sysLogDao.select(object);
    }

    /**
     * 删除
     *
     * @param pk
     * @return
     */
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public int delete(Integer pk) {
        return sysLogDao.delete(pk);
    }


}
