package com.qdone.module.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qdone.framework.core.page.CoreUtil;
import com.qdone.framework.core.page.PageInfo;
import com.qdone.framework.core.page.PageList;
import com.qdone.module.dao.SolrDao;
import com.qdone.module.model.Solr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * TODO 本代码由代码生成工具生成
 *
 * @author 付为地
 * @date 2018-05-15 03:16:12
 */

@Service("solrService")
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
public class SolrService {


    @Autowired
    private SolrDao solrDao;

    /**
     * 分页查询
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public PageList<Solr> selectPage(Solr object) {
        PageList<Solr> pageList = new PageList<Solr>();
        PageInfo pageInfo = CoreUtil.createBootStrapPage(object, true);
        String orders = CoreUtil.createSort(pageInfo, true);
        /* PageHelper.offsetPage(pageInfo.getCurrentNumber(), pageInfo.getPageSize());
        PageHelper.orderBy(orders);// 插件排序*/
        PageHelper.startPage(pageInfo.getCurrentNumber(), pageInfo.getPageSize(),orders);
        List<Solr> list = solrDao.select(object);
        // 设置总记录数
        pageList.setList(list);
        Page<Solr> pg = (Page<Solr>) list;
        pageInfo.setRecordCount(pg.getTotal());
        pageList.setPageInfo(pageInfo);
        return pageList;
    }

    /**
     * 新增
     */
    /*@Transactional(readOnly = false, rollbackFor = Exception.class)*/
    public Solr insert(Solr object) {
        object.setOperateResult(solrDao.insert(object));
        return object;
    }

    /**
     * 修改
     */
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Solr update(Solr object) {
        object.setOperateResult(solrDao.update(object));
        return object;
    }

    /**
     * 浏览单个
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public Solr view(String pk) {
        return solrDao.view(pk);
    }

    /**
     * 查询单个
     *
     * @param object
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public Solr query(Solr object) {
        return solrDao.query(object);
    }

    /**
     * 查询集合
     *
     * @param object
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public List<Solr> selectList(Solr object) {
        return solrDao.select(object);
    }

    /**
     * 删除
     *
     * @param pk
     * @return
     */
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public int delete(String pk) {
        return solrDao.delete(pk);
    }

    /**
     * 批量修改
     *
     * @param object
     */
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public int batchUpdate(List<Solr> object) {
        return solrDao.batchUpdate(object);
    }

    /**
     * 批量插入
     *
     * @param object
     */
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public int batchInsert(List<Solr> object) {
        return solrDao.batchInsert(object);
    }

    /**
     * 批量删除
     */
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public int batchDelete(List<Solr> pkList) {
        return solrDao.batchDelete(pkList);
    }

}
