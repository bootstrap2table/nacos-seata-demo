﻿# boot-mapper

#### 项目介绍
boot-mapper基于SpringBoot2.0.2版本，使用mybatis集成redis二级缓存。<br>

#### 极速入门
[入门配置](https://gitee.com/bootstrap2table/boot_master/wikis/welcome)   &nbsp;[分布式事务](https://gitee.com/bootstrap2table/boot_master/tree/feature/jta/druid)  &nbsp;[SpringCloud](https://gitee.com/bootstrap2table/spring-cloud)   &nbsp;[Dubbo](https://gitee.com/bootstrap2table/api-master)


#### 技术选型
    ● 系统核心框架：SpringBoot
    ● 定时任务调度：ElasticJob+Zookeeper
    ● 数据持久框架：MyBatis
    ● 数据库连接池：Alibaba Druid
    ● 系统监控插件：JavaMelody+Druid
    ● 系统缓存框架：Redis-cluster
    ● 系统前端框架：Freemaker+AdminLte
    ● 搜索引擎框架：Solr/SolrCloud
    ● 分布式线程锁：Redisson
    ● 分布式限流器：Redisson
    ● 系统消息队列：ActiveMq 
 
#### **项目特点**   

> * 配套Mybatis二级缓存，默认缓存5分钟（支持手动配置），可以提高系统性能。<br>
> * 配套[代码生成工具](https://gitee.com/bootstrap2table/AutoCode/tree/feature/mapper/):快速生成前后端代码，极大的提高开发效率。<br>
> * 引入[HibernateValidator](https://gitee.com/bootstrap2table/boot_master/blob/master/src/main/java/com/qdone/module/controller/TestController.java)校验框架，轻松实现后端校验。<br>
> * 引入druid,javaMelody监控系统各项指标，分析系统瓶颈。<br>
> * 前端采用freemarker模板化引擎,页面采用bootstrap-table灵活强大的表格插件。<br>
> * 前端使用layui弹出层框架，极大的简化了弹出层的开发过程。
> * 前端采用JqueryValidate插件,快捷方便进行数据验证。<br>
> * 后端配置swagger在线文档，极大的降低前后端项目成员的沟通成本，快速同步文档。 <br>
> * 配置druid,fastjson,cors,xss,redis-cluster等组件服务。<br>
> * 配置全局异常处理，通用日志打印,pagehelper分页。<br>
> * 配置redisson集群模式,使用分布式锁，保证并发的数据一致性。<br>
> * 配置全局errorPage和welcomeFile完善全局异常处理，优化异常处理代码。<br>
> * 配置devtools热部署，针对page目录下的css,js,html页面资源修改之后，项目不需要重新启动。<br>
> * 配置elastic-job定时器，强悍的分布式定时任务配置。<br>
> * 配置https安全协议，提高系统安全性,配置log4j日志，系统出现异常自动发送邮件。<br>

#### **项目结构**
```
boot-mapper
│ 
├─doc  项目SQL语句
│ 
├─common 公共配置
│ 
├─framework 框架配置
│ 
├─modules 功能模块
│  ├─controller 控制器层
│  ├─service 业务逻辑层
│  ├─dao    数据持久层
│  │  ├─mapper  SQL文件
│  │  └─persist 持久层接口
│  └─model 数据库实体类
│ 
├─StartUpApplication 项目启动类
│  
├──resources
│  ├─page 页面资源
│  │  ├─static 静态资源
│  │  │  ├─css  css样式
│  │  │  ├─js   js文件 
│  │  │  ├─images   图片文件 
│  │  │  ├─adminLTE 模板组件  
│  │  │  └─plugins  前端插件
│  │  │
│  │  └─view  前端页面
│  │     ├─error 系统错误页
│  │     ├─inc   公共资源页面
│  │     └─其他   系统功能页面
│  │
│  ├─application.properties 配置文件
│  ├─banner.txt  自定义启动图标
│  ├─mybatis_config.xml mybatis配置项
│  └─secure.jks  ssl安全证书
```
#### **软件环境** 
- JDK1.8
- MySQL5.5+
- Maven3.0+
 
#### **环境配置:**<br>
- 1.项目依赖redis-cluster集群,zookeeper,activeMq,solr工具,目前工具运行环境(win7 x64)。<br>
- 2.doc目录里面有初始化sql，运行项目前，请先创建mysql(编码UTF-8)。<br>
- 3.工具地址:https://pan.baidu.com/s/1Bm7udGJc40xEENFgnJjsIw
- 5.配置文档：https://gitee.com/bootstrap2table/boot_master/wikis/welcome
	 
#### **启动说明:**
- 1.创建mysql数据库isec实例,运行doc目录里面的sql文件。<br>
- 2.启动redis集群(127.0.0.1:6379~6384，密码:qdone)。<br>
- 3.启动activeMq(默认单机版)。<br>
- 4.启动solr(默认单机版)。<br>
- 5.启动zookeeper(默认单机版本2181)。<br>
- 6.运行StartUpApplication启动项目，浏览器访问https://localhost:9090/solr/init<br>
	
#### **友情链接：**
- GitHub：https://github.com/apple987/boot_walk <br>
- mycat版本: https://gitee.com/bootstrap2table/boot_master/tree/feature/mycat<br>
- sharding-jdbc版本: https://gitee.com/bootstrap2table/boot-sharding<br>
- jtaDruid版本: https://gitee.com/bootstrap2table/boot_master/tree/feature/jta/druid<br>
- 多数据源：https://github.com/ityouknow/spring-boot-examples/tree/master/spring-boot-mybatis/spring-boot-mybatis-xml-mulidatasource



#### **问题反馈：**
- 意见反馈：https://gitee.com/bootstrap2table/boot-mapper/issues
- 作者姓名：付为地<br>
- 作者QQ: 1335157415<br>
- 作者邮箱: m15171479289@163.com<br>

		
        