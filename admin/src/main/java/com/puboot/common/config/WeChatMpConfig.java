package com.puboot.common.config;
import java.util.HashMap;
import java.util.Map;
import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.puboot.enums.WechatMpIdEnum;

import static me.chanjar.weixin.common.api.WxConsts.EventType.SUBSCRIBE;
import static me.chanjar.weixin.common.api.WxConsts.EventType.UNSUBSCRIBE;


import lombok.extern.slf4j.Slf4j;

import static me.chanjar.weixin.common.api.WxConsts.XmlMsgType.EVENT;
@Configuration
@Slf4j
public class WeChatMpConfig 
{
//	@Autowired
//	private  UnsubscribeHandler unsubscribeHandler;
//    @Autowired
//	private  SubscribeHandler subscribeHandler;
//    
//    @Autowired
//	private  MsgHandler msgHandler;
    @Autowired
    private WechatAccountConfig wechatAccountConfig;
    
    @Bean
    public WxMpService wxMpService(){
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setMultiConfigStorages(configStorages(), "1");
        return wxMpService;
    }

    @Bean
    public Map<String, WxMpConfigStorage> configStorages(){

        Map<String, WxMpConfigStorage> configStorages = new HashMap<>();

        WxMpInMemoryConfigStorage configStorage_1 = new WxMpInMemoryConfigStorage();
        configStorage_1.setAppId(wechatAccountConfig.getAppid());
        configStorage_1.setSecret(wechatAccountConfig.getAppsecret());
        configStorages.put(WechatMpIdEnum.SERVICE_ACCOUNT.getMpId(), configStorage_1);
        return configStorages;
    }
    
//    @Bean
//    public WxMpMessageRouter messageRouter(WxMpService wxMpService) 
//    {
//        final WxMpMessageRouter newRouter = new WxMpMessageRouter(wxMpService);
//        // 关注事件
//        newRouter.rule().async(false).msgType(EVENT).event(SUBSCRIBE).handler(this.subscribeHandler).end();
//        // 取消关注事件
//        newRouter.rule().async(false).msgType(EVENT).event(UNSUBSCRIBE).handler(this.unsubscribeHandler).end();
//       
//        // 默认
//        newRouter.rule().async(false).handler(this.msgHandler).end();
//        return newRouter;
//    }
 

    
}
