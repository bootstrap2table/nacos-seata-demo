package com.puboot.common.intercepter;

import com.puboot.common.annotation.Login;
import com.puboot.common.util.AESUtil;
import com.puboot.common.util.CoreConst;
import com.puboot.exception.CommonException;
import com.puboot.module.admin.model.ClientUser;
import com.puboot.module.admin.service.UserService;
import com.puboot.module.admin.vo.ClientUserVo;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *门户网站
 * 权限拦截器
 */

@Component
public class PortalInterceptor implements HandlerInterceptor {
	
	@Autowired
	private UserService userService;

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	/**
	 * 门户网站权限验证
	 */
	@Override
	@SneakyThrows
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
    	request.setCharacterEncoding("UTF-8");
    	response.setCharacterEncoding("UTF-8");
    	boolean isCheck=true;//是否限流检测
    	Login annotation;
        if(handler instanceof HandlerMethod) {
            annotation = ((HandlerMethod) handler).getMethodAnnotation(Login.class);
        }else{
            return true;
        }
        if(ObjectUtils.isEmpty(annotation)){
            return true;
        }
        //判断是否支持限流
        isCheck=annotation.isCheck();
        //获取用户凭证
        String token = request.getHeader(CoreConst.PORTAL_HEADER_TOKEN);
        if(StringUtils.isBlank(token)){
            token = request.getParameter(CoreConst.PORTAL_HEADER_TOKEN);
        }
        //凭证为空
        if(StringUtils.isBlank(token)){
            throw new CommonException("令牌不能为空", HttpStatus.UNAUTHORIZED.value());
        }
        //验证token时效,正确性
		ClientUserVo redisUser= (ClientUserVo) redisTemplate.opsForValue().get(token);
        if(ObjectUtils.isEmpty(redisUser)){
		   throw new CommonException("令牌已经过期", HttpStatus.UNAUTHORIZED.value());
	    }
		String[] arr=AESUtil.decrypt(token,null).split(",");
		ClientUser user=new ClientUser();
		user.setUserId(arr[0]);
		user.setPassword(arr[1]);
		user.setStatus(CoreConst.STATUS_VALID);//有效
		ClientUser dbUser=userService.selectByClientUserId(user);
		if(ObjectUtils.isEmpty(dbUser)){
			throw new CommonException("令牌信息有误", HttpStatus.UNAUTHORIZED.value());
		}
		if(!ObjectUtils.isEmpty(redisUser)&&!ObjectUtils.isEmpty(dbUser)
				&&!ObjectUtils.isEmpty(redisUser.getUserId())&&!ObjectUtils.isEmpty(redisUser.getPassword())
				&&!ObjectUtils.isEmpty(dbUser.getUserId())&&!ObjectUtils.isEmpty(dbUser.getPassword())){
			if(redisUser.getUserId().equals(dbUser.getUserId())&&redisUser.getPassword().equals(dbUser.getPassword())){
				return true;
			}else{
				throw new CommonException("令牌信息有误", HttpStatus.UNAUTHORIZED.value());
			}
		}
        return true;
    }
    
	@Override
	@SneakyThrows
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,@Nullable ModelAndView modelAndView)  {
	}
}
