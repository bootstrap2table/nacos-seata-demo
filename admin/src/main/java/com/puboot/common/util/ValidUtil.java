package com.puboot.common.util;

import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.text.Collator;
import java.util.*;

/**
 * 表单验证结果
 */
public class ValidUtil {

    /**
     * 表单验证结果
     * @param userResult:表单验证参数
     * @return 表单验证结果
     */
    public static List<Map<String, String>> validErrorResult(BindingResult userResult) {
        List<Map<String, String>> errorList=new ArrayList<Map<String, String>>();
        if (userResult.hasErrors()) {
            List<FieldError> errors = userResult.getFieldErrors();
            for (FieldError error : errors) {
                //响应验证结果
                if(StringUtils.isNotEmpty(error.getField())){
                    Map<String, String> map  = Maps.newHashMap();
                    map.put("field", error.getField());
                    map.put("message", error.getDefaultMessage());
                    errorList.add(map);
                }
            }
            //针对field字段不为空，才做排序，空值需要单独处理
            if(CollectionUtils.isNotEmpty(errorList)){
                Collections.sort(errorList, new Comparator<Map>() {
                    @Override
                    public int compare(Map o1, Map o2) {
                        //获取英文环境
                        Comparator<Object> com = Collator.getInstance(Locale.ENGLISH);
                        return com.compare(o1.get("field"),o2.get("field"));
                    }
                });
            }
        }
        return errorList;
    }
}
