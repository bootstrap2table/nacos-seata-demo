package com.puboot.common.util;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页对象
 *
 * @author Linzhaoguan
 * @version V1.0
 * @date 2019年9月11日
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class Pagination<T> extends Page<T> {

    private static final long serialVersionUID = 5194933845448697148L;

    public Pagination(long current, long size) {
        super(current, size);
    }

    //总页数
    private long pages;

    //前一页
    private long prePage;
    //下一页
    private long nextPage;

    //是否为第一页
    private boolean isFirstPage;
    //是否为最后一页
    private boolean isLastPage;
    //是否有前一页
    private boolean hasPreviousPage;
    //是否有下一页
    private boolean hasNextPage;

    @Override
    public Pagination<T> setTotal(long total) {
        super.setTotal(total);
        pages = super.getPages();
        long current = getCurrent();
        isFirstPage = current == 1;
        isLastPage = current == pages || pages == 0;
        hasPreviousPage = current > 1;
        hasNextPage = current < pages;
        if (current > 1) {
            prePage = current - 1;
        }
        if (current < pages) {
            nextPage = current + 1;
        }
        return this;
    }

    /**
     * 驼峰转换成列类型(只转换_标注驼峰，其他.区分的不操作)
     * eg:aBbCc转换成a_bb_c
     */
    public static final String convertBeanToCloumn(String entityName){
        StringBuffer buffer = new StringBuffer(2048);
        List<Object> arr=new ArrayList<Object>(100);
        //先将结果暂时存入list中
        for(int i=0;i<entityName.length();i++){
            if(i>0&&i<entityName.length()-1){
                if(Character.isUpperCase(entityName.charAt(i))){
                    arr.add("_");
                    arr.add(entityName.charAt(i));
                }else{
                    arr.add(entityName.charAt(i));
                }
            }else{
                arr.add(entityName.charAt(i));
            }
        }
        //将list转换成
        for (int i = 0; i < arr.size(); i++) {
            buffer.append(arr.get(i));
        }
        return buffer.toString();
    }

    /**
     * 列转换成驼峰
     *  eg:a_bb_c转换成aBbCc
     */
    public static final String convertColumnToBean(String column) {
        StringBuilder result = new StringBuilder();
        // 快速检查
        if (column == null || column.isEmpty()) {
            // 没必要转换
            return "";
        } else if (!column.contains("_")) {
            // 不含下划线，仅将首字母小写
            return column.substring(0, 1).toLowerCase() + column.substring(1);
        } else {
            // 用下划线将原始字符串分割
            String[] columns = column.split("_");
            for (String columnSplit : columns) {
                // 跳过原始字符串中开头、结尾的下换线或双重下划线
                if (columnSplit.isEmpty()) {
                    continue;
                }
                // 处理真正的驼峰片段
                if (result.length() == 0) {
                    // 第一个驼峰片段，全部字母都小写
                    result.append(columnSplit.toLowerCase());
                } else {
                    // 其他的驼峰片段，首字母大写
                    result.append(columnSplit.substring(0, 1).toUpperCase()).append(columnSplit.substring(1).toLowerCase());
                }
            }
            return result.toString();
        }

    }
}