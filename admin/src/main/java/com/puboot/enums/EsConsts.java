package com.puboot.enums;

public interface EsConsts {
	 /**
     * 存储时使用的分词器
     */
    String STORE_ANALYZER="ik_smart";
    /**
     * 搜索时使用的分词器
     */
    String SEARCH_ANALYZER="ik_smart";

    String SEARCH_MAX_ANALYZER="ik_max_word";
    
    /**
     * 搜索关键字最大长度
     */
    Integer  KEY_WORLD_MAX_LENGTH  = 20;
    
    
    //高亮拼接的前缀
    String PRETAGES = "<font color=\"#FD5900\">";
    //高亮拼接的后缀
    String POSTTAGS ="</font>";
}
