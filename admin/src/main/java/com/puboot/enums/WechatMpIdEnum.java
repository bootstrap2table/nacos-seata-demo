package com.puboot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 微信开放平台配置区分appid
 * 日后也可用于多个服务号或者小程序
 */
@Getter
@AllArgsConstructor
public enum WechatMpIdEnum {

    SERVICE_ACCOUNT("1", "服务号"),

    ;

    private String mpId;

    private String appName;

    public static WechatMpIdEnum getById(String mpId) {

        for (WechatMpIdEnum o : WechatMpIdEnum.values()) {
            if (o.getMpId().equals(mpId)) {
                return o;
            }
        }
        return null;
    }

    public boolean equals(String mpId) {

        return this.mpId.equals(mpId);
    }

}
