package com.puboot.module.admin.vo.base;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author 付为地
 *   通用针对bootstrap-table分页基类
 *   项目中数据库表对应实体类，只需要集成此类即可
 */
@Data
public class MutiSort implements Serializable {

	private static final long serialVersionUID = 1L;
	/* queryParamsType采用limit方式,分页单列排序参数 */
	@TableField(exist = false)
	private String sort;// 单列排序名称
	@TableField(exist = false)
	private String order;// 单列排序命令
	@TableField(exist = false)
	private int offset;// 偏移量
	@TableField(exist = false)
	private int limit;// 每页显示多少条
	/* queryParamsType采用''方式,分页单列排序参数 */
	@TableField(exist = false)
	private String sortName;// 单列排序名称
	@TableField(exist = false)
	private String sortOrder;// 单列排序命令
	@TableField(exist = false)
	private int pageSize;// 每页多少条
	@TableField(exist = false)
	private int pageNumber;// 当前第几页

}
