package com.puboot.module.admin.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.puboot.module.admin.vo.base.MutiSort;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
* 用户登录历史
*
* @author 付为地
* @email 1335157415@qq.com
* @date 2021-03-17 17:02:04
*/
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("login_history")
public class LoginHistory extends MutiSort {


	@TableId(value = "id",type = IdType.AUTO)
	private Integer id;
	/**
	* 用户id
	*/
	private String userId;
	/**
	* 用户名
	*/
	private String username;
	/**
	* 状态：1有效; 2删除
	*/
	private Integer status;
	/**
	* 类型：1后台; 2 前台
	*/
	private Integer type;

	/**
	 * 登录ip
	 */
	private String loginIpAddress;
	/**
	* 创建时间
	*/
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	/**
	* 更新时间
	*/
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	private Date updateTime;
	/**
	* 登录时间
	*/
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	private Date loginTime;


}
