package com.puboot.module.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.puboot.common.util.AESUtil;
import com.puboot.common.util.CoreConst;
import com.puboot.common.util.PasswordHelper;
import com.puboot.common.util.ResultUtil;
import com.puboot.exception.CommonException;
import com.puboot.module.admin.model.ClientUser;
import com.puboot.module.admin.service.UserService;
import com.puboot.module.admin.vo.base.PageResultVo;
import com.puboot.module.admin.vo.base.ResponseVo;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 门户网站用户
 * @author Linzhaoguan
 * @version V1.0
 * @date 2019年9月11日
 */
@Controller
@RequestMapping("/clientUser/user")
@AllArgsConstructor
public class ClientUserController {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    private final UserService userService;

    // 门户网站用户列表
    @PostMapping("/list")
    @ResponseBody
    public PageResultVo clientUserList(ClientUser user) {
        IPage<ClientUser> userPage = userService.selectClientUsers(user, user.getPageNumber(), user.getPageSize());
        return ResultUtil.table(userPage.getRecords(), userPage.getTotal());
    }

    // 激活用户
    @PostMapping("/active")
    @ResponseBody
    public ResponseVo kickout(String userId) {
        if(StringUtils.isEmpty(userId)){
            throw new CommonException("用户编号不能为空",500);
        }
        ClientUser user=new ClientUser();
        user.setUserId(userId);
        ClientUser dbUser= userService.selectByClientUserId(user);
        if(!ObjectUtils.isEmpty(dbUser)){
            dbUser.setActive(CoreConst.UserActive.ACTIVE.getVal());
            userService.updateByClientUserId(dbUser);
        }
        return ResultUtil.success("请求成功");
    }

    // 批量激活用户
    @PostMapping("/batch/active")
    @ResponseBody
    public ResponseVo kickout(@RequestBody List<ClientUser> users) {
        if(CollectionUtils.isNotEmpty(users)){
            for (ClientUser user:users) {
                ClientUser dbUser= userService.selectByClientUserId(user);
                if(!ObjectUtils.isEmpty(dbUser)){
                    dbUser.setActive(CoreConst.UserActive.ACTIVE.getVal());
                    userService.updateByClientUserId(dbUser);
                }
            }
        }
        return ResultUtil.success("请求成功");
    }


    // 管理员重置前台用户密码
    @PostMapping("/resetPassword")
    @ResponseBody
    public ResponseVo resetPassword(String userId) {
        if (StringUtils.isEmpty(userId)) {
            throw new CommonException("用户编码不能为空", 500);
        }
        ClientUser user = new ClientUser();
        user.setUserId(userId);
        ClientUser dbUser = userService.selectByClientUserId(user);
        if (ObjectUtils.isEmpty(dbUser)) {
            throw new CommonException("用户账户不存在", 500);
        }
        //清理前台登录账户缓存
        String token= AESUtil.encrypt(dbUser.getUserId()+","+dbUser.getPassword(),null);
        redisTemplate.delete(token);
        //初始化账户密码
        String encodePassword = new SimpleHash(PasswordHelper.ALGORITHM_NAME, CoreConst.SYSTEM_INIT_PASSWORD, ByteSource.Util.bytes(dbUser.getCredentialsSalt()), PasswordHelper.HASH_ITERATIONS).toHex();
        ClientUser mdUser=new ClientUser();
        mdUser.setPassword(encodePassword);
        mdUser.setUserId(userId);
        userService.updateByClientUserId(mdUser);
        return ResultUtil.success("请求成功");
    }
}
