package com.puboot.module.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.puboot.module.admin.model.LoginHistory;
import lombok.AllArgsConstructor;

/**
 * 用户登录历史
 */
public interface LoginHistoryService extends IService<LoginHistory> {
}
