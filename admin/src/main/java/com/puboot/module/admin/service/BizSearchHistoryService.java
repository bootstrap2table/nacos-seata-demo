package com.puboot.module.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.puboot.module.admin.model.BizSearchHistory;

/**
 * <p>
 * app搜索历史表 服务类
 * </p>
 *
 * @author wgj
 * @since 2020-06-16
 */
public interface BizSearchHistoryService extends IService<BizSearchHistory> {


	public void saveSearchHistory(String searchText, String ip, String source, String userId);
}
