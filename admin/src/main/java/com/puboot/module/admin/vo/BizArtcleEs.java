/*
package com.puboot.module.admin.vo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.puboot.enums.EsConsts;

*/
/**
 * 其中，注解解释如下：
 *
 * @Document(indexName="blob3",type="article")： indexName：索引的名称（必填项）
 * type：索引的类型
 * @Id：主键的唯一标识
 * @Field(index=true,analyzer="ik_smart",store=true,searchAnalyzer="ik_smart",type =
 * FieldType.text)
 * index：是否索引
 * analyzer：存储时使用的分词器
 * searchAnalyze：搜索时使用的分词器
 * store：是否存储
 * type: 数据类型
 * <p>
 * 注： 一旦添加了@Filed注解，所有的默认值都不再生效。此外，如果添加了@Filed注解，那么type字段必须指定。
 * 所以一般不用添加。
 *//*

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "cms_article", shards = 1, replicas = 0)
public class BizArtcleEs 
{
	 
    @Id
    private Integer id;

    @Field(type = FieldType.Text, analyzer = EsConsts.SEARCH_MAX_ANALYZER, searchAnalyzer = EsConsts.SEARCH_ANALYZER)
    private String title;
    @Field(type = FieldType.Text, analyzer = EsConsts.SEARCH_MAX_ANALYZER, searchAnalyzer = EsConsts.SEARCH_ANALYZER)
    private String content;

    @Field(type = FieldType.Keyword)
    private String author;

    @Field(type = FieldType.Keyword)
    private Integer top;
    @Field(type = FieldType.Keyword)
    private String userId;
    @Field(type = FieldType.Keyword)
    private String coverImage;
    @Field(type = FieldType.Keyword)
    private String qrcodePath;
    @Field(type = FieldType.Keyword)
    private Boolean isMarkdown;
    @Field(type = FieldType.Keyword)
    private String contentMd;
    @Field(type = FieldType.Keyword)
    private Integer categoryId;
    @Field(type = FieldType.Keyword)
    private Integer status;
    @Field(type = FieldType.Keyword)
    private Integer recommended;
    @Field(type = FieldType.Keyword)
    private Integer slider;
    @Field(type = FieldType.Keyword)
    private String sliderImg;
    @Field(type = FieldType.Keyword)
    private Integer original;
    @Field(type = FieldType.Keyword)
    private String description;
    @Field(type = FieldType.Keyword)
    private String keywords;
    @Field(type = FieldType.Keyword)
    private Date createTime;
    @Field(type = FieldType.Keyword)
    private Date updateTime;

}
*/
