package com.puboot.module.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.puboot.common.util.CoreConst;
import com.puboot.common.util.Pagination;
import com.puboot.common.util.ResultUtil;
import com.puboot.module.admin.model.LoginHistory;
import com.puboot.module.admin.service.LoginHistoryService;
import com.puboot.module.admin.vo.base.PageResultVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 登录历史日志
 *
 * @author Linzhaoguan
 * @version V1.0
 * @date 2019年9月11日
 */
@Controller
@RequestMapping("/loginHistory")
@AllArgsConstructor
public class LoginHistoryController {

    private final LoginHistoryService loginHistoryService;


    /**
     * 用户登录列表数据
     */
    @PostMapping("/list")
    @ResponseBody
    public PageResultVo loadUsers(LoginHistory history) {
        IPage<LoginHistory> userPage = loginHistoryService.page(new Pagination<>(history.getPageNumber(), history.getPageSize()),new QueryWrapper<LoginHistory>()
                .eq(!ObjectUtils.isEmpty(history.getType()),"type", history.getType())
                .eq("status", CoreConst.STATUS_VALID)
                .like(!ObjectUtils.isEmpty(history.getUsername()),"username", history.getUsername())
                .orderBy(!ObjectUtils.isEmpty(history.getSortName())&&!ObjectUtils.isEmpty(history.getSortOrder()),history.getSortOrder().equalsIgnoreCase("asc"),Pagination.convertBeanToCloumn(history.getSortName()))
        );
        return ResultUtil.table(userPage.getRecords(), userPage.getTotal());
    }


}
