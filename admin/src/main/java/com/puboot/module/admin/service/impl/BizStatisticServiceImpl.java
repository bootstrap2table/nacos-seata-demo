package com.puboot.module.admin.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.puboot.module.admin.model.BizArticleLook;
import com.puboot.module.admin.model.ClientUser;
import com.puboot.module.admin.service.*;
import com.puboot.module.admin.vo.StatisticVo;
import com.puboot.module.admin.vo.UserOnlineVo;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Nobita
 * @date 2021/1/12 3:35 下午
 */
@Service
@AllArgsConstructor
public class BizStatisticServiceImpl implements BizStatisticService {

    private final BizArticleService articleService;
    private final BizCommentService commentService;
    private final BizArticleLookService articleLookService;
    private final UserService userService;




    @Override
    public StatisticVo indexStatistic() {
        int articleCount = articleService.count();
        int commentCount = commentService.count();
        int lookCount = articleLookService.count();
        int userCount = articleLookService.count(Wrappers.<BizArticleLook>query().select("DISTINCT user_ip"));
        int recentDays = 6;
        int onlineUserCount=0;
        ClientUser user=new ClientUser();
        user.setSortOrder("asc");
        user.setSortName("lastLoginTime");
        long clientUserCount=userService.selectClientUsers(user,1,Integer.MAX_VALUE).getTotal();
        List<UserOnlineVo> userOnlineVoList=userService.selectOnlineUsers(new UserOnlineVo());
        if(CollectionUtils.isNotEmpty(userOnlineVoList)){
            onlineUserCount=userOnlineVoList.size();
        }
        Map<String, Integer> lookCountByDay = articleLookService.lookCountByDay(recentDays);
        Map<String, Integer> userCountByDay = articleLookService.userCountByDay(recentDays);
        return StatisticVo.builder().articleCount(articleCount).commentCount(commentCount).lookCount(lookCount).userCount(userCount).lookCountByDay(lookCountByDay).userCountByDay(userCountByDay).onlineUserCount(onlineUserCount).clientUserCount(clientUserCount).build();
    }
}
