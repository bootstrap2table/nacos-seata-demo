/*
package com.puboot.module.admin.mapper;

import com.alibaba.fastjson.JSON;
import com.puboot.module.admin.vo.BizArtcleEs;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
@Slf4j
public class HighlightResultMapper implements SearchResultMapper 
{
    @Override
    public <T> AggregatedPage<T> mapResults(SearchResponse searchResponse, Class<T> clazz, Pageable pageable) {
        long totalHits = searchResponse.getHits().getTotalHits();
        List<T> list = new ArrayList<>();
        SearchHits hits = searchResponse.getHits();
        if (hits.getHits().length> 0) 
        {
            for (SearchHit searchHit : hits) {
                String id = searchHit.getId();
                Map<String, HighlightField> highlightFields = searchHit.getHighlightFields();
                T item = JSON.parseObject(searchHit.getSourceAsString(), clazz);

                Field idField = null;
                try {
                    if (item instanceof BizArtcleEs) {
                        idField = clazz.getDeclaredField("id");
                    }
                    idField.setAccessible(true);
                    idField.set(item, Integer.valueOf(id));
                } catch (NoSuchFieldException e) {
                    log.error("id field not found. ", e);
                } catch (IllegalAccessException e) {
                    log.error(e.getMessage());
                }

                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    field.setAccessible(true);
                    if (highlightFields.containsKey(field.getName())) {
                        try {
                            field.set(item, highlightFields.get(field.getName()).fragments()[0].toString());
                        } catch (IllegalAccessException e) {
                           log.error(e.getMessage());
                        }
                    }
                }
                list.add(item);
            }
        }
        return new AggregatedPageImpl<>(list, pageable, totalHits);
    }

	@Override
	public <T> T mapSearchHit(SearchHit searchHit, Class<T> type) {
		return null;
	}
}

*/
