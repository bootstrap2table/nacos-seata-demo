package com.puboot.module.admin.mapper;


import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.puboot.module.admin.model.BizSearchHistory;

/**
 * <p>
 * app搜索历史表 Mapper 接口
 * </p>
 *
 * @author wgj
 * @since 2020-06-16
 */
public interface BizSearchHistoryMapper extends BaseMapper<BizSearchHistory> {

   

}
