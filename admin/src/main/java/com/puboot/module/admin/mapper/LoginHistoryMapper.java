package com.puboot.module.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.puboot.module.admin.model.ClientUser;
import com.puboot.module.admin.model.LoginHistory;

/**
 * @author Linzhaoguan
 * @version V1.0
 * @date 2019年9月11日
 */
public interface LoginHistoryMapper extends BaseMapper<LoginHistory> {
}
