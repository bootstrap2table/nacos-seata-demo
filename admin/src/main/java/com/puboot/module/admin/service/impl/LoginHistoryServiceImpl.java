package com.puboot.module.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.puboot.module.admin.mapper.LoginHistoryMapper;
import com.puboot.module.admin.model.LoginHistory;
import com.puboot.module.admin.service.LoginHistoryService;
import org.springframework.stereotype.Service;

/**
 * 用户登录历史
 */
@Service
public class LoginHistoryServiceImpl extends ServiceImpl<LoginHistoryMapper, LoginHistory> implements LoginHistoryService {
}
