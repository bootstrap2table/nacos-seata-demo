/*
package com.puboot.module.admin.service.impl;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.jsoup.Jsoup;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.puboot.common.util.CoreConst;
import com.puboot.enums.EsConsts;
import com.puboot.module.admin.mapper.BizArticleESDao;
import com.puboot.module.admin.mapper.HighlightResultMapper;
import com.puboot.module.admin.model.BizArticle;
import com.puboot.module.admin.service.BizArticleESService;
import com.puboot.module.admin.service.BizArticleService;
import com.puboot.module.admin.vo.BizArtcleEs;

@Slf4j
@Service("bizArticleESService")
public class BizArticleESServiceImpl implements BizArticleESService , InitializingBean
{
    private static final String DYNAMIC_TITLE_NAME = "title";
    private static final String DYNAMIC_TEXT_NAME = "content";
    private static final String ISVALID = "status";

    

    @Autowired
    private BizArticleESDao dynamicDao;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private  BizArticleService articleService;
  

    @Override
    public void storeArtcleData(BizArtcleEs bandData) 
    {
        dynamicDao.save(bandData);
    }

    @Override
    public Page<BizArtcleEs> searchArtcleDataHit(String keyWord, Integer start, Integer size) 
    {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery()
                .should(QueryBuilders.matchPhraseQuery(DYNAMIC_TITLE_NAME, keyWord))
                .should(QueryBuilders.matchPhraseQuery(DYNAMIC_TEXT_NAME, keyWord))
                .minimumShouldMatch(1)
                .must(QueryBuilders.termQuery(ISVALID, CoreConst.STATUS_VALID));
                // 债券和期货
               // .must(QueryBuilders.termsQuery(MODULE, Arrays.asList("1", "3")));

        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.requireFieldMatch(true);
        // 下面这两项,如果你要高亮如文字内容等有很多字的字段,必须配置,不然会导致高亮不全,文章内容缺失等
        highlightBuilder.fragmentSize(800000);
        highlightBuilder.numOfFragments(0);

        //构建查询
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                //设置高亮字段
                .withHighlightFields(new HighlightBuilder.
                		Field(DYNAMIC_TITLE_NAME).
                		preTags(EsConsts.PRETAGES).postTags(EsConsts.POSTTAGS),
                        new HighlightBuilder.Field(DYNAMIC_TEXT_NAME).
                        preTags(EsConsts.PRETAGES).postTags(EsConsts.POSTTAGS)
                )
                .withHighlightBuilder(highlightBuilder)
                .withPageable(PageRequest.of(start, size))
                .withSort(SortBuilders.fieldSort("createTime").order(SortOrder.DESC))
                .build();
        Page<BizArtcleEs> result = elasticsearchTemplate.queryForPage(searchQuery, BizArtcleEs.class, new HighlightResultMapper());
        wrapperSearchResultData(result);
        return result;
    }

    private void wrapperSearchResultData(Page<BizArtcleEs> searchResultData) 
    {
        if (searchResultData.getContent() != null && searchResultData.getContent().size() > 0) 
        {
            for (BizArtcleEs dynamicEsData : searchResultData.getContent()) 
            {
                // image html转义
//                if (StringUtils.isNotEmpty(dynamicEsData.getDynamicImage())) {
//                    dynamicEsData.setDynamicImage(encodeStr(dynamicEsData.getDynamicImage()));
//                }

                // 是否收费和类型，重新设置内容长度
//                if ("1".equals(dynamic.getCharge())) {
//                    dynamicEsData.setDynamicText(substringContent(dynamicText, 60));
//                    String summaryInfo = dynamic.getSummaryInfo();
//                    if (!isVip && StringUtils.isNotEmpty(summaryInfo)) {
//                        dynamicEsData.setDynamicText(Jsoup.parse(summaryInfo).text());
//                    }
//                }
//                if ("4".equals(dynamic.getType()) && "1".equals(dynamic.getOperatype())) {
//                    //复盘只展示20个字
//                    dynamicEsData.setDynamicText(substringContent(dynamicText, 20));
//                }
            }
        }
    }

  

  

    private String encodeStr(String str) 
    {
        try {
            String encodeStr = URLEncoder.encode(str, "utf-8");
            return StringUtils.replaceEach(encodeStr, new String[]{"%3A", "%2F"}, new String[]{":", "/"});
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }

    private String substringContent(String text, int length) 
    {
        if (StringUtils.isBlank(text)) 
        {
            return "";
        }
        String replace = text.replaceAll(EsConsts.PRETAGES, "").replaceAll(EsConsts.POSTTAGS, "");
        // 高亮标签多占的长度
        int count = text.length() - replace.length();

        int endIndex = length + count;

        if (text.length() > endIndex) {
            text = text.substring(0, endIndex) + "...";
        }
        return text;
    }
    
    
    @Override
    public void afterPropertiesSet() throws Exception 
    {

        if (!elasticsearchTemplate.indexExists(BizArtcleEs.class) || dynamicDao.count() == 0) 
        {
        	//Wrappers.<BizArticle>lambdaQuery().eq(BizArticle::getCategoryId, categoryId)
            List<BizArticle> entities = articleService.list();
            List<BizArtcleEs> esEntities = new ArrayList<>();
            BizArtcleEs esEntity = null;
            for (BizArticle entity : entities) 
            {
                esEntity = new BizArtcleEs();
                BeanUtils.copyProperties(entity, esEntity);
                esEntities.add(esEntity);
            }
            if (!esEntities.isEmpty()) 
            {
            	dynamicDao.saveAll(esEntities);
                log.info("init ArtcleEsEntity success: {}", esEntities.size());
            }

        }

       

        

      
  

    }
    

}
*/
