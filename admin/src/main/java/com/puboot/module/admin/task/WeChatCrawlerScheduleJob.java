package com.puboot.module.admin.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialFileBatchGetResult;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialNewsBatchGetResult;
@Component
@Slf4j
public class WeChatCrawlerScheduleJob 
{
	@Autowired
    private WxMpService wxMpService;
	
	
    @Scheduled(fixedRate = 10000)
    public void execute() 
    {
        System.out.println("定时采集公众号内容");
        try 
        {
        	//String accessToken = wxMpService.getAccessToken();
        	//WxMpMaterialNewsBatchGetResult newsResult =wxMpService.getMaterialService().materialNewsBatchGet(0, 20);
          // System.out.println(newsResult);
        } catch (Exception e) {
            log.error("【采集微信公众号内容失败】{}", e);
        }
              
    }

}
