/*
package com.puboot.module.admin.service.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.puboot.module.admin.mapper.BizSearchHistoryMapper;
import com.puboot.module.admin.model.BizSearchHistory;
import com.puboot.module.admin.service.BizSearchHistoryService;

*/
/**
 * <p>
 * app搜索历史表 服务实现类
 * </p>
 *
 * @author wgj
 * @since 2020-06-16
 *//*

@Service
@Slf4j
public class BizSearchHistoryServiceImpl extends ServiceImpl<BizSearchHistoryMapper, BizSearchHistory> implements BizSearchHistoryService {


    @Override
    public void saveSearchHistory(String searchText,String ip, String source,String userId) 
    {
        	BizSearchHistory obj = new BizSearchHistory();
            obj.setSourceType(source);
            obj.setIp(ip);
            obj.setKeyword(searchText);
            obj.setSearchTime(new Date());
            obj.setCreator(userId);
            obj.setCreateTime(new Date());
            obj.setIsDelete("N");
            this.save(obj);
    }


}
*/
