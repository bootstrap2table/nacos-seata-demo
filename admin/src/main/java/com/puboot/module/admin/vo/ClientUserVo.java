package com.puboot.module.admin.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.puboot.module.admin.model.Role;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Linzhaoguan
 * @version V1.0
 * @date 2019年9月11日
 */
@Data
public class ClientUserVo implements Serializable {
    private static final long serialVersionUID = -8736616045315083846L;


    /**
     * 用户id
     */
    @TableId(type = IdType.UUID)
    private String userId;

    /**
     * 用户名
     */
    @NotEmpty(message = "姓名不能为空")
    private String username;
    @NotEmpty(message = "密码不能为空")
    private String password;
    /**
     * 确认密码
     */
    private String confirmPassword;

    /**
     * token，令牌
     */
    private String token;


    /**
     * 加密盐值
     */
    private String salt;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 真实姓名
     */
    private String realname;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 年龄：1男2女
     */
    private Integer sex;

    /**
     * 是否激活 0:否 1:是
     */
    private Integer active;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 头像
     */
    private String img;

    /**
     * 用户状态：1有效; 0无效
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 最后登录时间
     */
    private Date lastLoginTime;

    /**
     * 登录ip
     */
    @TableField(exist = false)
    private String loginIpAddress;

    /**
     * 角色
     */
    @TableField(exist = false)
    private List<Role> roles;



}