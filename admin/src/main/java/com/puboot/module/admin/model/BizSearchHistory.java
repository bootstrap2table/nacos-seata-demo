package com.puboot.module.admin.model;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * app搜索历史表
 * </p>
 * @author wgj
 * @since 2020-06-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("biz_search_history")
@AllArgsConstructor
@NoArgsConstructor
public class BizSearchHistory implements Serializable 
{
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 来源
     */
    private String ip;
    
    private String sourceType;

    /**
     * 创建人
     */
    @TableField("keyword")
    private String keyword;

    /**
     * 搜索时间
     */
    private Date searchTime;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 修改人
     */
    private String updator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 是否删除（Y是 N否）
     */
    private String isDelete;


}
