package com.puboot.module.admin.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.puboot.module.admin.vo.base.MutiSort;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Linzhaoguan
 * @version V1.0
 * @date 2019年9月11日
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientUser extends MutiSort {


    /**
     * 用户id
     */
    @TableId(type = IdType.UUID)
    private String userId;

    /**
     * 用户名
     */
    private String username;

    private String password;

    /**
     * 加密盐值
     */
    private String salt;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 年龄：1男2女
     */
    private Integer sex;

    /**
     * 是否激活 0:否 1:是
     */
    private Integer active;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 头像
     */
    private String img;

    /**
     * 用户状态：1有效; 0无效
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 最后登录时间
     */
    private Date lastLoginTime;

    /**
     * 登录ip
     */
    @TableField(exist = false)
    private String loginIpAddress;

    /**
     * 重写获取盐值方法，自定义realm使用
     * Gets credentials salt.
     *
     * @return the credentials salt
     */
    public String getCredentialsSalt() {
        return username + "puboot.com" + salt;
    }


}