package com.puboot.module.blog.controller;


import com.puboot.common.annotation.Login;
import com.puboot.common.util.*;
import com.puboot.module.admin.model.ClientUser;
import com.puboot.module.admin.model.LoginHistory;
import com.puboot.module.admin.service.LoginHistoryService;
import com.puboot.module.admin.service.UserService;
import com.puboot.module.admin.vo.ClientUserVo;
import com.puboot.module.admin.vo.base.ResponseVo;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 门户网站登录
 */
@Controller
public class LoginController extends ValidUtil {

    @Autowired
    private  UserService userService;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    LoginHistoryService loginHistoryService;

    /**
     * 门户网站注册接口
     * @return
     */
    @PostMapping("/portal/register")
    @ResponseBody
    public ResponseVo register(@RequestBody @Valid  ClientUserVo reqUser,BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return ResultUtil.error("验证失败",ValidUtil.validErrorResult(bindingResult));
        }
        String username = reqUser.getUsername();
        ClientUser user = userService.selectByClientUsername(username);
        if (null != user) {
            return ResultUtil.error("用户名已存在！");
        }
        String password = reqUser.getPassword();
        //判断两次输入密码是否相等
        if (reqUser.getConfirmPassword() != null && password != null) {
            if (!reqUser.getConfirmPassword().equals(password)) {
                return ResultUtil.error("两次密码不一致！");
            }
        }
        ClientUser registerUser=new ClientUser();
        BeanUtils.copyProperties(reqUser,registerUser);
        registerUser.setUserId(UUIDUtil.getUniqueIdByUUId());
        registerUser.setStatus(CoreConst.STATUS_VALID);
        Date date = new Date();
        registerUser.setCreateTime(date);
        registerUser.setUpdateTime(date);
        registerUser.setLastLoginTime(date);
        registerUser.setSalt(PasswordHelper.RANDOM_NUMBER_GENERATOR.nextBytes().toHex());
        String newPassword = new SimpleHash(PasswordHelper.ALGORITHM_NAME, registerUser.getPassword(), ByteSource.Util.bytes(registerUser.getCredentialsSalt()), PasswordHelper.HASH_ITERATIONS).toHex();
        registerUser.setPassword(newPassword);
        registerUser.setActive(CoreConst.UserActive.NOACTIVE.getVal());//注册未激活
        //注册
        int registerResult = userService.registerClientUser(registerUser);
        if(registerResult > 0){
            return ResultUtil.success("注册成功！");
        }else {
            return ResultUtil.error("注册失败，请稍后再试！");
        }
    }


    /**
     * 门户网站登录
     * @param reqUser
     * @return
     */
    @PostMapping("/portal/login")
    @ResponseBody
    public ResponseVo login(@RequestBody @Valid  ClientUserVo reqUser,BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return ResultUtil.error("验证失败",ValidUtil.validErrorResult(bindingResult));
        }
        String username = reqUser.getUsername();
        ClientUser user = userService.selectByClientUsername(username);
        if (ObjectUtils.isEmpty(user)) {
            return ResultUtil.error("用户名或者密码错误！");
        }
        //请求的密码
        String reqPassword= new SimpleHash(PasswordHelper.ALGORITHM_NAME, reqUser.getPassword(), ByteSource.Util.bytes(user.getCredentialsSalt()), PasswordHelper.HASH_ITERATIONS).toHex();
        //密码是否相同
        if (!ObjectUtils.isEmpty(user.getPassword())&&!ObjectUtils.isEmpty(reqPassword)) {
            if (!user.getPassword().equals(reqPassword)) {
                return ResultUtil.error("用户名或者密码错误！");
            }
        }
        //用户状态
        if(!ObjectUtils.isEmpty(user.getStatus())) {
            if (user.getStatus().equals(0)) {
                return ResultUtil.error("用户被锁定,请联系管理员！");
            }
        }
        //更新最新登录时间
        Date nowDate= new Date();
        user.setLastLoginTime(nowDate);
        ClientUser editUser=new ClientUser();
        editUser.setUserId(user.getUserId());
        editUser.setLastLoginTime(nowDate);
        userService.updateByClientUserId(editUser);
        BeanUtils.copyProperties(user,reqUser);
        String token=AESUtil.encrypt(user.getUserId()+","+user.getPassword(),null);
        reqUser.setToken(token);
        reqUser.setSalt("");
        reqUser.setConfirmPassword("");
        redisTemplate.opsForValue().set(token,reqUser,24, TimeUnit.HOURS);//默认存储24小时，存储用户信息时，密码保留
        reqUser.setPassword("");//密码返回前端时置空
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        LoginHistory loginHistory=new LoginHistory();
        loginHistory.setLoginTime(user.getLastLoginTime());
        loginHistory.setCreateTime(new Date());
        loginHistory.setType(CoreConst.UserType.FRONT.getVal());
        loginHistory.setStatus(CoreConst.STATUS_VALID);
        loginHistory.setUserId(user.getUserId());
        loginHistory.setUsername(user.getUsername());
        loginHistory.setLoginIpAddress(IpUtil.getIpAddr(request));
        loginHistoryService.save(loginHistory);
        return ResultUtil.success("登录成功",reqUser);
    }

    /**
     * 需要登录获取用户信息
     * @param token
     * @return
     */
    @PostMapping("/portal/getUser")
    @ResponseBody
    @Login
    public ResponseVo getUser(@RequestHeader String token){
       return ResultUtil.success("请求成功",redisTemplate.opsForValue().get(token));
    }
}
