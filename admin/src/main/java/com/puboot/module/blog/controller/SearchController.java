/*
package com.puboot.module.blog.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.puboot.common.util.IpUtil;
import com.puboot.common.util.ResultUtil;
import com.puboot.enums.EsConsts;
import com.puboot.module.admin.service.BizArticleESService;
import com.puboot.module.admin.service.BizSearchHistoryService;
import com.puboot.module.admin.vo.BizArtcleEs;
import com.puboot.module.admin.vo.ClientUserVo;
import com.puboot.module.admin.vo.base.ResponseVo;

*/
/**
 * <p>
 * app搜索历史表 前端控制器
 * </p>
 *
 * @author wgj
 * @since 2020-06-16
 *//*

@RestController
@RequestMapping("/portal/search")
public class SearchController 
{
    public static final Integer LIMIT = 3;
    
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private BizArticleESService dynamicESService;
    @Autowired
    private BizSearchHistoryService historyService;
    
    */
/**关键字搜索
     * @param keyword
     * @param start
     * @param limit
     * @return
     *//*

    @RequestMapping("/result")
    public ResponseVo search(String keyword,
    				@RequestParam(defaultValue = "10") String sourceType,
                    @RequestParam(defaultValue = "0") Integer start,
                    @RequestParam(defaultValue = "30") Integer limit,HttpServletRequest request) 
    {
    	 if (org.springframework.util.StringUtils.isEmpty(keyword)) {
             return ResultUtil.error("搜索关键字不能为空");
         }
    	String ip = IpUtil.getIpAddr(request);
    	String userId="999";
    	 String token = request.getHeader("token");
    	 if(StringUtils.isEmpty(token)) {
    		  token = request.getParameter("token");
    	 }
    	 if(StringUtils.isNotBlank(token)) 
    	 {
    		 ClientUserVo loginUser=(ClientUserVo)redisTemplate.opsForValue().get(token);
    		 userId=loginUser.getUserId() ;
    	 }
        if (null != start && start > 0) 
        {
            start = start - 1;
        }
        // 处理搜索的关键词，避免过长
        if (keyword.length() > EsConsts.KEY_WORLD_MAX_LENGTH) 
        {
        	keyword = keyword.substring(0, EsConsts.KEY_WORLD_MAX_LENGTH);
        }
        historyService.saveSearchHistory(keyword, ip,sourceType, userId);
        Map<String, Object> dynamicDto = new HashMap<>();
        Page<BizArtcleEs> dynamicResult = dynamicESService.searchArtcleDataHit(keyword, start, limit);
        dynamicDto.put("count", dynamicResult.getTotalElements());
        dynamicDto.put("listData", dynamicResult.getContent());
        return ResultUtil.success("成功",dynamicDto);
    }

    

}

*/
