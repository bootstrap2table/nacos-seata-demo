/*
Navicat MySQL Data Transfer

Source Server         : bdy
Source Server Version : 50730
Source Host           : 106.12.32.225:3333
Source Database       : pb_cms_base

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-27 11:10:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for biz_article
-- ----------------------------
DROP TABLE IF EXISTS `biz_article`;
CREATE TABLE `biz_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '文章标题',
  `user_id` varchar(20) DEFAULT NULL COMMENT '用户ID',
  `author` varchar(50) DEFAULT NULL COMMENT '作者',
  `cover_image` varchar(255) DEFAULT NULL COMMENT '文章封面图片',
  `qrcode_path` varchar(255) DEFAULT NULL COMMENT '文章专属二维码地址',
  `is_markdown` tinyint(1) unsigned DEFAULT '1',
  `content` longtext COMMENT '文章内容',
  `content_md` longtext COMMENT 'markdown版的文章内容',
  `top` tinyint(1) unsigned DEFAULT '0' COMMENT '是否置顶',
  `category_id` int(11) unsigned DEFAULT NULL COMMENT '类型',
  `status` tinyint(1) unsigned DEFAULT NULL COMMENT '状态',
  `recommended` tinyint(1) unsigned DEFAULT '0' COMMENT '是否推荐',
  `slider` tinyint(1) unsigned DEFAULT '0' COMMENT '是否轮播',
  `slider_img` varchar(255) DEFAULT NULL COMMENT '轮播图地址',
  `original` tinyint(1) unsigned DEFAULT '1' COMMENT '是否原创',
  `description` varchar(300) DEFAULT NULL COMMENT '文章简介，最多200字',
  `keywords` varchar(200) DEFAULT NULL COMMENT '文章关键字，优化搜索',
  `comment` tinyint(1) unsigned DEFAULT '1' COMMENT '是否开启评论',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx` (`description`(255))
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_article
-- ----------------------------
INSERT INTO `biz_article` VALUES ('1', '有史以来最复杂的软件', '1', '郑其锋', 'https://tse1-mm.cn.bing.net/th/id/OIP.Ups1Z8igjNjLuDfO38XhTgHaHa?pid=Api&rs=1', null, '1', '<p><strong>Stuxnet 蠕虫病毒可能是有史以来最复杂的软件。 </strong></p>\r\n<p>我们不知道 Stuxnet 的作者是谁，只知道大概是在2005年至2010年间编写的。</p>\r\n<p>这种病毒藏在 U 盘上。当 U 盘插入 PC，它会自动运行，将自已复制到该 PC。它至少有三种自动运行的方法。如果某种方法行不通，就尝试另一种。其中的两种运行方法是全新的，使用了 Windows 的两个无人知晓的秘密 Bug。</p>\r\n<p>一旦蠕虫进入 PC ，它会尝试获得该 PC 的管理员权限，使用的也是前面提到的那两个无人知道的秘密 Bug。然后，它把自己留下的痕迹全部清除，不让防病毒软件检测到它的存在，用户不会看到任何东西。这种蠕虫隐藏得很好，出现后一年多，没有任何一家安全公司发现它的存在。</p>\r\n<p>它会秘密访问 <a href=\"http://www.mypremierfutbol.com/\">http://www.mypremierfutbol.com</a> 或 <a href=\"http://www.todaysfutbol.com/\">http://www.todaysfutbol.com</a> 这两个网站，告诉服务器已经成功侵入了一台新的 PC，然后从网站下载最新版本自行更新。</p>\r\n<p>它会将自身复制到任何插入该 PC 的 U 盘。使用的 U 盘驱动程序由 Realtek 公司进行了数字签名，但是 Realtek 公司并不知道有这个签名。这意味着，蠕虫作者能够获取 Realtek 公司的最高密钥。</p>\r\n<p>它利用两个 Windows 的 Bug ----一个涉及网络打印机，另一个涉及网络文件----将自己传播到局域网里面的其他计算机上。</p>\r\n<p>直到这一步，它的真正任务还没有开始。</p>\r\n<p>它在每一台计算机上寻找一种由西门子设计的用于大型工业机械自动化的控制软件。一旦发现这种软件，它会使用另<em>一个</em>以前未知的 Bug，将自身复制到工业控制器的驱动程序。然后，它会检查两家特定公司的工业电机，其中一家公司在伊朗，另一家在芬兰。它要搜索的特定电机称为变频驱动器，主要用于运行工业离心机，提纯多种化学品，比如铀。</p>\r\n<p>由于蠕虫完全控制了离心机，因此它可以做任何事情，可以将离心机全部关闭，也可以将它们全部摧毁：只需设定以最大速度旋转离心机，直到它们全都像炸弹一样爆炸，杀死任何恰好站在附近的人。</p>\r\n<p>但它没有这么做，一旦它控制了每台离心机......它就进入潜伏。一旦达到设定的时间，它就会悄悄地唤醒自己，锁住离心机，使得人类无法关闭这些机器。然后悄悄地，蠕虫开始旋转这些离心机，修改了安全参数，增加了一些气体压力......</p>\r\n<p>此外，它还会在离心机正常运转的时候，偷录一段21秒的数据记录。当它控制离心机运行的时候，会一遍又一遍地播放这段数据记录。管理人员会看到，计算机屏幕上的所有离心机运行数据都很正常，但这其实是蠕虫让他们看的。</p>\r\n<p>现在让我们想象一下，有一家工厂正在用离心机净化铀。电脑上的所有数字都表明离心机运行正常。但是，离心机正在悄悄地出问题，一个接一个地倒下，这使得铀产量一直下降。铀必须是纯净的。你的铀不够纯净，无法做任何有用的事情。</p>\r\n<p>工厂的管理者根本找不到原因，离心机的数据是正常的。你永远不会知道，所有这些问题都是由一种计算机蠕虫引起的。这是一种历史上最狡猾和最聪明的计算机蠕虫，它由一些拥有无限资金和无限资源的令人难以置信的秘密团队编写，并且设计时只考虑一个目的：偷偷摧毁某个国家的核弹计划，并且不被发现。</p>\r\n', '**Stuxnet 蠕虫病毒可能是有史以来最复杂的软件。 **\r\n\r\n我们不知道 Stuxnet 的作者是谁，只知道大概是在2005年至2010年间编写的。\r\n\r\n这种病毒藏在 U 盘上。当 U 盘插入 PC，它会自动运行，将自已复制到该 PC。它至少有三种自动运行的方法。如果某种方法行不通，就尝试另一种。其中的两种运行方法是全新的，使用了 Windows 的两个无人知晓的秘密 Bug。\r\n\r\n一旦蠕虫进入 PC ，它会尝试获得该 PC 的管理员权限，使用的也是前面提到的那两个无人知道的秘密 Bug。然后，它把自己留下的痕迹全部清除，不让防病毒软件检测到它的存在，用户不会看到任何东西。这种蠕虫隐藏得很好，出现后一年多，没有任何一家安全公司发现它的存在。\r\n\r\n它会秘密访问 [http://www.mypremierfutbol.com](http://www.mypremierfutbol.com/) 或 [http://www.todaysfutbol.com](http://www.todaysfutbol.com/) 这两个网站，告诉服务器已经成功侵入了一台新的 PC，然后从网站下载最新版本自行更新。\r\n\r\n它会将自身复制到任何插入该 PC 的 U 盘。使用的 U 盘驱动程序由 Realtek 公司进行了数字签名，但是 Realtek 公司并不知道有这个签名。这意味着，蠕虫作者能够获取 Realtek 公司的最高密钥。\r\n\r\n它利用两个 Windows 的 Bug ----一个涉及网络打印机，另一个涉及网络文件----将自己传播到局域网里面的其他计算机上。\r\n\r\n直到这一步，它的真正任务还没有开始。\r\n\r\n它在每一台计算机上寻找一种由西门子设计的用于大型工业机械自动化的控制软件。一旦发现这种软件，它会使用另*一个*以前未知的 Bug，将自身复制到工业控制器的驱动程序。然后，它会检查两家特定公司的工业电机，其中一家公司在伊朗，另一家在芬兰。它要搜索的特定电机称为变频驱动器，主要用于运行工业离心机，提纯多种化学品，比如铀。\r\n\r\n由于蠕虫完全控制了离心机，因此它可以做任何事情，可以将离心机全部关闭，也可以将它们全部摧毁：只需设定以最大速度旋转离心机，直到它们全都像炸弹一样爆炸，杀死任何恰好站在附近的人。\r\n\r\n但它没有这么做，一旦它控制了每台离心机......它就进入潜伏。一旦达到设定的时间，它就会悄悄地唤醒自己，锁住离心机，使得人类无法关闭这些机器。然后悄悄地，蠕虫开始旋转这些离心机，修改了安全参数，增加了一些气体压力......\r\n\r\n此外，它还会在离心机正常运转的时候，偷录一段21秒的数据记录。当它控制离心机运行的时候，会一遍又一遍地播放这段数据记录。管理人员会看到，计算机屏幕上的所有离心机运行数据都很正常，但这其实是蠕虫让他们看的。\r\n\r\n现在让我们想象一下，有一家工厂正在用离心机净化铀。电脑上的所有数字都表明离心机运行正常。但是，离心机正在悄悄地出问题，一个接一个地倒下，这使得铀产量一直下降。铀必须是纯净的。你的铀不够纯净，无法做任何有用的事情。\r\n\r\n工厂的管理者根本找不到原因，离心机的数据是正常的。你永远不会知道，所有这些问题都是由一种计算机蠕虫引起的。这是一种历史上最狡猾和最聪明的计算机蠕虫，它由一些拥有无限资金和无限资源的令人难以置信的秘密团队编写，并且设计时只考虑一个目的：偷偷摧毁某个国家的核弹计划，并且不被发现。', '0', '2', '1', '0', '0', '', '1', 'Stuxnet 蠕虫病毒可能是有史以来最复杂的软件', 'Stuxnet 蠕虫病毒', '1', '2019-09-14 13:07:26', '2019-09-14 13:07:26');
INSERT INTO `biz_article` VALUES ('12', 'aaaa', '1', 'PUBOOT', 'http://8.136.211.31:8080/u/20210315\\微信图片_20210309143620_1615816465641.jpg', null, '1', '<p><img alt=\"\" src=\"http://8.136.211.31:8080/u/20210315\\微信图片_20210309143620_1615816432830.jpg\" style=\"height:1440px; width:1080px\" /></p>\r\n', '<p><img alt=\"\" src=\"http://8.136.211.31:8080/u/20210315\\微信图片_20210309143620_1615816432830.jpg\" style=\"height:1440px; width:1080px\" /></p>\r\n', '0', '15', '1', '0', '0', '', '1', 'aaa', 'aaa', '1', '2021-03-15 21:54:35', '2021-03-15 21:54:35');
INSERT INTO `biz_article` VALUES ('13', '党史学习教育该“学”啥？“指定书目”来啦～～～', '1', 'PUBOOT', '', null, '1', '<p>党史学习教育究竟该&ldquo;学&rdquo;啥？这下有数了。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>党史学习教育用书出版座谈会暨专题宣讲动员会15日在京召开，四本&ldquo;指定书目&rdquo;亮相&mdash;&mdash;习近平《论中国共产党历史》、《毛泽东、邓小平、江泽民、胡锦涛关于中国共产党历史论述摘编》、《习近平新时代中国特色社会主义思想学习问答》、《中国共产党简史》。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>开展党史学习教育，阅读党史著作堪称&ldquo;地基&rdquo;。这堂高规格&ldquo;党史课&rdquo;的&ldquo;指定书目&rdquo;，为何是这四本？不妨先看看书的内容。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>第一本是习近平《论中国共产党历史》。该书收录了习近平同志2012年11月29日至2020年11月24日期间关于中国共产党历史的重要文稿40篇，其中16篇文稿是首次公开发表。</p>\r\n\r\n<p><img alt=\"\" src=\"http://8.136.211.31:8080/u/20210318\\640_1616053133837.jpg\" style=\"height:140px; width:140px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>这本书以丰富的史料支撑和历史纵深的理论分析，对党史上若干重大问题作出新总结，提出新观点，全面、科学地反映党史的内在规律，这为广大党员深入学习和掌握党的历史提供了根本遵循。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>第二本是《毛泽东、邓小平、江泽民、胡锦涛关于中国共产党历史论述摘编》。该书分别摘录了毛泽东同志、邓小平同志、江泽民同志、胡锦涛同志在领导中国革命、建设、改革过程中，围绕中国共产党历史发表的讲话、报告、谈话、批示和书信等重要文献，共计141段论述、9.8万字。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"图片\" src=\"https://mmbiz.qpic.cn/mmbiz_png/RZpva33vYaAzDMSpnEhVlUIW1V0H7gtEciaU6anMPrfNGuxX08icwzISeUsxLvD6O4GtjiaLYmKiaIGHdibBL359k8g/640?wx_fmt=png&amp;tp=webp&amp;wxfrom=5&amp;wx_lazy=1&amp;wx_co=1\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>展卷披读，鉴往知来，犹如在和历史跨时空对话。其中，既有我们党的领导人对党在不同时期的历史回顾，也不乏对党史上重要人物、重要会议、重大事件的周年纪念以及经验总结，一些资料更是从浩渺档案中发掘后首次公开。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>第三本是《习近平新时代中国特色社会主义思想学习问答》。该书把党的创新理论分解为干部群众普遍关心的100个问题，将理论蕴含于生动实践中，将道理蕴含在历史经验中，不少篇章或用历史故事切入，或用历史事件印证，由点及面、深入浅出，具有极强的政治性、思想性和可读性。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"图片\" src=\"https://mmbiz.qpic.cn/mmbiz_png/RZpva33vYaAzDMSpnEhVlUIW1V0H7gtE8jg6Hk1b5dm7Dx6HtwfEics5ILiaf5rN7eiaCvwXeEGYYnpw0fShakagA/640?wx_fmt=png&amp;tp=webp&amp;wxfrom=5&amp;wx_lazy=1&amp;wx_co=1\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>如何利用历史让理论鲜活起来？百问百答就是一种有益探索，其中不乏我们耳熟能详的长征路上&ldquo;半条被子&rdquo;等故事，做到了论从史出、理从事出。该书既紧跟时代发展步伐，又聚焦理论难点热点，以通俗化、大众化方式回应干部群众关切。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>第四本是《中国共产党简史》。该书共10章、70节，记录了一百年来中国共产党团结带领人民进行革命、建设和改革的光辉历程，系统总结了党和国家事业不断从胜利走向胜利的宝贵经验，集中彰显了党在各个历史时期淬炼锻造的伟大精神。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"图片\" src=\"https://mmbiz.qpic.cn/mmbiz_png/RZpva33vYaAzDMSpnEhVlUIW1V0H7gtEvA1U3nFuF7M1ZVb6YdztdRlw7Vicamssa6BGibSu6fUVqWD04xR1qiaCA/640?wx_fmt=png&amp;tp=webp&amp;wxfrom=5&amp;wx_lazy=1&amp;wx_co=1\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>该书坚持史论结合、夹叙夹议，做到把基本事实讲明白，把基本脉络讲清晰，把基本道理讲透彻，以期实现人人看得进去，专家学者不觉得浅，普通群众也不觉得深的传播效果。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>沉甸甸的四本书，背后凝结的是数代中国共产党人的智慧和心血。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>四本书摆在一起看，一方面，内容各有侧重，每读一本都会有不同体悟；另一方面，四本书又融会贯通，是一个有机整体，需要相互参照，互相印证，互为补充，贯通起来学习。这样才能全面系统地弄清楚历史事件的来龙去脉，准确把握党的历史发展的主题主线、主流本质，进而举一反三，增强历史自觉，真正做到学党史、悟思想、办实事、开新局。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>了解历史，才能看得远；理解历史，方能走得更远。</p>\r\n', '<p>党史学习教育究竟该&ldquo;学&rdquo;啥？这下有数了。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>党史学习教育用书出版座谈会暨专题宣讲动员会15日在京召开，四本&ldquo;指定书目&rdquo;亮相&mdash;&mdash;习近平《论中国共产党历史》、《毛泽东、邓小平、江泽民、胡锦涛关于中国共产党历史论述摘编》、《习近平新时代中国特色社会主义思想学习问答》、《中国共产党简史》。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>开展党史学习教育，阅读党史著作堪称&ldquo;地基&rdquo;。这堂高规格&ldquo;党史课&rdquo;的&ldquo;指定书目&rdquo;，为何是这四本？不妨先看看书的内容。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>第一本是习近平《论中国共产党历史》。该书收录了习近平同志2012年11月29日至2020年11月24日期间关于中国共产党历史的重要文稿40篇，其中16篇文稿是首次公开发表。</p>\r\n\r\n<p><img alt=\"\" src=\"http://8.136.211.31:8080/u/20210318\\640_1616053133837.jpg\" style=\"height:140px; width:140px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>这本书以丰富的史料支撑和历史纵深的理论分析，对党史上若干重大问题作出新总结，提出新观点，全面、科学地反映党史的内在规律，这为广大党员深入学习和掌握党的历史提供了根本遵循。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>第二本是《毛泽东、邓小平、江泽民、胡锦涛关于中国共产党历史论述摘编》。该书分别摘录了毛泽东同志、邓小平同志、江泽民同志、胡锦涛同志在领导中国革命、建设、改革过程中，围绕中国共产党历史发表的讲话、报告、谈话、批示和书信等重要文献，共计141段论述、9.8万字。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"图片\" src=\"https://mmbiz.qpic.cn/mmbiz_png/RZpva33vYaAzDMSpnEhVlUIW1V0H7gtEciaU6anMPrfNGuxX08icwzISeUsxLvD6O4GtjiaLYmKiaIGHdibBL359k8g/640?wx_fmt=png&amp;tp=webp&amp;wxfrom=5&amp;wx_lazy=1&amp;wx_co=1\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>展卷披读，鉴往知来，犹如在和历史跨时空对话。其中，既有我们党的领导人对党在不同时期的历史回顾，也不乏对党史上重要人物、重要会议、重大事件的周年纪念以及经验总结，一些资料更是从浩渺档案中发掘后首次公开。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>第三本是《习近平新时代中国特色社会主义思想学习问答》。该书把党的创新理论分解为干部群众普遍关心的100个问题，将理论蕴含于生动实践中，将道理蕴含在历史经验中，不少篇章或用历史故事切入，或用历史事件印证，由点及面、深入浅出，具有极强的政治性、思想性和可读性。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"图片\" src=\"https://mmbiz.qpic.cn/mmbiz_png/RZpva33vYaAzDMSpnEhVlUIW1V0H7gtE8jg6Hk1b5dm7Dx6HtwfEics5ILiaf5rN7eiaCvwXeEGYYnpw0fShakagA/640?wx_fmt=png&amp;tp=webp&amp;wxfrom=5&amp;wx_lazy=1&amp;wx_co=1\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>如何利用历史让理论鲜活起来？百问百答就是一种有益探索，其中不乏我们耳熟能详的长征路上&ldquo;半条被子&rdquo;等故事，做到了论从史出、理从事出。该书既紧跟时代发展步伐，又聚焦理论难点热点，以通俗化、大众化方式回应干部群众关切。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>第四本是《中国共产党简史》。该书共10章、70节，记录了一百年来中国共产党团结带领人民进行革命、建设和改革的光辉历程，系统总结了党和国家事业不断从胜利走向胜利的宝贵经验，集中彰显了党在各个历史时期淬炼锻造的伟大精神。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"图片\" src=\"https://mmbiz.qpic.cn/mmbiz_png/RZpva33vYaAzDMSpnEhVlUIW1V0H7gtEvA1U3nFuF7M1ZVb6YdztdRlw7Vicamssa6BGibSu6fUVqWD04xR1qiaCA/640?wx_fmt=png&amp;tp=webp&amp;wxfrom=5&amp;wx_lazy=1&amp;wx_co=1\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>该书坚持史论结合、夹叙夹议，做到把基本事实讲明白，把基本脉络讲清晰，把基本道理讲透彻，以期实现人人看得进去，专家学者不觉得浅，普通群众也不觉得深的传播效果。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>沉甸甸的四本书，背后凝结的是数代中国共产党人的智慧和心血。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>四本书摆在一起看，一方面，内容各有侧重，每读一本都会有不同体悟；另一方面，四本书又融会贯通，是一个有机整体，需要相互参照，互相印证，互为补充，贯通起来学习。这样才能全面系统地弄清楚历史事件的来龙去脉，准确把握党的历史发展的主题主线、主流本质，进而举一反三，增强历史自觉，真正做到学党史、悟思想、办实事、开新局。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>了解历史，才能看得远；理解历史，方能走得更远。</p>\r\n', '0', '2', '1', '0', '0', '', '0', '', '', '0', '2021-03-18 15:48:57', '2021-03-18 15:48:57');

-- ----------------------------
-- Table structure for biz_article_look
-- ----------------------------
DROP TABLE IF EXISTS `biz_article_look`;
CREATE TABLE `biz_article_look` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(20) unsigned NOT NULL COMMENT '文章ID',
  `user_id` varchar(20) DEFAULT NULL COMMENT '已登录用户ID',
  `user_ip` varchar(50) DEFAULT NULL COMMENT '用户IP',
  `look_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '浏览时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_article_look
-- ----------------------------
INSERT INTO `biz_article_look` VALUES ('1', '1', '1', '0:0:0:0:0:0:0:1', '2018-09-13 23:30:25', '2018-09-13 23:30:25', '2018-09-13 23:30:25');
INSERT INTO `biz_article_look` VALUES ('2', '1', '2', '0:0:0:0:0:0:0:1', '2018-09-19 16:56:59', '2018-09-19 16:56:59', '2018-09-19 16:56:59');
INSERT INTO `biz_article_look` VALUES ('3', '1', null, '0:0:0:0:0:0:0:1', '2018-09-20 00:52:15', '2018-09-20 00:52:15', '2018-09-20 00:52:15');
INSERT INTO `biz_article_look` VALUES ('4', '1', null, '0:0:0:0:0:0:0:1', '2019-09-11 11:32:04', '2019-09-11 11:32:04', '2019-09-11 11:32:04');
INSERT INTO `biz_article_look` VALUES ('5', '1', null, '0:0:0:0:0:0:0:1', '2019-09-11 22:16:09', '2019-09-11 22:16:09', '2019-09-11 22:16:09');
INSERT INTO `biz_article_look` VALUES ('6', '1', null, '0:0:0:0:0:0:0:1', '2019-09-12 09:58:22', '2019-09-12 09:58:22', '2019-09-12 09:58:22');
INSERT INTO `biz_article_look` VALUES ('7', '1', null, '0:0:0:0:0:0:0:1', '2019-09-13 10:05:22', '2019-09-13 10:05:22', '2019-09-13 10:05:22');
INSERT INTO `biz_article_look` VALUES ('8', '1', null, '0:0:0:0:0:0:0:1', '2019-09-14 10:55:45', '2019-09-14 10:55:45', '2019-09-14 10:55:45');
INSERT INTO `biz_article_look` VALUES ('9', '2', null, '0:0:0:0:0:0:0:1', '2019-09-14 11:06:49', '2019-09-14 11:06:49', '2019-09-14 11:06:49');
INSERT INTO `biz_article_look` VALUES ('10', '3', null, '0:0:0:0:0:0:0:1', '2019-09-14 12:42:52', '2019-09-14 12:42:52', '2019-09-14 12:42:52');
INSERT INTO `biz_article_look` VALUES ('11', '1', null, '0:0:0:0:0:0:0:1', '2019-09-14 15:35:20', '2019-09-14 15:35:20', '2019-09-14 15:35:20');
INSERT INTO `biz_article_look` VALUES ('12', '6', null, '0:0:0:0:0:0:0:1', '2019-09-14 16:35:31', '2019-09-14 16:35:31', '2019-09-14 16:35:31');
INSERT INTO `biz_article_look` VALUES ('13', '4', null, '0:0:0:0:0:0:0:1', '2019-09-14 16:35:37', '2019-09-14 16:35:37', '2019-09-14 16:35:37');
INSERT INTO `biz_article_look` VALUES ('14', '9', null, '0:0:0:0:0:0:0:1', '2019-09-15 17:10:53', '2019-09-15 17:10:53', '2019-09-15 17:10:53');
INSERT INTO `biz_article_look` VALUES ('15', '10', null, '0:0:0:0:0:0:0:1', '2019-09-18 09:28:39', '2019-09-18 09:28:39', '2019-09-18 09:28:39');
INSERT INTO `biz_article_look` VALUES ('16', '1', null, '127.0.0.1', '2019-09-26 15:47:52', '2019-09-26 15:47:52', '2019-09-26 15:47:52');
INSERT INTO `biz_article_look` VALUES ('17', '1', null, '127.0.0.1', '2021-03-15 15:45:23', '2021-03-15 15:45:23', '2021-03-15 15:45:23');
INSERT INTO `biz_article_look` VALUES ('18', '2', '2', '187.91.120.1', '2021-03-15 15:45:39', '2021-03-15 15:45:39', '2021-03-15 15:45:39');
INSERT INTO `biz_article_look` VALUES ('19', '1', null, '124.74.105.114', '2021-03-17 17:51:25', '2021-03-17 17:51:25', '2021-03-17 17:51:25');
INSERT INTO `biz_article_look` VALUES ('20', '1', null, '0:0:0:0:0:0:0:1', '2021-03-17 21:50:38', '2021-03-17 21:50:38', '2021-03-17 21:50:38');

-- ----------------------------
-- Table structure for biz_article_tags
-- ----------------------------
DROP TABLE IF EXISTS `biz_article_tags`;
CREATE TABLE `biz_article_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) unsigned NOT NULL COMMENT '标签表主键',
  `article_id` int(11) unsigned NOT NULL COMMENT '文章ID',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_article_tags
-- ----------------------------
INSERT INTO `biz_article_tags` VALUES ('1', '2', '1', '2018-09-13 23:30:20', '2018-09-13 23:30:20');
INSERT INTO `biz_article_tags` VALUES ('2', '3', '1', '2018-09-13 23:30:20', '2018-09-13 23:30:20');
INSERT INTO `biz_article_tags` VALUES ('3', '4', '1', '2018-09-13 23:30:20', '2018-09-13 23:30:20');
INSERT INTO `biz_article_tags` VALUES ('6', '1', '2', '2019-09-14 12:36:21', '2019-09-14 12:36:21');
INSERT INTO `biz_article_tags` VALUES ('7', '2', '3', '2019-09-14 12:45:41', '2019-09-14 12:45:41');
INSERT INTO `biz_article_tags` VALUES ('11', '3', '6', '2019-09-14 13:10:34', '2019-09-14 13:10:34');
INSERT INTO `biz_article_tags` VALUES ('12', '4', '6', '2019-09-14 13:10:34', '2019-09-14 13:10:34');
INSERT INTO `biz_article_tags` VALUES ('13', '5', '7', '2019-09-14 13:16:10', '2019-09-14 13:16:10');
INSERT INTO `biz_article_tags` VALUES ('14', '2', '8', '2019-09-14 13:19:02', '2019-09-14 13:19:02');
INSERT INTO `biz_article_tags` VALUES ('19', '10', '11', '2019-09-14 15:31:55', '2019-09-14 15:31:55');
INSERT INTO `biz_article_tags` VALUES ('21', '6', '9', '2019-09-14 15:32:22', '2019-09-14 15:32:22');
INSERT INTO `biz_article_tags` VALUES ('22', '8', '9', '2019-09-14 15:32:22', '2019-09-14 15:32:22');
INSERT INTO `biz_article_tags` VALUES ('23', '6', '10', '2019-09-14 15:32:32', '2019-09-14 15:32:32');
INSERT INTO `biz_article_tags` VALUES ('24', '8', '10', '2019-09-14 15:32:32', '2019-09-14 15:32:32');
INSERT INTO `biz_article_tags` VALUES ('25', '7', '5', '2019-09-14 15:34:20', '2019-09-14 15:34:20');
INSERT INTO `biz_article_tags` VALUES ('26', '10', '4', '2019-09-14 15:34:32', '2019-09-14 15:34:32');
INSERT INTO `biz_article_tags` VALUES ('28', '1', '12', '2021-03-15 22:46:23', '2021-03-15 22:46:23');
INSERT INTO `biz_article_tags` VALUES ('29', '1', '13', '2021-03-18 15:48:57', '2021-03-18 15:48:57');

-- ----------------------------
-- Table structure for biz_category
-- ----------------------------
DROP TABLE IF EXISTS `biz_category`;
CREATE TABLE `biz_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '文章类型名',
  `description` varchar(200) DEFAULT NULL COMMENT '类型介绍',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '是否可用',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_category
-- ----------------------------
INSERT INTO `biz_category` VALUES ('1', '0', '组织任务', '组织任务', '1', 'fa fa-css3', '1', '2018-01-14 21:34:54', '2021-03-15 15:10:25');
INSERT INTO `biz_category` VALUES ('2', '0', '工作动态', '工作动态', '10', 'fa fa-coffee', '1', '2018-01-14 21:34:57', '2021-03-15 09:59:07');
INSERT INTO `biz_category` VALUES ('3', '0', '教育活动', '教育活动', '50', 'fa fa-folder-open-o', '1', '2018-01-20 22:28:03', '2021-03-15 13:36:43');
INSERT INTO `biz_category` VALUES ('4', '0', '特色品牌', '特色品牌', '40', null, '1', '2018-08-02 11:20:26', '2021-03-15 09:59:39');
INSERT INTO `biz_category` VALUES ('5', '3', '家庭教育', '家庭教育', '1', null, '1', '2019-09-11 11:28:15', '2021-03-15 10:02:35');
INSERT INTO `biz_category` VALUES ('6', '0', '回顾展望', '回顾展望', '20', null, '1', '2019-09-14 15:26:39', '2021-03-15 09:59:25');
INSERT INTO `biz_category` VALUES ('7', '0', '榜样垂范', '榜样垂范', '60', null, '1', '2021-03-15 10:00:22', '2021-03-15 10:00:32');
INSERT INTO `biz_category` VALUES ('8', '0', '学校党建', '学校党建', '65', null, '1', '2021-03-15 10:00:55', '2021-03-15 10:00:55');
INSERT INTO `biz_category` VALUES ('9', '0', '调查研究', '调查研究', '67', null, '1', '2021-03-15 10:01:14', '2021-03-15 10:01:14');
INSERT INTO `biz_category` VALUES ('10', '0', '荣誉集锦', '荣誉集锦', '68', null, '1', '2021-03-15 10:01:55', '2021-03-15 10:01:55');
INSERT INTO `biz_category` VALUES ('11', '0', '参考文献', '参考文献', '70', null, '1', '2021-03-15 10:02:13', '2021-03-15 10:02:13');
INSERT INTO `biz_category` VALUES ('12', '3', '社区教育', '社区教育', '2', null, '1', '2021-03-15 13:37:26', '2021-03-15 13:37:43');
INSERT INTO `biz_category` VALUES ('13', '3', '主题教育', '主题教育', '3', null, '1', '2021-03-15 13:38:25', '2021-03-15 13:38:25');
INSERT INTO `biz_category` VALUES ('14', '1', '领导讲话', '领导讲话', '1', null, '1', '2021-03-15 13:43:14', '2021-03-15 13:43:14');
INSERT INTO `biz_category` VALUES ('15', '1', '文件汇编', '文件汇编', '3', null, '1', '2021-03-15 13:43:39', '2021-03-15 13:44:46');
INSERT INTO `biz_category` VALUES ('16', '1', '组织概括', '组织概括', '2', null, '1', '2021-03-15 13:44:34', '2021-03-15 13:44:34');

-- ----------------------------
-- Table structure for biz_comment
-- ----------------------------
DROP TABLE IF EXISTS `biz_comment`;
CREATE TABLE `biz_comment` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `sid` int(20) DEFAULT NULL COMMENT '被评论的文章或者页面的ID(-1:留言板)',
  `user_id` varchar(20) DEFAULT NULL COMMENT '评论人的ID',
  `pid` int(20) unsigned DEFAULT NULL COMMENT '父级评论的id',
  `qq` varchar(13) DEFAULT NULL COMMENT '评论人的QQ（未登录用户）',
  `nickname` varchar(13) DEFAULT NULL COMMENT '评论人的昵称（未登录用户）',
  `avatar` varchar(255) DEFAULT NULL COMMENT '评论人的头像地址',
  `email` varchar(100) DEFAULT NULL COMMENT '评论人的邮箱地址（未登录用户）',
  `url` varchar(200) DEFAULT NULL COMMENT '评论人的网站地址（未登录用户）',
  `status` tinyint(1) DEFAULT '0' COMMENT '评论的状态',
  `ip` varchar(64) DEFAULT NULL COMMENT '评论时的ip',
  `lng` varchar(50) DEFAULT NULL COMMENT '经度',
  `lat` varchar(50) DEFAULT NULL COMMENT '纬度',
  `address` varchar(100) DEFAULT NULL COMMENT '评论时的地址',
  `os` varchar(64) DEFAULT NULL COMMENT '评论时的系统类型',
  `os_short_name` varchar(10) DEFAULT NULL COMMENT '评论时的系统的简称',
  `browser` varchar(64) DEFAULT NULL COMMENT '评论时的浏览器类型',
  `browser_short_name` varchar(10) DEFAULT NULL COMMENT '评论时的浏览器的简称',
  `content` varchar(2000) DEFAULT NULL COMMENT '评论的内容',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注（审核不通过时添加）',
  `support` int(10) unsigned DEFAULT '0' COMMENT '支持（赞）',
  `oppose` int(10) unsigned DEFAULT '0' COMMENT '反对（踩）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_comment
-- ----------------------------

-- ----------------------------
-- Table structure for biz_link
-- ----------------------------
DROP TABLE IF EXISTS `biz_link`;
CREATE TABLE `biz_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '链接名',
  `url` varchar(200) NOT NULL COMMENT '链接地址',
  `description` varchar(255) DEFAULT NULL COMMENT '链接介绍',
  `img` varchar(255) DEFAULT NULL COMMENT '友链图片地址',
  `email` varchar(100) DEFAULT NULL COMMENT '友链站长邮箱',
  `qq` varchar(50) DEFAULT NULL COMMENT '友链站长qq',
  `status` int(1) unsigned DEFAULT NULL COMMENT '状态',
  `origin` int(1) DEFAULT NULL COMMENT '1-管理员添加 2-自助申请',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='友情连接';

-- ----------------------------
-- Records of biz_link
-- ----------------------------
INSERT INTO `biz_link` VALUES ('1', '青创无忧', 'http://www.qdqcwy.cn', 'Just do it！', null, null, null, '1', '1', '', '2018-09-13 23:24:25', '2018-09-13 23:24:25');

-- ----------------------------
-- Table structure for biz_love
-- ----------------------------
DROP TABLE IF EXISTS `biz_love`;
CREATE TABLE `biz_love` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `biz_id` int(11) unsigned NOT NULL COMMENT '业务ID',
  `biz_type` tinyint(1) DEFAULT NULL COMMENT '业务类型：1.文章，2.评论',
  `user_id` varchar(20) DEFAULT NULL COMMENT '已登录用户ID',
  `user_ip` varchar(50) DEFAULT NULL COMMENT '用户IP',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_love
-- ----------------------------
INSERT INTO `biz_love` VALUES ('1', '1', '1', null, '0:0:0:0:0:0:0:1', '1', '2018-09-13 23:31:53', '2018-09-13 23:31:53');

-- ----------------------------
-- Table structure for biz_search_history
-- ----------------------------
DROP TABLE IF EXISTS `biz_search_history`;
CREATE TABLE `biz_search_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip` varchar(20) DEFAULT NULL COMMENT '来源',
  `keyword` varchar(50) DEFAULT NULL COMMENT '创建人',
  `search_time` datetime DEFAULT NULL COMMENT '搜索时间',
  `creator` varchar(20) DEFAULT NULL COMMENT '创建人',
  `updator` varchar(20) DEFAULT NULL COMMENT '修改人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_delete` char(1) DEFAULT 'N' COMMENT '是否删除（Y是 N否）',
  `source_type` varchar(10) DEFAULT '10' COMMENT '10:pc 20: wap',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1077 DEFAULT CHARSET=utf8 COMMENT='app搜索历史表';

-- ----------------------------
-- Records of biz_search_history
-- ----------------------------
INSERT INTO `biz_search_history` VALUES ('1069', '0:0:0:0:0:0:0:1', '病毒', '2021-03-16 14:13:43', '999', null, '2021-03-16 14:13:43', null, 'N', '10');
INSERT INTO `biz_search_history` VALUES ('1070', '0:0:0:0:0:0:0:1', '病毒', '2021-03-16 14:14:40', '999', null, '2021-03-16 14:14:40', null, 'N', '10');
INSERT INTO `biz_search_history` VALUES ('1071', '0:0:0:0:0:0:0:1', '病毒', '2021-03-16 14:15:10', '999', null, '2021-03-16 14:15:10', null, 'N', '10');
INSERT INTO `biz_search_history` VALUES ('1072', '0:0:0:0:0:0:0:1', '病毒', '2021-03-16 14:18:44', '999', null, '2021-03-16 14:18:44', null, 'N', '10');
INSERT INTO `biz_search_history` VALUES ('1073', '0:0:0:0:0:0:0:1', '软件', '2021-03-16 14:19:14', '999', null, '2021-03-16 14:19:14', null, 'N', '10');
INSERT INTO `biz_search_history` VALUES ('1074', '0:0:0:0:0:0:0:1', 'aaa', '2021-03-16 14:21:11', '999', null, '2021-03-16 14:21:11', null, 'N', '10');
INSERT INTO `biz_search_history` VALUES ('1075', '0:0:0:0:0:0:0:1', '软件', '2021-03-16 14:25:52', '999', null, '2021-03-16 14:25:52', null, 'N', '10');
INSERT INTO `biz_search_history` VALUES ('1076', '116.233.178.16', '病毒', '2021-03-18 15:02:54', '999', null, '2021-03-18 15:02:54', null, 'N', '10');

-- ----------------------------
-- Table structure for biz_site_info
-- ----------------------------
DROP TABLE IF EXISTS `biz_site_info`;
CREATE TABLE `biz_site_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) DEFAULT NULL,
  `site_desc` varchar(255) DEFAULT NULL,
  `site_pic` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='友情连接';

-- ----------------------------
-- Records of biz_site_info
-- ----------------------------
INSERT INTO `biz_site_info` VALUES ('1', 'PUBOOT', '热爱编程，热爱学习，喜欢分享，少走弯路。', 'https://tse1-mm.cn.bing.net/th/id/OIP.Ups1Z8igjNjLuDfO38XhTgHaHa?pid=Api&rs=1', '2018-07-22 22:13:26', '2018-07-22 22:13:29');

-- ----------------------------
-- Table structure for biz_tags
-- ----------------------------
DROP TABLE IF EXISTS `biz_tags`;
CREATE TABLE `biz_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '书签名',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='网站标签';

-- ----------------------------
-- Records of biz_tags
-- ----------------------------
INSERT INTO `biz_tags` VALUES ('1', '教育', '111', '2018-01-14 21:35:31', '2021-03-15 14:52:04');
INSERT INTO `biz_tags` VALUES ('11', '关怀', '关怀', '2021-03-15 14:52:17', '2021-03-15 14:52:17');

-- ----------------------------
-- Table structure for biz_theme
-- ----------------------------
DROP TABLE IF EXISTS `biz_theme`;
CREATE TABLE `biz_theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
  `name` varchar(50) DEFAULT NULL COMMENT '主题名（路径前缀）',
  `description` varchar(255) DEFAULT NULL COMMENT '主题描述',
  `img` varchar(255) DEFAULT NULL COMMENT '主题预览图url',
  `status` tinyint(1) DEFAULT NULL COMMENT '0-未启用 1-启用',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='网站主题表';

-- ----------------------------
-- Records of biz_theme
-- ----------------------------
INSERT INTO `biz_theme` VALUES ('1', 'pblog', '默认主题', '', '1', '2018-09-19 15:50:45', '2018-09-19 15:50:45');

-- ----------------------------
-- Table structure for clientdetails
-- ----------------------------
DROP TABLE IF EXISTS `clientdetails`;
CREATE TABLE `clientdetails` (
  `appId` varchar(128) COLLATE utf8_bin NOT NULL,
  `resourceIds` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `appSecret` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `scope` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `grantTypes` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `redirectUrl` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `authorities` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additionalInformation` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  `autoApproveScopes` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`appId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of clientdetails
-- ----------------------------

-- ----------------------------
-- Table structure for client_user
-- ----------------------------
DROP TABLE IF EXISTS `client_user`;
CREATE TABLE `client_user` (
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL,
  `salt` varchar(128) DEFAULT NULL COMMENT '加密盐值',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `realname` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `sex` int(255) DEFAULT NULL COMMENT '性别：1男2女',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `img` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `active` int(1) DEFAULT '0' COMMENT '是否激活 0:否 1:是',
  `status` int(1) NOT NULL COMMENT '用户状态：1有效; 2删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  UNIQUE KEY `user_user_id_uindex` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网站用户表';

-- ----------------------------
-- Records of client_user
-- ----------------------------
INSERT INTO `client_user` VALUES ('1000000683604852', 'superstar', '46c9bff8a55fff7c5642e5873b151f32', '1a170af0c173061a6c1465b154055bf7', '秋风', '王大锤', '151415241@qq.com', '147258369', '1', null, null, '1', '1', '2021-03-18 14:32:27', '2021-03-18 14:32:27', '2021-03-18 15:48:08');

-- ----------------------------
-- Table structure for login_history
-- ----------------------------
DROP TABLE IF EXISTS `login_history`;
CREATE TABLE `login_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `status` int(1) NOT NULL COMMENT '状态：1有效; 2删除',
  `type` int(1) NOT NULL COMMENT '类型：1后台; 2 前台',
  `login_ip_address` varchar(100) DEFAULT NULL COMMENT '登录IP',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=utf8 COMMENT='用户登录历史';

-- ----------------------------
-- Records of login_history
-- ----------------------------
INSERT INTO `login_history` VALUES ('6', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-17 17:40:11', null, '2021-03-17 17:40:11');
INSERT INTO `login_history` VALUES ('7', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-17 17:48:30', null, '2021-03-17 17:48:30');
INSERT INTO `login_history` VALUES ('8', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-17 18:19:23', null, '2021-03-17 18:19:23');
INSERT INTO `login_history` VALUES ('9', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-17 18:20:14', null, '2021-03-17 18:20:14');
INSERT INTO `login_history` VALUES ('10', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-17 18:32:11', null, '2021-03-17 18:32:11');
INSERT INTO `login_history` VALUES ('11', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-17 21:53:42', null, '2021-03-17 21:53:42');
INSERT INTO `login_history` VALUES ('12', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 09:00:53', null, '2021-03-18 09:00:53');
INSERT INTO `login_history` VALUES ('13', '1', 'admin', '1', '1', '223.104.212.228', '2021-03-18 09:05:47', null, '2021-03-18 09:05:47');
INSERT INTO `login_history` VALUES ('14', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 09:35:05', null, '2021-03-18 09:35:05');
INSERT INTO `login_history` VALUES ('15', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 09:35:39', null, '2021-03-18 09:35:39');
INSERT INTO `login_history` VALUES ('16', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 10:13:43', null, '2021-03-18 10:13:43');
INSERT INTO `login_history` VALUES ('17', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 10:20:59', null, '2021-03-18 10:20:59');
INSERT INTO `login_history` VALUES ('18', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 10:32:33', null, '2021-03-18 10:32:33');
INSERT INTO `login_history` VALUES ('19', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 10:34:18', null, '2021-03-18 10:34:18');
INSERT INTO `login_history` VALUES ('20', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 10:37:52', null, '2021-03-18 10:37:51');
INSERT INTO `login_history` VALUES ('21', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 10:42:40', null, '2021-03-18 10:42:40');
INSERT INTO `login_history` VALUES ('22', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 10:47:01', null, '2021-03-18 10:47:01');
INSERT INTO `login_history` VALUES ('23', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 10:52:55', null, '2021-03-18 10:52:55');
INSERT INTO `login_history` VALUES ('24', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 11:12:40', null, '2021-03-18 11:12:40');
INSERT INTO `login_history` VALUES ('25', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 11:30:38', null, '2021-03-18 11:30:37');
INSERT INTO `login_history` VALUES ('26', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 11:31:19', null, '2021-03-18 11:31:19');
INSERT INTO `login_history` VALUES ('27', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 12:23:31', null, '2021-03-18 12:23:31');
INSERT INTO `login_history` VALUES ('28', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 13:03:42', null, '2021-03-18 13:03:42');
INSERT INTO `login_history` VALUES ('29', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 13:10:22', null, '2021-03-18 13:10:22');
INSERT INTO `login_history` VALUES ('30', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 13:17:55', null, '2021-03-18 13:17:55');
INSERT INTO `login_history` VALUES ('31', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 13:23:26', null, '2021-03-18 13:23:26');
INSERT INTO `login_history` VALUES ('32', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 13:44:23', null, '2021-03-18 13:44:23');
INSERT INTO `login_history` VALUES ('33', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 14:32:43', null, '2021-03-18 14:32:43');
INSERT INTO `login_history` VALUES ('34', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 14:34:38', null, '2021-03-18 14:34:38');
INSERT INTO `login_history` VALUES ('35', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 14:35:42', null, '2021-03-18 14:35:42');
INSERT INTO `login_history` VALUES ('36', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 14:53:57', null, '2021-03-18 14:53:57');
INSERT INTO `login_history` VALUES ('37', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 14:55:21', null, '2021-03-18 14:55:21');
INSERT INTO `login_history` VALUES ('38', '1000000720175301', 'apple', '1', '1', '10.32.10.88', '2021-03-18 14:55:42', null, '2021-03-18 14:55:42');
INSERT INTO `login_history` VALUES ('39', '1000000720175301', 'apple', '1', '1', '10.32.10.88', '2021-03-18 14:56:10', null, '2021-03-18 14:56:10');
INSERT INTO `login_history` VALUES ('40', '1000000720175301', 'apple', '1', '1', '10.32.10.88', '2021-03-18 14:58:15', null, '2021-03-18 14:58:15');
INSERT INTO `login_history` VALUES ('41', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 15:22:25', null, '2021-03-18 15:22:25');
INSERT INTO `login_history` VALUES ('42', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 15:30:15', null, '2021-03-18 15:30:15');
INSERT INTO `login_history` VALUES ('43', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 15:31:35', null, '2021-03-18 15:31:34');
INSERT INTO `login_history` VALUES ('44', '1000000683604852', 'superstar', '1', '2', '127.0.0.1', '2021-03-18 15:48:09', null, '2021-03-18 15:48:08');
INSERT INTO `login_history` VALUES ('45', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:00:33', null, '2021-03-18 16:00:33');
INSERT INTO `login_history` VALUES ('46', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:10:39', null, '2021-03-18 16:10:39');
INSERT INTO `login_history` VALUES ('47', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:11:08', null, '2021-03-18 16:11:08');
INSERT INTO `login_history` VALUES ('48', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:21:50', null, '2021-03-18 16:21:50');
INSERT INTO `login_history` VALUES ('49', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:29:44', null, '2021-03-18 16:29:44');
INSERT INTO `login_history` VALUES ('50', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:29:54', null, '2021-03-18 16:29:54');
INSERT INTO `login_history` VALUES ('51', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:33:18', null, '2021-03-18 16:33:18');
INSERT INTO `login_history` VALUES ('52', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:33:50', null, '2021-03-18 16:33:50');
INSERT INTO `login_history` VALUES ('53', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:34:00', null, '2021-03-18 16:34:00');
INSERT INTO `login_history` VALUES ('54', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:50:42', null, '2021-03-18 16:50:42');
INSERT INTO `login_history` VALUES ('55', '1', 'admin', '1', '1', '116.233.178.16', '2021-03-18 16:51:02', null, '2021-03-18 16:51:02');
INSERT INTO `login_history` VALUES ('56', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:51:06', null, '2021-03-18 16:51:06');
INSERT INTO `login_history` VALUES ('57', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:51:34', null, '2021-03-18 16:51:34');
INSERT INTO `login_history` VALUES ('58', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:52:24', null, '2021-03-18 16:52:24');
INSERT INTO `login_history` VALUES ('59', '1', 'admin', '1', '1', '117.136.120.83', '2021-03-18 16:54:16', null, '2021-03-18 16:54:16');
INSERT INTO `login_history` VALUES ('60', '1', 'admin', '1', '1', '223.104.212.228', '2021-03-18 16:54:31', null, '2021-03-18 16:54:31');
INSERT INTO `login_history` VALUES ('61', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:55:51', null, '2021-03-18 16:55:51');
INSERT INTO `login_history` VALUES ('62', '1000000720175301', 'apple', '1', '1', '124.74.105.114', '2021-03-18 16:56:10', null, '2021-03-18 16:56:10');
INSERT INTO `login_history` VALUES ('63', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:56:30', null, '2021-03-18 16:56:29');
INSERT INTO `login_history` VALUES ('64', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:57:41', null, '2021-03-18 16:57:41');
INSERT INTO `login_history` VALUES ('65', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:58:23', null, '2021-03-18 16:58:23');
INSERT INTO `login_history` VALUES ('66', '1', 'admin', '1', '1', '116.233.178.16', '2021-03-18 16:58:31', null, '2021-03-18 16:58:31');
INSERT INTO `login_history` VALUES ('67', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 16:59:50', null, '2021-03-18 16:59:50');
INSERT INTO `login_history` VALUES ('68', '1', 'admin', '1', '1', '116.233.178.16', '2021-03-18 17:00:27', null, '2021-03-18 17:00:27');
INSERT INTO `login_history` VALUES ('69', '1', 'admin', '1', '1', '223.104.212.228', '2021-03-18 17:02:23', null, '2021-03-18 17:02:23');
INSERT INTO `login_history` VALUES ('70', '1', 'admin', '1', '1', '116.233.178.16', '2021-03-18 17:02:37', null, '2021-03-18 17:02:36');
INSERT INTO `login_history` VALUES ('71', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 17:17:29', null, '2021-03-18 17:17:29');
INSERT INTO `login_history` VALUES ('72', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 18:00:41', null, '2021-03-18 18:00:41');
INSERT INTO `login_history` VALUES ('73', '1', 'admin', '1', '1', '124.74.105.114', '2021-03-18 18:00:53', null, '2021-03-18 18:00:53');
INSERT INTO `login_history` VALUES ('74', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 18:07:13', null, '2021-03-18 18:07:13');
INSERT INTO `login_history` VALUES ('75', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:07:45', null, '2021-03-18 18:07:45');
INSERT INTO `login_history` VALUES ('76', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 18:07:47', null, '2021-03-18 18:07:47');
INSERT INTO `login_history` VALUES ('77', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:08:06', null, '2021-03-18 18:08:06');
INSERT INTO `login_history` VALUES ('78', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:09:27', null, '2021-03-18 18:09:27');
INSERT INTO `login_history` VALUES ('79', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:17:53', null, '2021-03-18 18:17:53');
INSERT INTO `login_history` VALUES ('80', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:17:53', null, '2021-03-18 18:17:53');
INSERT INTO `login_history` VALUES ('81', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:18:33', null, '2021-03-18 18:18:32');
INSERT INTO `login_history` VALUES ('82', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:27:39', null, '2021-03-18 18:27:39');
INSERT INTO `login_history` VALUES ('83', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:27:39', null, '2021-03-18 18:27:39');
INSERT INTO `login_history` VALUES ('84', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:27:39', null, '2021-03-18 18:27:39');
INSERT INTO `login_history` VALUES ('85', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:27:55', null, '2021-03-18 18:27:55');
INSERT INTO `login_history` VALUES ('86', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:28:48', null, '2021-03-18 18:28:48');
INSERT INTO `login_history` VALUES ('87', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:29:27', null, '2021-03-18 18:29:27');
INSERT INTO `login_history` VALUES ('88', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:32:14', null, '2021-03-18 18:32:14');
INSERT INTO `login_history` VALUES ('89', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 18:33:11', null, '2021-03-18 18:33:11');
INSERT INTO `login_history` VALUES ('90', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:33:54', null, '2021-03-18 18:33:54');
INSERT INTO `login_history` VALUES ('91', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 18:51:50', null, '2021-03-18 18:51:49');
INSERT INTO `login_history` VALUES ('92', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:52:01', null, '2021-03-18 18:52:01');
INSERT INTO `login_history` VALUES ('93', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-18 18:54:19', null, '2021-03-18 18:54:19');
INSERT INTO `login_history` VALUES ('94', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-18 18:54:33', null, '2021-03-18 18:54:33');
INSERT INTO `login_history` VALUES ('95', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 20:26:17', null, '2021-03-18 20:26:17');
INSERT INTO `login_history` VALUES ('96', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 20:26:39', null, '2021-03-18 20:26:38');
INSERT INTO `login_history` VALUES ('97', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 21:01:58', null, '2021-03-18 21:01:58');
INSERT INTO `login_history` VALUES ('98', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 21:02:12', null, '2021-03-18 21:02:12');
INSERT INTO `login_history` VALUES ('99', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 21:22:40', null, '2021-03-18 21:22:40');
INSERT INTO `login_history` VALUES ('100', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 21:27:52', null, '2021-03-18 21:27:52');
INSERT INTO `login_history` VALUES ('101', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 21:30:18', null, '2021-03-18 21:30:18');
INSERT INTO `login_history` VALUES ('102', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 21:32:43', null, '2021-03-18 21:32:42');
INSERT INTO `login_history` VALUES ('103', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-18 21:38:56', null, '2021-03-18 21:38:56');
INSERT INTO `login_history` VALUES ('104', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 22:02:43', null, '2021-03-18 22:02:43');
INSERT INTO `login_history` VALUES ('105', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 22:06:46', null, '2021-03-18 22:06:46');
INSERT INTO `login_history` VALUES ('106', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 22:15:16', null, '2021-03-18 22:15:15');
INSERT INTO `login_history` VALUES ('107', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 22:15:24', null, '2021-03-18 22:15:24');
INSERT INTO `login_history` VALUES ('108', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 22:20:45', null, '2021-03-18 22:20:45');
INSERT INTO `login_history` VALUES ('109', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-18 22:21:00', null, '2021-03-18 22:21:00');
INSERT INTO `login_history` VALUES ('110', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-18 22:26:08', null, '2021-03-18 22:26:08');
INSERT INTO `login_history` VALUES ('111', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-18 22:26:25', null, '2021-03-18 22:26:25');
INSERT INTO `login_history` VALUES ('112', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-18 22:30:07', null, '2021-03-18 22:30:07');
INSERT INTO `login_history` VALUES ('113', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-18 22:30:24', null, '2021-03-18 22:30:24');
INSERT INTO `login_history` VALUES ('114', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-18 22:30:50', null, '2021-03-18 22:30:50');
INSERT INTO `login_history` VALUES ('115', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-18 22:31:52', null, '2021-03-18 22:31:52');
INSERT INTO `login_history` VALUES ('116', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 08:46:12', null, '2021-03-19 08:46:12');
INSERT INTO `login_history` VALUES ('117', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 08:56:23', null, '2021-03-19 08:56:23');
INSERT INTO `login_history` VALUES ('118', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 08:57:25', null, '2021-03-19 08:57:25');
INSERT INTO `login_history` VALUES ('119', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 09:26:06', null, '2021-03-19 09:26:06');
INSERT INTO `login_history` VALUES ('120', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:26:23', null, '2021-03-19 09:26:22');
INSERT INTO `login_history` VALUES ('121', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 09:34:29', null, '2021-03-19 09:34:29');
INSERT INTO `login_history` VALUES ('122', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:34:47', null, '2021-03-19 09:34:47');
INSERT INTO `login_history` VALUES ('123', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:37:19', null, '2021-03-19 09:37:19');
INSERT INTO `login_history` VALUES ('124', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 09:37:32', null, '2021-03-19 09:37:32');
INSERT INTO `login_history` VALUES ('125', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:38:45', null, '2021-03-19 09:38:45');
INSERT INTO `login_history` VALUES ('126', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 09:39:12', null, '2021-03-19 09:39:12');
INSERT INTO `login_history` VALUES ('127', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:39:41', null, '2021-03-19 09:39:41');
INSERT INTO `login_history` VALUES ('128', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:40:19', null, '2021-03-19 09:40:19');
INSERT INTO `login_history` VALUES ('129', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 09:40:45', null, '2021-03-19 09:40:45');
INSERT INTO `login_history` VALUES ('130', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:46:10', null, '2021-03-19 09:46:10');
INSERT INTO `login_history` VALUES ('131', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:53:39', null, '2021-03-19 09:53:39');
INSERT INTO `login_history` VALUES ('132', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 09:54:09', null, '2021-03-19 09:54:09');
INSERT INTO `login_history` VALUES ('133', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:54:35', null, '2021-03-19 09:54:35');
INSERT INTO `login_history` VALUES ('134', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:54:57', null, '2021-03-19 09:54:57');
INSERT INTO `login_history` VALUES ('135', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:55:02', null, '2021-03-19 09:55:02');
INSERT INTO `login_history` VALUES ('136', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 09:55:10', null, '2021-03-19 09:55:10');
INSERT INTO `login_history` VALUES ('137', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 09:55:12', null, '2021-03-19 09:55:12');
INSERT INTO `login_history` VALUES ('138', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:57:13', null, '2021-03-19 09:57:13');
INSERT INTO `login_history` VALUES ('139', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 09:57:27', null, '2021-03-19 09:57:27');
INSERT INTO `login_history` VALUES ('140', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 09:59:56', null, '2021-03-19 09:59:56');
INSERT INTO `login_history` VALUES ('141', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 10:05:59', null, '2021-03-19 10:05:59');
INSERT INTO `login_history` VALUES ('142', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 10:05:59', null, '2021-03-19 10:05:59');
INSERT INTO `login_history` VALUES ('143', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 10:11:19', null, '2021-03-19 10:11:19');
INSERT INTO `login_history` VALUES ('144', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:12:29', null, '2021-03-19 10:12:29');
INSERT INTO `login_history` VALUES ('145', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 10:21:40', null, '2021-03-19 10:21:40');
INSERT INTO `login_history` VALUES ('146', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 10:22:52', null, '2021-03-19 10:22:52');
INSERT INTO `login_history` VALUES ('147', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:25:50', null, '2021-03-19 10:25:50');
INSERT INTO `login_history` VALUES ('148', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 10:26:07', null, '2021-03-19 10:26:07');
INSERT INTO `login_history` VALUES ('149', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:27:08', null, '2021-03-19 10:27:08');
INSERT INTO `login_history` VALUES ('150', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 10:27:16', null, '2021-03-19 10:27:16');
INSERT INTO `login_history` VALUES ('151', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:28:26', null, '2021-03-19 10:28:26');
INSERT INTO `login_history` VALUES ('152', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 10:29:07', null, '2021-03-19 10:29:06');
INSERT INTO `login_history` VALUES ('153', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 10:30:24', null, '2021-03-19 10:30:24');
INSERT INTO `login_history` VALUES ('154', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:30:57', null, '2021-03-19 10:30:57');
INSERT INTO `login_history` VALUES ('155', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:32:01', null, '2021-03-19 10:32:01');
INSERT INTO `login_history` VALUES ('156', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 10:32:46', null, '2021-03-19 10:32:46');
INSERT INTO `login_history` VALUES ('157', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 10:34:47', null, '2021-03-19 10:34:47');
INSERT INTO `login_history` VALUES ('158', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:35:04', null, '2021-03-19 10:35:04');
INSERT INTO `login_history` VALUES ('159', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 10:36:58', null, '2021-03-19 10:36:58');
INSERT INTO `login_history` VALUES ('160', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:51:17', null, '2021-03-19 10:51:17');
INSERT INTO `login_history` VALUES ('161', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 10:51:35', null, '2021-03-19 10:51:35');
INSERT INTO `login_history` VALUES ('162', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:52:09', null, '2021-03-19 10:52:09');
INSERT INTO `login_history` VALUES ('163', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:55:11', null, '2021-03-19 10:55:11');
INSERT INTO `login_history` VALUES ('164', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 10:55:24', null, '2021-03-19 10:55:24');
INSERT INTO `login_history` VALUES ('165', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 10:55:54', null, '2021-03-19 10:55:54');
INSERT INTO `login_history` VALUES ('166', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 11:00:51', null, '2021-03-19 11:00:51');
INSERT INTO `login_history` VALUES ('167', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:00:59', null, '2021-03-19 11:00:59');
INSERT INTO `login_history` VALUES ('168', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:08:37', null, '2021-03-19 11:08:37');
INSERT INTO `login_history` VALUES ('169', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 11:08:49', null, '2021-03-19 11:08:49');
INSERT INTO `login_history` VALUES ('170', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 11:10:24', null, '2021-03-19 11:10:24');
INSERT INTO `login_history` VALUES ('171', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:10:30', null, '2021-03-19 11:10:30');
INSERT INTO `login_history` VALUES ('172', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 11:10:49', null, '2021-03-19 11:10:49');
INSERT INTO `login_history` VALUES ('173', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:10:51', null, '2021-03-19 11:10:51');
INSERT INTO `login_history` VALUES ('174', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 11:11:42', null, '2021-03-19 11:11:42');
INSERT INTO `login_history` VALUES ('175', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 11:11:56', null, '2021-03-19 11:11:56');
INSERT INTO `login_history` VALUES ('176', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:12:59', null, '2021-03-19 11:12:59');
INSERT INTO `login_history` VALUES ('177', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:13:02', null, '2021-03-19 11:13:02');
INSERT INTO `login_history` VALUES ('178', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:13:16', null, '2021-03-19 11:13:16');
INSERT INTO `login_history` VALUES ('179', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:13:19', null, '2021-03-19 11:13:19');
INSERT INTO `login_history` VALUES ('180', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 11:15:29', null, '2021-03-19 11:15:29');
INSERT INTO `login_history` VALUES ('181', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:15:40', null, '2021-03-19 11:15:40');
INSERT INTO `login_history` VALUES ('182', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 11:16:45', null, '2021-03-19 11:16:45');
INSERT INTO `login_history` VALUES ('183', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:16:57', null, '2021-03-19 11:16:57');
INSERT INTO `login_history` VALUES ('184', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 11:17:55', null, '2021-03-19 11:17:55');
INSERT INTO `login_history` VALUES ('185', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:18:07', null, '2021-03-19 11:18:07');
INSERT INTO `login_history` VALUES ('186', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 11:18:33', null, '2021-03-19 11:18:33');
INSERT INTO `login_history` VALUES ('187', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:18:45', null, '2021-03-19 11:18:45');
INSERT INTO `login_history` VALUES ('188', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 11:21:10', null, '2021-03-19 11:21:10');
INSERT INTO `login_history` VALUES ('189', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:21:21', null, '2021-03-19 11:21:21');
INSERT INTO `login_history` VALUES ('190', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 11:22:59', null, '2021-03-19 11:22:59');
INSERT INTO `login_history` VALUES ('191', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 11:23:10', null, '2021-03-19 11:23:10');
INSERT INTO `login_history` VALUES ('192', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 12:25:50', null, '2021-03-19 12:25:50');
INSERT INTO `login_history` VALUES ('193', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 12:26:06', null, '2021-03-19 12:26:06');
INSERT INTO `login_history` VALUES ('194', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 12:26:26', null, '2021-03-19 12:26:26');
INSERT INTO `login_history` VALUES ('195', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 12:29:22', null, '2021-03-19 12:29:22');
INSERT INTO `login_history` VALUES ('196', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 12:36:38', null, '2021-03-19 12:36:37');
INSERT INTO `login_history` VALUES ('197', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 12:39:51', null, '2021-03-19 12:39:51');
INSERT INTO `login_history` VALUES ('198', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 12:40:13', null, '2021-03-19 12:40:13');
INSERT INTO `login_history` VALUES ('199', '1', 'admin', '1', '1', '10.32.10.88', '2021-03-19 12:40:40', null, '2021-03-19 12:40:40');
INSERT INTO `login_history` VALUES ('200', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 13:03:32', null, '2021-03-19 13:03:32');
INSERT INTO `login_history` VALUES ('201', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 13:04:07', null, '2021-03-19 13:04:07');
INSERT INTO `login_history` VALUES ('202', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 13:06:25', null, '2021-03-19 13:06:25');
INSERT INTO `login_history` VALUES ('203', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 13:06:29', null, '2021-03-19 13:06:28');
INSERT INTO `login_history` VALUES ('204', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 13:22:22', null, '2021-03-19 13:22:22');
INSERT INTO `login_history` VALUES ('205', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 13:22:33', null, '2021-03-19 13:22:33');
INSERT INTO `login_history` VALUES ('206', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 13:23:35', null, '2021-03-19 13:23:35');
INSERT INTO `login_history` VALUES ('207', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 13:23:45', null, '2021-03-19 13:23:45');
INSERT INTO `login_history` VALUES ('208', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 13:25:17', null, '2021-03-19 13:25:17');
INSERT INTO `login_history` VALUES ('209', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 13:25:33', null, '2021-03-19 13:25:33');
INSERT INTO `login_history` VALUES ('210', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 13:26:37', null, '2021-03-19 13:26:37');
INSERT INTO `login_history` VALUES ('211', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 13:26:54', null, '2021-03-19 13:26:54');
INSERT INTO `login_history` VALUES ('212', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 13:27:10', null, '2021-03-19 13:27:10');
INSERT INTO `login_history` VALUES ('213', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 13:28:47', null, '2021-03-19 13:28:47');
INSERT INTO `login_history` VALUES ('214', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 13:30:08', null, '2021-03-19 13:30:08');
INSERT INTO `login_history` VALUES ('215', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 13:30:24', null, '2021-03-19 13:30:24');
INSERT INTO `login_history` VALUES ('216', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 13:51:41', null, '2021-03-19 13:51:41');
INSERT INTO `login_history` VALUES ('217', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 13:51:58', null, '2021-03-19 13:51:58');
INSERT INTO `login_history` VALUES ('218', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 14:02:10', null, '2021-03-19 14:02:10');
INSERT INTO `login_history` VALUES ('219', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 14:08:22', null, '2021-03-19 14:08:21');
INSERT INTO `login_history` VALUES ('220', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 14:16:22', null, '2021-03-19 14:16:22');
INSERT INTO `login_history` VALUES ('221', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 14:17:17', null, '2021-03-19 14:17:17');
INSERT INTO `login_history` VALUES ('222', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 14:29:45', null, '2021-03-19 14:29:45');
INSERT INTO `login_history` VALUES ('223', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 14:30:16', null, '2021-03-19 14:30:15');
INSERT INTO `login_history` VALUES ('224', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 14:30:56', null, '2021-03-19 14:30:56');
INSERT INTO `login_history` VALUES ('225', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 14:31:13', null, '2021-03-19 14:31:13');
INSERT INTO `login_history` VALUES ('226', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 14:33:19', null, '2021-03-19 14:33:18');
INSERT INTO `login_history` VALUES ('227', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 14:33:35', null, '2021-03-19 14:33:34');
INSERT INTO `login_history` VALUES ('228', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 14:35:13', null, '2021-03-19 14:35:13');
INSERT INTO `login_history` VALUES ('229', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 14:57:11', null, '2021-03-19 14:57:11');
INSERT INTO `login_history` VALUES ('230', '1', 'admin', '1', '1', '116.233.178.16', '2021-03-19 14:59:40', null, '2021-03-19 14:59:39');
INSERT INTO `login_history` VALUES ('231', '1', 'admin', '1', '1', '116.233.178.16', '2021-03-19 15:14:06', null, '2021-03-19 15:14:06');
INSERT INTO `login_history` VALUES ('232', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:15:12', null, '2021-03-19 15:15:12');
INSERT INTO `login_history` VALUES ('233', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:15:41', null, '2021-03-19 15:15:41');
INSERT INTO `login_history` VALUES ('234', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:25:41', null, '2021-03-19 15:25:41');
INSERT INTO `login_history` VALUES ('235', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:41:57', null, '2021-03-19 15:41:57');
INSERT INTO `login_history` VALUES ('236', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:45:36', null, '2021-03-19 15:45:36');
INSERT INTO `login_history` VALUES ('237', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 15:46:39', null, '2021-03-19 15:46:39');
INSERT INTO `login_history` VALUES ('238', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:47:11', null, '2021-03-19 15:47:11');
INSERT INTO `login_history` VALUES ('239', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:48:55', null, '2021-03-19 15:48:55');
INSERT INTO `login_history` VALUES ('240', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:50:05', null, '2021-03-19 15:50:05');
INSERT INTO `login_history` VALUES ('241', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 15:50:16', null, '2021-03-19 15:50:16');
INSERT INTO `login_history` VALUES ('242', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:50:36', null, '2021-03-19 15:50:35');
INSERT INTO `login_history` VALUES ('243', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:51:06', null, '2021-03-19 15:51:06');
INSERT INTO `login_history` VALUES ('244', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:51:37', null, '2021-03-19 15:51:37');
INSERT INTO `login_history` VALUES ('245', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 15:52:11', null, '2021-03-19 15:52:11');
INSERT INTO `login_history` VALUES ('246', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:52:37', null, '2021-03-19 15:52:37');
INSERT INTO `login_history` VALUES ('247', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 15:52:57', null, '2021-03-19 15:52:57');
INSERT INTO `login_history` VALUES ('248', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:53:42', null, '2021-03-19 15:53:42');
INSERT INTO `login_history` VALUES ('249', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 15:54:16', null, '2021-03-19 15:54:16');
INSERT INTO `login_history` VALUES ('250', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 15:55:41', null, '2021-03-19 15:55:41');
INSERT INTO `login_history` VALUES ('251', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-19 15:56:04', null, '2021-03-19 15:56:04');
INSERT INTO `login_history` VALUES ('252', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:01:17', null, '2021-03-19 16:01:17');
INSERT INTO `login_history` VALUES ('253', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:15:24', null, '2021-03-19 16:15:24');
INSERT INTO `login_history` VALUES ('254', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:15:45', null, '2021-03-19 16:15:45');
INSERT INTO `login_history` VALUES ('255', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:16:41', null, '2021-03-19 16:16:40');
INSERT INTO `login_history` VALUES ('256', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:16:52', null, '2021-03-19 16:16:52');
INSERT INTO `login_history` VALUES ('257', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:36:25', null, '2021-03-19 16:36:25');
INSERT INTO `login_history` VALUES ('258', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:38:54', null, '2021-03-19 16:38:54');
INSERT INTO `login_history` VALUES ('259', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:39:57', null, '2021-03-19 16:39:57');
INSERT INTO `login_history` VALUES ('260', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:40:35', null, '2021-03-19 16:40:35');
INSERT INTO `login_history` VALUES ('261', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:40:55', null, '2021-03-19 16:40:55');
INSERT INTO `login_history` VALUES ('262', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:41:38', null, '2021-03-19 16:41:38');
INSERT INTO `login_history` VALUES ('263', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:42:16', null, '2021-03-19 16:42:16');
INSERT INTO `login_history` VALUES ('264', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:42:59', null, '2021-03-19 16:42:59');
INSERT INTO `login_history` VALUES ('265', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 16:43:21', null, '2021-03-19 16:43:21');
INSERT INTO `login_history` VALUES ('266', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 18:30:41', null, '2021-03-19 18:30:40');
INSERT INTO `login_history` VALUES ('267', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 18:30:54', null, '2021-03-19 18:30:53');
INSERT INTO `login_history` VALUES ('268', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-19 18:31:14', null, '2021-03-19 18:31:14');
INSERT INTO `login_history` VALUES ('269', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-20 08:40:22', null, '2021-03-20 08:40:21');
INSERT INTO `login_history` VALUES ('270', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-20 08:41:38', null, '2021-03-20 08:41:38');
INSERT INTO `login_history` VALUES ('271', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-20 08:52:55', null, '2021-03-20 08:52:55');
INSERT INTO `login_history` VALUES ('272', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-20 08:57:27', null, '2021-03-20 08:57:27');
INSERT INTO `login_history` VALUES ('273', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-20 18:48:54', null, '2021-03-20 18:48:54');
INSERT INTO `login_history` VALUES ('274', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:08:29', null, '2021-03-26 13:08:28');
INSERT INTO `login_history` VALUES ('275', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:27:10', null, '2021-03-26 13:27:09');
INSERT INTO `login_history` VALUES ('276', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:27:12', null, '2021-03-26 13:27:12');
INSERT INTO `login_history` VALUES ('277', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:27:20', null, '2021-03-26 13:27:19');
INSERT INTO `login_history` VALUES ('278', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:27:20', null, '2021-03-26 13:27:20');
INSERT INTO `login_history` VALUES ('279', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:27:46', null, '2021-03-26 13:27:46');
INSERT INTO `login_history` VALUES ('280', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:27:49', null, '2021-03-26 13:27:49');
INSERT INTO `login_history` VALUES ('281', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:27:55', null, '2021-03-26 13:27:55');
INSERT INTO `login_history` VALUES ('282', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:28:20', null, '2021-03-26 13:28:20');
INSERT INTO `login_history` VALUES ('283', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:50:57', null, '2021-03-26 13:50:57');
INSERT INTO `login_history` VALUES ('284', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 13:50:59', null, '2021-03-26 13:50:59');
INSERT INTO `login_history` VALUES ('285', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:11:49', null, '2021-03-26 16:11:49');
INSERT INTO `login_history` VALUES ('286', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:14:19', null, '2021-03-26 16:14:19');
INSERT INTO `login_history` VALUES ('287', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:15:04', null, '2021-03-26 16:15:04');
INSERT INTO `login_history` VALUES ('288', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:15:48', null, '2021-03-26 16:15:48');
INSERT INTO `login_history` VALUES ('289', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:19:54', null, '2021-03-26 16:19:54');
INSERT INTO `login_history` VALUES ('290', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:21:06', null, '2021-03-26 16:21:06');
INSERT INTO `login_history` VALUES ('291', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:21:35', null, '2021-03-26 16:21:35');
INSERT INTO `login_history` VALUES ('292', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:23:14', null, '2021-03-26 16:23:13');
INSERT INTO `login_history` VALUES ('293', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:23:26', null, '2021-03-26 16:23:26');
INSERT INTO `login_history` VALUES ('294', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:24:09', null, '2021-03-26 16:24:09');
INSERT INTO `login_history` VALUES ('295', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:24:54', null, '2021-03-26 16:24:54');
INSERT INTO `login_history` VALUES ('296', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:25:25', null, '2021-03-26 16:25:25');
INSERT INTO `login_history` VALUES ('297', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:26:15', null, '2021-03-26 16:26:15');
INSERT INTO `login_history` VALUES ('298', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:27:50', null, '2021-03-26 16:27:50');
INSERT INTO `login_history` VALUES ('299', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:28:55', null, '2021-03-26 16:28:55');
INSERT INTO `login_history` VALUES ('300', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:30:27', null, '2021-03-26 16:30:27');
INSERT INTO `login_history` VALUES ('301', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:31:31', null, '2021-03-26 16:31:30');
INSERT INTO `login_history` VALUES ('302', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:32:22', null, '2021-03-26 16:32:22');
INSERT INTO `login_history` VALUES ('303', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:39:16', null, '2021-03-26 16:39:15');
INSERT INTO `login_history` VALUES ('304', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:39:58', null, '2021-03-26 16:39:58');
INSERT INTO `login_history` VALUES ('305', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:42:11', null, '2021-03-26 16:42:10');
INSERT INTO `login_history` VALUES ('306', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:42:49', null, '2021-03-26 16:42:49');
INSERT INTO `login_history` VALUES ('307', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:43:28', null, '2021-03-26 16:43:28');
INSERT INTO `login_history` VALUES ('308', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:44:55', null, '2021-03-26 16:44:55');
INSERT INTO `login_history` VALUES ('309', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:45:31', null, '2021-03-26 16:45:31');
INSERT INTO `login_history` VALUES ('310', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 16:49:16', null, '2021-03-26 16:49:16');
INSERT INTO `login_history` VALUES ('311', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 17:12:57', null, '2021-03-26 17:12:57');
INSERT INTO `login_history` VALUES ('312', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 17:13:23', null, '2021-03-26 17:13:23');
INSERT INTO `login_history` VALUES ('313', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 18:15:29', null, '2021-03-26 18:15:29');
INSERT INTO `login_history` VALUES ('314', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 18:18:38', null, '2021-03-26 18:18:38');
INSERT INTO `login_history` VALUES ('315', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-26 18:29:19', null, '2021-03-26 18:29:19');
INSERT INTO `login_history` VALUES ('316', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 04:15:33', null, '2021-03-27 04:15:33');
INSERT INTO `login_history` VALUES ('317', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 04:24:25', null, '2021-03-27 04:24:24');
INSERT INTO `login_history` VALUES ('318', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 04:24:27', null, '2021-03-27 04:24:27');
INSERT INTO `login_history` VALUES ('319', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 04:25:34', null, '2021-03-27 04:25:33');
INSERT INTO `login_history` VALUES ('320', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 04:25:36', null, '2021-03-27 04:25:36');
INSERT INTO `login_history` VALUES ('321', '1', 'admin', '1', '1', '127.0.0.1', '2021-03-27 07:01:42', null, '2021-03-27 07:01:42');
INSERT INTO `login_history` VALUES ('322', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 07:17:43', null, '2021-03-27 07:17:43');
INSERT INTO `login_history` VALUES ('323', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 07:18:03', null, '2021-03-27 07:18:03');
INSERT INTO `login_history` VALUES ('324', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 07:46:53', null, '2021-03-27 07:46:53');
INSERT INTO `login_history` VALUES ('325', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 07:48:45', null, '2021-03-27 07:48:45');
INSERT INTO `login_history` VALUES ('326', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 10:13:44', null, '2021-03-27 10:13:44');
INSERT INTO `login_history` VALUES ('327', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 10:14:58', null, '2021-03-27 10:14:58');
INSERT INTO `login_history` VALUES ('328', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 10:15:33', null, '2021-03-27 10:15:33');
INSERT INTO `login_history` VALUES ('329', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 10:16:50', null, '2021-03-27 10:16:50');
INSERT INTO `login_history` VALUES ('330', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 10:17:36', null, '2021-03-27 10:17:36');
INSERT INTO `login_history` VALUES ('331', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 10:18:09', null, '2021-03-27 10:18:09');
INSERT INTO `login_history` VALUES ('332', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 10:18:47', null, '2021-03-27 10:18:47');
INSERT INTO `login_history` VALUES ('333', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 10:19:18', null, '2021-03-27 10:19:17');
INSERT INTO `login_history` VALUES ('334', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 10:30:19', null, '2021-03-27 10:30:19');
INSERT INTO `login_history` VALUES ('335', '1', 'admin', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 11:08:20', null, '2021-03-27 11:08:20');
INSERT INTO `login_history` VALUES ('336', '1000000720175301', 'apple', '1', '1', '0:0:0:0:0:0:0:1', '2021-03-27 11:10:00', null, '2021-03-27 11:10:00');

-- ----------------------------
-- Table structure for oauth_access_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_token`;
CREATE TABLE `oauth_access_token` (
  `token_id` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `user_name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `client_id` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `authentication` blob,
  `refresh_token` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of oauth_access_token
-- ----------------------------
INSERT INTO `oauth_access_token` VALUES ('1db7a97cea9f3cf1b889f76311d7a3a7', 0xACED0005737200436F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F4175746832416363657373546F6B656E0CB29E361B24FACE0200064C00156164646974696F6E616C496E666F726D6174696F6E74000F4C6A6176612F7574696C2F4D61703B4C000A65787069726174696F6E7400104C6A6176612F7574696C2F446174653B4C000C72656672657368546F6B656E74003F4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F636F6D6D6F6E2F4F417574683252656672657368546F6B656E3B4C000573636F706574000F4C6A6176612F7574696C2F5365743B4C0009746F6B656E547970657400124C6A6176612F6C616E672F537472696E673B4C000576616C756571007E000578707372001E6A6176612E7574696C2E436F6C6C656374696F6E7324456D7074794D6170593614855ADCE7D002000078707372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017858A11944787372004C6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744578706972696E674F417574683252656672657368546F6B656E2FDF47639DD0C9B70200014C000A65787069726174696F6E71007E0002787200446F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F417574683252656672657368546F6B656E73E10E0A6354D45E0200014C000576616C756571007E0005787074002434306434333138382D393765372D343531652D393530372D3264363030393338653634357371007E000977080000017858A1194278737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574801D92D18F9B80550200007872002C6A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65436F6C6C656374696F6E19420080CB5EF71E0200014C0001637400164C6A6176612F7574696C2F436F6C6C656374696F6E3B7870737200176A6176612E7574696C2E4C696E6B656448617368536574D86CD75A95DD2A1E020000787200116A6176612E7574696C2E48617368536574BA44859596B8B7340300007870770C000000023F400000000000017400067365727665727874000662656172657274002433626238313564372D336564332D343766612D623438312D623731376234626561626631, 'eb5e7c4ea617396ef2737ce168f857a0', 'user_7', 'client_2', 0xACED0005737200416F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F417574683241757468656E7469636174696F6EBD400B02166252130200024C000D73746F7265645265717565737474003C4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F4F4175746832526571756573743B4C00127573657241757468656E7469636174696F6E7400324C6F72672F737072696E676672616D65776F726B2F73656375726974792F636F72652F41757468656E7469636174696F6E3B787200476F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E416273747261637441757468656E7469636174696F6E546F6B656ED3AA287E6E47640E0200035A000D61757468656E746963617465644C000B617574686F7269746965737400164C6A6176612F7574696C2F436F6C6C656374696F6E3B4C000764657461696C737400124C6A6176612F6C616E672F4F626A6563743B787000737200266A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654C697374FC0F2531B5EC8E100200014C00046C6973747400104C6A6176612F7574696C2F4C6973743B7872002C6A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65436F6C6C656374696F6E19420080CB5EF71E0200014C00016371007E00047870737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A657870000000007704000000007871007E000C707372003A6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F41757468325265717565737400000000000000010200075A0008617070726F7665644C000B617574686F72697469657371007E00044C000A657874656E73696F6E7374000F4C6A6176612F7574696C2F4D61703B4C000B72656469726563745572697400124C6A6176612F6C616E672F537472696E673B4C00077265667265736874003B4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F546F6B656E526571756573743B4C000B7265736F7572636549647374000F4C6A6176612F7574696C2F5365743B4C000D726573706F6E7365547970657371007E0011787200386F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E426173655265717565737436287A3EA37169BD0200034C0008636C69656E74496471007E000F4C001172657175657374506172616D657465727371007E000E4C000573636F706571007E00117870740008636C69656E745F32737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654D6170F1A5A8FE74F507420200014C00016D71007E000E7870737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F400000000000067708000000080000000474000A6772616E745F7479706574000870617373776F7264740009636C69656E745F6964740008636C69656E745F3274000573636F7065740006736572766572740008757365726E616D65740006757365725F3778737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574801D92D18F9B80550200007871007E0009737200176A6176612E7574696C2E4C696E6B656448617368536574D86CD75A95DD2A1E020000787200116A6176612E7574696C2E48617368536574BA44859596B8B7340300007870770C000000103F4000000000000171007E001E78017371007E0024770C000000103F40000000000001737200426F72672E737072696E676672616D65776F726B2E73656375726974792E636F72652E617574686F726974792E53696D706C654772616E746564417574686F7269747900000000000001F40200014C0004726F6C6571007E000F78707400066F6175746832787371007E00173F40000000000000770800000010000000007870707371007E0024770C000000103F40000000000000787371007E0024770C000000103F40000000000000787372004F6F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E557365726E616D6550617373776F726441757468656E7469636174696F6E546F6B656E00000000000001F40200024C000B63726564656E7469616C7371007E00054C00097072696E636970616C71007E00057871007E0003017371007E00077371007E000B000000007704000000007871007E0030737200176A6176612E7574696C2E4C696E6B6564486173684D617034C04E5C106CC0FB0200015A000B6163636573734F726465727871007E00173F4000000000000C7708000000100000000571007E001971007E001A71007E001D71007E001E74000D636C69656E745F73656372657474000631323334353671007E001B71007E001C71007E001F71007E002078007073720028636F6D2E736572766963652E617574682E73657276696365617574682E656E746974792E557365728D26FC4843F38C1A0200044C000B617574686F72697469657371007E00084C000269647400104C6A6176612F6C616E672F4C6F6E673B4C000870617373776F726471007E000F4C0008757365726E616D6571007E000F78707372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E744261676563FD3F2082000C0200014C000362616771007E00087872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6EE9D3DC4DA9113BA502000A5A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65645A000D697354656D7053657373696F6E4C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E657271007E00054C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E003A787000FFFFFFFF0001007372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000671007E0037740034636F6D2E736572766963652E617574682E73657276696365617574682E656E746974792E557365722E617574686F726974696573707371007E000B00000000770400000000787371007E000B000000007704000000007871007E003E7400447B6263727970747D24326124313024614B435559504F57556B367370596B6C57785874592E383774466A7443796665642E4C6C525564465954534C776F43716D434A4B4F740006757365725F37, '6f518b3ac43d59370a8b3ce48e08614f');

-- ----------------------------
-- Table structure for oauth_approvals
-- ----------------------------
DROP TABLE IF EXISTS `oauth_approvals`;
CREATE TABLE `oauth_approvals` (
  `userId` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `clientId` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `scope` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `expiresAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of oauth_approvals
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
  `client_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `resource_ids` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `client_secret` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `scope` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `authorized_grant_types` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `web_server_redirect_uri` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `authorities` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  `autoapprove` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('client-app', '', '$2a$10$JSSOIXfvf.KRYSE3aVs6Q.FNaEQfk2PCCIbsvvy6QTUP1WHI831kC', 'all', 'password,refresh_token', '', 'oauth2', null, null, '', '');
INSERT INTO `oauth_client_details` VALUES ('client_1', null, '{bcrypt}$2a$10$HBX6q6TndkgMxhSEdoFqWOUtctaJEMoXe49NWh8Owc.4MTunv.wXa', 'select', 'client_credentials,refresh_token', null, 'oauth2', null, null, null, null);
INSERT INTO `oauth_client_details` VALUES ('client_2', '', '{bcrypt}$2a$10$HBX6q6TndkgMxhSEdoFqWOUtctaJEMoXe49NWh8Owc.4MTunv.wXa', 'server', 'password,refresh_token', null, 'oauth2', null, null, null, null);

-- ----------------------------
-- Table structure for oauth_client_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_token`;
CREATE TABLE `oauth_client_token` (
  `token_id` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `user_name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `client_id` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of oauth_client_token
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_code
-- ----------------------------
DROP TABLE IF EXISTS `oauth_code`;
CREATE TABLE `oauth_code` (
  `code` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of oauth_code
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_refresh_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_token`;
CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `token` blob,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of oauth_refresh_token
-- ----------------------------
INSERT INTO `oauth_refresh_token` VALUES ('9f2cee67a920975977405d5d6daf5b4d', 0xACED00057372004C6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744578706972696E674F417574683252656672657368546F6B656E2FDF47639DD0C9B70200014C000A65787069726174696F6E7400104C6A6176612F7574696C2F446174653B787200446F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F417574683252656672657368546F6B656E73E10E0A6354D45E0200014C000576616C75657400124C6A6176612F6C616E672F537472696E673B787074002464376661306338632D613934302D343033622D396136372D3564356436643132653964307372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001785442ACCE78, 0xACED0005737200416F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F417574683241757468656E7469636174696F6EBD400B02166252130200024C000D73746F7265645265717565737474003C4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F4F4175746832526571756573743B4C00127573657241757468656E7469636174696F6E7400324C6F72672F737072696E676672616D65776F726B2F73656375726974792F636F72652F41757468656E7469636174696F6E3B787200476F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E416273747261637441757468656E7469636174696F6E546F6B656ED3AA287E6E47640E0200035A000D61757468656E746963617465644C000B617574686F7269746965737400164C6A6176612F7574696C2F436F6C6C656374696F6E3B4C000764657461696C737400124C6A6176612F6C616E672F4F626A6563743B787000737200266A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654C697374FC0F2531B5EC8E100200014C00046C6973747400104C6A6176612F7574696C2F4C6973743B7872002C6A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65436F6C6C656374696F6E19420080CB5EF71E0200014C00016371007E00047870737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A657870000000007704000000007871007E000C707372003A6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F41757468325265717565737400000000000000010200075A0008617070726F7665644C000B617574686F72697469657371007E00044C000A657874656E73696F6E7374000F4C6A6176612F7574696C2F4D61703B4C000B72656469726563745572697400124C6A6176612F6C616E672F537472696E673B4C00077265667265736874003B4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F546F6B656E526571756573743B4C000B7265736F7572636549647374000F4C6A6176612F7574696C2F5365743B4C000D726573706F6E7365547970657371007E0011787200386F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E426173655265717565737436287A3EA37169BD0200034C0008636C69656E74496471007E000F4C001172657175657374506172616D657465727371007E000E4C000573636F706571007E00117870740008636C69656E745F32737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654D6170F1A5A8FE74F507420200014C00016D71007E000E7870737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F400000000000067708000000080000000474000A6772616E745F7479706574000870617373776F7264740009636C69656E745F6964740008636C69656E745F3274000573636F7065740006736572766572740008757365726E616D65740006757365725F3778737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574801D92D18F9B80550200007871007E0009737200176A6176612E7574696C2E4C696E6B656448617368536574D86CD75A95DD2A1E020000787200116A6176612E7574696C2E48617368536574BA44859596B8B7340300007870770C000000103F4000000000000171007E001E78017371007E0024770C000000103F40000000000001737200426F72672E737072696E676672616D65776F726B2E73656375726974792E636F72652E617574686F726974792E53696D706C654772616E746564417574686F7269747900000000000001F40200014C0004726F6C6571007E000F78707400066F6175746832787371007E00173F40000000000000770800000010000000007870707371007E0024770C000000103F40000000000000787371007E0024770C000000103F40000000000000787372004F6F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E557365726E616D6550617373776F726441757468656E7469636174696F6E546F6B656E00000000000001F40200024C000B63726564656E7469616C7371007E00054C00097072696E636970616C71007E00057871007E0003017371007E00077371007E000B000000007704000000007871007E0030737200176A6176612E7574696C2E4C696E6B6564486173684D617034C04E5C106CC0FB0200015A000B6163636573734F726465727871007E00173F4000000000000C7708000000100000000571007E001971007E001A71007E001D71007E001E74000D636C69656E745F73656372657474000631323334353671007E001B71007E001C71007E001F71007E002078007073720028636F6D2E736572766963652E617574682E73657276696365617574682E656E746974792E557365728D26FC4843F38C1A0200044C000B617574686F72697469657371007E00084C000269647400104C6A6176612F6C616E672F4C6F6E673B4C000870617373776F726471007E000F4C0008757365726E616D6571007E000F78707372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E744261676563FD3F2082000C0200014C000362616771007E00087872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6EE9D3DC4DA9113BA502000A5A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65645A000D697354656D7053657373696F6E4C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E657271007E00054C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E003A787000FFFFFFFF0001007372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000671007E0037740034636F6D2E736572766963652E617574682E73657276696365617574682E656E746974792E557365722E617574686F726974696573707371007E000B00000000770400000000787371007E000B000000007704000000007871007E003E7400447B6263727970747D24326124313024614B435559504F57556B367370596B6C57785874592E383774466A7443796665642E4C6C525564465954534C776F43716D434A4B4F740006757365725F37);
INSERT INTO `oauth_refresh_token` VALUES ('bd231adda72325f7eaf190370a1ae95f', 0xACED00057372004C6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744578706972696E674F417574683252656672657368546F6B656E2FDF47639DD0C9B70200014C000A65787069726174696F6E7400104C6A6176612F7574696C2F446174653B787200446F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F417574683252656672657368546F6B656E73E10E0A6354D45E0200014C000576616C75657400124C6A6176612F6C616E672F537472696E673B787074002465646436323133662D316265322D343265342D383431382D6138363661313436636236377372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017858A0037578, 0xACED0005737200416F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F417574683241757468656E7469636174696F6EBD400B02166252130200024C000D73746F7265645265717565737474003C4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F4F4175746832526571756573743B4C00127573657241757468656E7469636174696F6E7400324C6F72672F737072696E676672616D65776F726B2F73656375726974792F636F72652F41757468656E7469636174696F6E3B787200476F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E416273747261637441757468656E7469636174696F6E546F6B656ED3AA287E6E47640E0200035A000D61757468656E746963617465644C000B617574686F7269746965737400164C6A6176612F7574696C2F436F6C6C656374696F6E3B4C000764657461696C737400124C6A6176612F6C616E672F4F626A6563743B787000737200266A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654C697374FC0F2531B5EC8E100200014C00046C6973747400104C6A6176612F7574696C2F4C6973743B7872002C6A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65436F6C6C656374696F6E19420080CB5EF71E0200014C00016371007E00047870737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A657870000000007704000000007871007E000C707372003A6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F41757468325265717565737400000000000000010200075A0008617070726F7665644C000B617574686F72697469657371007E00044C000A657874656E73696F6E7374000F4C6A6176612F7574696C2F4D61703B4C000B72656469726563745572697400124C6A6176612F6C616E672F537472696E673B4C00077265667265736874003B4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F546F6B656E526571756573743B4C000B7265736F7572636549647374000F4C6A6176612F7574696C2F5365743B4C000D726573706F6E7365547970657371007E0011787200386F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E426173655265717565737436287A3EA37169BD0200034C0008636C69656E74496471007E000F4C001172657175657374506172616D657465727371007E000E4C000573636F706571007E00117870740008636C69656E745F32737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654D6170F1A5A8FE74F507420200014C00016D71007E000E7870737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F400000000000067708000000080000000474000A6772616E745F7479706574000870617373776F7264740009636C69656E745F6964740008636C69656E745F3274000573636F7065740006736572766572740008757365726E616D65740006757365725F3778737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574801D92D18F9B80550200007871007E0009737200176A6176612E7574696C2E4C696E6B656448617368536574D86CD75A95DD2A1E020000787200116A6176612E7574696C2E48617368536574BA44859596B8B7340300007870770C000000103F4000000000000171007E001E78017371007E0024770C000000103F40000000000001737200426F72672E737072696E676672616D65776F726B2E73656375726974792E636F72652E617574686F726974792E53696D706C654772616E746564417574686F7269747900000000000001F40200014C0004726F6C6571007E000F78707400066F6175746832787371007E00173F40000000000000770800000010000000007870707371007E0024770C000000103F40000000000000787371007E0024770C000000103F40000000000000787372004F6F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E557365726E616D6550617373776F726441757468656E7469636174696F6E546F6B656E00000000000001F40200024C000B63726564656E7469616C7371007E00054C00097072696E636970616C71007E00057871007E0003017371007E00077371007E000B000000007704000000007871007E0030737200176A6176612E7574696C2E4C696E6B6564486173684D617034C04E5C106CC0FB0200015A000B6163636573734F726465727871007E00173F4000000000000C7708000000100000000571007E001971007E001A71007E001D71007E001E74000D636C69656E745F73656372657474000631323334353671007E001B71007E001C71007E001F71007E002078007073720028636F6D2E736572766963652E617574682E73657276696365617574682E656E746974792E557365728D26FC4843F38C1A0200044C000B617574686F72697469657371007E00084C000269647400104C6A6176612F6C616E672F4C6F6E673B4C000870617373776F726471007E000F4C0008757365726E616D6571007E000F78707372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E744261676563FD3F2082000C0200014C000362616771007E00087872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6EE9D3DC4DA9113BA502000A5A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65645A000D697354656D7053657373696F6E4C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E657271007E00054C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E003A787000FFFFFFFF0001007372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000671007E0037740034636F6D2E736572766963652E617574682E73657276696365617574682E656E746974792E557365722E617574686F726974696573707371007E000B00000000770400000000787371007E000B000000007704000000007871007E003E7400447B6263727970747D24326124313024614B435559504F57556B367370596B6C57785874592E383774466A7443796665642E4C6C525564465954534C776F43716D434A4B4F740006757365725F37);
INSERT INTO `oauth_refresh_token` VALUES ('6f518b3ac43d59370a8b3ce48e08614f', 0xACED00057372004C6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744578706972696E674F417574683252656672657368546F6B656E2FDF47639DD0C9B70200014C000A65787069726174696F6E7400104C6A6176612F7574696C2F446174653B787200446F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F417574683252656672657368546F6B656E73E10E0A6354D45E0200014C000576616C75657400124C6A6176612F6C616E672F537472696E673B787074002434306434333138382D393765372D343531652D393530372D3264363030393338653634357372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017858A1194278, 0xACED0005737200416F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F417574683241757468656E7469636174696F6EBD400B02166252130200024C000D73746F7265645265717565737474003C4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F4F4175746832526571756573743B4C00127573657241757468656E7469636174696F6E7400324C6F72672F737072696E676672616D65776F726B2F73656375726974792F636F72652F41757468656E7469636174696F6E3B787200476F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E416273747261637441757468656E7469636174696F6E546F6B656ED3AA287E6E47640E0200035A000D61757468656E746963617465644C000B617574686F7269746965737400164C6A6176612F7574696C2F436F6C6C656374696F6E3B4C000764657461696C737400124C6A6176612F6C616E672F4F626A6563743B787000737200266A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654C697374FC0F2531B5EC8E100200014C00046C6973747400104C6A6176612F7574696C2F4C6973743B7872002C6A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65436F6C6C656374696F6E19420080CB5EF71E0200014C00016371007E00047870737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A657870000000007704000000007871007E000C707372003A6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F41757468325265717565737400000000000000010200075A0008617070726F7665644C000B617574686F72697469657371007E00044C000A657874656E73696F6E7374000F4C6A6176612F7574696C2F4D61703B4C000B72656469726563745572697400124C6A6176612F6C616E672F537472696E673B4C00077265667265736874003B4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F546F6B656E526571756573743B4C000B7265736F7572636549647374000F4C6A6176612F7574696C2F5365743B4C000D726573706F6E7365547970657371007E0011787200386F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E426173655265717565737436287A3EA37169BD0200034C0008636C69656E74496471007E000F4C001172657175657374506172616D657465727371007E000E4C000573636F706571007E00117870740008636C69656E745F32737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654D6170F1A5A8FE74F507420200014C00016D71007E000E7870737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F400000000000067708000000080000000474000A6772616E745F7479706574000870617373776F7264740009636C69656E745F6964740008636C69656E745F3274000573636F7065740006736572766572740008757365726E616D65740006757365725F3778737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574801D92D18F9B80550200007871007E0009737200176A6176612E7574696C2E4C696E6B656448617368536574D86CD75A95DD2A1E020000787200116A6176612E7574696C2E48617368536574BA44859596B8B7340300007870770C000000103F4000000000000171007E001E78017371007E0024770C000000103F40000000000001737200426F72672E737072696E676672616D65776F726B2E73656375726974792E636F72652E617574686F726974792E53696D706C654772616E746564417574686F7269747900000000000001F40200014C0004726F6C6571007E000F78707400066F6175746832787371007E00173F40000000000000770800000010000000007870707371007E0024770C000000103F40000000000000787371007E0024770C000000103F40000000000000787372004F6F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E557365726E616D6550617373776F726441757468656E7469636174696F6E546F6B656E00000000000001F40200024C000B63726564656E7469616C7371007E00054C00097072696E636970616C71007E00057871007E0003017371007E00077371007E000B000000007704000000007871007E0030737200176A6176612E7574696C2E4C696E6B6564486173684D617034C04E5C106CC0FB0200015A000B6163636573734F726465727871007E00173F4000000000000C7708000000100000000571007E001971007E001A71007E001D71007E001E74000D636C69656E745F73656372657474000631323334353671007E001B71007E001C71007E001F71007E002078007073720028636F6D2E736572766963652E617574682E73657276696365617574682E656E746974792E557365728D26FC4843F38C1A0200044C000B617574686F72697469657371007E00084C000269647400104C6A6176612F6C616E672F4C6F6E673B4C000870617373776F726471007E000F4C0008757365726E616D6571007E000F78707372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E744261676563FD3F2082000C0200014C000362616771007E00087872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6EE9D3DC4DA9113BA502000A5A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65645A000D697354656D7053657373696F6E4C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E657271007E00054C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E003A787000FFFFFFFF0001007372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000671007E0037740034636F6D2E736572766963652E617574682E73657276696365617574682E656E746974792E557365722E617574686F726974696573707371007E000B00000000770400000000787371007E000B000000007704000000007871007E003E7400447B6263727970747D24326124313024614B435559504F57556B367370596B6C57785874592E383774466A7443796665642E4C6C525564465954534C776F43716D434A4B4F740006757365725F37);

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` varchar(32) NOT NULL COMMENT '权限id',
  `name` varchar(100) NOT NULL COMMENT '权限名称',
  `description` varchar(255) DEFAULT NULL COMMENT '权限描述',
  `url` varchar(255) DEFAULT NULL COMMENT '权限访问路径',
  `perms` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `parent_id` int(11) DEFAULT NULL COMMENT '父级权限id',
  `type` int(1) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `order_num` int(3) DEFAULT '0' COMMENT '排序',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COMMENT='后台权限表';

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('1', '1', '工作台', '工作台', '/workdest', 'workdest', '0', '1', '1', 'fas fa-home', '1', '2017-09-27 21:22:02', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('2', '2', '权限管理', '权限管理', '', '', '0', '0', '4', 'far fa-folder-open', '1', '2017-07-13 15:04:42', '2021-03-15 14:49:09');
INSERT INTO `permission` VALUES ('3', '201', '用户管理', '用户管理', '/users', 'users', '2', '1', '1', 'far fa-file', '1', '2017-07-13 15:05:47', '2021-03-15 15:12:24');
INSERT INTO `permission` VALUES ('4', '20101', '列表查询', '用户列表查询', '/user/list', 'user:list', '3', '2', '0', null, '1', '2017-07-13 15:09:24', '2017-10-09 05:38:29');
INSERT INTO `permission` VALUES ('5', '20102', '新增', '新增用户', '/user/add', 'user:add', '3', '2', '0', null, '1', '2017-07-13 15:06:50', '2018-02-28 17:58:46');
INSERT INTO `permission` VALUES ('6', '20103', '编辑', '编辑用户', '/user/edit', 'user:edit', '3', '2', '0', null, '1', '2017-07-13 15:08:03', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('7', '20104', '删除', '删除用户', '/user/delete', 'user:delete', '3', '2', '0', null, '1', '2017-07-13 15:08:42', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('8', '20105', '批量删除', '批量删除用户', '/user/batch/delete', 'user:batchDelete', '3', '2', '0', '', '1', '2018-07-11 01:53:09', '2018-07-11 01:53:09');
INSERT INTO `permission` VALUES ('9', '20106', '分配角色', '分配角色', '/user/assign/role', 'user:assignRole', '3', '2', '0', null, '1', '2017-07-13 15:09:24', '2017-10-09 05:38:29');
INSERT INTO `permission` VALUES ('10', '202', '角色管理', '角色管理', '/roles', 'roles', '2', '1', '2', 'far fa-file', '1', '2017-07-17 14:39:09', '2021-03-15 15:12:32');
INSERT INTO `permission` VALUES ('11', '20201', '列表查询', '角色列表查询', '/role/list', 'role:list', '10', '2', '0', null, '1', '2017-10-10 15:31:36', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('12', '20202', '新增', '新增角色', '/role/add', 'role:add', '10', '2', '0', null, '1', '2017-07-17 14:39:46', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('13', '20203', '编辑', '编辑角色', '/role/edit', 'role:edit', '10', '2', '0', null, '1', '2017-07-17 14:40:15', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('14', '20204', '删除', '删除角色', '/role/delete', 'role:delete', '10', '2', '0', null, '1', '2017-07-17 14:40:57', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('15', '20205', '批量删除', '批量删除角色', '/role/batch/delete', 'role:batchDelete', '10', '2', '0', '', '1', '2018-07-10 22:20:43', '2018-07-10 22:20:43');
INSERT INTO `permission` VALUES ('16', '20206', '分配权限', '分配权限', '/role/assign/permission', 'role:assignPerms', '10', '2', '0', null, '1', '2017-09-26 07:33:05', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('17', '203', '资源管理', '资源管理', '/permissions', 'permissions', '2', '1', '3', 'far fa-file', '1', '2017-09-26 07:33:51', '2021-03-15 15:12:40');
INSERT INTO `permission` VALUES ('18', '20301', '列表查询', '资源列表', '/permission/list', 'permission:list', '17', '2', '0', null, '1', '2018-07-12 16:25:28', '2018-07-12 16:25:33');
INSERT INTO `permission` VALUES ('19', '20302', '新增', '新增资源', '/permission/add', 'permission:add', '17', '2', '0', null, '1', '2017-09-26 08:06:58', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20', '20303', '编辑', '编辑资源', '/permission/edit', 'permission:edit', '17', '2', '0', null, '1', '2017-09-27 21:29:04', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('21', '20304', '删除', '删除资源', '/permission/delete', 'permission:delete', '17', '2', '0', null, '1', '2017-09-27 21:29:50', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('22', '3', '运维管理', '运维管理', null, null, '0', '0', '7', 'fas fa-people-carry', '1', '2018-07-06 15:19:26', '2020-04-19 19:09:59');
INSERT INTO `permission` VALUES ('23', '301', '数据监控', '数据监控', '/database/monitoring', 'database', '22', '1', '1', 'far fa-file', '1', '2018-07-06 15:19:55', '2021-03-15 15:15:07');
INSERT INTO `permission` VALUES ('24', '4', '系统管理', '系统管理', null, null, '0', '0', '5', 'fas fa-cog', '1', '2018-07-06 15:20:38', '2020-04-19 19:08:58');
INSERT INTO `permission` VALUES ('25', '401', '在线用户', '在线用户', '/onlineUsers', 'onlineUsers', '24', '1', '1', 'far fa-file', '1', '2018-07-06 15:21:00', '2021-03-15 15:14:56');
INSERT INTO `permission` VALUES ('32', '1000001992372345', '在线用户查询', '在线用户查询', '/online/user/list', 'onlineUser:list', '25', '2', '0', '', '1', '2018-07-24 15:02:23', '2018-07-24 15:02:23');
INSERT INTO `permission` VALUES ('33', '1000002083579304', '踢出用户', '踢出用户', '/online/user/kickout', 'onlineUser:kickout', '25', '2', '0', '', '1', '2018-07-24 15:03:16', '2018-07-24 15:03:16');
INSERT INTO `permission` VALUES ('34', '1000000171409776', '批量踢出', '批量踢出', '/online/user/batch/kickout', 'onlineUser:batchKickout', '25', '2', '0', '', '1', '2018-07-24 15:04:09', '2018-07-24 15:04:09');
INSERT INTO `permission` VALUES ('35', '1000000863853891', '网站管理', '网站管理', '', '', '0', '0', '3', 'fas fa-chart-line', '1', '2018-07-24 15:44:23', '2021-03-15 14:47:50');
INSERT INTO `permission` VALUES ('36', '1000001264798222', '基础信息', '基础设置', '/siteinfo', 'siteinfo', '35', '1', '1', 'far fa-file', '1', '2018-07-24 15:48:13', '2021-03-15 15:11:56');
INSERT INTO `permission` VALUES ('37', '1000000432183856', '保存', '基础设置-保存', '/siteinfo/save', 'siteinfo:save', '36', '2', '0', '', '1', '2018-07-24 15:49:12', '2018-07-24 15:49:12');
INSERT INTO `permission` VALUES ('38', '1000001792841328', '系统公告', '系统公告', '/notifies', 'notifies', '35', '1', '2', 'fas fa-chess-queen', '0', '2018-07-24 23:40:45', '2018-09-13 12:34:18');
INSERT INTO `permission` VALUES ('39', '1000001351219537', '查询', '系统公告-查询', '/notify/list', 'notify:list', '38', '2', '0', '', '0', '2018-07-24 23:41:30', '2018-09-13 12:33:19');
INSERT INTO `permission` VALUES ('40', '1000000791685519', '新增', '系统公告-新增', '/notify/add', 'notify:add', '38', '2', '0', '', '0', '2018-07-24 23:42:20', '2018-09-13 12:33:26');
INSERT INTO `permission` VALUES ('42', '1000001531648485', '编辑', '系统公告-编辑', '/notify/edit', 'notify:edit', '38', '2', '0', '', '0', '2018-07-24 23:44:39', '2018-09-13 12:33:52');
INSERT INTO `permission` VALUES ('43', '1000001548165826', '删除', '系统公告-删除', '/notify/delete', 'notify:delete', '38', '2', '0', '', '0', '2018-07-24 23:45:27', '2018-09-13 12:33:57');
INSERT INTO `permission` VALUES ('44', '1000001530229707', '批量删除', '批量删除公告', '/notify/batch/delete', 'notify:batchDelete', '38', '2', '0', '', '0', '2018-07-24 23:46:25', '2018-09-13 12:34:02');
INSERT INTO `permission` VALUES ('45', '1000000237721285', '友链管理', '友情链接', '/links', 'links', '35', '1', '3', 'far fa-file', '1', '2018-07-25 11:05:49', '2021-03-15 15:12:13');
INSERT INTO `permission` VALUES ('46', '1000001238193773', '查询', '友链-查询', '/link/list', 'link:list', '45', '2', '0', '', '1', '2018-07-25 11:06:44', '2018-07-25 11:06:44');
INSERT INTO `permission` VALUES ('47', '1000001305005793', '新增', '友链-新增', '/link/add', 'link:add', '45', '2', '0', '', '1', '2018-07-25 11:07:46', '2018-07-25 11:07:46');
INSERT INTO `permission` VALUES ('48', '1000001679037501', '编辑', '友链-编辑', '/link/edit', 'link:edit', '45', '2', '0', '', '1', '2018-07-25 11:08:21', '2018-07-25 11:08:21');
INSERT INTO `permission` VALUES ('49', '1000001011730177', '删除', '友链-删除', '/link/delete', 'link:delete', '45', '2', '0', '', '1', '2018-07-25 11:08:53', '2018-07-25 11:08:53');
INSERT INTO `permission` VALUES ('50', '1000001312374743', '批量删除', '友链-批量删除', '/link/batch/delete', 'link:batchDelete', '45', '2', '0', '', '1', '2018-07-25 11:09:40', '2018-07-25 11:09:40');
INSERT INTO `permission` VALUES ('51', '1000001507480127', '审核', '友链-审核', '/link/audit', 'link:audit', '45', '2', '0', '', '1', '2018-07-25 11:42:28', '2018-07-25 11:42:28');
INSERT INTO `permission` VALUES ('52', '1000000602555213', '文章管理', '文章管理', '', '', '35', '0', '2', 'fas fa-bezier-curve', '1', '2018-07-25 17:43:12', '2021-03-15 22:47:29');
INSERT INTO `permission` VALUES ('53', '1000001729104792', '分类管理', '分类管理', '/categories', 'categories', '35', '1', '3', 'far fa-file', '1', '2018-07-25 17:43:50', '2021-03-15 22:48:13');
INSERT INTO `permission` VALUES ('54', '1000000015836901', '新增', '新增分类', '/category/add', 'category:add', '53', '2', '0', '', '1', '2018-07-25 17:44:28', '2018-07-25 17:44:28');
INSERT INTO `permission` VALUES ('55', '1000001439189167', '编辑', '编辑分类', '/category/edit', 'category:edit', '53', '2', '0', '', '1', '2018-07-25 17:44:52', '2018-07-25 17:44:52');
INSERT INTO `permission` VALUES ('56', '1000001647995753', '删除', '删除分类', '/category/delete', 'category:delete', '53', '2', '0', '', '1', '2018-07-25 17:45:28', '2018-07-25 17:45:28');
INSERT INTO `permission` VALUES ('58', '1000000841419865', '查询', '分类查询', '/category/list', 'category:list', '53', '2', '0', '', '1', '2018-07-25 17:49:43', '2018-07-25 17:49:43');
INSERT INTO `permission` VALUES ('59', '1000000976625379', '标签管理', '标签管理', '/tags', 'tags', '35', '1', '4', 'far fa-file', '1', '2018-07-25 18:50:47', '2021-03-15 22:48:34');
INSERT INTO `permission` VALUES ('60', '1000002127467055', '查询', '查询标签列表', '/tag/list', 'tag:list', '59', '2', '0', '', '1', '2018-07-25 18:51:20', '2018-07-25 18:51:20');
INSERT INTO `permission` VALUES ('61', '1000001458372033', '新增', '新增标签', '/tag/add', 'tag:add', '59', '2', '0', '', '1', '2018-07-25 18:51:42', '2018-07-25 18:51:42');
INSERT INTO `permission` VALUES ('62', '1000001832967209', '编辑', '编辑标签', '/tag/edit', 'tag:edit', '59', '2', '0', '', '1', '2018-07-25 18:52:17', '2018-07-25 18:52:17');
INSERT INTO `permission` VALUES ('63', '1000000754923037', '删除', '删除标签', '/tag/delete', 'tag:delete', '59', '2', '0', '', '1', '2018-07-25 18:52:40', '2018-07-25 18:52:40');
INSERT INTO `permission` VALUES ('64', '1000000759248744', '批量删除', '批量删除标签', '/tag/batch/delete', 'tag:batchDelete', '59', '2', '0', '', '1', '2018-07-25 18:53:14', '2018-07-25 18:53:14');
INSERT INTO `permission` VALUES ('65', '1000001038456544', '文章列表', '文章列表', '/articles', 'articles', '52', '1', '2', 'far fa-file', '1', '2018-07-29 20:20:23', '2021-03-15 15:11:24');
INSERT INTO `permission` VALUES ('66', '1000000686545782', '查询', '查询文章', '/article/list', 'article:list', '65', '2', '0', '', '1', '2018-07-29 20:20:54', '2018-07-29 20:20:54');
INSERT INTO `permission` VALUES ('67', '1000001642272578', '新增', '新增文章', '/article/add', 'article:add', '65', '2', '0', '', '1', '2018-07-29 20:21:21', '2018-07-29 20:21:21');
INSERT INTO `permission` VALUES ('68', '1000000804049447', '编辑', '编辑文章', '/article/edit', 'article:edit', '65', '2', '0', '', '1', '2018-07-29 20:21:50', '2018-07-29 20:21:50');
INSERT INTO `permission` VALUES ('69', '1000000488864959', '删除', '删除文章', '/article/delete', 'article:delete', '65', '2', '0', '', '1', '2018-07-29 20:23:27', '2018-07-29 20:23:27');
INSERT INTO `permission` VALUES ('70', '1000000512435306', '批量删除', '批量删除文章', '/article/batch/delete', 'article:batchDelete', '65', '2', '0', '', '1', '2018-07-29 20:23:49', '2018-07-29 20:23:49');
INSERT INTO `permission` VALUES ('71', '1000000899091444', '发布文章', '写文章', '/article/add', 'article:add', '52', '1', '1', 'far fa-file', '1', '2018-07-29 20:39:49', '2020-04-19 19:16:06');
INSERT INTO `permission` VALUES ('72', '1000000224901858', '评论管理', '评论管理', '/comments', 'comments', '35', '1', '4', 'far fa-file', '1', '2018-08-10 09:44:41', '2018-09-19 15:44:13');
INSERT INTO `permission` VALUES ('73', '1000001579533936', '查询', '查询', '/comment/list', 'comment:list', '72', '2', '0', '', '1', '2018-08-10 09:46:54', '2018-08-10 09:46:54');
INSERT INTO `permission` VALUES ('74', '1000000663968031', '审核', '审核评论', '/comment/audit', 'comment:audit', '72', '2', '0', '', '1', '2018-08-10 09:57:11', '2018-08-10 09:57:11');
INSERT INTO `permission` VALUES ('75', '1000000322655547', '回复', '回复评论', '/comment/reply', 'comment:audit', '72', '2', '0', '', '1', '2018-08-10 10:04:28', '2018-08-10 10:04:28');
INSERT INTO `permission` VALUES ('76', '1000001419287014', '删除', '删除评论', '/comment/delete', 'comment:delete', '72', '2', '0', '', '1', '2018-08-10 10:06:27', '2018-08-10 10:06:27');
INSERT INTO `permission` VALUES ('77', '1000002075182223', '批量删除', '批量删除评论', '/comment/batch/delete', 'comment:batchDelete', '72', '2', '0', '', '1', '2018-08-10 10:07:57', '2018-08-10 10:07:57');
INSERT INTO `permission` VALUES ('78', '1000000587822241', '上传管理', '上传管理', null, null, '0', '0', '6', 'fas fa-cloud-upload-alt', '0', '2018-09-12 17:08:41', '2021-03-15 15:09:27');
INSERT INTO `permission` VALUES ('79', '1000000493635111', '云存储配置', '云存储配置', '/attachment/config', 'upload:config', '24', '1', '1', 'far fa-file', '1', '2018-09-12 17:10:09', '2021-03-15 15:15:17');
INSERT INTO `permission` VALUES ('80', '1000000318760332', '保存', '保存云存储配置', '/upload/saveConfig', 'upload:saveConfig', '79', '2', '0', '', '1', '2018-09-12 17:10:42', '2018-09-12 17:10:42');
INSERT INTO `permission` VALUES ('81', '1000000919723998', '主题管理', '主题管理', '/themes', 'themes', '35', '1', '2', 'far fa-file', '1', '2018-09-19 15:43:50', '2021-03-15 15:12:04');
INSERT INTO `permission` VALUES ('82', '1000000784272506', '查询', '主题列表', '/theme/list', 'theme:list', '81', '2', '0', '', '1', '2018-09-19 15:44:50', '2018-09-19 15:44:50');
INSERT INTO `permission` VALUES ('83', '1000000215201942', '新增', '新增主题', '/theme/add', 'theme:add', '81', '2', '0', '', '1', '2018-09-19 15:45:34', '2018-09-19 15:45:34');
INSERT INTO `permission` VALUES ('84', '1000001065007557', '启用', '启用主题', '/theme/use', 'theme:use', '81', '2', '0', '', '1', '2018-09-19 15:46:28', '2018-09-19 15:46:28');
INSERT INTO `permission` VALUES ('85', '1000000431577803', '删除', '删除主题', '/theme/delete', 'theme:delete', '81', '2', '0', '', '1', '2018-09-19 15:48:06', '2018-09-19 15:48:06');
INSERT INTO `permission` VALUES ('86', '1000000207002458', '批量删除', '批量删除主题', 'theme/batch/delete', 'theme:batchDelete', '81', '2', '0', '', '1', '2018-09-19 15:48:39', '2018-09-19 15:48:39');
INSERT INTO `permission` VALUES ('87', '1000002051091207', '编辑', '编辑主题', '/theme/edit', 'theme:edit', '81', '2', '0', '', '1', '2018-09-19 15:54:34', '2018-09-19 15:54:34');
INSERT INTO `permission` VALUES ('88', '5011629010561508', '批量推送', '批量推送百度', '/article/batch/push', 'article:batchPush', '65', '2', '0', '', '1', '2018-10-28 15:15:00', '2018-10-28 15:15:00');
INSERT INTO `permission` VALUES ('89', '1000001481941855', '前台用户', '门户网站前台用户管理', '', '', '35', '0', '5', 'far fa-angry', '0', '2021-03-17 13:19:31', '2021-03-17 13:20:04');
INSERT INTO `permission` VALUES ('90', '1000000066764147', '会员管理', '门户网站前台用户管理', '/clientUser', 'clientUsers', '35', '1', '5', 'fas fa-asterisk', '1', '2021-03-17 13:21:58', '2021-03-18 16:13:17');
INSERT INTO `permission` VALUES ('91', '1000000071991573', '查询', '门户网站前台用户管理查询', '/clientUser/user/list', 'clientUser:list', '90', '2', '0', '', '1', '2021-03-17 13:24:15', '2021-03-17 13:25:29');
INSERT INTO `permission` VALUES ('92', '1000001549593102', '激活', '门户网站前台用户激活', '/clientUser/user/active', 'clientUser:active', '90', '2', '1', '', '1', '2021-03-17 13:27:45', '2021-03-17 13:28:30');
INSERT INTO `permission` VALUES ('93', '1000001293027093', '批量激活', '门户网站前台用户批量激活', '/clientUser/user/batch/active', 'clientUser:batchActive', '90', '2', '2', '', '1', '2021-03-17 13:29:00', '2021-03-17 13:29:34');
INSERT INTO `permission` VALUES ('94', '1000000112648709', '密码初始化', '后台管理员独有，重置门户网站密码为123456', '/clientUser/user/resetPassword', 'clientUser:restClientUserPassword', '90', '2', '3', '', '1', '2021-03-18 13:22:32', '2021-03-18 14:35:04');
INSERT INTO `permission` VALUES ('95', '1000001464499568', '密码初始化', '后台管理员，初始化其他用户密码为123456', '/user/resetAdminUserPassword', 'user:initPassword', '3', '2', '0', '', '1', '2021-03-18 14:51:57', '2021-03-18 14:53:00');
INSERT INTO `permission` VALUES ('96', '1000000271431673', '登录日志', '系统会员和后台用户登录系统的日志', '/loginHistory', 'loginHistorys', '22', '1', '6', 'fab fa-blackberry', '1', '2021-03-18 15:27:42', '2021-03-18 15:31:20');
INSERT INTO `permission` VALUES ('97', '1000000820519811', '查询', '登录历史查询接口', '/loginHistory/list', 'loginHistory:list', '96', '2', '0', '', '1', '2021-03-18 15:30:39', '2021-03-18 15:31:07');
INSERT INTO `permission` VALUES ('98', '1000001157854541', '商城系统', '商城微服务系统资源路径配置，不同于cms后台', '', '', null, '0', '8', 'fas fa-ambulance', '0', '2021-03-26 13:53:19', '2021-03-26 16:38:14');
INSERT INTO `permission` VALUES ('99', '1000001197076936', '业务服务', '商城系统的业务服务资源配置', '', '', '98', '0', '0', 'fab fa-500px', '0', '2021-03-26 13:54:51', '2021-03-26 16:38:10');
INSERT INTO `permission` VALUES ('100', '1000000032378677', '业务下单', '商城系统，业务服务的下单资源配置', '/businesses', '/business/booking', '99', '1', '0', 'fab fa-accessible-icon', '0', '2021-03-26 13:56:35', '2021-03-26 13:56:51');
INSERT INTO `permission` VALUES ('101', '1000001715962172', '服务菜单', '业务系统的服务接口菜单', '/businesses', 'businesses', '99', '1', '0', 'fab fa-accessible-icon', '0', '2021-03-26 13:58:41', '2021-03-26 16:38:05');
INSERT INTO `permission` VALUES ('102', '1000000772469197', '客户下单', '业务系统，客户下单资源配置', '/business/booking', 'business:booking', '101', '2', '0', '', '0', '2021-03-26 13:59:53', '2021-03-26 16:37:59');
INSERT INTO `permission` VALUES ('103', '1000001580020132', '商城业务', '商城系统，微服务资源配置', '', '', '0', '0', '9', 'fab fa-500px', '1', '2021-03-26 16:33:54', '2021-03-26 16:50:00');
INSERT INTO `permission` VALUES ('104', '1000001220168157', '业务服务', '业务系统，服务接口集', '/businesses', 'businesses', '103', '1', '0', 'fab fa-accusoft', '1', '2021-03-26 16:35:50', '2021-03-26 16:50:27');
INSERT INTO `permission` VALUES ('105', '1000001361410425', '客户下单', '业务服务，客户下单接口', '/business/booking', 'business:booking', '104', '2', '0', '', '1', '2021-03-26 16:36:58', '2021-03-27 07:03:03');
INSERT INTO `permission` VALUES ('106', '1000001219035473', '客户退单', '业务服务，客户退单接口', '/business/refund', 'business:refund', '104', '2', '0', '', '1', '2021-03-27 04:20:05', '2021-03-27 07:02:53');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(20) NOT NULL COMMENT '角色id',
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `description` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '1', '超级管理员', '超级管理员', '1', '2017-06-28 20:30:05', '2017-06-28 20:30:10');
INSERT INTO `role` VALUES ('2', '2', '管理员', '管理员', '1', '2017-06-30 23:35:19', '2017-10-11 09:32:33');
INSERT INTO `role` VALUES ('3', '3', '普通用户', '普通用户', '1', '2017-06-30 23:35:44', '2018-07-13 11:44:06');
INSERT INTO `role` VALUES ('4', '4', '数据库管理员', '数据库管理员', '1', '2017-07-12 11:50:22', '2017-10-09 17:38:02');
INSERT INTO `role` VALUES ('5', '1000001940440719', '商城业务系统', '商城服务的业务系统', '1', '2021-03-26 14:04:08', null);
INSERT INTO `role` VALUES ('6', '1000001586961115', '游客角色', '游客角色', '1', '2021-03-27 11:09:08', null);

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(20) NOT NULL COMMENT '角色id',
  `permission_id` varchar(20) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2855 DEFAULT CHARSET=utf8 COMMENT='后台角色权限表';

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES ('2604', '1', '1');
INSERT INTO `role_permission` VALUES ('2605', '1', '1000000863853891');
INSERT INTO `role_permission` VALUES ('2606', '1', '1000000602555213');
INSERT INTO `role_permission` VALUES ('2607', '1', '1000000899091444');
INSERT INTO `role_permission` VALUES ('2608', '1', '1000001038456544');
INSERT INTO `role_permission` VALUES ('2609', '1', '1000000686545782');
INSERT INTO `role_permission` VALUES ('2610', '1', '1000001642272578');
INSERT INTO `role_permission` VALUES ('2611', '1', '1000000804049447');
INSERT INTO `role_permission` VALUES ('2612', '1', '1000000488864959');
INSERT INTO `role_permission` VALUES ('2613', '1', '1000000512435306');
INSERT INTO `role_permission` VALUES ('2614', '1', '1000000237721285');
INSERT INTO `role_permission` VALUES ('2615', '1', '1000001238193773');
INSERT INTO `role_permission` VALUES ('2616', '1', '1000001305005793');
INSERT INTO `role_permission` VALUES ('2617', '1', '1000001679037501');
INSERT INTO `role_permission` VALUES ('2618', '1', '1000001011730177');
INSERT INTO `role_permission` VALUES ('2619', '1', '1000001312374743');
INSERT INTO `role_permission` VALUES ('2620', '1', '1000001729104792');
INSERT INTO `role_permission` VALUES ('2621', '1', '1000000015836901');
INSERT INTO `role_permission` VALUES ('2622', '1', '1000001439189167');
INSERT INTO `role_permission` VALUES ('2623', '1', '1000001647995753');
INSERT INTO `role_permission` VALUES ('2624', '1', '1000000841419865');
INSERT INTO `role_permission` VALUES ('2625', '1', '1000000976625379');
INSERT INTO `role_permission` VALUES ('2626', '1', '1000002127467055');
INSERT INTO `role_permission` VALUES ('2627', '1', '1000001458372033');
INSERT INTO `role_permission` VALUES ('2628', '1', '1000001832967209');
INSERT INTO `role_permission` VALUES ('2629', '1', '1000000754923037');
INSERT INTO `role_permission` VALUES ('2630', '1', '1000000759248744');
INSERT INTO `role_permission` VALUES ('2631', '1', '1000000224901858');
INSERT INTO `role_permission` VALUES ('2632', '1', '1000001579533936');
INSERT INTO `role_permission` VALUES ('2633', '1', '1000000663968031');
INSERT INTO `role_permission` VALUES ('2634', '1', '1000000322655547');
INSERT INTO `role_permission` VALUES ('2635', '1', '1000001419287014');
INSERT INTO `role_permission` VALUES ('2636', '1', '1000002075182223');
INSERT INTO `role_permission` VALUES ('2637', '1', '1000000066764147');
INSERT INTO `role_permission` VALUES ('2638', '1', '1000000071991573');
INSERT INTO `role_permission` VALUES ('2639', '1', '1000001549593102');
INSERT INTO `role_permission` VALUES ('2640', '1', '1000001293027093');
INSERT INTO `role_permission` VALUES ('2641', '1', '1000000112648709');
INSERT INTO `role_permission` VALUES ('2642', '1', '2');
INSERT INTO `role_permission` VALUES ('2643', '1', '201');
INSERT INTO `role_permission` VALUES ('2644', '1', '20101');
INSERT INTO `role_permission` VALUES ('2645', '1', '20102');
INSERT INTO `role_permission` VALUES ('2646', '1', '20103');
INSERT INTO `role_permission` VALUES ('2647', '1', '20104');
INSERT INTO `role_permission` VALUES ('2648', '1', '20105');
INSERT INTO `role_permission` VALUES ('2649', '1', '20106');
INSERT INTO `role_permission` VALUES ('2650', '1', '1000001464499568');
INSERT INTO `role_permission` VALUES ('2651', '1', '202');
INSERT INTO `role_permission` VALUES ('2652', '1', '20201');
INSERT INTO `role_permission` VALUES ('2653', '1', '20202');
INSERT INTO `role_permission` VALUES ('2654', '1', '20203');
INSERT INTO `role_permission` VALUES ('2655', '1', '20204');
INSERT INTO `role_permission` VALUES ('2656', '1', '20205');
INSERT INTO `role_permission` VALUES ('2657', '1', '20206');
INSERT INTO `role_permission` VALUES ('2658', '1', '203');
INSERT INTO `role_permission` VALUES ('2659', '1', '20301');
INSERT INTO `role_permission` VALUES ('2660', '1', '20302');
INSERT INTO `role_permission` VALUES ('2661', '1', '20303');
INSERT INTO `role_permission` VALUES ('2662', '1', '20304');
INSERT INTO `role_permission` VALUES ('2663', '1', '4');
INSERT INTO `role_permission` VALUES ('2664', '1', '401');
INSERT INTO `role_permission` VALUES ('2665', '1', '1000001992372345');
INSERT INTO `role_permission` VALUES ('2666', '1', '1000002083579304');
INSERT INTO `role_permission` VALUES ('2667', '1', '1000000171409776');
INSERT INTO `role_permission` VALUES ('2668', '1', '3');
INSERT INTO `role_permission` VALUES ('2669', '1', '301');
INSERT INTO `role_permission` VALUES ('2670', '1', '1000000271431673');
INSERT INTO `role_permission` VALUES ('2671', '1', '1000000820519811');
INSERT INTO `role_permission` VALUES ('2806', '1000001940440719', '1000001580020132');
INSERT INTO `role_permission` VALUES ('2807', '1000001940440719', '1000001220168157');
INSERT INTO `role_permission` VALUES ('2808', '1000001940440719', '1000001361410425');
INSERT INTO `role_permission` VALUES ('2809', '1000001940440719', '1000001219035473');
INSERT INTO `role_permission` VALUES ('2837', '3', '1');
INSERT INTO `role_permission` VALUES ('2838', '3', '1000000863853891');
INSERT INTO `role_permission` VALUES ('2839', '3', '1000000602555213');
INSERT INTO `role_permission` VALUES ('2840', '3', '1000001038456544');
INSERT INTO `role_permission` VALUES ('2841', '3', '1000000686545782');
INSERT INTO `role_permission` VALUES ('2842', '3', '1000000224901858');
INSERT INTO `role_permission` VALUES ('2843', '3', '1000001579533936');
INSERT INTO `role_permission` VALUES ('2844', '3', '1000000066764147');
INSERT INTO `role_permission` VALUES ('2845', '3', '1000000071991573');
INSERT INTO `role_permission` VALUES ('2846', '3', '4');
INSERT INTO `role_permission` VALUES ('2847', '3', '401');
INSERT INTO `role_permission` VALUES ('2848', '3', '1000001992372345');
INSERT INTO `role_permission` VALUES ('2849', '3', '3');
INSERT INTO `role_permission` VALUES ('2850', '3', '1000000271431673');
INSERT INTO `role_permission` VALUES ('2851', '3', '1000000820519811');
INSERT INTO `role_permission` VALUES ('2852', '1000001586961115', '3');
INSERT INTO `role_permission` VALUES ('2853', '1000001586961115', '1000000271431673');
INSERT INTO `role_permission` VALUES ('2854', '1000001586961115', '1000000820519811');

-- ----------------------------
-- Table structure for sys_action_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_action_log`;
CREATE TABLE `sys_action_log` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作用户',
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户令牌',
  `trace` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '链路编号',
  `project` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '项目名称',
  `moudle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '模块名称',
  `action_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作类型',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '1' COMMENT '日志类型 1:正常 0：异常',
  `request_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '请求URI',
  `class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '执行类名',
  `method_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '执行方法名称',
  `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户代理',
  `remote_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作IP地址',
  `request_method` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作方式',
  `request_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '请求参数',
  `response_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '返回参数',
  `request_mac` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '设备MAC',
  `exception` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '异常信息',
  `action_thread` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '执行线程',
  `action_start_time` datetime DEFAULT NULL COMMENT '开始执行时刻',
  `action_end_time` datetime DEFAULT NULL COMMENT '结束执行时刻',
  `action_time` bigint(20) DEFAULT NULL COMMENT '执行耗时 单位(毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建日志时间',
  PRIMARY KEY (`id`),
  KEY `sys_log_trace` (`trace`(191)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='用户日志';

-- ----------------------------
-- Records of sys_action_log
-- ----------------------------
INSERT INTO `sys_action_log` VALUES ('1', 'admin', '', '', 'cms', '网站系统', '新增用户', '1', '/user/add', 'com.puboot.module.admin.controller.UserController', 'add', 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', '0:0:0:0:0:0:0:1', 'POST', 0x5B7B22757365724964223A22222C22757365726E616D65223A223138303430313132222C2270617373776F7264223A22313233343536222C2273616C74223A6E756C6C2C226E69636B6E616D65223A6E756C6C2C22656D61696C223A223131313131313131314071712E636F6D222C2270686F6E65223A223130303836222C22736578223A312C22616765223A32352C22696D67223A6E756C6C2C22737461747573223A6E756C6C2C2263726561746554696D65223A6E756C6C2C2275706461746554696D65223A6E756C6C2C226C6173744C6F67696E54696D65223A6E756C6C2C226C6F67696E497041646472657373223A6E756C6C2C22726F6C6573223A6E756C6C7D2C22313233343536225D, 0x7B22737461747573223A3230302C226D7367223A22E6B7BBE58AA0E794A8E688B7E68890E58A9F222C2264617461223A6E756C6C7D, '54-E1-AD-EC-39-8A', null, 'http-nio-8080-exec-2', '2021-03-19 16:01:44', '2021-03-19 16:01:44', '66', '2021-03-19 16:01:44');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_key` varchar(50) DEFAULT NULL COMMENT 'key',
  `sys_value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`sys_key`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='系统配置信息表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'CLOUD_STORAGE_CONFIG', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudRegion\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"xxxAccessKey\",\"qiniuBucketName\":\"blog\",\"qiniuDomain\":\"http://xxx.com\",\"qiniuPrefix\":\"img/blog\",\"qiniuSecretKey\":\"xxxSecretKey\",\"type\":4}', '1', '云存储配置信息');
INSERT INTO `sys_config` VALUES ('5', 'SITE_NAME', '预见未来', '1', '网站名称');
INSERT INTO `sys_config` VALUES ('6', 'SITE_KWD', 'Java JavaScript Spring SpringBoot Vue React', '1', '网站关键字');
INSERT INTO `sys_config` VALUES ('7', 'SITE_DESC', '个人博客网站，技术交流，经验分享。', '1', '网站描述');
INSERT INTO `sys_config` VALUES ('8', 'SITE_PERSON_PIC', 'https://tse1-mm.cn.bing.net/th/id/OIP.Ups1Z8igjNjLuDfO38XhTgHaHa?pid=Api&rs=1', '1', '站长头像');
INSERT INTO `sys_config` VALUES ('9', 'SITE_PERSON_NAME', 'LinZhaoguan', '1', '站长名称');
INSERT INTO `sys_config` VALUES ('10', 'SITE_PERSON_DESC', '90后少年，热爱写bug，热爱编程，热爱学习，分享一些个人经验，共同学习，少走弯路。Talk is cheap, show me the code!', '1', '站长描述');
INSERT INTO `sys_config` VALUES ('11', 'BAIDU_PUSH_URL', 'http://data.zz.baidu.com/urls?site=https://www.puboot.com&token=EjnfUGGJ1drKk4Oy', '1', '百度推送地址');
INSERT INTO `sys_config` VALUES ('12', 'SITE_STATIC', 'on', '1', null);

-- ----------------------------
-- Table structure for sys_store_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_store_info`;
CREATE TABLE `sys_store_info` (
  `row_id` int(20) NOT NULL AUTO_INCREMENT,
  `cust_num` varchar(50) DEFAULT NULL COMMENT '门店编码',
  `cust_name` varchar(50) DEFAULT NULL COMMENT '门店名称',
  `top_cust_num` varchar(50) DEFAULT NULL COMMENT '父父客户编码',
  `par_cust_num` varchar(50) DEFAULT NULL COMMENT '父客户编码',
  `cntct_cust_num` varchar(50) DEFAULT NULL COMMENT '上级合同客户编码',
  `lob_unified_dealer_num` varchar(50) DEFAULT NULL COMMENT '经销商编码',
  `trmnl_chanl` varchar(50) DEFAULT NULL COMMENT '终端渠道编码',
  `trmnl_chanl_name` varchar(50) DEFAULT NULL COMMENT '终端渠道名称',
  `cust_categ_l1` varchar(50) DEFAULT NULL COMMENT '客户类型1编码',
  `cust_categ_l1_name` varchar(50) DEFAULT NULL COMMENT '客户类型1名称',
  `cust_categ_l2` varchar(50) DEFAULT NULL COMMENT '客户类型2编码',
  `cust_categ_l2_name` varchar(50) DEFAULT NULL COMMENT '客户类型2名称',
  `cust_categ_l3` varchar(50) DEFAULT NULL COMMENT '客户类型3编码',
  `cust_categ_l3_name` varchar(50) DEFAULT NULL COMMENT '客户类型3名称',
  `sale_grd` varchar(50) DEFAULT NULL COMMENT '销量等级',
  `dirct_sales_flg` char(1) DEFAULT NULL COMMENT '是否直营',
  `gourm_flg` char(1) DEFAULT NULL COMMENT '是否有美顾',
  `mvc_flg` char(1) DEFAULT NULL COMMENT '是否MVC',
  `tallyman_flg` char(1) DEFAULT NULL COMMENT '是否有理货员',
  `cp_prmptr_flg` char(1) DEFAULT NULL COMMENT '是否有CP推广员',
  `sales_rep_id` varchar(50) DEFAULT NULL COMMENT '	业代工号',
  `cust_since_date` varchar(50) DEFAULT NULL COMMENT '开始合作时间',
  `cust_stat_cd` varchar(50) DEFAULT NULL COMMENT '客户状态',
  `cust_close_date` varchar(50) DEFAULT NULL COMMENT '客户关闭时间',
  `sales_rep_name` varchar(50) DEFAULT NULL COMMENT '业代名称',
  `county_cd` varchar(50) DEFAULT NULL COMMENT '地域编码',
  `sales_bu_cd` varchar(50) DEFAULT NULL COMMENT '组织编码',
  `investment_type` varchar(50) DEFAULT NULL COMMENT '投资类型（目前没有数据，待切换CRM系统后有）',
  `store_address` varchar(50) DEFAULT NULL COMMENT '门店详细地址',
  `enable` char(1) DEFAULT NULL COMMENT '是否可用',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(50) DEFAULT NULL COMMENT '创建者',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_user` varchar(50) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`row_id`),
  KEY `idx_cust_num` (`cust_num`),
  KEY `idx_cust_name` (`cust_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='KA门店信息';

-- ----------------------------
-- Records of sys_store_info
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL,
  `salt` varchar(128) DEFAULT NULL COMMENT '加密盐值',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `sex` int(255) DEFAULT NULL COMMENT '性别：1男2女',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `img` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `status` int(1) NOT NULL COMMENT '用户状态：1有效; 2删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `oauth_password` varchar(255) DEFAULT NULL COMMENT 'Oauth2授权使用，DES加密',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_user_id_uindex` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '56963f91d151bcec486202ddfe961e1b', '637e75dbf633ad6625afe45f7ccbcc7c', 'PUBOOT', '888@qq.com', '15666666666', '1', '22', 'https://tse1-mm.cn.bing.net/th/id/OIP.Ups1Z8igjNjLuDfO38XhTgHaHa?pid=Api&rs=1', '1', '2018-05-23 21:22:06', '2018-07-17 23:04:46', '2021-03-27 11:08:20', 'a8dc404dda79c3af6c85f05a55d4eeee');
INSERT INTO `user` VALUES ('1000000668738013', '18040112', '241cf2eb46aedb06db9729b5ac383986', '5a9cce97b048d8f58b1d1ff3862d8743', null, '111111111@qq.com', '10086', '1', '25', null, '0', '2021-03-19 16:01:44', '2021-03-19 16:06:03', '2021-03-19 16:01:44', 'a8dc404dda79c3af6c85f05a55d4eeee');
INSERT INTO `user` VALUES ('1000000720175301', 'apple', '3bb2618b335cc29adba68c530acb9b0b', '9eb5b3cc3ac88880af1b6bdf9aeea083', null, '111111111@qq.com', '111111', '1', '11', null, '1', '2021-03-17 16:52:43', '2021-03-26 13:16:27', '2021-03-27 11:10:00', 'a8dc404dda79c3af6c85f05a55d4eeee');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `role_id` varchar(20) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='后台用户角色表';

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('6', '1000001354351919', '3');
INSERT INTO `user_role` VALUES ('11', '1', '1');
INSERT INTO `user_role` VALUES ('37', '1000000720175301', '1000001940440719');
INSERT INTO `user_role` VALUES ('38', '1000000720175301', '1000001586961115');
