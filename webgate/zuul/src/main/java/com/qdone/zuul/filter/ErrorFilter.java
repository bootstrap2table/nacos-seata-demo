package com.qdone.zuul.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Maps;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.netflix.zuul.filters.post.SendErrorFilter;
import org.springframework.http.MediaType;
import org.springframework.util.ReflectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * @author 付为地
 * 网关统一异常处理
 * 参考:
 * https://blog.csdn.net/xiweiller/article/details/100889575
 * https://blog.csdn.net/iechenyb/article/details/84976069
 */
@Slf4j
@ConditionalOnProperty(name="zuul.SendErrorFilter.error.disable")
public class ErrorFilter extends SendErrorFilter {

    @Override
    public String filterType() {
        return super.filterType();
    }

    @Override
    public int filterOrder() {
        return super.filterOrder();
    }

    @Override
    public boolean shouldFilter() {
        return super.shouldFilter();
    }
    /*
    * 处理异常
    * */
    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        HttpServletResponse response = ctx.getResponse();
        try {
            SendErrorFilter.ExceptionHolder exception = super.findZuulException(ctx.getThrowable());
            Map<String, Object> map = Maps.newLinkedHashMap();
            map.put("timestamp", new Date());
            map.put("status", ctx.getResponseStatusCode());
            map.put("error", "Zuul Internal Server Error");
            /*map.put("cause", exception.getErrorCause());*/
            map.put("message",exception.getThrowable().getMessage());
            map.put("path",request.getRequestURI());
            log.error("ErrorFilter处理异常 error:{},cause:{},message:{}",exception,exception.getErrorCause(),exception.getThrowable().getMessage());
            response.setStatus(ctx.getResponseStatusCode());
            response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            response.getOutputStream().write(JSON.toJSONStringWithDateFormat(map,"yyyy-MM-dd HH:mm:ss S", SerializerFeature.WriteMapNullValue).getBytes());
        } catch (IOException e) {
            ReflectionUtils.rethrowRuntimeException(e);
            log.error("ErrorFilter捕获异常 error:{},cause:{},message:{}",e,e.getCause(),e.getMessage());
        }
        return null;
    }

}
