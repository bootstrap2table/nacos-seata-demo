package com.qdone.zuul.filter;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Maps;
import com.mcy.common.constant.Constant;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.apache.skywalking.apm.toolkit.trace.TraceContext;
import org.slf4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

/**
 * Zuul全局拦截器
 * 配置sw全局traceId
 */
@Slf4j
public class AuthorizeFilter extends ZuulFilter {

    //判断过滤器是否生效
    @Override
    public boolean shouldFilter() {
        return true;
    }
    //表示在请求之前执行
    //过滤器的类型
    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    //过滤器执行的顺序 一个请求在同一个阶段存在多个过滤器时候，多个过滤器执行顺序问题
    @Override
    public int filterOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    //编写token拦截业务,以及全局traceId日志
    @Override
    @Trace
    public Object run() {
        // 案例：拦截所有都服务接口，判断服务接口上是否有传递userToekn参数
        //获取上下文
        RequestContext currentContext = RequestContext.getCurrentContext();
        //全局日志
        String globalLogPrifix= TraceContext.traceId();
        if(StringUtils.isEmpty(globalLogPrifix)||globalLogPrifix.equals("Ignored_Trace")){
            globalLogPrifix=UUID.randomUUID().toString().replaceAll("-","").toUpperCase();
        }
        //日志绑定MDC
        MDC.put(Constant.GLOBAL_LOG_PRIFIX, globalLogPrifix);
        //获取request对象
        HttpServletRequest request = currentContext.getRequest();
        //验证token时候 token的参数 从请求头获取
        String userToken = request.getHeader(Constant.X_AMZ_SECURITY_TOKEN);
        if (StringUtils.isEmpty(userToken)) {
            log.info("用户未登录或无授权");
            //返回错误提示 false  不会继续往下执行 不会调用服务接口了 网关直接响应给客户了
            currentContext.setSendZuulResponse(false);
            currentContext.addZuulResponseHeader("Content-Type", "application/json;charset=UTF-8");
            Map<String, Object> map = Maps.newLinkedHashMap();
            map.put("timestamp", new Date());
            map.put("status", HttpStatus.UNAUTHORIZED.value());
            map.put("error", "");
            map.put("message", "用户未登录或无授权");
            /*map.put("cause", "");*/
            map.put("path",request.getRequestURI());
            currentContext.setResponseBody(JSON.toJSONStringWithDateFormat(map,"yyyy-MM-dd HH:mm:ss S", SerializerFeature.WriteMapNullValue));
            currentContext.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
            return null;
        }
        currentContext.addZuulRequestHeader(Constant.GLOBAL_LOG_PRIFIX, globalLogPrifix);
        log.info("zuul网关==》授权令牌:{},全局日志:{}",userToken,globalLogPrifix);
        //否则正常执行 调用服务接口...
        return null;
    }


}
