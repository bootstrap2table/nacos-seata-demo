package com.mcy.order.error;

import com.google.common.collect.Maps;
import com.mcy.common.entity.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.text.Collator;
import java.util.*;

/**
 * 局部异常处理器
 * 注意确定异常，不要配置Exception基类
 * seata在配置Exception基类时,会出现无法回滚情况
 * 注意不要跟GlobalErrorHandler重复配置相同异常
 */
@ControllerAdvice(annotations = RestController.class)
@Slf4j
public class PartialErrorHandler {

    /**
     * 参数验证异常处理
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Object handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        log.error(printErrorStack(ex));
        BindingResult bindingResult=ex.getBindingResult();
        if(ObjectUtils.isNotEmpty(bindingResult)&&bindingResult.hasErrors()){
            return Result.error(HttpStatus.BAD_REQUEST.value(),"参数验证失败", validErrorResult(bindingResult));
        }else {
            return Result.error(HttpStatus.BAD_REQUEST.value(), "参数验证失败");
        }
    }

    /**
     * 打印异常堆栈信息
     */
    private String printErrorStack(Exception e){
        //存储异常堆栈信息到数据库,获取具体堆栈异常日志
        StringBuffer errStack=new StringBuffer(2048);
        errStack.append(e.toString());
        StackTraceElement[] stackArr=e.getStackTrace();
        if(!org.springframework.util.ObjectUtils.isEmpty(stackArr)){
            for (StackTraceElement stack: stackArr) {
                errStack.append("\n\tat " + stack);
            }
        }
        return errStack.toString();
    }

    /**
     * 表单验证结果
     * @param userResult:表单验证参数
     * @return 表单验证结果
     */
    private List<Map<String, String>> validErrorResult(BindingResult userResult) {
        List<Map<String, String>> errorList=new ArrayList<Map<String, String>>();
        if (userResult.hasErrors()) {
            List<FieldError> errors = userResult.getFieldErrors();
            for (FieldError error : errors) {
                //响应验证结果
                if(StringUtils.isNotEmpty(error.getField())){
                    Map<String, String> map  = Maps.newHashMap();
                    map.put("field", error.getField());
                    map.put("error", error.getDefaultMessage());
                    errorList.add(map);
                }
            }
            //针对field字段不为空，才做排序，空值需要单独处理
            if(CollectionUtils.isNotEmpty(errorList)){
                Collections.sort(errorList, new Comparator<Map>() {
                    @Override
                    public int compare(Map o1, Map o2) {
                        //获取英文环境
                        Comparator<Object> com = Collator.getInstance(Locale.ENGLISH);
                        return com.compare(o1.get("field"),o2.get("field"));
                    }
                });
            }
        }
        return errorList;
    }
}
