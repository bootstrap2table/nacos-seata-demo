package com.mcy.order.error;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.SentinelRpcException;
import com.mcy.common.entity.Result;
import com.mcy.common.error.GlobalException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

/**
 * 异常处理视图
 * @author 付为地
 */
@RestController
@Slf4j
public class GlobalErrorHandler {
	/**
	 * 沒有權限
	 */
	@RequestMapping("/404")
	public Object error404(HttpServletRequest req) {
		if(ObjectUtils.isNotEmpty(req.getAttribute("exception"))){
			Exception e= (Exception) req.getAttribute("exception");
			log.error(printErrorStack(e));
			return Result.error(HttpStatus.NOT_FOUND.value(), e.getMessage());
		}else{
			return Result.error(HttpStatus.NOT_FOUND.value(), "资源不存在");
		}
	}

	/**
	 * 请求异常
	 * @return
	 */
	@RequestMapping("/400")
	public Object error400(HttpServletRequest req) {
		if(ObjectUtils.isNotEmpty(req.getAttribute("exception"))){
			Exception e= (Exception) req.getAttribute("exception");
			log.error(printErrorStack(e));
			return Result.error(HttpStatus.BAD_REQUEST.value(), e.getMessage());
		}else{
			return Result.error(HttpStatus.BAD_REQUEST.value(), "错误的请求");
		}
	}

	/**
	 * 沒有權限
	 */
	@RequestMapping("/401")
	public Object error401(HttpServletRequest req) {
		if(ObjectUtils.isNotEmpty(req.getAttribute("exception"))){
			Exception e= (Exception) req.getAttribute("exception");
			log.error(printErrorStack(e));
			return Result.error(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
		}else{
			return Result.error(HttpStatus.UNAUTHORIZED.value(), "未获得授权");
		}

	}

	/**
	 * 多种类型异常时，
	 * 请参考business配置异常方式
	 * 依次配置多个异常具体返回值
	 * @param req
	 * @return
	 */
	@RequestMapping("/500")
	public Object error500(HttpServletRequest req) {
		if(ObjectUtils.isNotEmpty(req.getAttribute("exception"))){
			Exception e= (Exception) req.getAttribute("exception");
			log.error(printErrorStack(e));
			if(e instanceof ConstraintViolationException ||e instanceof IllegalArgumentException ||e instanceof GlobalException){
				return Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
			}
			if(e instanceof BlockException){
				return Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Sentinel Blocked The Request");
			}
			if(e instanceof SentinelRpcException){
				return Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), "An Exception Occurred While Sentinel Execution Rpc Request");
			}
			if(e instanceof NoHandlerFoundException){
				return Result.error(HttpStatus.NOT_FOUND.value(), e.getMessage());
			}
			return Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
		}else{
			return Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), "服务器错误");
		}
	}


	/**
	 * 打印全局异常堆栈信息
	 */
	private String printErrorStack(Exception e){
		//存储异常堆栈信息到数据库,获取具体堆栈异常日志
		StringBuffer errStack=new StringBuffer(2048);
		errStack.append(e.toString());
		StackTraceElement[] stackArr=e.getStackTrace();
		if(!org.springframework.util.ObjectUtils.isEmpty(stackArr)){
			for (StackTraceElement stack: stackArr) {
				errStack.append("\n\tat " + stack);
			}
		}
		return errStack.toString();
	}


}
