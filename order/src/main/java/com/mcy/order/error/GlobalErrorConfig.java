package com.mcy.order.error;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.ErrorPageRegistrar;
import org.springframework.boot.web.server.ErrorPageRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Properties;
/*import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;*/

/**
 * 异常配置
 */
@Configuration
public class GlobalErrorConfig implements ErrorPageRegistrar{

	 /**
	 * 全局异常处理器
	 * @author 付为地
	 */
	public class GlobalErrorResolver extends SimpleMappingExceptionResolver {
		@Override
		protected ModelAndView doResolveException(HttpServletRequest request,
												  HttpServletResponse response, Object handler, Exception ex) {
			response.setCharacterEncoding("utf-8");
			String viewName = determineViewName(ex, request);
			if (ObjectUtils.isNotEmpty(viewName)) {
				Integer statusCode = determineStatusCode(request, viewName);
				if (ObjectUtils.isNotEmpty(statusCode)) {
					applyStatusCodeIfPossible(request, response, statusCode);
				}
				return getModelAndView(viewName, ex, request);
			} else {
				return getModelAndView("/500", ex, request);
			}
		}
	}

	/**
	 * 异常处理
	 */
	@Bean
	public GlobalErrorResolver exceptionResolver() {
		GlobalErrorResolver resolver = new GlobalErrorResolver();
		resolver.setDefaultErrorView("/500");
		resolver.setDefaultStatusCode(500);
		Properties statusCodes = new Properties();
		statusCodes.setProperty("/500", "500");
		statusCodes.setProperty("/404", "404");
		resolver.setStatusCodes(statusCodes);
		Properties mappings = new Properties();
		mappings.setProperty("java.sql.SQLException", "/500");
		mappings.setProperty("org.springframework.web.bind.ServletRequestBindingException", "/500");
		mappings.setProperty("java.lang.IllegalArgumentException", "/500");
		mappings.setProperty("java.lang.Exception", "/500");
		mappings.setProperty("java.lang.ArithmeticException", "/500");
		mappings.setProperty("org.springframework.web.multipart.MaxUploadSizeExceededException", "/500");
		mappings.setProperty("org.springframework.web.bind.MethodArgumentNotValidException", "/500");
		mappings.setProperty("com.example.util.RRException", "/500");
		resolver.setExceptionMappings(mappings);
		return resolver;
	}


    /**
     *  异常页面处理
     *    ErrorPage相当于mvc的web配置文件里面默认配置
     *    本处特殊处理404页面错误
     */
	@Override
	public void registerErrorPages(ErrorPageRegistry errorPageRegistry) {
        /*1、按错误的类型显示错误的网页*/
        /*错误类型为404，找不到网页的，默认显示404.html网页*/
        ErrorPage e404 = new ErrorPage(HttpStatus.NOT_FOUND, "/404");
        /*错误类型为500，表示服务器响应错误，默认显示500.html网页*/
        /*ErrorPage e500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/500");
        errorPageRegistry.addErrorPages(e404, e500);*/
		errorPageRegistry.addErrorPages(e404);
    }
	
}
