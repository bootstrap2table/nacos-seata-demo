package com.mcy.order.controller;
import com.mcy.common.constant.Constant;
import com.mcy.common.entity.User;
import com.mcy.order.entity.TOrder;
import com.mcy.order.feign.BootClient;
import com.mcy.order.service.ITOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author mcy
 * @since 2020-06-06
 */
@Slf4j
@RestController
@RequestMapping("order/")
public class TOrderController {
    @Autowired
    private ITOrderService orderService;

    @Autowired
    private BootClient bootClient;

    @PostMapping("create")
    @Trace
    public String create(String userId, String productId, int count, BigDecimal amount){
        log.info("order服务创建订单:{}",userId);
        TOrder order = new TOrder();
        order.setUserId(userId);
        order.setOrderId(UUID.randomUUID().toString().replaceAll("-","").toUpperCase());
        order.setProductId(productId);
        order.setCount(count);
        order.setAmount(amount);
        orderService.save(order);
        return "success";
    }

    @PostMapping("token")
    public String token(String userId, String productId, int count, BigDecimal amount, HttpServletRequest request){
        System.err.println("order获取的token:"+request.getHeader(Constant.X_AMZ_SECURITY_TOKEN));
        bootClient.hello();
        System.err.println(1/0);
        return "success";
    }

    @DeleteMapping("delete")
    public String delByOrderId(String orderId){
        orderService.delByOrderId(orderId);
        return "success";
    }

    @GetMapping("build")
    @Trace
    public String build(String userId){
        log.info("order服务生成订单:{}",userId);
        TOrder order = new TOrder();
        order.setUserId(userId);
        order.setOrderId(UUID.randomUUID().toString().replaceAll("-","").toUpperCase());
        order.setProductId("123");
        order.setCount(3);
        order.setAmount(new BigDecimal("3"));
        orderService.save(order);
        return "success";
    }

    /**
     * 参数验证
     */
    @PostMapping("valid")
    public String valid(@Valid @RequestBody User user){
        log.info("优雅的参数验证:"+ user);
        return "success";
    }
}
