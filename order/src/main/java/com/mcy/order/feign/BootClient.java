package com.mcy.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 写熔断的方法，如下为第一种:
 *  非核心系统业务，添加熔断操作，需要保证系统的可用性
 */
@FeignClient(name = "boot",fallback = BootClientFallBack.class)
public interface BootClient {

    /**
     * hello
     * 传递参数时，最好不要直接传递httpRequest
     */
    @GetMapping("boot/hello")
    String hello();
}
