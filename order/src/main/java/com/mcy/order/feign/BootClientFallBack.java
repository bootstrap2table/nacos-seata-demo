package com.mcy.order.feign;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@Component
@Slf4j
public class BootClientFallBack implements BootClient {

    @Override
    public String hello() {
        log.error("boot系统熔断了");
        return "系统熔断了！";
    }
}
