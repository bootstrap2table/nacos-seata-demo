package com.mcy.order.config;

import com.mcy.common.constant.Constant;
import com.mcy.common.entity.Result;
import com.mcy.common.error.GlobalException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.lang.Nullable;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 配置全局拦截器
 * 添加MDC，权限到header
 * http://logback.qos.ch/manual/mdc.html#managedThreads
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class TokenInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    Environment env;

    @Override
    @Trace
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String traceId=request.getHeader(Constant.GLOBAL_LOG_PRIFIX);
        boolean isBindTraceId =false;
        try {
            isBindTraceId=bindTrace(traceId);
            //TODO如果有不需要验证的白名单地址，请在此处自行处理，然后放行
            List<String> whiteAddressList=Arrays.asList(new String[]{"/400","/401","/500"});
            PathMatcher pathMatcher = new AntPathMatcher();
            for (String ignoreUrl:whiteAddressList) {
                if (pathMatcher.match(ignoreUrl, request.getRequestURI())) {
                    return true;
                }
            }
            //调用AUTH验证权限
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add(Constant.X_AMZ_SECURITY_TOKEN,request.getHeader(Constant.X_AMZ_SECURITY_TOKEN));
            headers.add(Constant.USER_TOKEN_CLIENT_ID,request.getHeader(Constant.USER_TOKEN_CLIENT_ID));
            /*headers.add(Constant.SERVICE_VERSION,env.getProperty("info.version"));
            Map<String,Object> param= Maps.newHashMap();
            param.put(Constant.USER_TOKEN_PERMISSION,request.getRequestURI());
            HttpEntity<Map<String,Object>> httpEntity = new HttpEntity<Map<String,Object>>(param, headers);*/
            ResponseEntity<Result> responseEntity=restTemplate.exchange(env.getProperty("auth.center.verify.url")+"?"+Constant.USER_TOKEN_PERMISSION+"="+request.getRequestURI(), HttpMethod.GET,new HttpEntity<Map<String,Object>>(null, headers), Result.class);
            if(responseEntity.getStatusCode().equals(HttpStatus.OK)){
               Result result=responseEntity.getBody();
                if(!ObjectUtils.isEmpty(result)){
                    if(result.getCode().equals(HttpStatus.OK.value())&&result.getData().equals(true)){
                        return true;
                    }else{
                        throw new GlobalException(result.getData().toString(), HttpStatus.UNAUTHORIZED.value());
                    }
                }else{
                    throw new GlobalException("资源认证失败", HttpStatus.UNAUTHORIZED.value());
                }
            }else{
                throw new GlobalException("资源认证失败", HttpStatus.UNAUTHORIZED.value());
            }
        }finally {
            if(isBindTraceId){
                MDC.remove(Constant.GLOBAL_LOG_PRIFIX);
            }
        }
    }
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
    }
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
    }

    /**
     * 将traceId绑定到MDC
     */
    private boolean bindTrace(String traceId){
        if(StringUtils.isNotEmpty(traceId)){
            MDC.put(Constant.GLOBAL_LOG_PRIFIX, traceId);
            return true;
        }
        return false;
    }
}
