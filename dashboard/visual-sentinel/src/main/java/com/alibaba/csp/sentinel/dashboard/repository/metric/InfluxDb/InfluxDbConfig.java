package com.alibaba.csp.sentinel.dashboard.repository.metric.InfluxDb;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class InfluxDbConfig {

    private final Logger logger = LoggerFactory.getLogger(InfluxDbConfig.class);

    @Value("${spring.influx.url:''}")
    private String influxDBUrl;

    @Value("${spring.influx.user:''}")
    private String userName;

    @Value("${spring.influx.password:''}")
    private String password;

    @Value("${spring.influx.database:''}")
    private String database;

    @Bean
    public InfluxDB influxDB(){
        InfluxDB influxDB = InfluxDBFactory.connect(influxDBUrl, userName, password);
        try {
            /**
             * 异步插入：
             * enableBatch这里第一个是point的个数，第二个是时间，单位毫秒
             * point的个数和时间是联合使用的，如果满1000条或者2000毫秒
             * 满足任何一个条件就会发送一次写的请求。
             */
            if(!influxDB.databaseExists(database)){
                influxDB.createDatabase(database);
            }
            influxDB.setDatabase(database)
                    .enableBatch(1000,2000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            logger.error("error with influxdb config:{}",e);
        } finally {
            influxDB.setRetentionPolicy("autogen");
        }
        influxDB.setLogLevel(InfluxDB.LogLevel.BASIC);
        return influxDB;
    }
}
