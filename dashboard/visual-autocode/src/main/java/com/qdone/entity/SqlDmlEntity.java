package com.qdone.entity;

import java.io.Serializable;

public class SqlDmlEntity implements Serializable {

    private String table;//sql脚本

    private String mainPath="com";//根目录
    private String packageName="com.shinho.c4i";//根包名
    private String moduleName="ci";//服务的模块名称，通常模块包含controller和service两层
    private String bottomName="dao";//底层持久化名称,通常将dao,mapper,entity拆分在此本模块
    private String controllerName="controller";//controller的包名称
    private String serviceName="service";//service的包名称
    private String daoName="dao";//mapper的dao包名称
    private String entityName="entity";//实体类的包名称
    private Boolean dynamicLikeEnable=false;//mapper文件动态sql,主要针对字符串类型，判断是否采用like
    private String author="admin";//默认作者
    private String email="1335157415@qq.com";//开发者邮箱

    private String schema;//pgsql当前库的schema

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getMainPath() {
        return mainPath;
    }

    public void setMainPath(String mainPath) {
        this.mainPath = mainPath;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getBottomName() {
        return bottomName;
    }

    public void setBottomName(String bottomName) {
        this.bottomName = bottomName;
    }

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getDaoName() {
        return daoName;
    }

    public void setDaoName(String daoName) {
        this.daoName = daoName;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Boolean getDynamicLikeEnable() {
        return dynamicLikeEnable;
    }

    public void setDynamicLikeEnable(Boolean dynamicLikeEnable) {
        this.dynamicLikeEnable = dynamicLikeEnable;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
