package com.qdone;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;
import java.net.InetAddress;

@SpringBootApplication
@Slf4j
public class CodeApplication {

	@SneakyThrows
	public static void main(String[] args) {
		ConfigurableApplicationContext application = SpringApplication.run(CodeApplication.class, args);
		Environment env = application.getEnvironment();
		String activeProfiles= StringUtils.arrayToCommaDelimitedString(env.getActiveProfiles());
		activeProfiles=StringUtils.isEmpty(activeProfiles)?"default":activeProfiles;
		log.info("\n----------------------------------------------------------\n\t" +
						"项目[{}]启动完成! 运行环境[{}]\n\t" +
						"访问地址:http://{}:{}\n\t"+
						"-----------------------------------------------------",
				env.getProperty("spring.application.name"),
				activeProfiles,
				InetAddress.getLocalHost().getHostAddress(),
				env.getProperty("server.port"));
	}

	/**
	 * url传参处理
	 * @return
	 */
	@Bean
	public ConfigurableServletWebServerFactory webServerFactory() {
		TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
		factory.addConnectorCustomizers((TomcatConnectorCustomizer) connector -> connector.setProperty("relaxedQueryChars", "|{}[]\\"));
		return factory;
	}
}
