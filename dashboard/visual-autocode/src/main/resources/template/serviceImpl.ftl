package ${package}.${moduleName}.service.impl;

import ${package}.${bottomName}.${daoName}.${className}Mapper;
import ${package}.${bottomName}.${entityName}.${className}Entity;
import ${package}.${bottomName}.dto.req.${className}Req;
import ${package}.${bottomName}.dto.resp.${className}Resp;
import ${package}.${moduleName}.service.${className}Service;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.BeanUtils;
import com.xiangxiang.commonbase.utils.PageHandle;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;


/**
* ${classname}服务接口
* ${comments}
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
@Service("${classname}Service")
public class ${className}ServiceImpl extends ServiceImpl<${className}Mapper, ${className}Entity> implements ${className}Service{

    @Autowired
    private ${className}Mapper ${classname}Mapper;

    /**
    *分页查询
    */
    @Override
    public PageInfo<${className}Resp> page(${className}Req entity){
         PageHelper.startPage(entity.getPage(), entity.getPageSize(), PageHandle.handleSort(entity));
         return new PageInfo<${className}Resp>(${classname}Mapper.select(entity));
    }


    /**
     * 保存数据
     * @param entity 参数
     * @return
     */
     @Override
     public boolean save(${className}Req entity) {
        ${className}Entity pojo=new ${className}Entity();
        BeanUtils.copyProperties(entity,pojo);
        return save(pojo);
     }

     /**
      * 更新数据
      * @param entity 参数
      * @return
      */
      @Override
      public boolean update(${className}Req entity) {
          ${className}Entity pojo=new ${className}Entity();
          BeanUtils.copyProperties(entity,pojo);
          return updateById(pojo);
      }
}
