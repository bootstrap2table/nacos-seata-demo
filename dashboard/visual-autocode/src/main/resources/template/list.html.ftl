<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>${classname}管理</title>
    <meta name="description" content=""/>
    <meta name="author" content="pc"/>
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0, maximum-scale=1, minimum-scale=1, user-scalable=no"/>
    <meta name="screen-orientation" content="portrait">
    <meta name="format-detection" content="telephone=no"/>
    <#noparse><#include "/inc/common.html"></#noparse>
</head>
<body>
<table id="${classname}Dg"
       data-toggle="table"
       data-method="post"
       data-content-type="application/json"
       data-url="<#noparse>${request.contextPath}</#noparse>/${classname}/selectPage"
       data-toolbar="#${classname}Tb"
       data-height="355"
       data-show-header="true"
       data-cache="false"
       data-click-to-select="true"
       data-single-select="false"
       data-striped="true"
       data-search="false"
       data-pagination="true"
       data-side-pagination="server"
       data-query-params-type="''"
       data-page-size="10"
       data-page-number="1"
       data-page-list="[10,20,50,100,500,1000]"
       data-query-params="${classname}QueryParams"
       data-pagination-loop="false"
       data-classes="table table-hover table-condensed"
       data-sort-stable="true"
       data-show-footer="false"
       data-resizable="true"
       data-reorderable-columns="true"
       data-show-toggle="true"
       data-show-columns="true"
       data-show-multi-sort="true"
       data-show-pagination-switch="true"
       data-show-export="true"
       data-export-types="['excel']"
       data-export-options='{
				         "fileName": "${classname}列表", 
				         "worksheetName": "sheet"
				    }'
>
    <thead>
    <tr>
        <th data-field="state"  data-align="center"  data-checkbox="true"></th>
        <th data-field="index" data-title="序号" data-align="center"  data-formatter="getRowIndex"></th>
        <#if columns?? &&columns?size gt 0>
            <#list columns as column>
                <#if  column.columnName?? && pk.columnName?? && column.columnName == pk.columnName>
        <th data-field='${pk.attrname}' data-sortable='true' data-align='center'  data-title='${pk.comments}'></th>
                <#elseif column.columnName?? && pk.columnName?? && column.columnName != pk.columnName>
        <th data-field='${column.attrname}' data-sortable='true' data-align='center'  data-title='${column.comments}'></th>
                </#if>
            </#list>
        </#if>
    </tr>
    </thead>
</table>

<div id="${classname}Tb" style="padding: 5px; height: auto">
    <div style="margin-bottom: 5px">
        <input type="button" value="添加" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;"
               class="btn-primary" onclick="add${className}()"/>
        <input type="button" value="修改" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;"
               class="btn-primary" onclick="edit${className}()"/>
        <input type="button" value="删除" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;"
               class="btn-primary" onclick="delete${className}()"/>
        <input type="button" value="查询" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;"
               class="btn-primary" onclick="find${className}()"/>
        <input type="button" value="清空" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;"
               class="btn-primary" onclick="clear${className}()"/>
    </div>
    <div>
        <form id="${classname}QueryForm" method="post" action="">
            <table align="left">
                <tr>
                    <#if columns?? &&columns?size gt 0>
                        <#list columns as column>
                            <#if column.columnName?? && pk.columnName?? && column.columnName != pk.columnName>
                    <td>${column.comments}:<input id='${column.attrname}' name='${column.attrname}' style="width: 80px;height:25px;"/></td>
                            </#if>
                        </#list>
                    </#if>
                </tr>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript">
    function ${classname}QueryParams(params){
       <#if columns?? &&columns?size gt 0>
            <#list columns as column>
                <#if  column.columnName?? && pk.columnName?? && column.columnName != pk.columnName>
       params.${column.attrname} = <#noparse>$('#</noparse>${column.attrname}<#noparse>').val();</noparse>
                </#if>
            </#list>
        </#if>
       return params;
    }
    //显示行号
    function getRowIndex(value, row, index) {
        var page=$("#${classname}Dg").bootstrapTable("getOptions");
        return page.pageSize*(page.pageNumber-1)+index+1;
    }
    //添加
    function add${className}(){
        var index=layer.open({
            id:"insert${className}",//唯一Id
            type: 2,//iframe模式
            area: ['800px', '500px'],
            title: '添加${className}',//标题
            shade: 0.6 ,//遮罩透明度
            maxmin: true, //允许全屏最小化
            anim: 1, //0-6的动画形式，-1不开启
            content: "<#noparse>${request.contextPath}/</#noparse>${classname}/preInsert",//内容
        });
    }
    //修改
    function edit${className}(){
        var row = $('#${classname}Dg').bootstrapTable('getSelections');
        if (row.length==1&&row[0].${pk.attrname}!=null&&row[0].${pk.attrname}!=""){//主键不为空
            var index=layer.open({
                id:"update${className}",//唯一Id
                type: 2,//iframe模式
                area: ['800px', '500px'],
                title: '修改${className}',//标题
                shade: 0.6 ,//遮罩透明度
                maxmin: true, //允许全屏最小化
                anim: 1, //0-6的动画形式，-1不开启
                content: "<#noparse>${request.contextPath}/</#noparse>${classname}/preUpdate?${pk.attrname}="+row[0].${pk.attrname},//内容
            });
        }else{
            layer.open({
                title: '提示信息'
                ,content: '请选择一行记录'
            });
        }
    }
    //删除
    function delete${className}(){
        var row = $('#${classname}Dg').bootstrapTable('getSelections');
        if (row.length>0){
            layer.confirm('你确定要删除选中的记录？', {icon: 3, title:'提示'}, function(index){
                $.ajax({
                    url:'<#noparse>${request.contextPath}/</#noparse>${classname}/delete',
                    contentType:"application/json",
                    dataType : "json",
                    type : "post",
                    data : JSON.stringify(row),
                    success:function(json,status){
                        layer.msg('删除成功', {
                            icon:1,
                            time: 1000,
                            end: function(index, layero){
                                find${className}();
                            }
                        });
                    },
                    error:function(json,status){
                        layer.msg('系统异常,请稍后重试或联系技术人员', {
                            icon:5,
                            time: 1000,
                            end: function(index, layero){
                                find${className}();
                            }
                        });
                    }
                });
                layer.close(index);
            });
        }else{
            layer.open({
                title: '提示信息'
                ,content: '请选择需要删除的行!',
                btnAlign: 'c'
            });
        }
    }
    //查询
    function find${className}(){
        $("#${classname}Dg").bootstrapTable("refresh",{
            "silent":true,
            "resetOffset":false,
            "url":"<#noparse>${request.contextPath}/</#noparse>${classname}/selectPage",
            query:$('#${classname}QueryForm').serializeObject()
        });
    }
    //清空查询条件
    function clear${className}(){
        $(':input','#${classname}QueryForm')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    }
</script>
</body>
</html>