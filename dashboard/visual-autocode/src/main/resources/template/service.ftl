package ${package}.${moduleName}.service;

import ${package}.${bottomName}.${entityName}.${className}Entity;
import ${package}.${bottomName}.dto.req.${className}Req;
import ${package}.${bottomName}.dto.resp.${className}Resp;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;


/**
* ${classname}服务接口
* ${comments}
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
public interface ${className}Service extends IService<${className}Entity>{

    /**
    *分页查询
    */
    PageInfo<${className}Resp> page(${className}Req entity);

    /**
    * 保存数据
    * @param entity 参数
    * @return
    */
    boolean save(${className}Req entity);

    /**
    * 更新数据
    * @param entity 参数
    * @return
    */
    boolean update(${className}Req  entity);

}
