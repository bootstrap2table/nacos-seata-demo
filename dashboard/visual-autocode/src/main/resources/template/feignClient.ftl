package ${package}.${moduleName}.feign.remote;

import ${package}.${bottomName}.dto.req.${className}Req;
import org.springframework.cloud.openfeign.FeignClient;
import ${package}.${moduleName}.feign.fallback.factory.${className}ClientFallBackFactory;
<#if feignDefault?? && feignDefault>
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
<#else>
import feign.RequestLine;
</#if>

/**
* ${classname}服务接口
* ${comments}
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
@FeignClient(name = "${moduleName}",fallbackFactory = ${className}ClientFallBackFactory.class)
public interface ${className}Client{


    /**
    * 保存数据
    * @param entity 参数
    * @return
    */
    <#if feignDefault?? && feignDefault>
    @PostMapping("${moduleName}/${classname}/insert")
    Boolean insert(@RequestBody ${className}Req entity);
    <#else>
    @RequestLine("POST /${classname}/insert")
    Boolean insert(${className}Req entity);
    </#if>


    /**
    * 更新数据
    * @param entity 参数
    * @return
    */
    <#if feignDefault?? && feignDefault>
    @PutMapping("${moduleName}/${classname}/update")
    Boolean update(@RequestBody ${className}Req  entity);
    <#else>
     @RequestLine("PUT /${classname}/update")
     Boolean update(${className}Req  entity);
    </#if>


}
