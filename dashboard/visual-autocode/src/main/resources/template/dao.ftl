package  ${package}.${bottomName}.${daoName};

import ${package}.${bottomName}.${entityName}.${className}Entity;
import ${package}.${bottomName}.dto.resp.${className}Resp;
import ${package}.${bottomName}.dto.req.${className}Req;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
* ${classname}服务接口
* ${comments}
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
@Mapper
@Repository
public interface ${className}Mapper extends BaseMapper<${className}Entity> {

    /**
    * 查询列表
    * @param entity 查询入参
    * @return 返回列表
    */
    List<${className}Resp> select(${className}Req entity);
     /**
     * 批量添加数据
     * @param arr 数据
     * @return 批量添加数据
     */
    int batchInsert(List<${className}Entity> arr);

    /**
    * 新增或更新(主键必填)
    * @param entity 待处理参数
    * @return 处理结果
    */
    int merge(${className}Entity entity) ;

    /**
    * 批量添加或修改
    * @param arr 数据
    * @return 批量添加或修改
    */
    int batchMerge(List<${className}Entity> arr);


}