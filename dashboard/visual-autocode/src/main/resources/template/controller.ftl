package ${package}.${moduleName}.${controllerName};

import ${package}.${moduleName}.${serviceName}.${className}Service;
import ${package}.${bottomName}.dto.req.${className}Req;
import ${package}.${bottomName}.dto.resp.${className}Resp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.xiangxiang.commonlog.annote.ActionLog;
import com.github.pagehelper.PageInfo;
import org.springframework.util.Assert;

/**
* ${classname}管理
* ${comments}
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
@RestController
@RequestMapping("/${classname}")
@Api(tags = "${comments}管理",description = "${comments}信息管理")
public class ${className}Controller{

    @Autowired
    private ${className}Service ${classname}Service;


    /**
    * 分页查询
    * @param entity 查询参数
    * @return 分页查询结果
    */
    @PostMapping("/page")
    @ApiOperation(value = "${comments}分页列表", notes = "${comments}分页列表", httpMethod = "POST",response = ${className}Resp.class)
    @ActionLog(moudle = "${comments}管理",actionType = "${comments}分页列表")
    public PageInfo<${className}Resp> page(@RequestBody ${className}Req entity){
        Assert.notNull(entity,"参数不允许为空");
        return ${classname}Service.page(entity);
    }


    /**
    * 添加数据
    * @param entity 对象参数
    * @return 添加数据
    */
    @PostMapping("/insert")
    @ActionLog(moudle = "${comments}管理",actionType = "创建${comments}")
    @ApiOperation(value = "${comments}添加", notes = "创建${comments}", httpMethod = "PUT",response = Boolean.class)
    public Boolean insert(@ApiParam(name = "${comments}对象", value = "传入json格式", required = true) @RequestBody ${className}Req entity) {
        Assert.notNull(entity,"参数不允许为空");
        return ${classname}Service.save(entity);
    }


    /**
    * 更新数据
    * @param entity 对象参数
    * @return 更新数据
    */
    @PutMapping("/update")
    @ActionLog(moudle = "${comments}管理",actionType = "更新${comments}")
    @ApiOperation(value = "更新${comments}", notes = "更新${comments}信息", httpMethod = "PUT",response = Boolean.class)
    public Boolean update(@ApiParam(name = "${comments}对象", value = "传入json格式", required = true)  @RequestBody ${className}Req entity) {
        Assert.notNull(entity,"参数不允许为空");
        return ${classname}Service.update(entity);
    }

    /**
    * 删除数据
    * @param  ${pk.attrname} 主键
    * @return 删除数据
    */
    @PostMapping("/delete")
    @ActionLog(moudle = "${comments}管理",actionType = "删除${comments}")
    @ApiOperation(value = "删除${comments}",notes = "删除${comments}", httpMethod = "POST",response = Boolean.class)
    public Boolean delete(@RequestParam(value="${pk.attrname}", required=true) ${pk.attrType} ${pk.attrname}) {
        Assert.notNull(${pk.attrname},"参数不允许为空");
        return ${classname}Service.removeById(${pk.attrname});
    }

}
