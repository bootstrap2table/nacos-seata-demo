package ${package}.${moduleName}.${controllerName};

import ${package}.${moduleName}.${serviceName}.${className}Service;
import ${package}.${bottomName}.dto.req.${className}Req;
import ${package}.${moduleName}.BaseWebTest;
import org.junit.FixMethodOrder;
import org.junit.Test;
import lombok.SneakyThrows;
import org.junit.runners.MethodSorters;
import org.springframework.http.MediaType;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


/**
* ${classname}管理单元测试
* ${comments}
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
@FixMethodOrder(MethodSorters.JVM)
public class ${className}ControllerTest extends BaseWebTest {


    /**
    * 测试分页
    */
    @Test
    @SneakyThrows
    public void testPage(){
         mockMvc.perform(MockMvcRequestBuilders.post("/${classname}/page")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .header(tokenKey,tokenValue)
        //TODO 需要您自己填写分页参数
        .content(JSON.toJSONString(new ${className}Req(), SerializerFeature.WriteMapNullValue))
        .accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print());
    }

    /**
    * 测试添加
    */
    @Test
    @SneakyThrows
    public void testInsert(){
        mockMvc.perform(MockMvcRequestBuilders.post("/${classname}/insert")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .header(tokenKey,tokenValue)
        //TODO 需要您自己填写添加参数
        .content(JSON.toJSONString(new ${className}Req(), SerializerFeature.WriteMapNullValue))
        .accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print());
    }


    /**
    * 测试更新
    */
    @Test
    @SneakyThrows
    public void testUpdate(){
        mockMvc.perform(MockMvcRequestBuilders.put("/${classname}/update")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .header(tokenKey,tokenValue)
        //TODO 需要您自己填写更新参数
        .content(JSON.toJSONString(new ${className}Req(), SerializerFeature.WriteMapNullValue))
        .accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print());
    }

    /**
    * 测试删除
    */
    @Test
    @SneakyThrows
    public void testDelete(){
         mockMvc.perform(MockMvcRequestBuilders.post("/${classname}/delete")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .header(tokenKey,tokenValue)
        //TODO 需要您自己填写删除参数
        .param("${pk.attrname}",null)
        .accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print());
    }

}
