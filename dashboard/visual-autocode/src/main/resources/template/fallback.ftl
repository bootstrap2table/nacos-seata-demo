package ${package}.${moduleName}.feign.fallback;

import ${package}.${bottomName}.dto.req.${className}Req;
import ${package}.${moduleName}.feign.remote.${className}Client;
import lombok.extern.slf4j.Slf4j;

/**
* ${classname}服务降级处理
* ${comments}
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
@Slf4j
public class ${className}ClientFallBack implements ${className}Client {

    private Throwable throwable;
    public ${className}ClientFallBack(Throwable throwable) {
        this.throwable = throwable;
    }

    /**
    * 保存数据降级处理
    * @param entity 参数
    * @return
    */
    @Override
    public Boolean insert(${className}Req entity) {
        log.error("${classname}创建失败,降级处理:{}",throwable.getMessage());
        return false;
    }
    /**
    * 更新数据降级处理
    * @param entity 参数
    * @return
    */
    @Override
    public Boolean update(${className}Req entity) {
        log.error("${classname}更新失败,降级处理:{}",throwable.getMessage());
        return false;
    }

}
