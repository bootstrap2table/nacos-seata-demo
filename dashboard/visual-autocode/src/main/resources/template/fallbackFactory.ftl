package ${package}.${moduleName}.feign.fallback.factory;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;
import ${package}.${moduleName}.feign.fallback.${className}ClientFallBack;

/**
* ${classname}降级处理
* ${comments}
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
@Component
public class ${className}ClientFallBackFactory implements FallbackFactory<${className}ClientFallBack> {

    @Override
    public ${className}ClientFallBack create(Throwable throwable) {
       return new ${className}ClientFallBack(throwable);
    }

}
