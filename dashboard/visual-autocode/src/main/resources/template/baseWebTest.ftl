package ${package}.${moduleName};

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.xiangxiang.commonrpc.utils.OkHttpClientUtil;
import lombok.SneakyThrows;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import java.util.Map;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


/**
* 单元测试基类
*/
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
@WebAppConfiguration
@FixMethodOrder(MethodSorters.JVM)
public class BaseWebTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    protected MockMvc mockMvc;

    protected String tokenKey="Authorization";
    /**
    * 单元测试用户令牌
    */
    protected String tokenValue="123456789";

    /**
    * 单元测试网关地址
    */
    private String authGatewayUrl="http://127.0.0.1:15109/user/api/mall/login";

    /**
    * 单元测试登录账户
    */
    private String userMobile="mall_admin";
    /**
    * 单元测试登录密码
    */
    private String userPassword="25d55ad283aa400af464c76d713c07ad";


    @SneakyThrows
    @Before
    public void setUp(){
            mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
            //请求头
            Map<String, String> headers = Maps.newHashMap();
            //入参数
            Map<String, Object> data = Maps.newHashMap();
            data.put("userMobile", userMobile);
            data.put("userPassword", userPassword);
            //请求请求网关获取登录地址
            Response response = OkHttpClientUtil.executePostJSON(authGatewayUrl, JSON.toJSONString(data), headers);
            //从响应中拿返回的数据
            ResponseBody body = response.body();
            Buffer buffer = body.source().getBuffer();
            String str = buffer.readUtf8();
            JSONObject jsonObject = JSONObject.parseObject(str);
            tokenValue=jsonObject.getString("data");
    }
}