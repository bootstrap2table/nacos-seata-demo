<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="${package}.${bottomName}.${daoName}.${className}Mapper">

    <resultMap id="${className}Map" type="${package}.${bottomName}.dto.resp.${className}Resp">
        <#if columns?? &&columns?size gt 0>
            <#list columns as column>
                <#if column.dataType?? && column.dataType=='BLOB'>
                    <result column="${column.columnName}"  property="${column.attrname}" typeHandler="org.apache.ibatis.type.BlobTypeHandler"/>
                <#else>
                    <result column="${column.columnName}"  property="${column.attrname}" />
                </#if>
            </#list>
        </#if>
    </resultMap>

    <sql id="allColumns">
        <#if columns?? &&columns?size gt 0>
            <#list columns as column>
                <#if column_index+1<columns?size>${column.columnName},<#else>${column.columnName}</#if>
            </#list>
        </#if>
    </sql>

    <sql id="dynamicWhere">
        <trim  suffixOverrides="," prefix="WHERE" prefixOverrides="AND">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String' && pk.columnName?? && pk.columnName == column.columnName>
                    <if test="${column.attrname}!=null and ${column.attrname}!=''">AND ${column.columnName} = <#noparse>#{</#noparse>${column.attrname}<#noparse>}</if></#noparse>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String' && pk.columnName?? && pk.columnName!=column.columnName && dynamicLikeEnable?? && dynamicLikeEnable>
                    <if test="${column.attrname}!=null and ${column.attrname}!=''">AND ${column.columnName} LIKE concat('%', <#noparse>#{</#noparse>${column.attrname}<#noparse>} ,'%')</if></#noparse>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String' && pk.columnName?? && pk.columnName!=column.columnName && dynamicLikeEnable?? && !dynamicLikeEnable>
                    <if test="${column.attrname}!=null and ${column.attrname}!=''">AND ${column.columnName} = <#noparse>#{</#noparse>${column.attrname}<#noparse>}</if></#noparse>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String' && pk.columnName?? && pk.columnName == column.columnName>
                    <if test="${column.attrname}!=null ">AND ${column.columnName} = <#noparse>#{</#noparse>${column.attrname}<#noparse>}</if></#noparse>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String' && pk.columnName?? && pk.columnName != column.columnName>
                    <if test="${column.attrname}!=null ">AND ${column.columnName} = <#noparse>#{</#noparse>${column.attrname}<#noparse>}</if></#noparse>
                    </#if>
                </#list>
            </#if>
        </trim>
    </sql>

    <!-- select -->
    <select id="select" parameterType="${package}.${bottomName}.dto.req.${className}Req"  resultMap="${className}Map">
        SELECT <include refid="allColumns" />
        FROM ${tableName}  <include refid="dynamicWhere" />
    </select>


    <!-- batchInsert -->
    <insert id="batchInsert" parameterType="java.util.List">
        INSERT INTO ${tableName}  (<include refid="allColumns" />)
        VALUES
        <foreach collection="list" item="item" index="index" separator=",">
            (
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column_index+1<columns?size><#noparse>#{item.</#noparse>${column.attrname}<#noparse>}</#noparse>,<#else><#noparse>#{item.</#noparse>${column.attrname}<#noparse>}</#noparse></#if>
                </#list>
            </#if>
            )
        </foreach>
    </insert>

    <!-- merge -->
    <insert id="merge" parameterType="${package}.${bottomName}.${entityName}.${className}" >
        INSERT INTO ${tableName}
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String'>
                        <if test="${column.attrname}!=null and ${column.attrname}!=''">${column.columnName},</if>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String'>
                        <if test="${column.attrname}!=null">${column.columnName},</if>
                    </#if>
                </#list>
            </#if>
        </trim>
        VALUES
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String'>
                        <if test="${column.attrname}!=null and ${column.attrname}!=''"><#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,</if>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String'>
                        <if test="${column.attrname}!=null"><#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,</if>
                    </#if>
                </#list>
            </#if>
        </trim>
        ON DUPLICATE KEY UPDATE
        <trim suffixOverrides=",">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String'>
                        <if test="${column.attrname}!=null and ${column.attrname}!=''">${column.columnName}=VALUES(${column.columnName}),</if>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String'>
                        <if test="${column.attrname}!=null">${column.columnName}=VALUES(${column.columnName}),</if>
                    </#if>
                </#list>
            </#if>
        </trim>
    </insert>

    <!-- batchMerge -->
    <insert id="batchMerge"  parameterType="java.util.List">
        INSERT INTO ${tableName} (
        <#if columns?? &&columns?size gt 0>
            <#list columns as column>
                <#if column_index+1<columns?size>${column.columnName},<#else>${column.columnName}</#if>
            </#list>
        </#if>
        )
        VALUES
        <foreach collection="list" item="item" index="index" separator=",">
            (
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column_index+1<columns?size><#noparse>#{item.</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,<#else><#noparse>#{item.</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse></#if>
                </#list>
            </#if>
            )
        </foreach>
        ON DUPLICATE KEY UPDATE
        <#if columns?? &&columns?size gt 0>
            <#list columns as column>
                <#if column_index+1<columns?size>${column.columnName}=VALUES(${column.columnName}),<#else>${column.columnName}=VALUES(${column.columnName})</#if>
            </#list>
        </#if>
    </insert>
</mapper>