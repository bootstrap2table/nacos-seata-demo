$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL +'sys/generator/list',
        datatype: "json",
        colModel: [
			{ label: '表名',name: 'tableName', width: 100, key: true },
            { label: '支持降级', id:'fallBack',name: 'fallBack', width: 50, key: false,
                align: "center",
                editable : true,
                edittype : "select",
                editoptions : {value :"false:否;true:是"},
                formatter: function(value, options, row){
                    return value === 'false' ?
                        '<span class="label label-success">否</span>' :
                        '<span class="label label-danger">是</span>';
                }},
            { label: '表备注',name: 'tableComment', width: 120 },
            { label: 'Engine',name: 'engine', width: 100},
			{ label: '创建时间',name: 'createTime', width: 100 }
        ],
        onSelectRow : function(id) {
             jQuery('#jqGrid').jqGrid('editRow', id, true);
        },
		viewrecords: true,
        height: 300,
        rowNum: 10,
		rowList : [10,30,50,100,200],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
    $('#errorModalTip').on('click', function () {
        $("#pwdFormContent").empty();
        $("#pwdFormModal").modal("hide");
    })
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			tableName: null
		},
        showBtn: true,
        showList: true,
        showDDL:false,
        showCreate:false,
        showCreateTitle:false,
        title: null,
        config: {
                table:"",
                mainPath:"com",
                packageName:"com.shinho.c4i",
                moduleName:"ci",
                bottomName:"core",
                controllerName:"controller",
                serviceName:"service",
                daoName:"dao",
                entityName:"entity",
                dynamicLikeEnable:false,
                author:"admin",
                email:"1335157415@qq.com"
        }
	},
	methods: {
		query: function () {
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{'tableName': vm.q.tableName},
                page:1 
            }).trigger("reloadGrid");
		},
        add: function(){
            vm.showBtn = false;
            vm.showList = false;
            vm.showDDL = true;
            vm.showCreate = false;
            vm.showCreateTitle=false;
            vm.title = "创建表";
            vm.config.table="";
            vm.config.mainPath="com";
            vm.config.packageName="com.shinho.c4i";
            vm.config.moduleName="ci";
            vm.config.bottomName="core";
            vm.config.controllerName="controller";
            vm.config.serviceName="service";
            vm.config.daoName="dao";
            vm.config.entityName="entity";
            vm.config.dynamicLikeEnable=false;
            vm.config.author="admin";
            vm.config.email="1335157415@qq.com";
        },
        saveOrUpdate: function (event) {
		    if(vm.config.table==null||vm.config.table==""||vm.config.table.length==0){
		        alert("SQL脚本不允许为空!");
		        return;
            }
            $.ajax({
                type: "POST",
                url: baseURL +"/sys/generator/save" ,
                contentType: "application/json",
                data: JSON.stringify(vm.config),
                success: function(r){
                    if(r.code === 0){
                        layer.msg('SQL执行成功', {
                            icon:1,
                            time: 1000,
                            end: function(index){
                                vm.reload();
                            }
                        });
                    }else{
                        $("#pwdFormModal").modal('show');
                        $("#pwdFormContent").html("<span color='blue'>"+r.msg+"</span>");
                        /*layer.alert(r.msg, {
                            time: 10*1000
                            ,success: function(layero, index){
                                var timeNum = this.time/1000, setText = function(start){
                                    layer.title("脚本错误提示框，将在<font color='red'>"+(start ? timeNum : --timeNum) + '</font> 秒后关闭', index);
                                };
                                setText(!0);
                                this.timer = setInterval(setText, 1000);
                                if(timeNum <= 0) clearInterval(this.timer);
                            }
                            ,end: function(){
                                clearInterval(this.timer);
                            }
                        });*/
                    }
                }
            });
        },
        reload: function (event) {
            vm.showBtn = true;
            vm.showList = true;
            vm.showDDL = false;
            vm.showCreate = false;
            vm.showCreateTitle=false;
            var page = $("#jqGrid").jqGrid('getGridParam','page');
            $("#jqGrid").jqGrid('setGridParam',{
                postData:{'tableName': vm.q.tableName},
                page:page
            }).trigger("reloadGrid");
        },
        create: function (event) {
            vm.showBtn = false;
            vm.showList = true;
            vm.showDDL = false;
            vm.showCreate = true;
            vm.showCreateTitle=true;
            vm.title = "配置代码生成";
            vm.config.table="";
            vm.config.mainPath="com";
            vm.config.packageName="com.xiangxiang";
            vm.config.moduleName="order";
            vm.config.bottomName="order";
            vm.config.controllerName="controller";
            vm.config.serviceName="service";
            vm.config.daoName="dao";
            vm.config.entityName="entity";
            vm.config.dynamicLikeEnable=false;
            vm.config.author="admin";
            vm.config.email="1335157415@qq.com";
        },
		generator: function() {
			var tableNames = getSelectedRows();
			if(tableNames == null){
				return ;
			}
            var rows = new Array();
            for (var i=0;i<tableNames.length;i++){
              var row=$("#jqGrid").jqGrid('getRowData',tableNames[i]);
              if(row.fallBack.indexOf("</select>") != -1){
                  $("#generatorRender").empty();
                  $("#generatorRender").html(row.fallBack);
                  row.fallBack=$("#"+row.tableName+"_fallBack").find("option:selected").val();
                  $("#generatorRender").empty();
              }else{
                  $("#generatorRender").empty();
                  var text=$("#generatorRender").html(row.fallBack).text();
                  if(text=="是"){
                      row.fallBack=true;
                  }else{
                      row.fallBack=false;
                  }
                  $("#generatorRender").empty();
              }
              rows.push(row);
            }
			var service="com"+","+vm.config.packageName+","+vm.config.moduleName+","+vm.config.bottomName+","+vm.config.controllerName+","+vm.config.serviceName+","+vm.config.daoName+","+vm.config.entityName+","+vm.config.dynamicLikeEnable+","+vm.config.author+","+vm.config.email;
            location.href =baseURL +"sys/generator/code?tables=" + JSON.stringify(rows)+"&service="+service;
		}
	}
});

