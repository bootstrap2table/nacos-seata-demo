# open-code

#### 介绍
boot-master配套在线代码生成工具

#### 软件架构
SpringBoot整合freemarker+AdminLTE，开发的在线代码生成器!

#### [在线演示](http://114.67.207.106:8082)

#### 安装教程

1.  下载源码
2.  导入IDEA或者ECliPSE
3.  配置本地数据库连接，切换本地数据生成代码

#### 使用说明

1.  启动项目访问 http://localhost:8082
2.  页面选择需要生产代码的表
3.  下载生成的代码，贴入配套项目
4.  有问题可以提出ISSUES，也可以直接联系作者1335157415@qq.com
5.  工具结构Controller,Service,Dao,Entity，适配boot-mapper项目。

#### 注意事项

1.  暂时支持mysql数据库，后续请等待。。。
2.  设计好数据库表结构（表备注，字段备注尽量全面，页面会需要，主键必须设置）。
3.  生成的zip文件解压之后，注意文件是否为BOM的UTF-8格式，可以使用NOTEPAD++转成无BOM的UTF-8粘贴到对应项目。
4.  配套项目：https://gitee.com/bootstrap2table/boot-mapper



