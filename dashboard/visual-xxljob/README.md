# 定时任务调度服务，基于xxl-job2.3.0稳定版本

#### 项目介绍
任务调度中心,基于xxlJob2.3.0稳定版,部署运行环境jdk8+mysql8.0

#### **项目结构**
```
visual-xxljob
│
├─doc      初始化文档         
│
├─visual-xxljob-api   外部接口服务模块 
│
├─visual-xxljob-admin 核心调度服务 端口:8281
│
└─visual-xxljob-extra 服务依赖组件 

```
#### 部署方式：
- 1.运行指定doc目录中的mysql脚本。(初始账户admin/123456)
- 2.打包部署,采用maven多模块依赖方式,直接从visual-xxljob.pom全局打包,最终运行job-admin服务
- 3.visual-xxljob-admin的配置项在application.properties文件中,需要关注mysql和token,邮箱的具体配置
- 4.visual-xxljob-admin启动完毕，[点我访问](http://127.0.0.1:8281/job-admin) (初始账户admin/123456)
- 5.visual-xxljob-admin支持管理员和一般用户两种角色,只有管理员才可以管理任务,一般用户只能执行任务

#### 微服务接入
### 1. 添加`xxl-job-spring-boot-starter`依赖到您项目:
Maven

```xml
    <dependency>
        <groupId>com.mcy</groupId>
        <artifactId>xxl-job-spring-boot-starter</artifactId>
          <version>${project.version}</version>
    </dependency>
```
### 2. 添加配置项 `application.yaml`
```yaml
xxl:
  job:
    enable: true #是否开启xxl-job,默认开启
    accessToken: #xxl-job令牌
    admin:
      addresses: http://127.0.0.1:8281/job-admin #xxl-job-admin地址，多个使用逗号区分
    executor:
      address:  #执行器地址
      appname: ${spring.application.name} #执行器名称
      ip: #执行器IP
      port: 9999 #执行器端口
      logpath: /data/applogs/xxl-job/jobhandler #xxl-job日志目录
      logretentiondays: 30 #xxl-job存储日志时效
```
### 3.Java代码使用

```java
     @Component
     public class SampleXxlJob {
         private static Logger logger = LoggerFactory.getLogger(SampleXxlJob.class);
         /**
          * 1、简单任务示例（Bean模式）
          */
         @XxlJob("demoJobHandler")
         public void demoJobHandler(String param) throws Exception {
             XxlJobHelper.log("XXL-JOB, Hello World args:{}",XxlJobHelper.getJobParam());
             logger.info("第一个调度任务arg:{}",param);
             logger.info("第一个调度任务jobParam:{}",XxlJobHelper.getJobParam());
             for (int i = 0; i < 5; i++) {
                 XxlJobHelper.log("beat at:" + i);
                 TimeUnit.SECONDS.sleep(2);
             }
             // default success
         }
     }
```
