package com.xxl.job.admin.controller;

import com.xxl.job.admin.core.model.SysActionLog;
import com.xxl.job.admin.core.model.XxlJobUser;
import com.xxl.job.admin.dao.SysActionLogDao;
import com.xxl.job.admin.service.LoginService;
import com.xxl.job.core.biz.model.ReturnT;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统操作日志
 * @author 付为地
 */
@Controller
@RequestMapping("/systemLog")
public class SystemLogController {

    @Resource
    private  SysActionLogDao sysActionLogDao;

    @RequestMapping
    public String index(Model model) {
        return "systemLog/systemLog.index";
    }

    @RequestMapping("/pageList")
    @ResponseBody
    public Map<String, Object> pageList(HttpServletRequest request,
                                        @RequestParam(required = false, defaultValue = "0") int start,
                                        @RequestParam(required = false, defaultValue = "10") int length,
                                        String user, String module,String type) {
        XxlJobUser loginUser = (XxlJobUser) request.getAttribute(LoginService.LOGIN_IDENTITY_KEY);
        //普通用户,只能查询本用户的日志
        if (ObjectUtils.isNotEmpty(loginUser)&&loginUser.getRole()!=1) {
            user=loginUser.getUsername();
        }
        // page query
        List<SysActionLog> list = sysActionLogDao.pageList(start, length, user,module,type);
        int list_count = sysActionLogDao.pageListCount(start, length,  user,module,type);
        // package result
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("recordsTotal", list_count);		// 总记录数
        maps.put("recordsFiltered", list_count);	// 过滤后的总记录数
        maps.put("data", list);  					// 分页列表
        return maps;
    }


    @RequestMapping("/remove")
    @ResponseBody
    public ReturnT<String> remove(@RequestBody List<SysActionLog> ids){
        int ret = sysActionLogDao.batchDelete(ids);
        return (ret>0)?ReturnT.SUCCESS:ReturnT.FAIL;
    }
}
