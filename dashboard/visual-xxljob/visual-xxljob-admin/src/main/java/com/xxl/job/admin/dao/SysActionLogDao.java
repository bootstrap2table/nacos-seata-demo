package com.xxl.job.admin.dao;

import com.xxl.job.admin.core.model.SysActionLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * sysActionLog服务接口
 * 用户日志
 * @author 付为地
 * @email 1335157415@qq.com
 * @date 2021-08-10 15:41:30
 */
@Mapper
public interface SysActionLogDao {

    /**
     * 查询列表
     * @param entity 查询参数
     * @return 查询列表结果
     */
    public List<SysActionLog> selectList(SysActionLog entity);

    /**
     * 查询单项
     * @param entity 查询参数
     * @return 查询结果
     */
    public SysActionLog selectOne(SysActionLog entity);

    /**
     * 根据主键查看
     * @param pk 主键
     * @return 根据主键查询结果
     */
    public SysActionLog view(Long pk);


    /**
     * 删除
     * @param pkList 主键集合
     * @return 根据主键集合删除数据
     */
    public int batchDelete(List<SysActionLog> pkList);

    /**
     * 查詢分頁
     */
    public List<SysActionLog> pageList(@Param("offset") int offset,
                                       @Param("pagesize") int pagesize,
                                       @Param("user") String user,
                                       @Param("moudle") String moudle,
                                       @Param("type") String type);

    /**
     * 查詢分頁总数
     */
    public int pageListCount(@Param("offset") int offset,
                             @Param("pagesize") int pagesize,
                             @Param("user") String user,
                             @Param("moudle") String moudle,
                             @Param("type") String type);
}
