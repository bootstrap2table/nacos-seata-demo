package com.xxl.job.admin.core.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
* 用户日志
*
* @author 付为地
* @email 1335157415@qq.com
* @date 2021-08-10 15:41:30
*/
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysActionLog implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	* 编号
	*/
	private Long id;
	/**
	* 用户令牌
	*/
	private String token;
	/**
	* 链路编号
	*/
	private String trace;
	/**
	* 项目名称
	*/
	private String project;
	/**
	* 模块名称
	*/
	private String moudle;
	/**
	* 操作类型
	*/
	private String actionType;
	/**
	* 日志类型 1:正常 0：异常
	*/
	private char type =1;
	/**
	* 请求URI
	*/
	private String requestUri;
	/**
	* 执行类名
	*/
	private String className;
	/**
	* 执行方法名称
	*/
	private String methodName;
	/**
	* 用户代理
	*/
	private String userAgent;
	/**
	* 操作IP地址
	*/
	private String remoteIp;
	/**
	* 操作方式
	*/
	private String requestMethod;
	/**
	* 请求参数
	*/
	private String requestParams;
	/**
	* 返回参数
	*/
	private String responseParams;
	/**
	* 设备MAC
	*/
	private String requestMac;
	/**
	* 异常信息
	*/
	private String exception;
	/**
	* 执行线程
	*/
	private String actionThread;
	/**
	* 开始执行时刻
	*/
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	private Date actionStartTime;
	/**
	* 结束执行时刻
	*/
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	private Date actionEndTime;
	/**
	* 执行耗时 单位(毫秒)
	*/
	private Long actionTime;
	/**
	* 创建日志时间
	*/
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;
}
