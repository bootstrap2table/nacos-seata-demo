package com.xxl.job.admin;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import java.net.InetAddress;

/**
 * @author xuxueli 2018-10-28 00:38:13
 */
@SpringBootApplication
@Slf4j
public class XxlJobAdminApplication {

	@SneakyThrows
	public static void main(String[] args) {
		ConfigurableApplicationContext application = SpringApplication.run(XxlJobAdminApplication.class, args);
		Environment env = application.getEnvironment();
		String activeProfiles= StringUtils.arrayToCommaDelimitedString(env.getActiveProfiles());
		activeProfiles=StringUtils.isEmpty(activeProfiles)?"default":activeProfiles;
		log.info("<===========[{}]启动完成！"+"运行环境:[{}] IP:[{}] PORT:[{}]===========>", env.getProperty("spring.application.name"),activeProfiles, InetAddress.getLocalHost().getHostAddress(),env.getProperty("server.port"));
	}

}