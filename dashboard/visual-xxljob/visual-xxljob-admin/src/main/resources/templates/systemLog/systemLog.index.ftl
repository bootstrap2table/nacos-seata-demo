<!DOCTYPE html>
<html>
<head>
  	<#import "../common/common.macro.ftl" as netCommon>
	<@netCommon.commonStyle />
	<!-- DataTables -->
  	<link rel="stylesheet" href="${request.contextPath}/static/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <title>${I18n.admin_name}</title>
</head>
<body class="hold-transition skin-blue sidebar-mini <#if cookieMap?exists && cookieMap["xxljob_adminlte_settings"]?exists && "off" == cookieMap["xxljob_adminlte_settings"].value >sidebar-collapse</#if> ">
<div class="wrapper">
	<!-- header -->
	<@netCommon.commonHeader />
	<!-- left -->
	<@netCommon.commonLeft "jobgroup" />
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>系统日志</h1>
		</section>

		<!-- Main content -->
	    <section class="content">

            <div class="row">
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-addon">用户姓名</span>
                        <input type="text" class="form-control" id="user" placeholder="用户名" >
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-addon">业务模块</span>
                        <input type="text" class="form-control" id="module" placeholder="模块" >
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-addon">日志类型:</span>
                        <#--<input type="text" class="form-control" id="type" placeholder="日志类型" >-->
                        <select id='type' name='type' class="form-control" >
                            <option value="">全部</option>
                            <option value="1">正常</option>
                            <option value="0">异常</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <button class="btn btn-block btn-info" id="searchBtn">${I18n.system_search}</button>
                </div>
            </div>
			
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
			            <div class="box-body">
			              	<table id="systemLog_list" class="table table-bordered table-striped display" width="100%" >
				                <thead>
					            	<tr>
                                        <th name="id" >ID</th>
                                        <th name="token" >用户名称</th>
                                        <th name="moudle" >业务模块</th>
                                        <th name="actionType" >业务类型</th>
                                        <th name="type" >日志类型</th>
                                        <th name="requestUri" >请求地址</th>
                                        <th name="requestParams" >请求参数</th>
                                        <th name="responseParams" >返回结果</th>
                                        <th name="actionStartTime" >开始时间</th>
                                        <th name="actionEndTime" >结束时间</th>
                                        <th name="actionTime" >执行时间</th>
					                </tr>
				                </thead>
                                <tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
	    </section>
	</div>

	<!-- footer -->
	<@netCommon.commonFooter />
</div>

<@netCommon.commonScript />
<!-- DataTables -->
<script src="${request.contextPath}/static/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${request.contextPath}/static/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="${request.contextPath}/static/js/systemLog.index.1.js"></script>
</body>
</html>
