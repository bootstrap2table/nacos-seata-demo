$(function() {

	// init date tables
	var jobGroupTable = $("#systemLog_list").dataTable({
		"deferRender": true,
		"processing" : true,
		"serverSide": true,
		"ajax": {
			url: base_url + "/systemLog/pageList",
			type:"post",
			data : function ( d ) {
				var obj = {};
				obj.user = $('#user').val();
				obj.module = $('#module').val();
				obj.type = $('#type').val();
				obj.start = d.start;
				obj.length = d.length;
				return obj;
			}
		},
		"searching": false,
		"ordering": false,
		//"scrollX": true,	// scroll x，close self-adaption
		"columns": [
			{
				"data": 'id',
				"visible" : false
			},
			{
				"data": 'token',
				"visible" : true
			},
			{
				"data": 'moudle',
				"visible" : true
			},
			{
				"data": 'actionType',
				"visible" : true
			},
			{
				"data": 'type',
				"width":'10%',
				"visible" : true,
				"render": function ( data, type, row ) {
					if (row.type == 1) {
						return "正常";
					} else {
						return "异常";
					}
				}
			},
			{
				"data": 'requestUri',
				"visible" : true
			},
			{
				"data": 'requestParams',
				"visible" : true
			},
			{
				"data": 'responseParams',
				"visible" : true
			},
			{
				"data": 'actionStartTime',
				"visible" : true
			},
			{
				"data": 'actionEndTime',
				"visible" : true
			},			{
				"data": 'actionTime',
				"visible" : true
			}
		],
		"language" : {
			"sProcessing" : I18n.dataTable_sProcessing ,
			"sLengthMenu" : I18n.dataTable_sLengthMenu ,
			"sZeroRecords" : I18n.dataTable_sZeroRecords ,
			"sInfo" : I18n.dataTable_sInfo ,
			"sInfoEmpty" : I18n.dataTable_sInfoEmpty ,
			"sInfoFiltered" : I18n.dataTable_sInfoFiltered ,
			"sInfoPostFix" : "",
			"sSearch" : I18n.dataTable_sSearch ,
			"sUrl" : "",
			"sEmptyTable" : I18n.dataTable_sEmptyTable ,
			"sLoadingRecords" : I18n.dataTable_sLoadingRecords ,
			"sInfoThousands" : ",",
			"oPaginate" : {
				"sFirst" : I18n.dataTable_sFirst ,
				"sPrevious" : I18n.dataTable_sPrevious ,
				"sNext" : I18n.dataTable_sNext ,
				"sLast" : I18n.dataTable_sLast
			},
			"oAria" : {
				"sSortAscending" : I18n.dataTable_sSortAscending ,
				"sSortDescending" : I18n.dataTable_sSortDescending
			}
		}
	});

	// table data
	var tableData = {};

	// search btn
	$('#searchBtn').on('click', function(){
		jobGroupTable.fnDraw();
	});
});
