package com.qdone.support.log.autoconfig;

import com.alibaba.ttl.threadpool.TtlExecutors;
import com.qdone.support.log.aspect.ActionLogAspect;
import com.qdone.support.log.properties.ActionLogProperties;
import com.qdone.support.log.util.MdcExecutor;
import lombok.AllArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.skywalking.apm.toolkit.trace.RunnableWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.task.TaskExecutionAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author 傅为地
 * 通用日志装配类
 */
@Configuration
@EnableAsync
@EnableScheduling
@AutoConfigureBefore(value = {TaskExecutionAutoConfiguration.class})
@AutoConfigureAfter(value = {DataSourceAutoConfiguration.class})
@ConditionalOnBean(DataSource.class)
@EnableConfigurationProperties(ActionLogProperties.class)
@ConditionalOnProperty(prefix="action.database.log",name = "enable", havingValue = "true")
public class ActionLogAutoConfig {

    @Autowired
    private ActionLogProperties actionLogProperties;

    /**
     * 实例化日志工具
     * @return jdbcTemplate
     */
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    @ConditionalOnMissingBean(JdbcTemplate.class)
    public JdbcTemplate jdbcTemplate(@Qualifier("dataSource") DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }

    /**
     * ttlExecutor线程池
     * @return 线程池
     */
    @Bean(name = "mdcExecutor")
    @ConditionalOnMissingBean(Executor.class)
    @Order(Ordered.HIGHEST_PRECEDENCE)
    @Primary
    public ExecutorService executor(){
        //任务队列
        BlockingQueue<Runnable> queue=(BlockingQueue)(actionLogProperties.getTask().getQueue() > 0 ? new LinkedBlockingQueue(actionLogProperties.getTask().getQueue()) : new SynchronousQueue());
        //拒绝策略
        RejectedExecutionHandler handler=new ThreadPoolExecutor.CallerRunsPolicy();
        switch (actionLogProperties.getTask().getReject()){
            case 0: handler=new ThreadPoolExecutor.AbortPolicy();
                break;
            case 2: handler=new ThreadPoolExecutor.DiscardOldestPolicy();
                break;
            case 3: handler=new ThreadPoolExecutor.DiscardPolicy();
                break;
            default:
                handler=new ThreadPoolExecutor.CallerRunsPolicy();
        }
        ExecutorService executorService =new MdcExecutor(actionLogProperties.getTask().getCore(),
                actionLogProperties.getTask().getMax(),
                actionLogProperties.getTask().getKeep(),
                TimeUnit.SECONDS,queue, MdcExecutor.mdcThreadFactory(actionLogProperties.getTask().getPrefix()),handler
        );
        return TtlExecutors.getTtlExecutorService(executorService);
    }

    /**
     *实例化请求工具
     * @return restTemplate
     */
    @Bean
    @ConditionalOnMissingBean(RestTemplate.class)
    @Order(Ordered.HIGHEST_PRECEDENCE)
    @Primary
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * 初始化日志处理
     * @param jdbcTemplate
     * @return actionLogAspect
     */
    @Bean
    @ConditionalOnBean({JdbcTemplate.class,ExecutorService.class})
    public ActionLogAspect actionLogAspect(@Autowired JdbcTemplate jdbcTemplate,@Autowired ExecutorService mdcExecutor){
        if(actionLogProperties.isDbEnable()){
            FutureTask<Object> task = new FutureTask<Object>(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    //判断是否存在sys_action_log表，不存在则创建
                    List<Map<String, Object>> result=jdbcTemplate.queryForList("select count(table_name) total from information_schema.tables  where table_schema = (select database()) and table_name = 'sys_action_log'");
                    if(CollectionUtils.isNotEmpty(result)){
                        //不存在sys_action_log表，创建
                        if(result.get(0).get("total").toString().equals("0")){
                            String createSql="CREATE TABLE `sys_action_log` (\n" +
                                    "  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '编号',\n" +
                                    "  `token` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '用户令牌',\n" +
                                    "  `trace` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '链路编号',\n" +
                                    "  `project` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '项目名称',\n" +
                                    "  `moudle` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '模块名称',\n" +
                                    "  `action_type` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '操作类型',\n" +
                                    "  `type` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '日志类型 1:正常 0：异常',\n" +
                                    "  `request_uri` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '请求URI',\n" +
                                    "  `class_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '执行类名',\n" +
                                    "  `method_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '执行方法名称',\n" +
                                    "  `user_agent` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '用户代理',\n" +
                                    "  `remote_ip` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '操作IP地址',\n" +
                                    "  `request_method` varchar(5) COLLATE utf8_bin DEFAULT NULL COMMENT '操作方式',\n" +
                                    "  `request_params` text COLLATE utf8_bin COMMENT '请求参数',\n" +
                                    "  `response_params` text COLLATE utf8_bin COMMENT '返回参数',\n" +
                                    "  `request_mac` varchar(60) COLLATE utf8_bin DEFAULT NULL COMMENT '设备MAC',\n" +
                                    "  `exception` text COLLATE utf8_bin COMMENT '异常信息',\n" +
                                    "  `action_thread` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '执行线程',\n" +
                                    "  `action_start_time` datetime DEFAULT NULL COMMENT '开始执行时刻',\n" +
                                    "  `action_end_time` datetime DEFAULT NULL COMMENT '结束执行时刻',\n" +
                                    "  `action_time` bigint(20) DEFAULT NULL COMMENT '执行耗时 单位(毫秒)',\n" +
                                    "  `create_time` datetime DEFAULT NULL COMMENT '创建日志时间',\n" +
                                    "  PRIMARY KEY (`id`),\n" +
                                    "  KEY `sys_log_trace` (`trace`) USING BTREE\n" +
                                    ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户日志';";
                            jdbcTemplate.execute(createSql);
                            //存在表时，清除超过指定间隔天数的数据,默认存储时间30天
                        }else{
                            long storage=0-actionLogProperties.getStorage();
                            String delSql="delete from sys_action_log where create_time<date_add(now(), interval "+storage+" day)";
                            jdbcTemplate.execute(delSql);
                        }
                    }
                    return "ok";
                }
            });
            mdcExecutor.execute(RunnableWrapper.of(task));
        }
        return new ActionLogAspect();
    }

    /**
     * 配置内置日志清理任务
     * 默认14天执行一次
     * @param jdbcTemplate 数据库操作实例
     * @return logEvictTask
     */
    @Bean
    @ConditionalOnMissingBean(LogEvictTask.class)
    @ConditionalOnBean({JdbcTemplate.class,TaskExecutor.class})
    @ConditionalOnExpression("${action.async.log.job.enable:true}")
    public LogEvictTask logEvictTask(@Autowired JdbcTemplate jdbcTemplate,@Autowired ExecutorService mdcExecutor,@Autowired RestTemplate restTemplate){
        return new LogEvictTask(jdbcTemplate,actionLogProperties,mdcExecutor,restTemplate);
    }

    /**
     * 定义内置日志清理任务
     * 不允许外部直接访问，
     * 仅满足条件时实例化
     */
    @AllArgsConstructor
    private class LogEvictTask {
        private JdbcTemplate jdbcTemplate;
        private ActionLogProperties actionLogProperties;
        private ExecutorService mdcExecutor;
        private RestTemplate restTemplate;

        /**
         * 定时清理日志
         * 配置异步执行
         * 默认每月1号执行
         */
        @Scheduled(cron = "${action.async.log.job.cron:0 0 0 1 * ?}")
        private void clean() {
            if (actionLogProperties.isDbEnable()) {
                mdcExecutor.execute(RunnableWrapper.of(new FutureTask<Object>(new Callable<Object>() {
                            @Override
                            public Object call() throws Exception {
                                //数据库日志推送到solr
                                if(actionLogProperties.getSolr().isEnable()){
                                    HttpHeaders headers = new HttpHeaders();
                                    headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
                                    Map<String, Object> body = new LinkedHashMap<>();
                                    body.put("command",actionLogProperties.getSolr().getCommand());
                                    body.put("clean",true);
                                    body.put("commit",true);
                                    body.put("wt","json");
                                    body.put("indent",true);
                                    body.put("entity",actionLogProperties.getSolr().getEntity());
                                    body.put("verbose",false);
                                    body.put("optimize",false);
                                    body.put("debug",false);
                                    HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(body, headers);
                                    restTemplate.postForEntity(actionLogProperties.getSolr().getUrl(), request, String.class);
                                }
                                //判断是否存在sys_action_log表
                                List<Map<String, Object>> result = jdbcTemplate.queryForList("select count(table_name) total from information_schema.tables  where table_schema = (select database()) and table_name = 'sys_action_log'");
                                if (CollectionUtils.isNotEmpty(result) && !result.get(0).get("total").toString().equals("0")) {
                                    //存在表时，清理日志
                                    long storage = 0 - actionLogProperties.getStorage();
                                    String delSql = "delete from sys_action_log where create_time<date_add(now(), interval " + storage + " day)";
                                    jdbcTemplate.execute(delSql);
                                };
                                return "ok";
                            }
                        })
                ));
            }
        }
    }
}
