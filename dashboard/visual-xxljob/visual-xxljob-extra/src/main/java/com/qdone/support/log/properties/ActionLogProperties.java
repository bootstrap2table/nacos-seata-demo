package com.qdone.support.log.properties;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 傅为地
 * 用户操作自定义日志配置项
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = "action.database.log")
public class ActionLogProperties {

    /**
     * 是否开启日志
     */
    private boolean enable=true;

    /**
     * 是否开启日志记录数据库
     */
    private boolean dbEnable=true;

    /**
     * 项目名称，多项目配置
     */
    private String project="";

    /**
     * 微服务网关链路key
     */
    private String trace="GLOBAL_LOG_PRIFIX";

    /**
     * 微服务用户令牌key
     */
    private String token="X_AMZ_SECURITY_TOKEN";

    /**
     * 存储天数,默认存储最近30天
     */
    private long storage=30;


    /**
     * 日志清理任务
     */
    private Job job=new Job();

    /**
     * 日志清理任务
     */
    @Data
    public class Job{

        /**
         * 是否开启任务
         */
        private boolean enable=true;

        /**
         * 清理日志任务表达式,每月1执行一次
         */
        private String cron="0 0 0 1 * ?";
    }

    /**
     * 日志线程池
     */
    private Task task=new Task();
    /**
     * 日志线程池配置
     */
    @Data
    public class Task{

        /**
         * 日志线程池，核心线程数
         */
        private int core=5;

        /**
         * 日志线程池，最大线程数
         */
        private int max=10;

        /**
         * 日志线程池，缓冲队列数
         */
        private int queue=50;

        /**
         * 日志线程池，线程名称前缀
         */
        private String prefix="async-mdc-task-executor-";

        /**
         * 日志线程池，允许的空闲时间
         */
        private int keep=60;
        /**
         * 日志线程池，线程池拒绝策略0:AbortPolicy,1:CallerRunsPolicy,2:DiscardOldestPolicy,3:DiscardPolicy
         */
        private int reject=1;

    }

    /**
     * 日志上传solr服务
     */
    private Solr solr=new Solr();

    /**
     * 日志上传solr服务
     */
    @Data
    public class Solr{

        /**
         * 是否上传Solr
         */
        private boolean enable=true;

        /**
         * 更新solr数据地址
         */
        private String url="http://127.0.0.1:8983/syslog/dataimport";

        /**
         * 更新solr指令,full-import/delta-import(默认增量)
         */
        private String command="delta-import";

        /**
         * 更新solr数据库实体
         */
        private String entity="syslog";


    }

}
