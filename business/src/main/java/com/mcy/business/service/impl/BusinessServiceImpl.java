package com.mcy.business.service.impl;

import com.mcy.business.feign.BootClient;
import com.mcy.business.feign.OrderClient;
import com.mcy.business.feign.ProductClient;
import com.mcy.business.feign.UserClient;
import com.mcy.business.service.BusinessService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.IOUtils;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;

@Slf4j
@Service
public class BusinessServiceImpl implements BusinessService {

    @Autowired
    private OrderClient orderClient;
    @Autowired
    private UserClient userClient;
    @Autowired
    private ProductClient productClient;
    @Autowired
    private BootClient bootClient;

    @Override
    public String token(String userId, String productId, int count) {
        BigDecimal money = new BigDecimal(1000);
        orderClient.token(userId,productId,count,money);
        return "success";
    }

    @Override
    public String upload(String name) {
        File file = new File("pom.xml");
        DiskFileItem fileItem = (DiskFileItem) new DiskFileItemFactory().createItem("file",
                MediaType.TEXT_PLAIN_VALUE, true, file.getName());

        try (InputStream input = new FileInputStream(file);
             OutputStream os = fileItem.getOutputStream()) {
            IOUtils.copy(input, os);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid file: " + e, e);
        }
        MultipartFile multi = new CommonsMultipartFile(fileItem);
        return userClient.upload(multi);
    }

    /**
     * skywalking打印traceId到日志中
     * @param userId
     * @param productId
     * @param count
     * @return
     */
    @Override
    @GlobalTransactional(rollbackFor = Exception.class)
    @Trace
    public String booking(String userId, String productId, int count) {
        BigDecimal money = new BigDecimal(1000);
        log.info("下单，用户：{}，产品：{},数量：{}",userId,productId,count);
        /*下面就是调用订单服务、用户服务、产品服务*/
        userClient.debit(userId,money);
        productClient.deduct(productId,count);
        orderClient.create(userId,productId,count,money);
        //模拟积分服务熔断处理
        bootClient.merge(userId,money);
        return "success";
    }
}
