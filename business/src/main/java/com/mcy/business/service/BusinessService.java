package com.mcy.business.service;

public interface BusinessService {

    String booking(String userId,String productId,int count);

    String token(String userId,String productId,int count);

     String upload(String name);
}
