package com.mcy.business.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.fastjson.JSON;
import com.mcy.business.error.ExceptionUtil;
import com.mcy.business.service.BusinessService;
import com.mcy.common.constant.Constant;
import com.mcy.common.entity.User;
import com.mcy.common.error.GlobalException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.apache.skywalking.apm.toolkit.trace.TraceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
@RestController
@RequestMapping("business")
public class BusinessController {

    @Autowired
    private BusinessService businessService;

    private Lock lock=new ReentrantLock();


    /**
     * 下单暂时锁定对应商品库存
     * 此处模拟购买一个商品下单
     * 加入谷歌json，传递request会出现问题，这里调整一下
     * @param userId
     * @param productId
     * @param count
     * @return
     */
    @PostMapping("booking")
    @Trace
    /*@ActionLog(moudle="业务系统",actionType = "用户下单")*/
    public String booking(String userId, String productId, Integer count) {
        lock.lock();
        try{
            //判断库存是否充足，如果充足下单，锁定商品固定库存30分钟，
            //其他用户可用库存，总库存减去已经下单和已经下单未支付数量
            //30分钟后，用户未支付，订单自动取消，预定库存回归真实库存
            //当用户点击确定付款，系统付款接口先判断当前订单号的数据,
            //订单过期时间是否已到，没有过期自动修改当前订单的最大付款时间
            System.err.println("全局traceId:"+TraceContext.traceId());
            log.info("bussness执行下单:{},{}",userId,productId);
            log.error("测试日志级别===========>:{},{}",userId,productId);
            /*System.err.println("bussness获取的id:"+request.getHeader("GLOBAL_LOG_PRIFIX"));*/
            return businessService.booking(userId,productId,count);
        }finally {
            lock.unlock();
        }

    }



    /**
     * 下单
     * @param userId
     * @param productId
     * @param count
     * @return
     */
    @PostMapping("token")
    public String token(String userId, String productId, int count, HttpServletRequest request) {
        System.err.println("bussness获取的token:"+request.getHeader(Constant.X_AMZ_SECURITY_TOKEN));
        return businessService.token(userId,productId,count);
    }

    /**
     * 模拟创建文件类型数据
     */
    @RequestMapping("/upload/{name}")
    public String upload(@PathVariable("name") String name) {
        return businessService.upload(name);
    }

    /**
     * 配置限流器
     * @param name
     * @return
     */
    @RequestMapping(value = "/sayHello", method = RequestMethod.GET)
    @ResponseBody
    @SentinelResource(value = "sayHello",fallback = "doFallback",defaultFallback = "defaultFallback")
    public String sayHello(String name) {
        if(StringUtils.isEmpty(name)){
            throw new GlobalException("用户名称不能为空!", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return "Hello, " + name;
    }


    /**
     * Sentinel限流配置:
     *   https://github.com/alibaba/spring-cloud-alibaba/wiki/Sentinel
     *   https://github.com/alibaba/Sentinel/blob/master/sentinel-extension/sentinel-annotation-aspectj/README.md
     *   https://github.com/alibaba/Sentinel/blob/master/sentinel-demo/sentinel-demo-annotation-spring-aop/src/main/java/com/alibaba/csp/sentinel/demo/annotation/aop/service/TestServiceImpl.java
     * speakHello方法简单配置通用阻塞熔断异常
     */
    @RequestMapping(value = "/speakHello", method =RequestMethod.GET)
    @ResponseBody
    @SentinelResource(value = "speakHello",blockHandler = "handleException", blockHandlerClass = {ExceptionUtil.class})
    public String speakHello(String name) {
        if(StringUtils.isEmpty(name)){
            throw new GlobalException("用户名称不能为空!", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return "Hello, " + name;
    }
    /**
     * Sentinel限流配置:
     *   https://github.com/alibaba/spring-cloud-alibaba/wiki/Sentinel
     */
    public String doFallback(String name, Throwable t) {
        // Return fallback value.
        System.err.println("我是指定的熔断器，异常信息是:"+t.getMessage());
        t.printStackTrace();
        return "fallback:"+name;
    }
    /**
     * Sentinel限流配置:
     *   https://github.com/alibaba/spring-cloud-alibaba/wiki/Sentinel
     */
    public String defaultFallback(Throwable t) {
        System.err.println("我是默认的熔断器,异常信息是:"+t.getMessage());
        return "default_fallback";
    }


    /**
     * 测试负载均衡策略
     * @return
     */
    @PostMapping("refund")
    @Trace
    public String refund() {
        System.err.println("全局traceId:"+TraceContext.traceId());
        log.info("全局traceId:"+TraceContext.traceId());
        /*System.err.println("bussness获取的id:"+request.getHeader("GLOBAL_LOG_PRIFIX"));*/
        return "success";
    }

    /**
     * 参数验证
     */
    @PostMapping("/valid")
    public String valid(@Valid @RequestBody User user){
        log.info("优雅的参数验证:"+ JSON.toJSONString(user));
        return "success";
    }
}
