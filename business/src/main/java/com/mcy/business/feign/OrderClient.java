package com.mcy.business.feign;

import com.mcy.business.fallback.OrderClientFallBack;
import com.mcy.business.fallback.factory.OrderClientFallBackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * 不需要指定，token即可拿到token
 */
@FeignClient(name = "${feign.client.order.name}",fallbackFactory = OrderClientFallBackFactory.class)
public interface OrderClient {

    @PostMapping("order/create")
    String create(@RequestParam("userId") String userId, @RequestParam("productId") String productId, @RequestParam("count") int count, @RequestParam("amount") BigDecimal amount);

    @PostMapping("order/token")
    String token(@RequestParam("userId") String userId, @RequestParam("productId") String productId, @RequestParam("count") int count, @RequestParam("amount") BigDecimal amount);
}
