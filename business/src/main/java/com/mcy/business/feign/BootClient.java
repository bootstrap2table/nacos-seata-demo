package com.mcy.business.feign;

import com.mcy.business.fallback.factory.BootClientFallBackFactory;
import com.mcy.common.remote.feign.MultipartConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * 写熔断的方法，如下为第一种:
 *  非核心系统业务，添加熔断操作，需要保证系统的可用性
 */
@FeignClient(name = "${feign.client.boot.name}",fallbackFactory = BootClientFallBackFactory.class,configuration = MultipartConfig.class)
/*@FeignClient(name = "boot",fallback = BootClientFallBack.class)*/
/*@RequestMapping("boot") 接口上面不要直接写@RequestMapping("boot"),强烈不建议如此写法*/
public interface BootClient {

    /**
     * 调用boot-mapper
     */
    @GetMapping("boot/merge")
    String merge(@RequestParam("userId") String userId, @RequestParam("money") BigDecimal money) ;

    @GetMapping("boot/hello/init")
    String init(HttpServletRequest request);
}
