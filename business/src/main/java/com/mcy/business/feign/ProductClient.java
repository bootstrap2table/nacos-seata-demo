package com.mcy.business.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "${feign.client.product.name}")
public interface ProductClient {

    @PostMapping("product/deduct")
    String deduct(@RequestParam("productId") String productId, @RequestParam("count") int count) ;
}
