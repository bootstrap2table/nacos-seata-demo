package com.mcy.business.feign;

import com.mcy.common.remote.feign.MultipartConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
/*
 * 需要文件上传时，配置
 */
@FeignClient(name = "${feign.client.user.name}",configuration = MultipartConfig.class)
public interface UserClient {

    /**
     * 扣减余额
     * @param userId
     * @param money
     * @return
     */
    @PostMapping("user/debit")
    String debit(@RequestParam("userId") String userId, @RequestParam("money") BigDecimal money) ;

    /*文件上传*/
    @PostMapping(value = "user/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    String upload(@RequestPart(value = "file") MultipartFile file);
}
