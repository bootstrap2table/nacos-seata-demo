package com.mcy.business.fallback;

import com.mcy.business.feign.OrderClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Slf4j
public class OrderClientFallBack implements OrderClient {

    private Throwable throwable;
    public OrderClientFallBack(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public String create(String userId, String productId, int count, BigDecimal amount) {
        log.error("order创建订单降级:{}",throwable.getMessage());
        return "order创建订单降级";
    }

    @Override
    public String token(String userId, String productId, int count, BigDecimal amount) {
        log.error("order获取token降级:{}",throwable.getMessage());
        return "order获取token降级";
    }
}
