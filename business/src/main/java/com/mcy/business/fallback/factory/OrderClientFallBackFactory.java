package com.mcy.business.fallback.factory;

import com.mcy.business.fallback.OrderClientFallBack;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * springCloudAlibaba官方推荐用法
 * 配置fallback异常处理
 */
@Component
public class OrderClientFallBackFactory implements FallbackFactory<OrderClientFallBack> {
    @Override
    public OrderClientFallBack create(Throwable throwable) {
        return new OrderClientFallBack(throwable);
    }
}
