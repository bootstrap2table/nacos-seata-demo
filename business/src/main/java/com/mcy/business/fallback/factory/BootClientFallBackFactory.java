package com.mcy.business.fallback.factory;

import com.mcy.business.fallback.BootClientFallBack;
import com.mcy.business.mq.JMSProducer;
import feign.hystrix.FallbackFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * springCloudAlibaba官方推荐用法
 * 配置fallback异常处理
 */
@Component
public class BootClientFallBackFactory  implements FallbackFactory<BootClientFallBack> {
    @Autowired
    private JMSProducer jmsProducer;

    @Override
    public BootClientFallBack create(Throwable throwable) {
        return new BootClientFallBack(throwable,jmsProducer);
    }
}
