package com.mcy.business.fallback;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Maps;
import com.mcy.business.feign.BootClient;
import com.mcy.business.mq.JMSProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Destination;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

@Slf4j
public class BootClientFallBack implements BootClient {

    private Throwable throwable;
    private JMSProducer jmsProducer;

    public BootClientFallBack(Throwable throwable) {
        this.throwable = throwable;
    }

    public BootClientFallBack(Throwable throwable, JMSProducer jmsProducer) {
        this.throwable = throwable;
        this.jmsProducer = jmsProducer;
    }




    /**
     * 业务服务模拟，
     * 积分熔断操作
     * 服务熔断时
     * 积分录入activeMq
     * demo服务处理积分数据
     * @param userId
     * @param money
     * @return
     */
    @Override
    @Trace
    public String merge(String userId, BigDecimal money) {
        log.error("boot系统熔断了！"+userId+"\t "+money+"异常信息:"+throwable.getMessage());
        Map<String,Object> param= Maps.newHashMap();
        param.put("userId",userId);
        param.put("money",money);
        jmsProducer.sendMessage(new ActiveMQQueue("mall.business.score"), JSON.toJSONString(param, SerializerFeature.WRITE_MAP_NULL_FEATURES));
        return "系统熔断了！"+userId+"\t "+money+"异常信息:"+throwable.getMessage();
    }

    @Override
    @Trace
    public String init(HttpServletRequest request) {
        log.error("boot系统熔断了！");
        return "系统熔断了！";
    }
}
