package com.mcy.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mcy.product.entity.TProduct;
import com.mcy.product.mapper.TProductMapper;
import com.mcy.product.service.ITProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mcy
 * @since 2020-06-06
 */
@Service
@Slf4j
public class TProductServiceImpl extends ServiceImpl<TProductMapper, TProduct> implements ITProductService {

    @Override
    @Trace
    public boolean deduct(String productId, int count)  {
        log.info("product服务扣减库存:{},{}",productId,count);
        QueryWrapper<TProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(TProduct::getProductId, productId);
        TProduct product = baseMapper.selectOne(queryWrapper);
        if(product == null){
            throw new RuntimeException("无此产品");
        }
        if(product.getCount()< count){
            throw new RuntimeException("库存不够扣减");
        }
        return baseMapper.deduct(productId,product.getCount()-count) > 0;
    }
}
