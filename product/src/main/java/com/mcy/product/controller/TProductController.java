package com.mcy.product.controller;


import com.mcy.product.service.ITProductService;
import com.qdone.support.async.log.db.annotation.ActionLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author mcy
 * @since 2020-06-06
 */
@RestController
@RequestMapping("product")
public class TProductController {

    @Autowired
    private ITProductService productService;

    @PostMapping("deduct")
    @ActionLog(moudle="产品系统",actionType = "扣减库存")
    public String deduct(String productId,int count)  {
       productService.deduct(productId,count);
        return "success";
    }

}
