# nacos-seata-demo

#### 介绍
SpringCloud集成Seata并使用Nacos做注册中心与配置中心的微服务源码，配置sentinel作为微服务的熔断限流工具，确保系统的稳定安全运行。

#### 软件架构
nacos1.4.2+seata1.4.2+sentinel1.8.0+SpringCloudAlibaba2.2.6+SpringCloud.Hoxton.SR12+SpringBoot2.3.12+nexpion6.11+mybatis-plus+mybatis+oauth2.0

#### 依赖工具
- sentinel源码,本处已经基于1.8.0持久化到nacos,请&nbsp;[点我查看](https://gitee.com/bootstrap2table/sentinel1.8.0.git)
- 系统配置自动代码生成工具,获取源码,请&nbsp;[点我查看](https://gitee.com/bootstrap2table/open-code.git)。<br>

#### **项目特点**  
> * SpringBoot快速入门与进阶，请[点我查看](https://gitee.com/bootstrap2table/boot_master.git)。<br>
> * 项目配置nexpion,实现微服务的蓝绿灰度发布等多场景的特性功能。<br>
> * 项目admin服务,管理微服务权限,接口需在admin配置角色权限才可访问。<br>
> * 项目gateway网关配置oauth2.0，针对微服务访问入口进行权限验证。<br>
> * 微服务调用配置oauth2.0实现授权认证，确保系统的安全性。<br>
> * 采用nacos作为配置和注册中心，采用分环境的不同配置来实现多环境配置。<br>
> * 采用sentinel作为服务熔断与限流工具，基于1.8.0持久化规则到nacos。<br>
> * 采用gateway持久化nacos方式，实现网关的路由动态路由功能。<br>
> * 分布式事务采用seata实现，确保微服务跨系统的数据一致性。<br>
> * 配置xxl-job2.3.0轻量级定时任务管理，集中管理微服务的定时任务。<br>
> * 配置activeMq微服务解耦,客户下单时,积分服务降级处理送积分功能。<br>
> * 配置完善的全局异常处理,参数验证等功能,swagger在线文档功能。<br>
> * 采用SkyWalking实现微服务的链路追踪功能，完善微服务的链路监控。<br>
> * 配置springBootAdmin监控集群环境下各微服务节点的系统运行状态,实施预警。<br>
> * 基于spi封装的Redisson分布式锁组件[点我查看](https://gitee.com/bootstrap2table/nacos-seata-demo/blob/center/support/lock-redisson-spring-boot-starter/README.md)。<br>
> * 基于spi封装的Redisson限流组件[点我查看](https://gitee.com/bootstrap2table/nacos-seata-demo/blob/center/support/rate-redisson-spring-boot-starter/README.md)。<br>
> * 基于spi封装的通用线程池切面日志组件[点我查看](https://gitee.com/bootstrap2table/nacos-seata-demo/blob/center/support/log-async-db-spring-boot-starter/README.md)。<br>


#### **项目结构**
```
soft
│
├─doc    微服务初始化文档         
│
├─admin  微服务后台权限系统  端口:7000
│ 
├─auth   微服务认证授权中心  端口:7001
│ 
├─business 微服务业务服务系统 端口:7002
│
├─demo   微服务演示服务部分 端口:7003
│
├─order   微服务订单服务中心 端口:7004
│
├─product 微服务产品服务中心 端口:7005
│
├─user    微服务用户服务中心 端口:7006
│
├─common 微服务公共组件
│  ├─common-core   微服务公共核心组件
│  └─common-remote 微服务远程调用组件
│
├─dashboard 微服务可视化组件
│  ├─visual-admin     springBootAdmin服务监控端    端口:8010
│  ├─visual-sentinel  sentinel服务熔断器控制台     端口:8080
│  ├─visual-autocode  服务mybatis-plus代码生成模块 端口:8020
│  └─vusual-xxl-job   xxljob分布式调度任务控制台   端口:8030
│  
├─support 微服务组件增强模块
│  ├─cache-redis-spring-boot-starter   redis缓存模块
│  ├─lock-redisson-spring-boot-starter 分布式锁模块
│  ├─lock-zookeeper-spring-boot-starter zk分布式锁模块
│  ├─log-async-db-spring-boot-starter  日志组件模块 
│  ├─rate-redisson-spring-boot-starter 限流组件模块
│  ├─task-layout-spring-boot-starter   任务编排模块  
│  └─xxl-job-spring-boot-starter       任务调度模块
│  
├──webgate 微服务网关模块
│  ├─gateway 网关模块(集成oauth2.0) 端口:9999
│  └─zuul    网关模块               端口:9989
```
#### 快速启动
- 1.启动系统之前，请仔细参考微服务部分的[doc文档](https://docs.apipost.cn/preview/1add88839cfb5033/9522c14528d7003b)，了解系统的信息，快速入门上手使用。
- 2.远程服务器已经配置大部分资源，请拉取seata分布式事务分支，并且本地下载jar。
- 3.官网下载nacos，本地解压直接启动本地nacos(项目代码默认加载本地nacos，只需要本地启动即可，配置信息还是读取的远程)。
- 4.依次启动所有服务，看下控制台是否启动成功。
- 5.本地安装nginx，映射一下demo项目的静态页面，启动nginx，默认80端口
- 6.请求参数参考代码，测试seata分布式事务可以post访问：http://localhost:9999/business/booking
- 7.测试nacos配置中心，启动demo项目成功，读取到nacos配置访问http://localhost/boot/nacosValue
- 8.测试sentinel,下载sentinel,直接本地启动,所有项目启动，访问：http://localhost:8080 用户名:sentinel 密码: sentinel

#### 注意事项

- #### 1.seata1.4跟以前版本不大一样，需要在nacos中导入seata-server配置信息，否则seata无法正常服务

- #### 2.尤其是nacos配置单机启动之后，需要将seata的配置导入nacos，不然服务启动会出现找不到服务的错误

- #### 3.数据库的脚本在doc文件夹下面，还有导入的nacos-config.sh脚本，请将config.txt调整好配置放入到nacos/conf目录下

- #### 4.config.txt配置项vgroupMapping.my_test_tx_group是和在项目里配置的一致，项目中配置的也是my_test_tx_group。请保持一致

- ####5.每一个业务系统必须含有表undo_log表，同时每个表必须设置主键，不然发起全局事物时，可能会出现getmata filed错误。

- ####6.本处采用了nacos+springCloud配置中心的方式,针对的多环境的配置，多data-id配置
  - 参考: https://www.cnblogs.com/larscheng/p/11392466.html
  - 参考: https://github.com/nacos-group/nacos-examples
  - 启动之后记得到nacos页面新增一个boot.properties，
  - boot.properties配置内容:
  - service.name=apple 
  - useLocalCache=true
 - ####7.事务分组时，请务必配置seata.tx-service-group=my_test_tx_group,其他暂时无法连接到seata-server，原因后续研究在看
 - seata分布式事务
  - seata:
  -  enabled: true
  -  application-id: ${spring.application.name}
  -  tx-service-group: my_test_tx_group
  -    service:
  -  vgroup-mapping: 《boot》-seata-service-group: {boot}     #这个boot名字随便但是要保证{}括号的地方要一致,《》包住的内容取得${spring.application.name}也可以写别的
  -  grouplist:
  -    {boot}: 114.67.207.106:8091
- #### 8.sentinel-dashboard官网默认的限流规则存储内存中，一旦服务重启就会导致限流规则失效，本人基于sentinel1.7.1已经修改持久化到nacos环境
- #### 9.sentinel基于1.8.0版本开发持久化nacos，运行过程自行maven打包。
- sentinel的nacos.config配置信息:
- nacos.server.addr=127.0.0.1:8848
- nacos.server.group-id=SENTINEL
- nacos.server.namespace=
- nacos.data.id.postfix.gateway.api=-sentinel-gateway-api
- nacos.data.id.postfix.gateway.flow=-sentinel-gateway-flow
- nacos.data.id.postfix.authority=-sentinel-authority
- nacos.data.id.postfix.degrade=-sentinel-degrade
- nacos.data.id.postfix.flow=-sentinel-flow
- nacos.data.id.postfix.param.flow=-sentinel-param-flow
- nacos.data.id.postfix.system=-sentinel-system
- 启动方式：
- java -Dserver.port=8080 -Dnacos.server.addr=127.0.0.1:8848 -jar sentinel-dashboard.jar
- #### 10.gateway启动时，建议配置JVM参数：-Dcsp.sentinel.app.type=1 -Dcsp.sentinel.log.use.pid=true
- #### 11.gateway动态路由配置，采用nacos持久化gateway.yaml，修改nacos的yaml之后，gateway服务会自动重新采用nacos的配置，绑定路由属性
- #### 12.admin监控针对内存还有线程导出时，存在泄漏用户信息风险，建议监控以及客户端都采用sercurityj加密，对于不需要健康检查的项目，例如:mail 可以选择actuator关闭
- #### 13.配置okhttp替换httpclient，发起feign请求时，具有更高的性能，目前采用的都是okhttp的默认配置
- #### 14.配置skywalking时，需要加上忽略监控检查路由信息，链路追踪时，排除无效检查信息
- #### 15.系统采用的都是yaml文件发版时，在父pom.xml里面配置多个环境参数，打包替换掉子服务内部的bootstrap.yaml配置项，application.yaml或application-uat.yaml文件直接都是放在nacos动态配置即可，每个微服务只需要配置bootstrap.yaml的nacos注册配置。
- #### 16.seata开启分布式事务时，主线程开启子线程进行其他数据库操作，当子线程抛出异常,（主线程不会回滚），默认情况下，主线程不会受到干扰。
- #### 16.1如果想要主线程等待子线程执行完毕，继续执行可以采用Thread.join()。如果将主线程设置守护线程，（主线程停止子线程结束）采用Thread.currentThread().setDaemon(true);
- #### 17 sentinel目前最新版1.7.2还不支持设置设置用户名密码，所以在使用sentinel动态路由时，nacos最好不开启验证，需要等待sentinel发布新版修复此问题
- #### 18 项目发版时，设置统一版本号可以在顶级pom.xml运行mvn -N versions:update-child-modules，以及 mvn versions:set -DnewVersion=1.0.1-SNAPSHOT 可以[版本统一更新](https://www.cnblogs.com/zhengwangzw/p/10604763.html)
- #### 19 项目发版时，在最顶级POM，从(dev,test,uat)选择一个profile打包，整个项目会统一打包成对应环境程序，本地IDEA开发时，需要idea指定运行的profile,指定JVM参数-Dspring.profiles.active=dev
- #### 20 配置log4j2时，log4j2-test.yml(xml,properties,json)是log4j2默认加载的文件，如果存在log4j2-test.yml时，指定dev环境时，会出现日志文件多出一个空的demo-test.log日志，需要把log4j2-test.yaml改个名字,比如:log4j2-fat.yml
- #### 21 配置使用skywalking时，整个微服务集群都采用log4j2，相同的一次请求中网关gateway获取的traceId跟其他内部微服务获取的traceId不一样，boot的微服务内部使用线程池开启的子线程获取的traceId也跟主线程获取traceId不一致。
- #### 22 全链路还是使用gateway生成一次请求的全局traceId，然后将全局traceId放入header，后续的微服务直接获取header的traceId，针对微服务开启线程池子线程情况，使用mdc可以在子线程跟主线程保持全局traceId。
- #### 23 全链路还是使用gateway生成一次请求的全局traceId，然后将全局traceId放入header，后续的微服务使用OncePerRequestFilter配置MDC绑定traceId到线程上下文，线程池方式请参考demo项目线程池配置。
- #### 24 全链路还是使用zuul生成一次请求的全局traceId，是跟skywalking的全局traceId对的上，微服务内部线程池开启子线程获取traceId时，线程池方式请参考demo项目线程池配置。
- #### 25 全链路skywalking针对多线程环境，[跨线程追踪](https://skyapm.github.io/document-cn-translation-of-skywalking/zh/8.0.0/setup/service-agent/java-agent/Application-toolkit-trace-cross-thread.html) 更多资料请参考:[官网文档](https://skyapm.github.io/document-cn-translation-of-skywalking/zh/8.0.0/)
- #### 26 全链路日志追踪时，实际上不需要在feign的header传递网关的token，因为SW的trace是可以串联到一起，代码可以进一步优化，此处只是展示用法，框架使用可以参考business和demo两个结合即可。
- #### 27 集成XxlJob集中式定时任务管理，所有定时任务全部集中管理，负责整个微服务环境的定时任务触发调度与管理
- #### 28 sentinel官网已经升级到1.8.0，最好等待alibabaSpringCloud组件升级适配1.8.0版本，目前可以尝试升级，但是会存在切换成sentinel1.8.0之后跟alibabaSpringCloud2.2.1兼容性的问题，建议等待alibabaSpringCloud整个版本全量升级。
- #### 29 support已经针对redisson分布式锁，zookeeper分布式锁(zookeeper>=3.5.8),redis自动cacheable注解方法级缓存，redisson限流器提供starter封装，具体使用实例，可以参考user服务的TUserController对应方法，大幅提高开发效率。
- #### 30 Starter生成元数据文档，存在一定问题，cache-redis直接配置文档，建议开启元数据文档，[元数据文档](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-configuration-metadata.html#configuration-metadata-format)。
- #### 31 系统全面升级sentinel1.8.0,nacos1.4.0,seata1.4.0,采用nacos多环境配置,目前全部接入线上dev环境配置项，配置多环境时，namespace区分dev,test,pre等，group区分多个项目所在组，比如c4i项目,app,bi等，data-id区分具体哪个项目组的项目,例如app。目前全面接入dev环境，配置nacos账户密码
- #### 32 系统添加MALL_GROUP分组，用来标识一个微服务组，比如C4I系统，里面含有admin,app,bi,ci，job等微服务，这些微服务所属的项目组，data-id是项目组里面的具体项目。
- #### 33 目前采用bootstrap.yml里面配置多套环境，后续改成直接在父pom里面设置多套环境的配置参数，每个服务统一读取POM.XML里面的配置。
- #### 34 nacos-dsicovery服务，默认采用nacos的rule，开启ribbon负载均衡，需要单独配置rule的话建议参考bussiness服务RibbonConfig配置方法。
- #### 35 SpringCloudAlibaba对应springBoot以及nacos等组件版本建议严格按照官网说明的版本使用组件，否则可能会出现一些版本兼容性问题，[版本对应说明](https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E)
- #### 36 引入组件nepxion解决服务的多版本实例共存的场景，配置全链路版本号执行，[nepxion官方说明](http://nepxion.gitee.io/discovery/#/)
- #### 37 微服务需要在网关接口的header传递对应版本号，实现是采用NepxionStrategy自定义的组合策略，实际上也可以直接采用如下的38说明，整个服务都是相同版才允许访问参数:version:1.0.0,表示整个微服务都是走的1.0.0版本实例，如果传递version:2.0.0表示整个服务都是走的2.0.0实例
- #### 38 组件nepxion默认加载规则时，先加载局部规则在加载全局规则，两个规则使用时，依次生效，远程局部规则，比如服务gateway:那么group为元数据spring.cloud.nacos.discovery.metadata.group=discovery-guide-group,dataId就是spring.application.name=gateway,全局规则时，group和dataId都是spring.cloud.nacos.discovery.metadata.group=discovery-guide-group
- #### 39 系统引入OAuth2.0，统一在网关做服务认证和鉴权，各微服务之间调用不在单独做鉴权服务，参考资料[SpringCloudOAuth2教程](https://www.jianshu.com/p/3427549a148a),[SpringCloudGatewayOauth2实现统一认证和鉴权](https://blog.csdn.net/zhenghongcs/article/details/107241168?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-2.control&dist_request_id=0c2bcf3a-8aca-4f23-a508-0bb7a429a36e&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-2.control)
- #### 40 OAuth2.0采用RedisTokenStore时，默认采用jdk序列化，userDetailServie鉴权通过的用户会一起存入redis，网关读取token用户权限时，必须保证auth跟gateway存储的user路径完全一致，否则没法序列化，这里auth和gateway同时依赖common，将auth查询的用户user放在common模块，防止出现auth存储user，gateway无法序列化的情况出现，gateway目前针对token有一个filter验证token，鉴权服务器验证权限.
- #### 41 整个微服务系统gateway-->auth认证---->user获取用户权限-->认证通过，存储到tokenStore。user服务初始化和权限变动时，自动维护redis存储的全系统角色资源权限数据。gateway鉴权时，获取tokenStroe权限数据和redis存储的全系统角色权限数据比对验证，鉴权当前token用户资源权限。
- #### 42 微服务整合springBootAdmin，配置日志在线展示，统一配置在线日志,需要配置logging.file.name=/logs/auth-dev.log,management.endpoint.logfile.external-file=/logs/auth-dev.log，这两个参数才会在admin在线展示日志和配置日志级别
- #### 43 微服务整合springBootAdmin，配置多台admin监控时，需要指定spring.boot.admin.client.url=http://localhost:9999,http://localhost:9998，多个admin地址之间使用逗号区分，配置多个监控admin地址
- #### 44 Skywalking8.4.0配置grpc日志组件，会存在gateway日志webflux异常，zuul组件配置完毕但是不生效，skywalking日志组件查看时间格式不对等问题，此处暂时关闭下线grpc的日志功能，等待skywalking修复缺陷发布新版在做尝试。
- #### 46 微服务调用文档，使用时请参考[微服务文档](https://gitee.com/bootstrap2table/nacos-seata-demo/tree/auth2/doc/docment.htm),测试oauth2微服务网关时，请参考[网关文档](https://gitee.com/bootstrap2table/nacos-seata-demo/tree/auth2/doc/apipost.txt)
- #### 47 后台权限采用[pb_cms](https://gitee.com/bootstrap2table/pb-cms/tree/oauth2),后台配置的微服务系统资源数据，微服务授权登录时，可直接读取权限资源并鉴权处理，可生产环境使用该项目。
- #### 48 客户端请求微服务资源服务器时，需要带上client_id和token,以及version，微服务的资源服务器会自动连接auth验证token和资源权限，用来保证微服务资源服务器数据安全，具体请参考business和order微服务。
- #### 49 统一打包版本号，设置版本号，打包运行(如下命令表示打包2.0.0.RELEASE版本，发布程序运行，设置版本号: mvn versions:set -DnewVersion=2.0.0.RELEASE -DgenerateBackupPoms=false 打包:mvn clean install -Dmaven.test.skip=true 
- #### 50 demo服务目前同时配置devtools和spring-cloud-starter-alibaba-sentinel2.2.6时，启动会提示CGLIB无法注入feign的异常，调整spring-cloud-starter-alibaba-sentinel2.2.5或者拿掉devtools工具皆可正常启动，存在devtools影响到sentinel2.2.6的bean初始化顺序，建议排除掉devtools热部署插件
- #### 51 定时任务XXL-JOB使用时，建议一个微服务配置一个执行器，对应多个任务。同时微服务内部执行任务时，采用XxlJobHelper.getJobParam()获取执行参数，并做后续任务解析参数处理。
- #### 52 用户服务集成knife4j,完善接口自动化配置,完善项目结构,分层更加合理且完善,打包时采用@key@方式，不要使用${key}，会出现无法替换的问题。
                 
       







