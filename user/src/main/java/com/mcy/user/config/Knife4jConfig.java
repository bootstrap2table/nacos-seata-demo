/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.mcy.user.config;

import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * 配置knife4j
 * @author 付为地
 */
@EnableOpenApi
@Configuration
@Import(BeanValidatorPluginsConfiguration.class)
public class Knife4jConfig {
    /**
     * 扩展插件
     */
    private final OpenApiExtensionResolver openApiExtensionResolver;

    public Knife4jConfig(OpenApiExtensionResolver openApiExtensionResolver) {
        this.openApiExtensionResolver = openApiExtensionResolver;
    }

    /**
     * swagger分组InnerApi
     */
    @Bean
    public Docket defaultApi() {
        Docket docket=new Docket(DocumentationType.OAS_30)
                //分组名称
                .groupName("InnerApi")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.mcy.user.controller"))
                .paths(PathSelectors.any())
                .build()
//                .extensions(openApiExtensionResolver.buildExtensions("1.2.x"))
                .extensions(openApiExtensionResolver.buildSettingExtensions())
                .apiInfo(apiInfo());
        return docket;
    }
    private ApiInfo apiInfo(){
        StringBuffer sb=new StringBuffer(1024);
        sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>商城微服务</strong>用户微服务，内部开发文档系统，提供用户服务接口的详细描述以及在线调试,使用起来总体目标是使客户端和文件系统作为服务器以同样的速度来更新,文件的方法，参数和模型紧密集成到服务器端的代码，允许API来始终保持同步。");
        sb.append("<br><strong>注意事项:</strong><ul>");
        sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<li>1.本接口文档,如果没有特殊限定,默认仅支持<strong>POST</strong>方式。</li>");
        sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<li>2.参数描述<strong>模型</strong>和<strong>示例</strong>,分别针对请求的<strong>参数结构</strong>和<strong>参数示例</strong>进行描述。</li>");
        sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<li>3.响应描述<strong>模型</strong>和<strong>示例</strong>,分别针对响应的<strong>数据结构</strong>和<strong>数据示例</strong>进行描述。</li></ul>");
        return new ApiInfoBuilder()
                .title("用户服务")
                .description(sb.toString())
                .termsOfServiceUrl("https://gitee.com/bootstrap2table/nacos-seata-demo")
                .contact(new Contact("付为地","https://gitee.com/bootstrap2table/boot_master","1335157415@qq.com"))
                .version("1.0")
                .build();
    }


    /**
     * swagger分组OpenApi
     * @return
     */
    @Bean
    public Docket openDocket() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("OpenApi")
                .genericModelSubstitutes(DeferredResult.class)
                .useDefaultResponseMessages(false)
                .forCodeGeneration(true)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.mcy.user.open"))
                .paths(PathSelectors.any())
                .build()
                .extensions(openApiExtensionResolver.buildSettingExtensions())
                .apiInfo(apiInfo());
    }

}
