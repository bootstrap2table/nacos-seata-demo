package com.mcy.user.config;

import com.google.common.collect.Maps;
import com.mcy.common.entity.Result;
import com.mcy.common.error.FallBackException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.text.Collator;
import java.util.*;

/**
 * 全局异常处理
 * @author 傅为地
 */
@ControllerAdvice(annotations = {RestController.class})
@Slf4j
public class GlobalErrorHandler {

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    @Trace
    public Object validExceptionHandler(MethodArgumentNotValidException ex) {
        log.error(printErrorStack(ex));
        BindingResult bindingResult = ((MethodArgumentNotValidException) ex).getBindingResult();
        if (!ObjectUtils.isEmpty(bindingResult) && bindingResult.hasErrors()) {
            return Result.error(HttpStatus.BAD_REQUEST.value(), "参数验证失败", validErrorResult(bindingResult));
        } else {
            return Result.error(HttpStatus.BAD_REQUEST.value(), "参数验证失败");
        }
    }

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseBody
    @Trace
    public Object validExceptionHandler(ConstraintViolationException ex) {
        log.error(printErrorStack(ex));
        ConstraintViolationException exception = (ConstraintViolationException) ex;
        List<Map<String, String>> errorList=new ArrayList<Map<String, String>>();
        for (ConstraintViolation constraint : exception.getConstraintViolations()) {
            Map<String, String> map=Maps.newHashMap();
            map.put("field",Objects.toString(constraint.getInvalidValue()));
            map.put("error",constraint.getMessage());
            //针对field字段不为空，才做排序，空值需要单独处理
            sortList(errorList);
        }
        return Result.error(HttpStatus.BAD_REQUEST.value(), "参数验证失败", errorList);
    }

    @ExceptionHandler({BindException.class})
    @ResponseBody
    @Trace
    public Object validExceptionHandler(BindException ex) {
        log.error(printErrorStack(ex));
        BindingResult bindingResult = ((BindException) ex).getBindingResult();
        if (!ObjectUtils.isEmpty(bindingResult) && bindingResult.hasErrors()) {
            return Result.error(HttpStatus.BAD_REQUEST.value(), "参数验证失败", validErrorResult(bindingResult));
        } else {
            return Result.error(HttpStatus.BAD_REQUEST.value(), "参数验证失败");
        }
    }

    /**
     * 针对降级异常，不要被全局异常捕获
     * 服务出现异常，需要进入降级的服务
     * @param ex
     * @return
     */
    @ExceptionHandler({Exception.class})
    @ResponseBody
    @Trace
    public Object validExceptionHandler(Exception ex) {
        if(ex instanceof FallBackException){
            throw new FallBackException(ex.getMessage(),ex.getCause());
        }
        log.error(printErrorStack(ex));
        return Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
    }

    /**
     * 打印全局异常堆栈信息
     */
    private String printErrorStack(Exception e){
        //存储异常堆栈信息到数据库,获取具体堆栈异常日志
        StringBuffer errStack=new StringBuffer(2048);
        errStack.append(e.toString());
        StackTraceElement[] stackArr=e.getStackTrace();
        if(!ObjectUtils.isEmpty(stackArr)){
            for (StackTraceElement stack: stackArr) {
                errStack.append("\n\tat " + stack);
            }
        }
        return errStack.toString();
    }

    /**
     * 表单验证结果
     * @param userResult:表单验证参数
     * @return 表单验证结果
     */
    private  List<Map<String, String>> validErrorResult(BindingResult userResult) {
        List<Map<String, String>> errorList=new ArrayList<Map<String, String>>();
        if (userResult.hasErrors()) {
            List<ObjectError> errors = userResult.getAllErrors();
            for (ObjectError error : errors) {
                Map<String, String> map  = Maps.newHashMap();
                if(error instanceof FieldError){
                    FieldError err= (FieldError) error;
                    map.put("field", err.getField());
                }else{
                    map.put("field", error.getObjectName());
                }
                map.put("error", error.getDefaultMessage());
                errorList.add(map);
            }
            //针对field字段不为空，才做排序，空值需要单独处理
            sortList(errorList);
        }
        return errorList;
    }

    /**
     * 排序处理
     * @param errorList
     * @return
     */
    private void  sortList(List<Map<String, String>> errorList){
        //针对field字段不为空，才做排序，空值需要单独处理
        if(CollectionUtils.isNotEmpty(errorList)){
            Collections.sort(errorList, new Comparator<Map>() {
                @Override
                public int compare(Map o1, Map o2) {
                    //获取英文环境
                    Comparator<Object> com = Collator.getInstance(Locale.ENGLISH);
                    return com.compare(o1.get("field"),o2.get("field"));
                }
            });
        }
    }

}
