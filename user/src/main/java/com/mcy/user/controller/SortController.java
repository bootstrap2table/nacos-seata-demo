/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.mcy.user.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicResponseParameters;
import com.mcy.common.entity.Result;
import com.mcy.user.dto.Solr;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2020/09/18 11:02
 * @since:knife4j-spring-boot2-demo 1.0
 */
@Api(tags = "SOLR管理", description = "SOLR管理")
@RestController
@ApiSupport(order = 1)
@RequestMapping("/sort")
public class SortController {

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "向客人问好")
    @GetMapping("/sayHi")
    public ResponseEntity<String> sayHi(@RequestParam("name") String name) {
        return ResponseEntity.ok("Hi:" + name);
    }


    @ApiOperationSupport(order = 2, ignoreParameters = {"id", "createtime"}, includeParameters = {"title", "name"}, author = "fuweidi")
    @ApiOperation(value = "忽略参数值-Form类型")
    @PostMapping("/ex")
    public Result<Solr> findAll(Solr longUser) {
        return Result.success(longUser);
    }


    @PostMapping("/createOrder426")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "jdk-HashMap-动态创建显示参数-无@RequestBody")
    @DynamicResponseParameters(name = "CreateOrderHashMapModel", properties = {
            @DynamicParameter(name = "", value = "注解id", example = "X000111", required = true, dataTypeClass = Integer.class),
            @DynamicParameter(name = "name3", value = "订单编号-gson"),
            @DynamicParameter(name = "name1", value = "订单编号1-gson"),
    })
    public Result<HashMap> createOrder1235332(@RequestBody HashMap map) {
        return Result.success(map);
    }
}
