package com.mcy.user.controller;


import com.alibaba.fastjson.JSON;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.google.common.collect.Maps;
import com.mcy.common.entity.Solr;
import com.mcy.user.entity.TUser;
import com.mcy.user.service.ITUserService;
import com.qdone.support.async.log.db.util.RtException;
import com.qdone.support.async.log.db.util.ThreadUtil;
import com.qdone.support.cache.redis.autoconfig.SpringContextHolder;
import com.qdone.support.lock.redisson.annotation.LockAction;
import com.qdone.support.lock.redisson.annotation.LockType;
import com.qdone.support.lock.zookeeper.annotation.ZkLock;
import com.qdone.support.lock.zookeeper.annotation.ZkLockType;
import com.qdone.support.lock.zookeeper.repository.ZookeeperLock;
import com.qdone.support.async.log.db.annotation.ActionLog;
import com.qdone.support.rate.redisson.anontation.RateLimiter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.redisson.api.RedissonClient;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpMessage;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author mcy
 * @since 2020-06-06
 */
@Slf4j
@RestController
@RequestMapping("user")
@Api(tags = "用户管理",description = "用户管理")
public class TUserController {

    @Autowired
    private ITUserService userService;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired(required = false)
    private ZookeeperLock zookeeperLock;

    @PostMapping("create")
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "创建用户", notes = "创建用户", httpMethod = "POST",response = String.class)
    public String create(String name,String password,int age,String sex,String address){
        TUser user = new TUser();
        user.setUserId(UUID.randomUUID().toString().replaceAll("-","").toUpperCase());
        user.setUsername(name);
        user.setPassword(password);
        user.setAge(age);
        user.setSex(sex);
        user.setAddress(address);
        userService.save(user);
        return "success";
    }

    /**
     *  扣款
     * @param userId
     * @param money
     */
    @PostMapping("debit")
    @ActionLog(moudle="用户系统",actionType = "扣除款项")
    public String debit(String userId, BigDecimal money)  {
        userService.debit(userId,money);
        return "success";
    }


    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String upload(@RequestPart(value = "file") MultipartFile file) throws IllegalStateException, IOException {
        List<String> arr=org.apache.commons.io.IOUtils.readLines(file.getInputStream(),"utf-8");
        System.err.println("读取文件内容是:"+JSON.toJSONString(arr));
        String resp="上传文件名称:"+file.getName()+"\t 文件内容:"+ JSON.toJSONString(arr);
        return resp;
    }

    @PostMapping("/testRestTemplate")
    @ResponseBody
    public Map<String,Object> testRestTemplate(@RequestBody Solr entity, @RequestHeader String token){
        System.err.println("进入testRestTemplate方法");
        System.err.println("token参数"+token);
        System.err.println("body参数:"+ JSON.toJSONString(entity));
        Map<String,Object> result= Maps.newLinkedHashMap();
        result.put("a", null);
        result.put("b", "");
        return result;
    }


    /**
     * 测试redisLock
     */
    @RequestMapping(value = "/redisLock")
    @ResponseBody
    @LockAction(value = "#redisLockKey", lockType = LockType.REENTRANT_LOCK, waitTime = 30000)
    public Boolean redisLock(@RequestParam(value = "redisLockKey")  String redisLockKey) throws InterruptedException {
        System.err.println(redissonClient.getLock(redisLockKey));
        System.err.println("开始执行业务逻辑");
        testRedisLock();
        userService.testRedisLock();
        TimeUnit.SECONDS.sleep(20);
        System.err.println("业务逻辑执行完毕");
        return true;
    }


    /**
     * 测试redisLock
     * 无参数
     */
    @RequestMapping(value = "/testRedisLock")
    @ResponseBody
    @LockAction
    @ActionLog(moudle="基础数据",actionType = "用户登录")
    public Boolean testRedisLock() throws InterruptedException {
        System.err.println("无参数开始执行业务逻辑");
        TimeUnit.SECONDS.sleep(20);
        System.err.println("无参数业务逻辑执行完毕");
        return true;
    }

    /**
     * 测试redisson缓存
     * 无参数
     */
    @RequestMapping(value = "/testRedissonCache")
    @ResponseBody
    @Cacheable(value="defaultCache",key="'testRedissonCache'")
    public Boolean testRedissonCache()  {
        System.err.println("testRedissonCache开始执行业务逻辑");
        return true;
    }


    /**
     * 测试redisLock
     * 无参数
     */
    @RequestMapping(value = "/testZkLock")
    @ResponseBody
    @ZkLock(value = "#zkLockKey",lockType = ZkLockType.WRITE_LOCK,holdTime=30)
    public Boolean testZkLock() throws InterruptedException {
        System.err.println("无参数开始执行业务逻辑");
        TimeUnit.SECONDS.sleep(20);
        System.err.println("无参数业务逻辑执行完毕");
        return true;
    }

    /**
     * 测试缓存
     * @param user
     * @return
     */
    @PostMapping("/testCache")
    @ResponseBody
    /*@Cacheable(value="defaultCache",key="#root.targetClass.toString().substring(#root.targetClass.toString().lastIndexOf('.')+1).concat('.'+#root.method.name).concat('.'+#user.getName()+'.'+#user.getAge())")*/
    /*@Cacheable(value="default",key="#user.getUsername()")*/
    @Cacheable(value="view",keyGenerator="keyGenerator")
    @ActionLog(moudle="字典数据",actionType = "系统数据")
    public String testCache(@RequestBody TUser user) {
        System.err.println("进入testCache方法，本次传入参数:name="+user.getUsername()+",age="+user.getAge()+"\t 当前线程:"+Thread.currentThread().getName());
        /*if(user.getAge().equals(35)){
            int a=1/0;
        }
        return user.getUsername();*/
        try {
            if(user.getAge().equals(35)){
                int a=1/0;
            }
            return user.getUsername();
        } catch (Exception ex) {
            log.error("错误error：",ex);
            throw new RtException(ex.toString(), HttpStatus.INTERNAL_SERVER_ERROR.value(),ex);
        }
    }


    @PostMapping("/testCache1")
    @ResponseBody
    /*@Cacheable(value="defaultCache",key="#root.targetClass.toString().substring(#root.targetClass.toString().lastIndexOf('.')+1).concat('.'+#root.method.name).concat('.'+#user.getName()+'.'+#user.getAge())")*/
    @Cacheable(value="default",key="#user.getAge()")
    public String testCache1(@RequestBody  TUser user){
        System.err.println("进入testCache1方法，本次传入参数:name="+user.getUsername()+",age="+user.getAge());
        System.err.println(JSON.toJSONString(userService.select(null)));
        return user.getUsername();
    }


    @PostMapping("/testCache2")
    @ResponseBody
    /*@Cacheable(value="defaultCache",key="#root.targetClass.toString().substring(#root.targetClass.toString().lastIndexOf('.')+1).concat('.'+#root.method.name).concat('.'+#user.getName()+'.'+#user.getAge())")*/
    @Cacheable(value="default")
    public String testCache2(){
        System.err.println("进入testCache2方法，本次传入参数");
        return "true";
    }

    @RequestMapping("/lock")
    public Boolean lock(){
        for(int i=0; i<10; i++){
            new RedisLockThread().start();
        }
        return true;
    }

    class RedisLockThread extends Thread {

        @Override
        public void run() {
            String key = "lockKey";
            boolean result = zookeeperLock.lock(key, 10000);
            log.info(result ? "get lock success : " + key : "get lock failed : " + key);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                log.error("exp", e);
            } finally {
                zookeeperLock.releaseLock(key);
                log.info("release lock : " + key);
            }
        }
    }


    /**
     * 简单测试限流器
     */
    @PostMapping(value = "/testRateLimiter")
    @RateLimiter(limit=1, timeout=1,unit = TimeUnit.MINUTES)
    @ResponseBody
    @ActionLog(moudle="用户模块",actionType = "获取流量")
    public Map<String,Object> testRateLimiter(@RequestBody  TUser user){
        System.err.println("进入testRateLimiter方法:"+Thread.currentThread().getName());
//        RedisTemplate<String, Object> redisTemplate= (RedisTemplate<String, Object>) SpringContextHolder.getBean("mybatisCache");
//        System.err.println("进入getBean方法:"+redisTemplate);

        /*Map<String,Object> result=new ConcurrentHashMap<String,Object>();*/
//        testRate();
        Map<String,Object> result=new HashMap<String,Object>();
        result.put("a", null);
        result.put("b", "");
        result.put("c", "马大哈");
        return result;
    }

    @GetMapping(value = "/testRate")
    @RateLimiter(limit=1, timeout=1,unit = TimeUnit.MINUTES)
    @ResponseBody
    public Map<String,Object> testRate(){
        System.err.println("进入testRate方法");
        /*Map<String,Object> result=new ConcurrentHashMap<String,Object>();*/
        Map<String,Object> result=new HashMap<String,Object>();
        result.put("a", null);
        result.put("b", "");
        result.put("c", "testRate");
        return result;
    }


    @PostMapping(value = "/testRate1")
    @RateLimiter(limit=1, timeout=1,unit = TimeUnit.MINUTES,key="#user.getAge()")
    @ResponseBody
    public Map<String,Object> testRate1(@RequestBody  TUser user){
        System.err.println("进入testRate方法");
        /*Map<String,Object> result=new ConcurrentHashMap<String,Object>();*/
        Map<String,Object> result=new HashMap<String,Object>();
        result.put("a", null);
        result.put("b", "");
        result.put("c", "testRate");
        return result;
    }


    @GetMapping(value = "/testRate2")
    @RateLimiter(limit=1, timeout=1,unit = TimeUnit.MINUTES,value = "apple")
    @ResponseBody
    public Map<String,Object> testRate2(){
        System.err.println("进入testRate2方法");
        /*Map<String,Object> result=new ConcurrentHashMap<String,Object>();*/
        Map<String,Object> result=new HashMap<String,Object>();
        result.put("a", null);
        result.put("b", "");
        result.put("c", "testRate");
        return result;
    }

    @PostMapping(value = "/testRate3")
    @RateLimiter(limit=1, timeout=1,unit = TimeUnit.MINUTES)
    @ResponseBody
    public Map<String,Object> testRate3(@RequestBody  TUser user){
        System.err.println("进入testRate3方法");
        /*Map<String,Object> result=new ConcurrentHashMap<String,Object>();*/
        Map<String,Object> result=new HashMap<String,Object>();
        result.put("a", null);
        result.put("b", "");
        result.put("c", "testRate");
        return result;
    }


    @PostMapping("/testError")
    @ResponseBody
    public String testError(){
        System.err.println("进入testError方法，本次传入参数");
        int a=1/0;
       /* try {


        }catch (Throwable e){
            log.error(\);
        }*/
        return "true";
    }

    @ActionLog(moudle = "定时任务",actionType = "同步关差sellin数据")
    public Map<String,String> create(String arg){
        Map<String, String> resp= org.apache.curator.shaded.com.google.common.collect.Maps.newHashMap();
        resp.put("code","200");
        resp.put("content",null);
        resp.put("msg",null);
        return resp;
    }

    /**
     * 附件上传接口
     * @param file
     * @return
     */
    @PostMapping(path = "/uploadUrl")
    @ActionLog(moudle = "文件管理",actionType = "附件上传")
    public String uploadUrl(@RequestBody MultipartFile file) {
        System.err.println(file);
        return "ok";
    }

    @GetMapping(path = "/req")
    @ActionLog(moudle = "特殊管理",actionType = "请求参数")
    public String servletRequestReq(HttpServletRequest req) {
        System.err.println(req);
        return "ok";
    }

    @PostMapping("/testThreadLocal")
    @ActionLog(moudle = "特殊管理",actionType = "testThreadLocal")
    @ResponseBody
    public String testThreadLocal(){
        System.err.println("进入testThreadLocal方法，本次传入参数");
        log.info("threadLocal拿到数据:"+ ThreadUtil.getInstance().get("name"));
        log.info("MDC拿到数据:"+MDC.get("name"));
        if(StringUtils.isEmpty(ThreadUtil.getInstance().get("name"))){
            ThreadUtil.getInstance().put("name","main "+Thread.currentThread().getName());
        }
        if(StringUtils.isEmpty(MDC.get("name"))){
            MDC.put("name","main "+Thread.currentThread().getName());
        }
        System.err.println(1/0);
        return "true";
    }

}
