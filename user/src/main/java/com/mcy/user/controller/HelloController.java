/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.mcy.user.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.mcy.common.entity.Result;
import com.mcy.user.dto.Solr;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2020/09/18 11:02
 * @since:knife4j-spring-boot2-demo 1.0
 */
@Api(tags = "欢迎管理",description = "欢迎管理")
@RestController
@ApiSupport(order = 2)
@RequestMapping("/hello")
public class HelloController {

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "欢迎", notes = "欢迎", httpMethod = "GET",response = String.class)
    @GetMapping("/sayHi")
    public ResponseEntity<String> sayHi(@RequestParam("name")String name){
        return ResponseEntity.ok("Hi:"+name);
    }

    @ApiOperationSupport(order = 2,ignoreParameters = {"id","createtime"})
    @ApiOperation(value = "添加", notes = "创建", httpMethod = "PUT",response = Solr.class)
    @PostMapping("/ex")
    public Result<Solr> findAll(Solr longUser) {
        return Result.success(longUser);
    }
}
