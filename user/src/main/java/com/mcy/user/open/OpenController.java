/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.mcy.user.open;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.mcy.common.entity.Result;
import com.mcy.user.dto.Solr;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.groups.Default;
import java.util.Date;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2020/09/18 11:02
 * @since:knife4j-spring-boot2-demo 1.0
 */
@Api(tags = "open管理",description = "open管理")
@RestController("/open")
@ApiSupport(order = 1)
public class OpenController {

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "欢迎", notes = "欢迎", httpMethod = "GET",response = String.class)
    @GetMapping("/sayHi")
    public ResponseEntity<String> sayHi(@RequestParam("name")String name){
        return ResponseEntity.ok("Hi:"+name);
    }

    @ApiOperationSupport(order = 2,ignoreParameters = {"id","createtime"})
    @ApiOperation(value = "添加", notes = "添加", httpMethod = "POST",response = Solr.class)
    @PostMapping("/create")
    public Result<Solr> create(@Validated({com.mcy.user.dto.Solr.AddGroup.class, Default.class}) @RequestBody Solr longUser) {
        longUser.setCreatetime(new Date());
        return Result.success(longUser);
    }

    @ApiOperationSupport(order = 3,ignoreParameters = {"createtime"})
    @ApiOperation(value = "更新", notes = "更新", httpMethod = "POST",response = Solr.class)
    @PostMapping("/update")
    public Result<Solr> update(@Validated({com.mcy.user.dto.Solr.UpdateGroup.class, Default.class})  @RequestBody Solr longUser) {
        longUser.setCreatetime(new Date());
        return Result.success(longUser);
    }


    @ApiOperationSupport(order = 3,ignoreParameters = {"createtime"})
    @ApiOperation(value = "处理", notes = "处理", httpMethod = "POST",response = Solr.class)
    @PostMapping("/merge")
    public Result<Solr> merger(@Validated Solr longUser) {
        longUser.setCreatetime(new Date());
        return Result.success(longUser);
    }

    @ApiOperation(value = "APP登录", httpMethod = "POST", notes = "手机端登录", response = Result.class)
    @PostMapping("/login")
    public Result<Solr> userLogin( @Validated @RequestParam(value = "userId",required=true) String userId,
                                  @Validated @RequestParam(value = "password")  String password){
        Assert.isTrue(StringUtils.isNotEmpty(userId), "账户名称不能为空");
        Assert.isTrue(StringUtils.isNotEmpty(password), "账户密码不能为空");
        return Result.success(Solr.builder().title(userId).name(password).build());
    }


}
