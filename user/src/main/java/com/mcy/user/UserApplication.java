package com.mcy.user;

import com.nepxion.banner.BannerConstant;
import com.qdone.support.lock.redisson.annotation.EnableRedissonLock;
import com.qdone.support.rate.redisson.anontation.EnableRateLimiter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import java.net.InetAddress;


@MapperScan("com.mcy.**.mapper")
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@EnableRedissonLock
@EnableRateLimiter
@Slf4j
public class UserApplication {
    @SneakyThrows
    public static void main(String[] args){
        //关闭nexpion控制台打印
        System.setProperty(BannerConstant.BANNER_SHOWN,"false");
        ConfigurableApplicationContext application = SpringApplication.run(UserApplication.class, args);
        Environment env = application.getEnvironment();
        String activeProfiles= StringUtils.arrayToCommaDelimitedString(env.getActiveProfiles());
        activeProfiles=StringUtils.isEmpty(activeProfiles)?"default":activeProfiles;
        log.info("<===========[{}]启动完成！"+"运行环境:[{}] IP:[{}] PORT:[{}]===========>", env.getProperty("spring.application.name"),activeProfiles, InetAddress.getLocalHost().getHostAddress(),env.getProperty("server.port"));
    }

    /**
     * url传递参数处理
     * @return
     */
    @Bean
    public ConfigurableServletWebServerFactory webServerFactory() {
        TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
        factory.addConnectorCustomizers((TomcatConnectorCustomizer) connector -> connector.setProperty("relaxedQueryChars", "|{}[]\\"));
        return factory;
    }

}
