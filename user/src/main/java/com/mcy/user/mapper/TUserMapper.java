package com.mcy.user.mapper;

import com.mcy.user.entity.TUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mcy
 * @since 2020-06-06
 */
public interface TUserMapper extends BaseMapper<TUser> {

    int debit(@Param("userId") String userId, @Param("balance") BigDecimal balance);

    List<TUser> select(TUser user);
}
