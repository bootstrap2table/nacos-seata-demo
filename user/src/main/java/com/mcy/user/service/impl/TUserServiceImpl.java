package com.mcy.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mcy.user.entity.TUser;
import com.mcy.user.mapper.TUserMapper;
import com.mcy.user.service.ITUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdone.support.lock.redisson.annotation.LockAction;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Wrapper;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mcy
 * @since 2020-06-06
 */
@Service
@Slf4j
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements ITUserService {

    @Override
    @Trace
    public boolean debit(String userId, BigDecimal amount)  {
        log.info("user服务扣款:{},{}",userId,amount);
        QueryWrapper<TUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(TUser::getUserId, userId);
        TUser user = baseMapper.selectOne(queryWrapper);
        if(user.getBalance().compareTo(amount)  == -1){
            throw new RuntimeException("余额不够付款");
        }
        return baseMapper.debit(userId,user.getBalance().subtract(amount)) > 0;
    }


    @Override
    @LockAction
    public Boolean testRedisLock() throws InterruptedException {
        System.err.println("TUserServiceImpl无参数开始执行业务逻辑");
        TimeUnit.SECONDS.sleep(20);
        System.err.println("TUserServiceImpl无参数业务逻辑执行完毕");
        return true;
    }

    @Override
    public List<TUser> select(TUser user) {
        return baseMapper.select(user);
    }
}
