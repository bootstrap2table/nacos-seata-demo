package com.mcy.user;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={UserApplicationTest.class})
@ActiveProfiles("dev")
@WebAppConfiguration
@FixMethodOrder(MethodSorters.JVM)
public class BaseWebTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    protected MockMvc mockMvc;

    protected TestRestTemplate testRestTemplate=new TestRestTemplate();

    protected String tokenKey="Authorization";
    /**
     * 单元测试用户令牌
     */
    protected String tokenValue="123456789";

    @Before
    public void setUp(){
        System.err.println("初始化容器");
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @Ignore
    public void doTest(){
        System.err.println("基类测试");
    }
}
