package com.mcy.user;

import lombok.SneakyThrows;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@FixMethodOrder(MethodSorters.JVM)
public class UserTest extends BaseWebTest{

    /**
     * 测试导入
     */
    @Test
    @SneakyThrows
    public void testImport(){
        Resource resource=new PathMatchingResourcePatternResolver().getResource("classpath:test.txt");
        MockMultipartFile testFile = new MockMultipartFile("data", "test.txt", "text/plain",resource.getInputStream());
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/user/upload").file("file",testFile.getBytes())
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .header(tokenKey,tokenValue)
                .param("id","123456"))
                .andDo(print());
    }
}
