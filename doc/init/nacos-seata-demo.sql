/*
Navicat MySQL Data Transfer

Source Server         : jdy
Source Server Version : 50730
Source Host           : 114.67.207.106:3333
Source Database       : nacos-seata-demo

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-03 10:06:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for leaf_alloc
-- ----------------------------
DROP TABLE IF EXISTS `leaf_alloc`;
CREATE TABLE `leaf_alloc` (
  `biz_tag` varchar(128) NOT NULL DEFAULT '',
  `max_id` bigint(20) NOT NULL DEFAULT '1',
  `step` int(11) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`biz_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of leaf_alloc
-- ----------------------------
INSERT INTO `leaf_alloc` VALUES ('leaf-segment-test', '1', '2000', 'Test leaf Segment Mode Get Id', '2020-10-30 13:50:24');

-- ----------------------------
-- Table structure for sys_action_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_action_log`;
CREATE TABLE `sys_action_log` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `token` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '用户令牌',
  `trace` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '链路编号',
  `project` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '项目名称',
  `moudle` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '模块名称',
  `action_type` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '操作类型',
  `type` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '日志类型 1:正常 0：异常',
  `request_uri` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '请求URI',
  `class_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '执行类名',
  `method_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '执行方法名称',
  `user_agent` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '用户代理',
  `remote_ip` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '操作IP地址',
  `request_method` varchar(5) COLLATE utf8_bin DEFAULT NULL COMMENT '操作方式',
  `request_params` text COLLATE utf8_bin COMMENT '请求参数',
  `response_params` text COLLATE utf8_bin COMMENT '返回参数',
  `request_mac` varchar(60) COLLATE utf8_bin DEFAULT NULL COMMENT '设备MAC',
  `exception` text COLLATE utf8_bin COMMENT '异常信息',
  `action_thread` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '执行线程',
  `action_start_time` datetime DEFAULT NULL COMMENT '开始执行时刻',
  `action_end_time` datetime DEFAULT NULL COMMENT '结束执行时刻',
  `action_time` bigint(20) DEFAULT NULL COMMENT '执行耗时 单位(毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建日志时间',
  PRIMARY KEY (`id`),
  KEY `sys_log_trace` (`trace`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户日志表';

-- ----------------------------
-- Records of sys_action_log
-- ----------------------------
INSERT INTO `sys_action_log` VALUES ('49', 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJtYWNybyIsInNjb3BlIjpbImFsbCJdLCJpZCI6MSwiZXhwIjoxNjEzOTcwNTM3LCJhdXRob3JpdGllcyI6WyJBRE1JTiJdLCJqdGkiOiIyNmJmODJiNC04ZTk1LTRmYjEtYWY1ZC1mMjVhNmQ3NTFjNTYiLCJjbGllbnRfaWQiOiJjbGllbnQtYXBwIn0.MYSXSGFr0Vwv8C--cIo8ktgn7js-Mu-DnZGYb-IJCrWcF0RO5IuaZZOyZY_MjqxT6HF2-8XMDq24D-ec4-4oY6v7Kx-OI5a0djiZMd0tcz5jX2zrBiRJXhmqmSytsdz1QrBAZpthVKe9HxQDC7dSMfW87O7H2EKyuOGWzH2Xoy4', 'Ignored_Trace', 'business', '业务系统', '用户下单', '1', '/business/booking', 'com.mcy.business.controller.BusinessController', 'booking', 'ApiPOST Runtime +https://www.apipost.cn', '0:0:0:0:0:0:0:1', 'POST', 0x5B223344324536453342333443353445434339393634304544374334313437304338222C22313233222C335D, 0x73756363657373, '', null, 'http-nio-8089-exec-5', '2021-02-26 13:11:12', '2021-02-26 13:11:15', '3151', '2021-02-26 13:11:15');
INSERT INTO `sys_action_log` VALUES ('50', 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJtYWNybyIsInNjb3BlIjpbImFsbCJdLCJpZCI6MSwiZXhwIjoxNjEzOTcwNTM3LCJhdXRob3JpdGllcyI6WyJBRE1JTiJdLCJqdGkiOiIyNmJmODJiNC04ZTk1LTRmYjEtYWY1ZC1mMjVhNmQ3NTFjNTYiLCJjbGllbnRfaWQiOiJjbGllbnQtYXBwIn0.MYSXSGFr0Vwv8C--cIo8ktgn7js-Mu-DnZGYb-IJCrWcF0RO5IuaZZOyZY_MjqxT6HF2-8XMDq24D-ec4-4oY6v7Kx-OI5a0djiZMd0tcz5jX2zrBiRJXhmqmSytsdz1QrBAZpthVKe9HxQDC7dSMfW87O7H2EKyuOGWzH2Xoy4', 'Ignored_Trace', 'business', '业务系统', '用户下单', '1', '/business/booking', 'com.mcy.business.controller.BusinessController', 'booking', 'ApiPOST Runtime +https://www.apipost.cn', '0:0:0:0:0:0:0:1', 'POST', 0x5B223344324536453342333443353445434339393634304544374334313437304338222C22313233222C335D, 0x73756363657373, '', null, 'http-nio-8089-exec-9', '2021-02-26 13:12:10', '2021-02-26 13:12:25', '15839', '2021-02-26 13:12:25');
INSERT INTO `sys_action_log` VALUES ('51', 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJtYWNybyIsInNjb3BlIjpbImFsbCJdLCJpZCI6MSwiZXhwIjoxNjEzOTcwNTM3LCJhdXRob3JpdGllcyI6WyJBRE1JTiJdLCJqdGkiOiIyNmJmODJiNC04ZTk1LTRmYjEtYWY1ZC1mMjVhNmQ3NTFjNTYiLCJjbGllbnRfaWQiOiJjbGllbnQtYXBwIn0.MYSXSGFr0Vwv8C--cIo8ktgn7js-Mu-DnZGYb-IJCrWcF0RO5IuaZZOyZY_MjqxT6HF2-8XMDq24D-ec4-4oY6v7Kx-OI5a0djiZMd0tcz5jX2zrBiRJXhmqmSytsdz1QrBAZpthVKe9HxQDC7dSMfW87O7H2EKyuOGWzH2Xoy4', 'Ignored_Trace', 'business', '业务系统', '用户下单', '1', '/business/booking', 'com.mcy.business.controller.BusinessController', 'booking', 'ApiPOST Runtime +https://www.apipost.cn', '0:0:0:0:0:0:0:1', 'POST', 0x5B223344324536453342333443353445434339393634304544374334313437304338222C22313233222C335D, 0x73756363657373, '', null, 'http-nio-8089-exec-1', '2021-02-26 13:16:44', '2021-02-26 13:17:00', '16739', '2021-02-26 13:17:00');
INSERT INTO `sys_action_log` VALUES ('52', 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJtYWNybyIsInNjb3BlIjpbImFsbCJdLCJpZCI6MSwiZXhwIjoxNjEzOTcwNTM3LCJhdXRob3JpdGllcyI6WyJBRE1JTiJdLCJqdGkiOiIyNmJmODJiNC04ZTk1LTRmYjEtYWY1ZC1mMjVhNmQ3NTFjNTYiLCJjbGllbnRfaWQiOiJjbGllbnQtYXBwIn0.MYSXSGFr0Vwv8C--cIo8ktgn7js-Mu-DnZGYb-IJCrWcF0RO5IuaZZOyZY_MjqxT6HF2-8XMDq24D-ec4-4oY6v7Kx-OI5a0djiZMd0tcz5jX2zrBiRJXhmqmSytsdz1QrBAZpthVKe9HxQDC7dSMfW87O7H2EKyuOGWzH2Xoy4', 'Ignored_Trace', 'business', '业务系统', '用户下单', '1', '/business/booking', 'com.mcy.business.controller.BusinessController', 'booking', 'ApiPOST Runtime +https://www.apipost.cn', '0:0:0:0:0:0:0:1', 'POST', 0x5B223344324536453342333443353445434339393634304544374334313437304338222C22313233222C335D, 0x73756363657373, '', null, 'http-nio-8089-exec-8', '2021-02-26 13:17:26', '2021-02-26 13:17:31', '5034', '2021-02-26 13:17:31');
INSERT INTO `sys_action_log` VALUES ('53', 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJtYWNybyIsInNjb3BlIjpbImFsbCJdLCJpZCI6MSwiZXhwIjoxNjEzOTcwNTM3LCJhdXRob3JpdGllcyI6WyJBRE1JTiJdLCJqdGkiOiIyNmJmODJiNC04ZTk1LTRmYjEtYWY1ZC1mMjVhNmQ3NTFjNTYiLCJjbGllbnRfaWQiOiJjbGllbnQtYXBwIn0.MYSXSGFr0Vwv8C--cIo8ktgn7js-Mu-DnZGYb-IJCrWcF0RO5IuaZZOyZY_MjqxT6HF2-8XMDq24D-ec4-4oY6v7Kx-OI5a0djiZMd0tcz5jX2zrBiRJXhmqmSytsdz1QrBAZpthVKe9HxQDC7dSMfW87O7H2EKyuOGWzH2Xoy4', 'Ignored_Trace', 'business', '业务系统', '用户下单', '1', '/business/booking', 'com.mcy.business.controller.BusinessController', 'booking', 'ApiPOST Runtime +https://www.apipost.cn', '0:0:0:0:0:0:0:1', 'POST', 0x5B223344324536453342333443353445434339393634304544374334313437304338222C22313233222C335D, 0x73756363657373, '', null, 'http-nio-8089-exec-3', '2021-02-26 13:17:58', '2021-02-26 13:17:59', '546', '2021-02-26 13:17:59');

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(32) DEFAULT NULL,
  `product_id` varchar(32) DEFAULT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES ('27', 'AFCBCDCD72F34DBD9460146C57378FAE', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-02-25 17:23:04');
INSERT INTO `t_order` VALUES ('28', '7796594095D74FEBB01F55A0A0495883', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-02-25 17:57:35');
INSERT INTO `t_order` VALUES ('29', 'C055E8A421614863A53DB7BDEF74E334', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-02-26 13:11:14');
INSERT INTO `t_order` VALUES ('30', '47789F5A4E8647C097AB1993F249A39E', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-02-26 13:12:20');
INSERT INTO `t_order` VALUES ('31', 'D9F24B56A4DC4BE2A1C41A7C7D87D8C8', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-02-26 13:16:57');
INSERT INTO `t_order` VALUES ('32', '354A413037074EDC8B6F179A015C8490', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-02-26 13:17:30');
INSERT INTO `t_order` VALUES ('33', 'E35C46A4EFB344D2B2093CF3097AD384', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-02-26 13:17:58');
INSERT INTO `t_order` VALUES ('34', '27C6AFCD48FA433AAAC313775398EAE9', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-03-01 10:49:02');
INSERT INTO `t_order` VALUES ('35', '97EC197210F54C179FE54E61C97915D2', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-03-01 13:26:04');
INSERT INTO `t_order` VALUES ('36', 'C261926A9BA24C74A236CE5D61C5E569', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-03-01 13:26:52');
INSERT INTO `t_order` VALUES ('37', '8C9C69C5B7A04C8597936ED39C8B755A', '123', '3D2E6E3B34C54ECC99640ED7C41470C8', '3', null, '1000.00', '2021-03-01 14:29:54');

-- ----------------------------
-- Table structure for t_product
-- ----------------------------
DROP TABLE IF EXISTS `t_product`;
CREATE TABLE `t_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(32) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_product
-- ----------------------------
INSERT INTO `t_product` VALUES ('2', '123', '苹果', null, '10.00', '99725');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `sex` varchar(2) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', '3D2E6E3B34C54ECC99640ED7C41470C8', 'admin', '123456', '99912000.00', '18', '男', '广州市');

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'increment id',
  `branch_id` bigint(20) NOT NULL COMMENT 'branch transaction id',
  `xid` varchar(100) NOT NULL COMMENT 'global transaction id',
  `context` varchar(128) NOT NULL COMMENT 'undo_log context,such as serialization',
  `rollback_info` longblob NOT NULL COMMENT 'rollback info',
  `log_status` int(11) NOT NULL COMMENT '0:normal status,1:defense status',
  `log_created` datetime NOT NULL COMMENT 'create datetime',
  `log_modified` datetime NOT NULL COMMENT 'modify datetime',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COMMENT='AT transaction mode undo table';

-- ----------------------------
-- Records of undo_log
-- ----------------------------
