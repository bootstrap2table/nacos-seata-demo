/*
Navicat MySQL Data Transfer

Source Server         : jdy
Source Server Version : 50730
Source Host           : 114.67.207.106:3333
Source Database       : autocode

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-03 10:09:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cust_info
-- ----------------------------
DROP TABLE IF EXISTS `cust_info`;
CREATE TABLE `cust_info` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `lake_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '大数据湖编码',
  `cust_name` varchar(500) DEFAULT NULL COMMENT '客户名称',
  `cust_type` varchar(10) DEFAULT NULL COMMENT '客户类型，1经销商 2系统父父客户 3系统父客户 4 KA上级供应商',
  `unified_lake_id` varchar(255) DEFAULT NULL COMMENT '统一的大数据湖编码',
  `unified_cust_name` varchar(255) DEFAULT NULL COMMENT '统一的客户名称',
  `org_code` varchar(255) DEFAULT NULL COMMENT '组织编码',
  `org_name` varchar(255) DEFAULT NULL COMMENT '组织名称',
  `pra_geoc_prov` varchar(255) DEFAULT NULL COMMENT '省编码',
  `pra_geoc_prov_name` varchar(255) DEFAULT NULL COMMENT '省名称',
  `pra_geoc_city` varchar(255) DEFAULT NULL COMMENT '市编码',
  `pra_geoc_city_name` varchar(255) DEFAULT NULL COMMENT '市名称',
  `pra_geoc_county` varchar(255) DEFAULT NULL COMMENT '区编码',
  `pra_geoc_county_name` varchar(255) DEFAULT NULL COMMENT '区名称',
  `pra_dealer_chl` varchar(255) DEFAULT NULL COMMENT '经销商渠道编码',
  `pra_dealer_chl_name` varchar(255) DEFAULT NULL COMMENT '经销商渠道名称',
  `pra_cust_stat_cd` varchar(255) DEFAULT NULL COMMENT '客户关闭状态',
  `pra_cust_close_date` varchar(255) DEFAULT NULL COMMENT '客户关闭时间',
  `insdt` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '维护时间',
  `parent_id` varchar(50) DEFAULT NULL COMMENT 'KA客户父层级ID',
  `empid` varchar(255) DEFAULT NULL COMMENT '业代id',
  `empname` varchar(255) DEFAULT NULL COMMENT '业代名称',
  `dcloud_flag` varchar(255) DEFAULT NULL COMMENT 'dcloud_flag=’Y‘ 即使用dcloud系统',
  `pra_org_ct` varchar(255) DEFAULT NULL COMMENT 'CT组织编码',
  `pra_org_ct_name` varchar(255) DEFAULT NULL COMMENT 'CT组织名称',
  `last_org_id` varchar(255) DEFAULT NULL COMMENT '上级组织id',
  `last_org_name` varchar(255) DEFAULT NULL COMMENT '上级组织名称',
  `war_zone` varchar(255) DEFAULT NULL COMMENT '战区',
  `ct_area` varchar(1000) DEFAULT NULL COMMENT 'ct组织对应的地域',
  `cust_categ_l1_name` varchar(255) DEFAULT NULL,
  `cust_categ_l2_name` varchar(255) DEFAULT NULL,
  `cust_level` varchar(20) DEFAULT NULL COMMENT '客户等级',
  `ct_org_person` varchar(45) DEFAULT NULL,
  `dirct_sales_flg` varchar(50) DEFAULT NULL COMMENT '是否直营',
  `cust_categ_l3_name` varchar(255) DEFAULT NULL COMMENT '客户三级分类(NKA or LKA类似数据)',
  `postn_name` varchar(255) DEFAULT NULL COMMENT '负责人岗位名称',
  PRIMARY KEY (`row_id`),
  KEY `idx_lake_id` (`lake_id`),
  KEY `idx_org_code` (`org_code`),
  KEY `idx_org_name` (`org_name`),
  KEY `idx_pra_geoc_prov` (`pra_geoc_prov`),
  KEY `idx_pra_geoc_prov_name` (`pra_geoc_prov_name`),
  KEY `idx_pra_geoc_city` (`pra_geoc_city`),
  KEY `idx_pra_geoc_city_name` (`pra_geoc_city_name`),
  KEY `idx_pra_geoc_county` (`pra_geoc_county`),
  KEY `idx_pra_geoc_county_name` (`pra_geoc_county_name`),
  KEY `idx_pra_dealer_chl` (`pra_dealer_chl`),
  KEY `idx_pra_dealer_chl_name` (`pra_dealer_chl_name`),
  KEY `idx_pra_cust_close_date` (`pra_cust_close_date`),
  KEY `idx_empid` (`empid`),
  KEY `idx_parentId` (`parent_id`),
  KEY `idx_unified_lake_id` (`unified_lake_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='客户信息';

-- ----------------------------
-- Table structure for fdt_brand_prdcateg_list
-- ----------------------------
DROP TABLE IF EXISTS `fdt_brand_prdcateg_list`;
CREATE TABLE `fdt_brand_prdcateg_list` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `prdcateg_lv1_name` varchar(50) DEFAULT NULL COMMENT '一级品类',
  `brand_name` varchar(40) DEFAULT NULL COMMENT '品牌',
  `brand_prdcateg_name` varchar(100) DEFAULT NULL COMMENT '品牌品类名称',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='品牌品类值列表';

-- ----------------------------
-- Table structure for fdt_brand_prdcateg_list_fx
-- ----------------------------
DROP TABLE IF EXISTS `fdt_brand_prdcateg_list_fx`;
CREATE TABLE `fdt_brand_prdcateg_list_fx` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `prdcateg_lv1_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '一级品类',
  `brand_name` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '品牌',
  `brand_code` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '品牌编码',
  `brand_prdcateg_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '品牌品类名称',
  `brand_prdcateg_code` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '品牌品类编码',
  `level` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '品牌品类层级',
  `creator` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='KA系统分销品牌品类';

-- ----------------------------
-- Table structure for gdt_brand_prdcateg_storetype_sales
-- ----------------------------
DROP TABLE IF EXISTS `gdt_brand_prdcateg_storetype_sales`;
CREATE TABLE `gdt_brand_prdcateg_storetype_sales` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `regional_level` varchar(50) DEFAULT NULL COMMENT '地域层级',
  `regional_cd` varchar(100) DEFAULT NULL COMMENT '地域编码',
  `regional_name` varchar(50) DEFAULT NULL COMMENT '地域名称',
  `prdcateg_lv1_name` varchar(50) DEFAULT NULL COMMENT '一级品类',
  `brand_name` varchar(50) DEFAULT NULL COMMENT '品牌',
  `brand_prdcateg_name` varchar(50) DEFAULT NULL COMMENT '品牌品类名称',
  `ctp_chanl` varchar(50) DEFAULT NULL COMMENT '渠道',
  `ctp_type` varchar(50) DEFAULT NULL COMMENT '门店类型',
  `deal_ctp_num` decimal(10,4) DEFAULT NULL COMMENT '现状-已铺货门店数',
  `sales_qty` varchar(50) DEFAULT NULL COMMENT '现状-销量瓶数',
  `ods_output` decimal(10,4) DEFAULT NULL COMMENT '现状-单日单店产出',
  `distribute_rate` decimal(10,4) DEFAULT NULL COMMENT '现状-分销率',
  `tar_distribute_rate` varchar(50) DEFAULT NULL COMMENT '目标-分销率',
  `tar_inc_ctp_num` varchar(50) DEFAULT NULL COMMENT '目标-增长门店数',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `status` char(1) DEFAULT '0' COMMENT '0 未修改过,1 修改过',
  `inc_1st_bse_output` decimal(15,4) DEFAULT NULL COMMENT '增量-第一年-初始门店-单日单店产出',
  `inc_1st_bse_sal` decimal(15,4) DEFAULT NULL COMMENT '增量-第一年-初始门店-销售瓶数',
  `tot_1st_bse_output` decimal(15,4) DEFAULT NULL COMMENT '总计-第一年-初始门店-单日单店产出',
  `tot_1st_bse_sal` decimal(15,4) DEFAULT NULL COMMENT '总计-第一年-初始门店-销售瓶数',
  `inc_1st_1re_output` decimal(15,4) DEFAULT NULL COMMENT '增量-第一年-新增门店1-单日单店产出',
  `inc_1st_1re_num` decimal(15,4) DEFAULT NULL COMMENT '增量-第一年-新增门店1-门店数',
  `inc_1st_1re_sal` decimal(15,4) DEFAULT NULL COMMENT '增量-第一年-新增门店1-销售瓶数',
  `sum_1st_output` decimal(15,4) DEFAULT NULL COMMENT '合计-第一年-单日单店产出',
  `sum_1st_num` decimal(15,4) DEFAULT NULL COMMENT '合计-第一年-门店数',
  `sum_1st_sal` decimal(15,4) DEFAULT NULL COMMENT '合计-第一年-销售瓶数',
  `inc_2nd_bse_output` decimal(15,4) DEFAULT NULL COMMENT '增量-第二年-初始门店-单日单店产出',
  `inc_2nd_bse_sal` decimal(15,4) DEFAULT NULL COMMENT '增量-第二年-初始门店-销售瓶数',
  `tot_2nd_bse_output` decimal(15,4) DEFAULT NULL COMMENT '总计-第二年-初始门店-单日单店产出',
  `tot_2nd_bse_sal` decimal(15,4) DEFAULT NULL COMMENT '总计-第二年-初始门店-销售瓶数',
  `inc_2nd_1re_output` decimal(15,4) DEFAULT NULL COMMENT '增量-第二年-新增门店1-单日单店产出',
  `inc_2nd_1re_sal` decimal(15,4) DEFAULT NULL COMMENT '增量-第二年-新增门店1-销售瓶数',
  `tot_2nd_1re_output` decimal(15,4) DEFAULT NULL COMMENT '总计-第二年-新增门店1-单日单店产出',
  `tot_2nd_1re_sal` decimal(15,4) DEFAULT NULL COMMENT '总计-第二年-新增门店1-销售瓶数',
  `inc_2nd_2re_output` decimal(15,4) DEFAULT NULL COMMENT '增量-第二年-新增门店2-单日单店产出',
  `inc_2nd_2re_num` decimal(15,4) DEFAULT NULL COMMENT '增量-第二年-新增门店2-门店数',
  `inc_2nd_2re_sal` decimal(15,4) DEFAULT NULL COMMENT '增量-第二年-新增门店2-销售瓶数',
  `sum_2nd_output` decimal(15,4) DEFAULT NULL COMMENT '合计-第二年-单日单店产出',
  `sum_2nd_num` decimal(15,4) DEFAULT NULL COMMENT '合计-第二年-门店数',
  `sum_2nd_sal` decimal(15,4) DEFAULT NULL COMMENT '合计-第二年-销售瓶数',
  `inc_3rd_bse_output` decimal(15,4) DEFAULT NULL COMMENT '增量-第三年-初始门店-单日单店产出',
  `inc_3rd_bse_sal` decimal(15,4) DEFAULT NULL COMMENT '增量-第三年-初始门店-销售瓶数',
  `tot_3rd_bse_output` decimal(15,4) DEFAULT NULL COMMENT '总计-第三年-初始门店-单日单店产出',
  `tot_3rd_bse_sal` decimal(15,4) DEFAULT NULL COMMENT '总计-第三年-初始门店-销售瓶数',
  `inc_3rd_1re_output` decimal(15,4) DEFAULT NULL COMMENT '增量-第三年-新增门店1-单日单店产出',
  `inc_3rd_1re_sal` decimal(15,4) DEFAULT NULL COMMENT '增量-第三年-新增门店1-销售瓶数',
  `tot_3rd_1re_output` decimal(15,4) DEFAULT NULL COMMENT '总计-第三年-新增门店1-单日单店产出',
  `tot_3rd_1re_sal` decimal(15,4) DEFAULT NULL COMMENT '总计-第三年-新增门店1-销售瓶数',
  `inc_3rd_2re_output` decimal(15,4) DEFAULT NULL COMMENT '增量-第三年-新增门店2-单日单店产出',
  `inc_3rd_2re_sal` decimal(15,4) DEFAULT NULL COMMENT '增量-第三年-新增门店2-销售瓶数',
  `tot_3rd_2re_output` decimal(15,4) DEFAULT NULL COMMENT '总计-第三年-新增门店2-单日单店产出',
  `tot_3rd_2re_sal` decimal(15,4) DEFAULT NULL COMMENT '总计-第三年-新增门店2-销售瓶数',
  `inc_3rd_3re_output` decimal(15,4) DEFAULT NULL COMMENT '增量-第三年-新增门店3-单日单店产出',
  `inc_3rd_3re_num` decimal(15,4) DEFAULT NULL COMMENT '增量-第三年-新增门店3-门店数',
  `inc_3rd_3re_sal` decimal(15,4) DEFAULT NULL COMMENT '增量-第三年-新增门店3-销售瓶数',
  `sum_3rd_output` decimal(15,4) DEFAULT NULL COMMENT '合计-第三年-单日单店产出',
  `sum_3rd_num` decimal(15,4) DEFAULT NULL COMMENT '合计-第三年-门店数',
  `sum_3rd_sal` decimal(15,4) DEFAULT NULL COMMENT '合计-第三年-销售瓶数',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `task_index` (`regional_name`,`prdcateg_lv1_name`,`brand_prdcateg_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='地域品牌品类门店类型销量信息';

-- ----------------------------
-- Table structure for gdt_brand_prdcateg_storetype_sales_sum
-- ----------------------------
DROP TABLE IF EXISTS `gdt_brand_prdcateg_storetype_sales_sum`;
CREATE TABLE `gdt_brand_prdcateg_storetype_sales_sum` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `regional_level` varchar(50) DEFAULT NULL COMMENT '地域层级',
  `prov_cd` varchar(100) DEFAULT NULL COMMENT '省编码',
  `prov_name` varchar(50) DEFAULT NULL COMMENT '省名称',
  `city_cd` varchar(50) DEFAULT NULL COMMENT '市编码',
  `city_name` varchar(50) DEFAULT NULL COMMENT '市名称',
  `county_cd` varchar(50) DEFAULT NULL COMMENT '区编码',
  `county_name` varchar(100) DEFAULT NULL COMMENT '区名称',
  `brand_name` varchar(50) DEFAULT NULL COMMENT '品牌',
  `brand_prdcateg_name` varchar(50) DEFAULT NULL COMMENT '品牌品类名称',
  `output` varchar(50) DEFAULT NULL COMMENT '单日单店产出汇总',
  `output_compare` varchar(50) DEFAULT NULL COMMENT '单日单店产出汇总对比目标',
  `store_num` varchar(50) DEFAULT NULL COMMENT '门店数量汇总',
  `store_num_compare` varchar(50) DEFAULT NULL COMMENT '门店数量汇总对比目标',
  `sales_qty` varchar(50) DEFAULT NULL COMMENT '销售瓶数汇总',
  `sales_qty_compare` varchar(50) DEFAULT NULL COMMENT '销售瓶数汇总对比目标',
  `type` char(1) DEFAULT NULL COMMENT '1 第一年 2 第二年  3第三年',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `task_index` (`prov_name`,`city_name`,`county_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='地域品牌品类门店类型销量汇总信息';

-- ----------------------------
-- Table structure for gdt_forcast_area_info
-- ----------------------------
DROP TABLE IF EXISTS `gdt_forcast_area_info`;
CREATE TABLE `gdt_forcast_area_info` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `contrycode` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '国家编码',
  `contryname` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '国家名称',
  `provincecode` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '省编码',
  `provincename` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '省名称',
  `citycode` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '市编码',
  `cityname` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '市名称',
  `contycode` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '区编码',
  `contyname` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '区名称',
  `citytype` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '城市类型',
  `mktstage` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '战区',
  `geoctype` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '地域类型',
  `citylevel` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '城市等级',
  `creator` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_contycode` (`contycode`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='预测地域信息';

-- ----------------------------
-- Table structure for gdt_forcast_area_levlel
-- ----------------------------
DROP TABLE IF EXISTS `gdt_forcast_area_levlel`;
CREATE TABLE `gdt_forcast_area_levlel` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `provincecode` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '省编码',
  `provincename` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '省名称',
  `citycode` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '城市编码',
  `cityname` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '城市名称',
  `shortname` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '城市简称',
  `citylevel` varchar(10) CHARACTER SET utf8 DEFAULT NULL COMMENT '城市等级',
  `score` decimal(10,4) DEFAULT NULL COMMENT '城市指数',
  `calculation` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '指数统计时段(eg:2019)',
  `rankprovin` int(12) DEFAULT NULL COMMENT '省内排名',
  `ranknation` int(12) DEFAULT NULL COMMENT '国内排名',
  `creator` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='预测城市等级';

-- ----------------------------
-- Table structure for gdt_forcast_cust_info
-- ----------------------------
DROP TABLE IF EXISTS `gdt_forcast_cust_info`;
CREATE TABLE `gdt_forcast_cust_info` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `lake_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '大数据湖编码',
  `cust_name` varchar(500) DEFAULT NULL COMMENT '客户名称',
  `cust_type` varchar(10) DEFAULT NULL COMMENT '客户类型，1经销商 2系统父父客户 3系统父客户 4 KA上级供应商',
  `unified_lake_id` varchar(255) DEFAULT NULL COMMENT '统一的大数据湖编码',
  `unified_cust_name` varchar(255) DEFAULT NULL COMMENT '统一的客户名称',
  `org_code` varchar(255) DEFAULT NULL COMMENT '组织编码',
  `org_name` varchar(255) DEFAULT NULL COMMENT '组织名称',
  `pra_geoc_prov` varchar(255) DEFAULT NULL COMMENT '省编码',
  `pra_geoc_prov_name` varchar(255) DEFAULT NULL COMMENT '省名称',
  `pra_geoc_city` varchar(255) DEFAULT NULL COMMENT '市编码',
  `pra_geoc_city_name` varchar(255) DEFAULT NULL COMMENT '市名称',
  `pra_geoc_county` varchar(255) DEFAULT NULL COMMENT '区编码',
  `pra_geoc_county_name` varchar(255) DEFAULT NULL COMMENT '区名称',
  `pra_dealer_chl` varchar(255) DEFAULT NULL COMMENT '经销商渠道编码',
  `pra_dealer_chl_name` varchar(255) DEFAULT NULL COMMENT '经销商渠道名称',
  `pra_cust_stat_cd` varchar(255) DEFAULT NULL COMMENT '客户关闭状态',
  `pra_cust_close_date` varchar(255) DEFAULT NULL COMMENT '客户关闭时间',
  `insdt` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '维护时间',
  `parent_id` varchar(50) DEFAULT NULL COMMENT 'KA客户父层级ID',
  `empid` varchar(255) DEFAULT NULL COMMENT '业代id',
  `empname` varchar(255) DEFAULT NULL COMMENT '业代名称',
  `dcloud_flag` varchar(255) DEFAULT NULL COMMENT 'dcloud_flag=’Y‘ 即使用dcloud系统',
  `pra_org_ct` varchar(255) DEFAULT NULL COMMENT 'CT组织编码',
  `pra_org_ct_name` varchar(255) DEFAULT NULL COMMENT 'CT组织名称',
  `last_org_id` varchar(255) DEFAULT NULL COMMENT '上级组织id',
  `last_org_name` varchar(255) DEFAULT NULL COMMENT '上级组织名称',
  `war_zone` varchar(255) DEFAULT NULL COMMENT '战区',
  `ct_area` varchar(1000) DEFAULT NULL COMMENT 'ct组织对应的地域',
  `postn_name` varchar(255) DEFAULT NULL COMMENT '负责人岗位名称',
  `cust_categ_l1_name` varchar(255) DEFAULT NULL COMMENT '客户类型',
  `cust_categ_l2_name` varchar(255) DEFAULT NULL COMMENT '客户子类型',
  `cust_categ_l3_name` varchar(255) DEFAULT NULL COMMENT '客户三级分类(NKA or LKA类似数据)',
  `cust_level` varchar(20) DEFAULT NULL COMMENT '客户等级',
  `dirct_sales_flg` varchar(10) DEFAULT NULL COMMENT '是否直营',
  `ct_org_person` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `idx_lake_id` (`lake_id`),
  KEY `idx_org_code` (`org_code`),
  KEY `idx_org_name` (`org_name`),
  KEY `idx_pra_geoc_prov` (`pra_geoc_prov`),
  KEY `idx_pra_geoc_prov_name` (`pra_geoc_prov_name`),
  KEY `idx_pra_geoc_city` (`pra_geoc_city`),
  KEY `idx_pra_geoc_city_name` (`pra_geoc_city_name`),
  KEY `idx_pra_geoc_county` (`pra_geoc_county`),
  KEY `idx_pra_geoc_county_name` (`pra_geoc_county_name`),
  KEY `idx_pra_dealer_chl` (`pra_dealer_chl`),
  KEY `idx_pra_dealer_chl_name` (`pra_dealer_chl_name`),
  KEY `idx_pra_cust_close_date` (`pra_cust_close_date`),
  KEY `idx_empid` (`empid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='预测客户信息';

-- ----------------------------
-- Table structure for gdt_forcast_list
-- ----------------------------
DROP TABLE IF EXISTS `gdt_forcast_list`;
CREATE TABLE `gdt_forcast_list` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `regional_level` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '地域层级',
  `prov_cd` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '省编码',
  `prov_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '省名称',
  `city_cd` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '市编码',
  `city_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '市名称',
  `county_cd` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '区编码',
  `county_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '区名称',
  `sale_total` decimal(20,4) DEFAULT NULL COMMENT '总销售瓶数',
  `market_total` decimal(20,4) DEFAULT NULL COMMENT '门店数',
  `daily_store_total` decimal(20,4) DEFAULT '0.0000' COMMENT '单日单店产出',
  `y1` bit(1) DEFAULT b'0' COMMENT 'y1 true:达标,false:未达标',
  `y2` bit(1) DEFAULT b'0' COMMENT 'y2 true:达标,false:未达标',
  `y3` bit(1) DEFAULT b'0' COMMENT 'y3 true:达标,false:未达标',
  `status` varchar(1) CHARACTER SET utf8 DEFAULT '0' COMMENT '状态:1未填写 2:进行中 3:已完成',
  `operator` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '操作人',
  `operator_time` datetime DEFAULT NULL COMMENT '操作时间',
  `creator` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `county_cd_index` (`county_cd`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='地域宏观信息';

-- ----------------------------
-- Table structure for gdt_forcast_person
-- ----------------------------
DROP TABLE IF EXISTS `gdt_forcast_person`;
CREATE TABLE `gdt_forcast_person` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `regional_level` varchar(50) DEFAULT NULL COMMENT '地域层级:省STATE_ABBREV,市CITY,区COUNTY,直辖市MUNICIPALITY,直辖市下区MUNICIPALITY-COUNTY',
  `prov_name` varchar(255) DEFAULT NULL COMMENT '省份名称',
  `prov_cd` varchar(50) DEFAULT NULL COMMENT '省份编码',
  `city_name` varchar(255) DEFAULT NULL COMMENT '城市名称',
  `city_cd` varchar(50) DEFAULT NULL COMMENT '城市编码',
  `county_name` varchar(255) DEFAULT NULL COMMENT '区县名称',
  `county_cd` varchar(50) DEFAULT NULL COMMENT '区县编码',
  `csi` decimal(10,2) DEFAULT '0.00' COMMENT 'CSI',
  `ctl` decimal(10,2) DEFAULT NULL COMMENT 'CTL',
  `di` decimal(10,2) DEFAULT NULL COMMENT 'DI',
  `dtl` decimal(10,2) DEFAULT NULL COMMENT 'DTL',
  `gtdi` decimal(10,2) DEFAULT NULL COMMENT 'GTDI',
  `gt_biz` decimal(10,2) DEFAULT NULL COMMENT 'GT业务高级主管',
  `mptl` decimal(10,2) DEFAULT NULL COMMENT 'MPTL',
  `ptl` decimal(10,2) DEFAULT NULL COMMENT 'PTL',
  `stl` decimal(10,2) DEFAULT NULL COMMENT 'STL',
  `wsi` decimal(10,2) DEFAULT NULL COMMENT 'WSI-二批',
  `gt_sum` decimal(10,2) DEFAULT NULL COMMENT 'GT汇总',
  `kadi` decimal(10,2) DEFAULT NULL COMMENT 'KADI',
  `kai` decimal(10,2) DEFAULT NULL COMMENT 'KAI',
  `ka_biz` decimal(10,2) DEFAULT NULL COMMENT 'KA业务高级主管',
  `ka_leader` decimal(10,2) DEFAULT NULL COMMENT 'KA业务经理',
  `ka_master` decimal(10,2) DEFAULT NULL COMMENT 'KA业务主管',
  `big_kam` decimal(10,2) DEFAULT NULL COMMENT '大区KAM',
  `area_kam` decimal(10,2) DEFAULT NULL COMMENT '区域KAM',
  `ka_sum` decimal(10,2) DEFAULT NULL COMMENT 'KA汇总',
  `rdi` decimal(10,2) DEFAULT NULL COMMENT 'RDI',
  `food_bae` decimal(10,2) DEFAULT NULL COMMENT '餐饮BAE',
  `food_deputy` decimal(10,2) DEFAULT NULL COMMENT '餐饮业务代表',
  `food_biz` decimal(10,2) DEFAULT NULL COMMENT '餐饮业务高级主管',
  `food_leader` decimal(10,2) DEFAULT NULL COMMENT '餐饮业务经理',
  `food_master` decimal(10,2) DEFAULT NULL COMMENT '餐饮业务主管',
  `team_master` decimal(10,2) DEFAULT NULL COMMENT '团膳特渠主管',
  `r_sum` decimal(10,2) DEFAULT NULL COMMENT 'R汇总',
  `pretty_biz` decimal(10,2) DEFAULT NULL COMMENT '美顾管理组高级主管',
  `pretty_adviser_leader` decimal(10,2) DEFAULT NULL COMMENT '美食顾问管理专员',
  `pretty_adviser_biz` decimal(10,2) DEFAULT NULL COMMENT '美食顾问管理组高级主管',
  `pretty_sum` decimal(10,2) DEFAULT NULL COMMENT '美顾管理汇总',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`),
  KEY `idx_provCd` (`prov_cd`),
  KEY `idx_cityCd` (`city_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='预测人员数量';

-- ----------------------------
-- Table structure for gdt_geoc_brand_prdcateg_sales
-- ----------------------------
DROP TABLE IF EXISTS `gdt_geoc_brand_prdcateg_sales`;
CREATE TABLE `gdt_geoc_brand_prdcateg_sales` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `regional_level` varchar(50) DEFAULT NULL COMMENT '地域层级',
  `regional_cd` varchar(100) DEFAULT NULL COMMENT '地域编码',
  `regional_name` varchar(50) DEFAULT NULL COMMENT '地域名称',
  `prdcateg_lv1_name` varchar(50) DEFAULT NULL COMMENT '一级品类',
  `brand_name` varchar(255) DEFAULT NULL COMMENT '品牌',
  `brand_prdcateg_name` varchar(50) DEFAULT NULL COMMENT '品牌品类名称',
  `tar_sales_qty` decimal(15,4) DEFAULT NULL COMMENT '目标-销量（瓶）',
  `market_cap` decimal(15,4) DEFAULT NULL COMMENT '市场容量（瓶）',
  `tar_market_share` decimal(10,4) DEFAULT NULL COMMENT '目标-市占率',
  `tar_market_share_rank` varchar(50) DEFAULT NULL COMMENT '目标-三年目标市占率排名',
  `pre_sales_qty_unit` decimal(10,4) DEFAULT NULL COMMENT '现状-已有销量（瓶）',
  `pre_deal_ctp_qty` decimal(10,4) DEFAULT NULL COMMENT '现状-已铺门店（家）',
  `growth_space` varchar(50) DEFAULT NULL COMMENT '增长空间',
  `mt_deal_ctp` varchar(50) DEFAULT NULL COMMENT '现状-已铺货门店数-MT合计',
  `gt_deal_ctp` decimal(15,4) DEFAULT NULL COMMENT '现状-已铺货门店数-GT合计',
  `all_deal_ctp` decimal(15,4) DEFAULT NULL COMMENT '现状-已铺货门店数-总合计',
  `mt_sales_qty` decimal(15,4) DEFAULT NULL COMMENT '现状-销售瓶数-MT合计',
  `gt_sales_qty` decimal(15,4) DEFAULT NULL COMMENT '现状-销售瓶数-GT合计',
  `all_sales_qty` decimal(15,4) DEFAULT NULL COMMENT '现状-销售瓶数-总合计',
  `mt_ods_output` decimal(15,4) DEFAULT NULL COMMENT '现状-单日单店产出-MT合计',
  `gt_ods_output` decimal(15,4) DEFAULT NULL COMMENT '现状-单日单店产出-GT合计',
  `all_ods_output` decimal(15,4) DEFAULT NULL COMMENT '现状-单日单店产出-总合计',
  `mt_distribute_rate` decimal(15,4) DEFAULT NULL COMMENT '现状-分销率-MT',
  `gt_distribute_rate` decimal(15,4) DEFAULT NULL COMMENT '现状-分销率-GT',
  `all_distribute_rate` decimal(15,4) DEFAULT NULL COMMENT '现状-分销率-总合计',
  `tar_ctp_num` decimal(15,4) DEFAULT NULL COMMENT '目标-门店数合计',
  `tar_sales_qty_total` decimal(15,4) DEFAULT NULL COMMENT '目标-销售瓶数合计',
  `key_distribute_rate` decimal(15,4) DEFAULT NULL COMMENT '关键结果-分销率',
  `key_ods_output` decimal(15,4) DEFAULT NULL COMMENT '关键结果-单日单店产出',
  `inc_ods_output` decimal(15,4) DEFAULT NULL COMMENT '增量-单日单店产出合计',
  `inc_ctp_num` decimal(15,4) DEFAULT NULL COMMENT '增量-门店数合计',
  `inc_sales_qty` decimal(15,4) DEFAULT NULL COMMENT '增量-销量瓶数合计',
  `inc_distribute_rate` decimal(15,4) DEFAULT NULL COMMENT '增量-分销率合计',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `last_modified` varchar(50) DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `task_index` (`regional_name`,`prdcateg_lv1_name`,`brand_prdcateg_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='地域+品牌品类销量信息';

-- ----------------------------
-- Table structure for gdt_geoc_macro_info
-- ----------------------------
DROP TABLE IF EXISTS `gdt_geoc_macro_info`;
CREATE TABLE `gdt_geoc_macro_info` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `regional_level` varchar(50) DEFAULT NULL COMMENT '地域层级',
  `prov_cd` varchar(100) DEFAULT NULL COMMENT '省编码',
  `prov_name` varchar(50) DEFAULT NULL COMMENT '省名称',
  `city_cd` varchar(50) DEFAULT NULL COMMENT '市编码',
  `city_name` varchar(50) DEFAULT NULL COMMENT '市名称',
  `county_cd` varchar(50) DEFAULT NULL COMMENT '区编码',
  `county_name` varchar(100) DEFAULT NULL COMMENT '区名称',
  `resident_population` decimal(10,4) DEFAULT NULL COMMENT '常住人口数量（万）',
  `urban_rate` decimal(10,4) DEFAULT NULL COMMENT '城镇化率',
  `urban_pop` decimal(10,4) DEFAULT NULL COMMENT '城镇人口（万）',
  `urban_prop` decimal(10,4) DEFAULT NULL COMMENT '城镇人口比例',
  `pop_area_prop` decimal(10,4) DEFAULT NULL COMMENT '该区域占当省/直辖市人口比重',
  `GDP_rank` varchar(50) DEFAULT NULL COMMENT 'GDP排名',
  `total_retail_rank` varchar(50) DEFAULT NULL COMMENT '社会消费品零售总额排名',
  `total_retail` decimal(15,4) DEFAULT NULL COMMENT '社会消费品零售总额（亿）',
  `GDP` decimal(15,4) DEFAULT NULL COMMENT '国内生产总值GDP亿',
  `house_num` decimal(15,4) DEFAULT NULL COMMENT '家庭户数（万户）',
  `hyper_ctp_num` decimal(15,4) DEFAULT NULL COMMENT 'Market门店数-大卖场（Hyper）',
  `super_ctp_num` decimal(15,4) DEFAULT NULL COMMENT 'Market门店数-大超市（Super）',
  `mini_ctp_num` decimal(15,4) DEFAULT NULL COMMENT 'Market门店数-小超市（Mini）',
  `cvs_ctp_num` decimal(15,4) DEFAULT NULL COMMENT 'Market门店数-便利店（CVS）',
  `cs_ctp_num` decimal(15,4) DEFAULT NULL COMMENT 'Market门店数-食杂店（CS）',
  `mt_market_num` decimal(15,4) DEFAULT NULL COMMENT 'Market门店数-MT合计-地域',
  `gt_market_num` decimal(15,4) DEFAULT NULL COMMENT 'Market门店数-GT合计-地域',
  `all_market_num` decimal(15,4) DEFAULT NULL COMMENT 'Market门店数-总合计-地域',
  `mt_lead_num` decimal(15,4) DEFAULT NULL COMMENT 'DL门店数-MT合计-地域',
  `gt_lead_num` decimal(15,4) DEFAULT NULL COMMENT 'DL门店数-GT合计-地域',
  `all_lead_num` decimal(15,4) DEFAULT NULL COMMENT 'DL门店数-总合计-地域',
  `mt_deal_num` decimal(15,4) DEFAULT NULL COMMENT '已铺货门店数-MT合计-地域',
  `gt_deal_num` decimal(15,4) DEFAULT NULL COMMENT '已铺货门店数-GT合计-地域',
  `all_deal_num` decimal(15,4) DEFAULT NULL COMMENT '已铺货门店数-总合计-地域',
  `market_size_soy` decimal(15,4) DEFAULT NULL COMMENT '市场容量-酱油合计',
  `market_size_sauce` decimal(15,4) DEFAULT NULL COMMENT '市场容量-酱合计',
  `market_size_oyster` decimal(15,4) DEFAULT NULL COMMENT '市场容量-蚝油合计',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='地域宏观信息';

-- ----------------------------
-- Table structure for gdt_parent_cust
-- ----------------------------
DROP TABLE IF EXISTS `gdt_parent_cust`;
CREATE TABLE `gdt_parent_cust` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `par_cust_num` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '父客户编码',
  `par_cust_name` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '父客户名称',
  `type` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '客户类型',
  `creator` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='KA系统客户主数据';

-- ----------------------------
-- Table structure for gdt_parent_cust_rank
-- ----------------------------
DROP TABLE IF EXISTS `gdt_parent_cust_rank`;
CREATE TABLE `gdt_parent_cust_rank` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `cal_year` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '年',
  `par_cust_num` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父客户编码',
  `par_cust_name` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父客户名称',
  `prov_cd` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '省份编码',
  `prov_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '省份名称',
  `city_cd` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '城市编码',
  `city_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '城市名称',
  `county_cd` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '区县编码',
  `county_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '区县名称',
  `ctp_sellout_amt` decimal(15,4) DEFAULT NULL COMMENT 'Sellout金额',
  `sh_sellout_rnkng_country` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量全国排名',
  `sh_sellout_rnkng_prov` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量省份排名',
  `sh_sellout_rnkng_city` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量城市排名',
  `sh_sellout_rnkng_county` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量区县排名',
  `mtavg_soy_sales` decimal(15,4) DEFAULT NULL COMMENT '酱油容量',
  `mtavg_soy_rnkng_country` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '酱油容量全国排名',
  `mtavg_soy_rnkng_prov` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '酱油容量省份排名',
  `mtavg_soy_rnkng_city` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '酱油容量城市排名',
  `mtavg_soy_rnkng_county` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '酱油容量区县排名',
  `sh_sellout_cnrb_country` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量-全国贡献度',
  `sh_sellout_cnrb_prov` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量-省份贡献度',
  `sh_sellout_cnrb_city` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量-城市贡献度',
  `creator` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='系统排名';

-- ----------------------------
-- Table structure for gdt_storetype_sales
-- ----------------------------
DROP TABLE IF EXISTS `gdt_storetype_sales`;
CREATE TABLE `gdt_storetype_sales` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `regional_level` varchar(50) DEFAULT NULL COMMENT '地域层级',
  `regional_cd` varchar(100) DEFAULT NULL COMMENT '地域编码',
  `regional_name` varchar(50) DEFAULT NULL COMMENT '地域名称',
  `ctp_chanl` varchar(50) DEFAULT NULL COMMENT '渠道',
  `ctp_type` varchar(50) DEFAULT NULL COMMENT '门店类型',
  `deal_ctp_num` decimal(10,4) DEFAULT NULL COMMENT '已铺货门店数',
  `market_cust_qty` decimal(10,4) DEFAULT NULL COMMENT 'market门店数',
  `distribute_rate` decimal(10,4) DEFAULT NULL COMMENT '分销率',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地域品牌品类门店类型销量信息';

-- ----------------------------
-- Table structure for gdt_store_rank
-- ----------------------------
DROP TABLE IF EXISTS `gdt_store_rank`;
CREATE TABLE `gdt_store_rank` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `cal_year` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '年',
  `prov_cd` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '省份编码',
  `prov_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '省份名称',
  `city_cd` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '城市编码',
  `city_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '城市名称',
  `county_cd` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '区县编码',
  `county_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '区县名称',
  `sales_bu_level3` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '大区编码',
  `sales_bu_level3_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '大区名称',
  `dealer_chanl_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '经销商服务渠道',
  `par_cust_num` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父客户编码',
  `par_cust_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父客户名称',
  `cust_num` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '门店编码',
  `cust_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '门店名称',
  `dirct_sales_flg` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '是否直营',
  `cust_stat_cd` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '门店状态',
  `cust_vendor_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '供货商编码',
  `cust_vendor_num` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '供货商名称',
  `cust_categ_l3_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '客户三级分类',
  `employee_id` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '该店对应要考核的业务人员工号',
  `employee_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '该店对应要考核的业务人员姓名',
  `postn_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '人员岗位',
  `ctp_sellout_amt` decimal(15,4) DEFAULT NULL COMMENT '门店酱油Sellout金额',
  `sh_sellout_rnkng_country` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量-全国排名',
  `sh_sellout_rnkng_pt` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量-大区排名',
  `sh_sellout_rnkng_prov` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量-P-省排名',
  `sh_sellout_rnkng_city` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量-C-城市排名',
  `sh_sellout_rnkng_county` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量-D-区县排名',
  `sh_sellout_rnkng_system` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '欣和销量-CS-系统内排名',
  `mtavg_soy_sales` decimal(15,4) DEFAULT NULL COMMENT '酱油容量（元/年）',
  `mtavg_soy_rnkng_country` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '门店容量-全国排名',
  `mtavg_soy_rnkng_pt` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '门店容量-大区排名',
  `mtavg_soy_rnkng_prov` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '门店容量-P-省排名',
  `mtavg_soy_rnkng_city` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '门店容量-C-城市排名',
  `mtavg_soy_rnkng_county` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '门店容量-D-区县排名',
  `mtavg_soy_rnkng_system` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '门店容量-CS-系统内排名',
  `creator` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modified` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '最后修改者',
  `last_modified_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`),
  KEY `idx_par_cust_num` (`par_cust_num`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='门店排名';

-- ----------------------------
-- Table structure for score_ranking_area
-- ----------------------------
DROP TABLE IF EXISTS `score_ranking_area`;
CREATE TABLE `score_ranking_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pra_org_ct` varchar(255) DEFAULT NULL COMMENT 'CT组织编码',
  `pra_org_ct_name` varchar(255) DEFAULT NULL COMMENT 'CT组织名称',
  `activity_score` decimal(10,0) DEFAULT NULL COMMENT '活动执行率',
  `avg_score` decimal(10,0) DEFAULT NULL COMMENT '综合成绩',
  `car_score` decimal(10,0) DEFAULT NULL COMMENT '车辆到位率',
  `cost_score` decimal(10,0) DEFAULT NULL COMMENT '费用达成率',
  `distribution_score` decimal(10,0) DEFAULT NULL COMMENT '分销达标率',
  `in_score` decimal(10,0) DEFAULT NULL COMMENT '进库',
  `out_score` decimal(10,0) DEFAULT NULL COMMENT '出库',
  `inventory_score` decimal(10,0) DEFAULT NULL COMMENT '库存',
  `order_score` decimal(10,0) DEFAULT NULL COMMENT '订单一致性',
  `person_score` decimal(10,0) DEFAULT NULL COMMENT '人员到位率',
  `rule_score` decimal(10,0) DEFAULT NULL COMMENT '价盘违规',
  `shelves_score` decimal(10,0) DEFAULT NULL COMMENT '货架占有率',
  `vivid_score` decimal(10,0) DEFAULT NULL COMMENT '生动化',
  `total_score` decimal(10,0) DEFAULT NULL COMMENT '总分数',
  `rank` decimal(10,0) DEFAULT NULL COMMENT '排名',
  `Input_time` varchar(255) DEFAULT NULL COMMENT '录入时间',
  `create_user` varchar(255) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_lakeid_cust` (`pra_org_ct`,`Input_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='区人客的区分数';

-- ----------------------------
-- Table structure for score_ranking_city
-- ----------------------------
DROP TABLE IF EXISTS `score_ranking_city`;
CREATE TABLE `score_ranking_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `prov_cd` varchar(255) DEFAULT NULL COMMENT '省编码',
  `prov_name` varchar(255) DEFAULT NULL COMMENT '省名称',
  `city_cd` varchar(255) DEFAULT NULL COMMENT '城市编码',
  `city_name` varchar(255) DEFAULT NULL COMMENT '城市名称',
  `activity_score` decimal(10,0) DEFAULT NULL COMMENT '活动执行率',
  `avg_score` decimal(10,0) DEFAULT NULL COMMENT '综合成绩',
  `car_score` decimal(10,0) DEFAULT NULL COMMENT '车辆到位率',
  `cost_score` decimal(10,0) DEFAULT NULL COMMENT '费用达成率',
  `distribution_score` decimal(10,0) DEFAULT NULL COMMENT '分销达标率',
  `in_score` decimal(10,0) DEFAULT NULL COMMENT '进库',
  `out_score` decimal(10,0) DEFAULT NULL COMMENT '出库',
  `inventory_score` decimal(10,0) DEFAULT NULL COMMENT '库存',
  `order_score` decimal(10,0) DEFAULT NULL COMMENT '订单一致性',
  `person_score` decimal(10,0) DEFAULT NULL COMMENT '人员到位率',
  `rule_score` decimal(10,0) DEFAULT NULL COMMENT '价盘违规',
  `shelves_score` decimal(10,0) DEFAULT NULL COMMENT '货架占有率',
  `vivid_score` decimal(10,0) DEFAULT NULL COMMENT '生动化',
  `total_score` decimal(10,0) DEFAULT NULL COMMENT '总分数',
  `rank` decimal(10,0) DEFAULT NULL COMMENT '排名',
  `Input_time` varchar(255) DEFAULT NULL COMMENT '录入时间',
  `create_user` varchar(255) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_input_prov_city` (`prov_cd`,`city_cd`,`Input_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='区人客城市分数';

-- ----------------------------
-- Table structure for score_ranking_cust
-- ----------------------------
DROP TABLE IF EXISTS `score_ranking_cust`;
CREATE TABLE `score_ranking_cust` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cust_cd` varchar(50) DEFAULT NULL COMMENT '客户编码',
  `cust_name` varchar(255) DEFAULT NULL COMMENT '客户名称',
  `activity_score` decimal(10,0) DEFAULT NULL COMMENT '活动执行率',
  `avg_score` decimal(10,0) DEFAULT NULL COMMENT '综合成绩',
  `car_score` decimal(10,0) DEFAULT NULL COMMENT '车辆到位率',
  `cost_score` decimal(10,0) DEFAULT NULL COMMENT '费用达成率',
  `distribution_score` decimal(10,0) DEFAULT NULL COMMENT '分销达标率',
  `in_score` decimal(10,0) DEFAULT NULL COMMENT '进库',
  `out_score` decimal(10,0) DEFAULT NULL COMMENT '出库',
  `inventory_score` decimal(10,0) DEFAULT NULL COMMENT '库存',
  `order_score` decimal(10,0) DEFAULT NULL COMMENT '订单一致性',
  `person_score` decimal(10,0) DEFAULT NULL COMMENT '人员到位率',
  `rule_score` decimal(10,0) DEFAULT NULL COMMENT '价盘违规',
  `shelves_score` decimal(10,0) DEFAULT NULL COMMENT '货架占有率',
  `vivid_score` decimal(10,0) DEFAULT NULL COMMENT '生动化',
  `total_score` decimal(10,0) DEFAULT NULL COMMENT '总分数',
  `rank` decimal(10,0) DEFAULT NULL COMMENT '排名',
  `input_time` varchar(255) DEFAULT NULL COMMENT '录入时间(如:202012)',
  `create_user` varchar(255) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_input_custCd` (`cust_cd`,`input_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='区人客客分数';

-- ----------------------------
-- Table structure for score_ranking_person
-- ----------------------------
DROP TABLE IF EXISTS `score_ranking_person`;
CREATE TABLE `score_ranking_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `empid` varchar(255) DEFAULT NULL COMMENT '业主编码',
  `empname` varchar(255) DEFAULT NULL COMMENT '客户名称',
  `activity_score` decimal(10,0) DEFAULT NULL COMMENT '活动执行率',
  `avg_score` decimal(10,0) DEFAULT NULL COMMENT '综合成绩',
  `car_score` decimal(10,0) DEFAULT NULL COMMENT '车辆到位率',
  `cost_score` decimal(10,0) DEFAULT NULL COMMENT '费用达成率',
  `distribution_score` decimal(10,0) DEFAULT NULL COMMENT '分销达标率',
  `in_score` decimal(10,0) DEFAULT NULL COMMENT '进库',
  `out_score` decimal(10,0) DEFAULT NULL COMMENT '出库',
  `inventory_score` decimal(10,0) DEFAULT NULL COMMENT '库存',
  `order_score` decimal(10,0) DEFAULT NULL COMMENT '订单一致性',
  `person_score` decimal(10,0) DEFAULT NULL COMMENT '人员到位率',
  `rule_score` decimal(10,0) DEFAULT NULL COMMENT '价盘违规',
  `shelves_score` decimal(10,0) DEFAULT NULL COMMENT '货架占有率',
  `vivid_score` decimal(10,0) DEFAULT NULL COMMENT '生动化',
  `total_score` decimal(10,0) DEFAULT NULL COMMENT '总分数',
  `rank` decimal(10,0) DEFAULT NULL COMMENT '排名',
  `Input_time` varchar(255) DEFAULT NULL COMMENT '录入时间',
  `create_user` varchar(255) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_lakeid_cust` (`empid`,`Input_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='区人客的人分数';

-- ----------------------------
-- Table structure for score_ranking_province
-- ----------------------------
DROP TABLE IF EXISTS `score_ranking_province`;
CREATE TABLE `score_ranking_province` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `prov_cd` varchar(255) DEFAULT NULL COMMENT '省编码',
  `prov_name` varchar(255) DEFAULT NULL COMMENT '省名称',
  `activity_score` decimal(10,0) DEFAULT NULL COMMENT '活动执行率',
  `avg_score` decimal(10,0) DEFAULT NULL COMMENT '综合成绩',
  `car_score` decimal(10,0) DEFAULT NULL COMMENT '车辆到位率',
  `cost_score` decimal(10,0) DEFAULT NULL COMMENT '费用达成率',
  `distribution_score` decimal(10,0) DEFAULT NULL COMMENT '分销达标率',
  `in_score` decimal(10,0) DEFAULT NULL COMMENT '进库',
  `out_score` decimal(10,0) DEFAULT NULL COMMENT '出库',
  `inventory_score` decimal(10,0) DEFAULT NULL COMMENT '库存',
  `order_score` decimal(10,0) DEFAULT NULL COMMENT '订单一致性',
  `person_score` decimal(10,0) DEFAULT NULL COMMENT '人员到位率',
  `rule_score` decimal(10,0) DEFAULT NULL COMMENT '价盘违规',
  `shelves_score` decimal(10,0) DEFAULT NULL COMMENT '货架占有率',
  `vivid_score` decimal(10,0) DEFAULT NULL COMMENT '生动化',
  `total_score` decimal(10,0) DEFAULT NULL COMMENT '总分数',
  `rank` decimal(10,0) DEFAULT NULL COMMENT '排名',
  `Input_time` varchar(255) DEFAULT NULL COMMENT '录入时间',
  `create_user` varchar(255) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_input_provCd` (`prov_cd`,`Input_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='区人客省分数';

-- ----------------------------
-- Table structure for sys_action_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_action_log`;
CREATE TABLE `sys_action_log` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `token` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '用户令牌',
  `trace` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '链路编号',
  `project` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '项目名称',
  `moudle` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '模块名称',
  `action_type` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '操作类型',
  `type` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '日志类型 1:正常 0：异常',
  `request_uri` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '请求URI',
  `class_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '执行类名',
  `method_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '执行方法名称',
  `user_agent` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '用户代理',
  `remote_ip` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '操作IP地址',
  `request_method` varchar(5) COLLATE utf8_bin DEFAULT NULL COMMENT '操作方式',
  `request_params` text COLLATE utf8_bin COMMENT '请求参数',
  `response_params` text COLLATE utf8_bin COMMENT '返回参数',
  `request_mac` varchar(60) COLLATE utf8_bin DEFAULT NULL COMMENT '设备MAC',
  `exception` text COLLATE utf8_bin COMMENT '异常信息',
  `action_thread` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '执行线程',
  `action_start_time` datetime DEFAULT NULL COMMENT '开始执行时刻',
  `action_end_time` datetime DEFAULT NULL COMMENT '结束执行时刻',
  `action_time` bigint(20) DEFAULT NULL COMMENT '执行耗时 单位(毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建日志时间',
  PRIMARY KEY (`id`),
  KEY `sys_log_trace` (`trace`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1356 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户日志';

-- ----------------------------
-- Table structure for sys_menu_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_post`;
CREATE TABLE `sys_menu_post` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL COMMENT '菜单id',
  `post_id` int(11) DEFAULT NULL COMMENT '岗位编码',
  `insdt` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '生成时间',
  `userid` varchar(255) DEFAULT NULL COMMENT '创建人',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `type` tinyint(1) DEFAULT NULL COMMENT '对应平台1：app，2：web前台 3：web后台',
  `menu_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109523 DEFAULT CHARSET=utf8 COMMENT='岗位与菜单关联关系表';

-- ----------------------------
-- Table structure for sys_order
-- ----------------------------
DROP TABLE IF EXISTS `sys_order`;
CREATE TABLE `sys_order` (
  `pkid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `type` varchar(1) DEFAULT NULL COMMENT '订单类型 0:支付订单 1:退款单',
  `money` decimal(10,4) DEFAULT NULL COMMENT '订单金额',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新人',
  `user_id` int(11) DEFAULT NULL COMMENT '用户外键',
  PRIMARY KEY (`pkid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='订单表';

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(1) DEFAULT NULL COMMENT '性别 1:男 0:女',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';
